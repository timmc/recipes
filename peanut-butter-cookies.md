# Peanut butter cookies

## 2022-02-13

- 2 eggs
- 2 cups granulated sugar
- 2 tsp vanilla extract
- 1 lb (~2 cups) chunky salted peanut butter (natural)

Directions:

- Starting with the first one, stir in each ingredient in turn
- Form into smallish balls (2-3 cm?)
- Flatten onto baking sheet
- Bake in preheated oven for 14 minutes at 325°F

They'll come out soft and will harden as they cool.

You'll probably need 2 baking sheets and will want to aim for about 40
small cookies.

Notes:

- I did a batch without flattening, and the bottoms caramelized and
  stuck pretty badly for a lot of them. Might be related.
- Future variation: Sprinkle large-grain salt on top after flattening.
