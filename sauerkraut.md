# Sauerkraut

Use about 2% salt relative to vegetable weight.

## 2019-05-11

First attempt at sauerkraut. Didn't have any caraway seeds, and the
half a cabbage was a little dry from its time in the crisper drawer.

Ingredients:

- 700 g of white cabbage
- 87 g carrot
- 2 Tbsp koshering salt (sodium chloride, yellow prussiate of soda)
- 20 dried juniper berries

Steps:

- Reserve one cabbage leaf
- Shred cabbage and carrot finely with mandoline, then with knife for
  parts that refuse to go through mandoline
- Combine all in bowl
- Pound and squeeze mixture until the salt pulls enough water out of
  the vegetables to form a brine that will be able to cover them (may
  need to take breaks to allow salt to dissolve fully)
- Pack into 3–4 cup jar, pushing shreds as far down under brine as
  possible and pushing out any air bubbles.
- Push cabbage leaf down through brine, using it to keep shreds well
  under the brine. Push down the edges to keep the leaf from floating
  up. (It will anyway.)

Notes:

- Fermentation started in earnest on the 7th day (2019-05-17)
- I added a 1/2 tsp caraway seeds on the 8th day
- Turned out pretty salty; try reducing that ratio

## 2019-07-20-a

Cabbage with a trace of carrot.

- 989 g cabbage (3/4, without core)
- 77 g carrot
- 1.5 Tbsp fine sea salt (19 g)
- 2 tsp caraway seeds

Shred cabbage and carrot, combine with salt and caraway, pound and
squeeze until there is water to cover when compressed. Pack into 2L
jar (immersion blender attachement, not powered, was good as a
pusher), along with cabbage leaf and glass as weight. Keep sealed,
burp daily.

Seemed to have finished by 8/1. Pretty heavy on the caraway.

## 2019-07-20-b

Mostly beets. (NB: I've been warned that a high beet content can
sometimes result in slime, although the slime may be fermented in
turn.)

- 764 g golden beets, trimmed (5)
- 328 g cabbage (1/4, without core)
- 55 g scallions, mostly the whites
- 25 g ginger
- 21 g fine sea salt

Shred, pound, squeeze, and pack into 2L jar under cabbage leaf and
glass jar as weight. Keep sealed and burp daily.

Started fermenting after just a day. Seemed to have finished by
7/30. Lovely flavor, although the beets have a bit of a rubbery
texture.

## 2019-11-20

- 1480 g purple cabbage, minus the core
- 6.5 tsp fine sea salt
- 1 tsp caraway seeds

Reserved 1 leaf of cabbage, removed and discarded core, and shredded
rest using food processor's slicing attachments. Mixed with salt and
caraway, then squeezed and pounded intermittently with rest
periods. Packed into 2L jar under cabbage leaf with glass jar as
weight. Kept sealed and burped daily.

- After 8 days the sauerkraut wasn't bubbling much, but also hadn't
  developed much tang. I wonder if using the food processor's slicing
  attachment rather than a mandoline may have made it too coarse. The
  smell is also more sulfurous than I was expecting.
- Done after about 3 or 4 weeks; took a while to soften.

Good flavor. Caraway quantity is appropriate.

## 2020-02-08

- 1408 g shredded green cabbage (half an XL cabbage, 1607 g before
  core and some darkened bits trimmed)
- 1 tsp caraway seeds (2.8 g)
- 6.5 tsp fine sea salt (30.8 g)

Same process as 2019-11-20 (shredded with food processor).

I capped the brine and shreds with the cap leaf from 2019-11-20, which
might be a mistake. We'll find out! (Update: No problem!)

## 2020-02-09

This time with iodized table salt. Not expecting a difference.

- 1432 g green cabbage, shredded (by knife)
- Reserved leaf or two
- 6.5 tsp (31 g?) iodized table salt
- 1 tsp caraway seed

- Combined, tossed, and left for about 8 hours, tossing every couple hours
- Pounded and squeezed to ensure that a brine was forming
- Poured into quart jar and compacted under the brine
- Leaf placed on top of solids
- Open jar filled with water placed on top of leaf
- Covered with plastic bag rubber-banded on to keep flies out

## 2021-02-26

Double batch. 2% salt as usual. Used food processor for shredding.

- Several reserved outer leaves
- 2843 g green cabbage, shredded (3 smallish cabbages)
- 57 g (~3 Tbsp) table salt, non-iodized
- 2 tsp caraway seed

Double recipe was a mistake! It was too much to mix in even my 7 qt
mixing bowl.

Also forgot to actually reserve the leaves... they got chopped up with
the rest.
