# Indian snack spice mix

I've been trying to re-create the spice mixes found in various
prepackaged snacks I've found in Indian grocery stores.

If you're looking for "election spice", it has graduated to its own
recipe file.

## 2014-06-06

On popcorn, the following in decreasing order by volume: Coriander,
korarima (Ethiopian or false cardamom), mustard seed, black pepper,
cumin, amchur powder, chili powder, black salt, fenugreek seeds,
cinnamon powder, and clove powder.

Lacked some sweetness or sourness. I think there was enough black
salt, though, and when I added more amchur it started to taste
slightly soapy.

## 2016-10-01 - Mint/chili v1

I observed a commercial snack mix with the following spices on fried
chickpea batter: Coriander, cumin, mango, ginger, chili, mint. Here's
my attempt on popcorn:

- 2 tsp coriander powder
- 2 tsp cumin powder
- 1.25 tsp amchur powder
- 1 tsp ginger powder
- 1 tsp red chili flakes
- 0.75 tsp mint leaves (dried flakes)
- Some salt, I think...

Grind all to powder.

Entirely pleasant, although maybe a bit bitter/soapy. Could have more
fruitiness and sourness.

Used Trader Joe's Red Chili Flakes, for reference. (Heat of chili
pepper in bottles varies considerably...)

## 2016-10-?? (v2 of 2014-06-06)

4 t coriander seeds, whole
1.75 t mustard seeds, whole
1.5 t cumin seeds, whole
1 t korarima seeds, whole
0.5 t black pepper, whole
0.5 t amchur powder
0.5 t black salt
0.25 t fenugreek seeds, whole
0.25 t garlic powder
0.25 t ginger powder
0.0625 t cinnamon powder
0.0625 t clove powder
0.0625 t cayenne powder

This coriander is not the freshest stuff. Possibly contributing to a
stale flavor?

Hinting a bit sulfurous, still kinda bitter. Less soapy.

Maxed out on black salt (due to sulfurousness) and amchur (due to
soapiness) so should find a different way to add sourness. Citric
acid?

Bitterness indicates maybe too much korarima; not sure the
eucalpytus-y flavor adds much anyhow.

## 2016-10-16 - Mint/chili v2

- Double most ingredients
- Triple chili flakes
- Triple+ mint flakes
- Add salt

Ingredients:

- 4 tsp coriander powder
- 4 tsp cumin powder
- 2.5 tsp amchur powder
- 2 tsp ginger powder
- 3 tsp red chili flakes
- 2.5 tsp mint leaves (dried flakes)
- 1 tsp iodized salt

Grind all to powder.

The chili/mint tension stands out more now, and having the salt
already in makes it easier to test the flavor.

## 2016-11-01 - Mint/chili v3

I ran out of red chili flakes, so I'm using cayenne and Anaheim.

- 4 tsp cumin powder
- 4 tsp coriander powder
- 2.5 tsp amchur powder
- 2.5 tsp mint leaves (dried flakes)
- 2 tsp ginger powder
- 1 tsp Anaheim chili powder
- 3/8 tsp cayenne powder
- 1 tsp iodized salt

Kind of strong on the Amchur. Might have used scant measures of cumin
and coriander.

## 2017-03-31 - Navarattan attempt

Trying to replicate flavor of Haldiram's Navrattan snack mix. Vitamin
C standing in for citric acid.

- 500 mg Vitamin C (in a tablet with other, inert ingredients)
- 1/8 tsp cayenne powder
- 1/2 small bay leaf
- 3/8 tsp (or 1/2 tsp?) amchur powder
- 1/2 tsp ginger powder
- 1/2 tsp cumin powder
- 1/2 tsp coriander powder
- 3/4 tsp whole black peppercorns
- 1/4 tsp salt
- 1/8 tsp clove powder
- 1/8 tsp cardamom powder
- 1/8 tsp fenugreek powder
- 1/2+ tsp mint

Notes:

- Total failure to replicate. *Very* bright flavor; next try making
  without amchur, ginger, coriander, and vitamin C (or other acid),
  then add them slowly in.
- It was tasting pretty close before I added the fenugreek. I think it
  helped get it closer. Then I felt the mix was missing a sort of
  coolness, so I added the mint, but I think I added too much.
- 1/4 tsp whole black peppercorns is 12 peppercorns
