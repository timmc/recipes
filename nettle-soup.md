# Stinging nettle soup

## 2023-05-15

Based on <https://www.thespruceeats.com/stinging-nettles-soup-2217495>. Substituted mace for nutmeg.

- 215 g stinging nettles w/tender stems
- 2 small/medium onions (248 g intact)
- 2 tablespoons unsalted butter, divided
- 1 teaspoon iodized table salt
- 489 g potatoes
- 6 cups Seitenbacher vegetable broth
- 1/2 teaspoon freshly ground black pepper
- 1/4 teaspoon ground mace
- Optional:_Heavy cream, sour cream, yogurt, or horseradish cream

Directions:

- Rinse the nettles if they are gritty. Consider soaking them if they
  have not already been soaked, since that can impair the
  stinging. (Gloves may still be necessary.) Chop roughly.
- Peel and chop the onion.
- In a large pot, melt 1 tablespoon of the butter over medium-high
  heat. Add the chopped onion and salt. Cook, stirring occasionally,
  until the onions are soft, about 3 minutes.
- While the onions cook, chop the potatoes.
- Add the potatoes and the broth to the onions and bring to a
  boil. Reduce the heat to maintain a steady simmer, and cook until
  the potatoes are mostly tender, about 15 minutes.
- Add the nettles and cook until they're very tender, about 10
  minutes.
- Stir in the remaining 1 tablespoon of butter, plus the pepper and
  mace.
- Puree the soup with an immersion blender.
- Optionally: Stir in the yogurt, once mostly cooled. Serve hot,
  garnished with sour cream, yogurt, or horseradish cream, if desired.

Notes:

- The nettles I got were so tender that the stems cooked down very
  readily, and did not leave any fibrous texture to the soup. No
  straining was required.
- I didn't end up adding any yogurt, cream, etc. -- the flavor was
  lovely as it was. (I tried adding some yogurt to a bowl and it was
  slightly better, but not really necessary.)
- The color is a lurid green. Very nice.
- The salt and pepper are already quite high; it might be worth
  reducing them in the base recipe and indicating to add more if
  desired. The pepper is quite strong (as is the mace).

## 2023-06-17

~1.5x version of 2023-05-15

- 300 g stinging nettles w/tender stems
- 350 g Spanish onion
- 3 tablespoons unsalted butter, divided
- 1.5 teaspoon iodized table salt
- 685 g potatoes
- 8.5 cups Seitenbacher vegetable broth
- 3/4 teaspoon freshly ground black pepper
- 1/3 teaspoon ground mace

Notes:

- Definitely reduce the mace and black pepper to 75% next time.

## 2023-07-04 - Different greens

~2x version of 2023-05-15, but with chickweed and komatsuna instead of
nettles. Reduced mace and black pepper to 75% proportionally.

- 457 g Spanish onion (483 g before peeling and trimming)
- 2 + 2 tablespoons unsalted butter
- 2 teaspoon iodized table salt
- 1050 g white potatoes (1086 before trimming)
- 12 cups Seitenbacher vegetable broth
- 210 g komatsuna greens
- 120 g garden greens (mostly chickweed, some evening primrose and horseradish)
- 3/4 teaspoon freshly ground black pepper
- 3/8 teaspoon ground mace

Notes:

- Tasted great at first. After a day it tasted less good. Mostly
  *different*. Really hard to describe. Not bad, and interesting, but
  decidedly odd. Not sure which of the greens is responsible for the
  flavor. (Suspect it is the komatsuna.)

## 2024-05-14

2023-06-17 but 0.8x recipe; ~75% of black pepper and mace; yellow onion instead
of Spanish onion; nettle with tougher stems.

- 280 g yellow onion, chopped
- 7 tsp unsalted butter, divided
- 1.25 tsp iodized table salt
- 550 g potatoes
- 6.5 cups Seitenbacher vegetable broth
- 247 g stinging nettles w/firm stems
- 1/2 tsp freshly ground black pepper
- 1/8 tsp (heaping) ground mace

Notes:

- Stems became tender enough to blend after cooking. (Small amount of
  fibrous stuff I would filter out if serving to other people.)
- But I think I also overcooked the nettles; it wasn't the bright
  green color I remember from before.
- More black-pepper-forward than I'm used to as well.
