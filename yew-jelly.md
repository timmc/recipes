About 1350 mL of arils.

# Getting the pulp

This is the approach I used; it's all based on theory, not previous
practice. I have no evidence this is the best way.

- Remove toxic foliage: Gently go through arils a small handful at a
  time (5-15), removing leaf and stem fragments and rotting arils
  before transferring to a "clean" bowl.
- Clean the outsides: Gently agitate the arils in plentiful water, an
  inch or so deep at a time.
- Pulp the arils: I did this by hand, grabbing and squeezing.
- Agitate and continue pulping.
- The mixture is now quite gelatinous, and can be picked up in
  almost-wads.
- Add some water (perhaps 20%?) and agitate and pulp.
- Strain through a colander. This will remove almost all the
  seeds. You may need colanders of successively smaller hole size; the
  mixture is gluey enough that it will not fall through holes
  easily. I may have coaxed some out by letting the colander sit low
  enough to touch the accumulating "liquid".
- Put it through a sieve. This is essential, even though the liquid
  will not readily flow through and will often be clogged by bits of
  skin. You do not want seeds or seed fragments in your pulp!

I was able to extract a little under 500 mL of liquid from this
process; I did not measure the volume of pulp and seeds remaining. The
liquid was a little foamy, and I had to throw out some that was mixed
with pulp in the sieve, so my number may be high.

The liquid feels slightly silky when I run my fingers through it.

The taste is a little more bitter than the fresh pulp normally is, so
some of the bitter compounds on the surface of the seed may have
rubbed off. This may be unavoidable, and perhaps indicates that the
jelly should not be consumed in large amounts at a time.

# Freezing

I don't have time to work with the pulp right now, so I'm freezing it.
