# Roasted squash

## 2019-10-20 - Honeynut

- 2 honeynut squash, washed, halved, and with seeds scooped out
- olive oil
- garlic cloves
- fresh thyme
- salt
- black pepper

Directions:

- Brushed faces of squash in generous olive oil (and a little on the
  skin side as well) and placed a little extra in the bowls
- Add a garlic clove and a thyme sprig to each squash's bowl
- Sprinkle 1/16th tsp of salt across all (retrospective note: I think it was across all, not per half)
- Add a grind of black pepper on each half
- Flip each squash over onto a baking sheet
- Bake at 400°F in a preheated oven, for 30 minutes

Delicious and soft and rich (cheesecake-like) at the bowl end,
somewhat more squashy and firm in the main body. Skin tender and
edible. Two things to consider for next time:

1. Try 45 minutes to allow more thorough cooking
2. Try poking holes from the face into the main body to allow olive
   oil (and salt?) to permeate

Interesting note: The goo left on the pan was sweet and oily and umami
in a way that tasted very strongly of a certain kind of cheese pizza.

## 2019-11-01 - Jack O' Lantern

White-yellow pumpkin that we used for Halloween. Very hard rind. Raw
flesh tastes like spaghetti squash, not like the usual orange
pumpkins.

With one half (1284 g after trimming):

- Rind cut off
- Inside scraped clean
- Cut into 1.5 to 2 cm dice
- Tossed with 1 tsp salt and 42 g evoo
- Packed onto baking sheet and roasted at 375°F for 1 hr 5 min.

Other half done similarly.

## 2019-11-13 - Honeynut, no spices

- 4 honeynut squash, washed, halved, and with seeds scooped out
- Olive oil
- 1/8 tsp iodized salt
- 4 garlic cloves, some cut in half

Directions:

- For 4 of the halves, make deep, parallel, diagonal slices in the
  neck of the squash, not piercing the skin. (An experiment to see if
  oil and salt will penetrate.)
- Coat all in plenty of olive oil
- Sprinkle equal portions of the salt on all the cut faces
- Place a garlic clove in each bowl (some left without, as an
  experiment)
- Place face down on baking sheet
- Roast in preheated 400°F oven for 44 minutes

Notes:

- Really tasty. I think 45 minutes is the right duration.
- By 30 minutes, the bowls became tender; by 44, the bowls were very
  soft, and the necks were completely tender. The skins wrinkled and
  darkened slightly near the end, which might be a good "done sign".
- In that last ~15 minutes, the squash released sugars into the pooled
  oil, which became a sugary, caramelized gel that lay in patches and
  then hardened. Very sweet. Would be interesting to experiment with
  pureeing squash, mixing it with oil and salt, and frying it in thin
  sheets. (As noted before, it's a bit cheese-like. Certainly, roasted
  honeynut would make an amazing sauce for a pizza.) Some areas were
  more bubbly and opaque, a bit more burnt.
- The garlic only seems to add flavor where the liquid pools against
  the cut edges of the bowl. Not worth adding, unless you want roasted
  garlic, but I'm not sure this roasts the garlic as well as other
  ways.
- I don't think I missed the thyme or black pepper either. Just
  squash, salt, and olive oil seem to be essential and sufficient.

## 2020-11-05 - Honeynut, butter

I tried 2019-11-13 but with butter, and it was kind of a
disaster. Instead of gooey amber froth, the froth ended up black and
bitter by the end of the 45 minutes. The garlic was also a bit burnt
on one side, so I think this may have been a temperature/time issue
rather than the butter's fault... maybe I usually do this on a high
rack in the oven, rather than a mid/low rack.

## 2021-10-05 - Honeynut, no garlic

2019-11-13 but without garlic. Top rack, 45 minutes. Not as amazing
this time. Might just be that the squash is less sweet, but try garlic
again next time.

## 2021-11-04 - Jack O' Lantern

Pumpkin washed and inside scraped clean. Discarded part burnt by
candle. (Used beeswax candle.)

- 1000 g pumpkin diced 1-2 cm wide (rind was about 2.5 cm thick, but
  did not shorten that)
- 1 tsp salt
- 40 g evoo

Tossed pumpkin cubes with salt and then evoo; spread on baking sheet
and baked on top rack at 375°F until soft, 1 hour.

Tasted amazing.
