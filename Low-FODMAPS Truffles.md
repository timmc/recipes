# Low-FODMAPS truffles

Chocolate truffles sweetened with low-fructose fruits.

Recipe by Alexandra Thorn

Approximately:
* 1/3 lb unsweetened baker's chocolate
* 2 tbsp almond butter (just almonds)
* 2 bananas
* 5-10 oz red raspberries
* 1 cup crushed pecans (optional)

In parallel:
1. Melt chocolate in double boiler. Mix in almond butter.
2. Combine bananas and raspberries in blender.

When both are relatively uniform, combine in a bowl and mix thoroughly by hand.

If desired, crush pecans with mortar and pestle and roll spheres of
truffle mix in them.

Chill.

## 2017-09-07

1.5x recipe, setting raspberries to 9 oz in original recipe. Ended up
needing to add more almond butter and chocolate to balance texture,
but was aiming for original steps (except for using food processor for
final mixing.)

- 286 g unsweetened baker's chocolate (10 oz)
- 30 g almond butter, coarse ground (6 tbsp)
- 379 g frozen raspberries (13.3 oz)
- 288 g ripe bananas, cheetah-spotted (3 -- 430 g with peel)
- 228 g pecans

1. Melt 230 g of chocolate in double boiler; mix in half of almond
   butter.
2. Combine rasberries and bananas in food processor.
3. Combine both in food processor, add remaining almond butter and
   melted chocolate.

Was too liquidy with just 15 g almond butter and 230 g baking
chocolate, so I added 100% more almond butter and 25% more
chocolate. (The chocolate appears to have had greater effect.)

It's possible that mixing in the food processor instead of by hand
caused the texture to be different, or perhaps the quantity of
raspberries was just too much.

Next time:

- Don't use food processor
- Use low end of range for raspberries -- can add more more easily
  than can add chocolate

Added 82 g banana (cheetah-spotted to blackening), hand-stirred
Added 63 g almond butter

...ok this actually ended up kind of a disaster; it was too gooey to
properly roll in pecans and I couldn't figure out how to fix it.
