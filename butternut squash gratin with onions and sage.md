# Butternut squash gratin with onions and sage

## 2021-11-20

Double the recipe from "Vegetarian Cooking For Everyone" pg 287, except:

- Used leek and scallion greens instead of onion
- Oat flour as the flour
- Almond milk instead of milk
- GF bread crumbs

Ingredients for first layer:

- 1/4 cup olive oil
- 453 g leek greens (5.5 cups)
- 130 g scallion greens (2.5 cups)
- 1 tsp chopped thyme sprigs
- 4 Tbsp chopped sage (17 fresh leaves, minced)
- 1 tsp salt
- 1/4 tsp freshly ground black pepper

Ingredients for second layer, though I split this into two batches:

- 1/4 cup olive oil
- 1757 g (12 cups) butternut squash, 1 cm dice
- a handful or so of oat flour
- 1/4 tsp salt
- 1/4 tsp freshly ground black pepper
- 4 tbsp frozen minced parsley

Remaining ingredients:

- ~140 g (2 cups) grated gruyere (Grace Hill Farm "Wild Alpine")
- 1.25 cups almond milk (unsweetened, "original flavor"), warmed
- 1.5 cups bread crumbs (gluten free bread, minced)

Notes:

- Really quite a lot of work, though mostly for cleaning and chopping
  the leeks and scallions
- Needs more salt, probably in the squash layer (and less in the
  allium layer)

## 2024-11-28

Mostly the same substitutions as 2021-11-20. Moved some salt from
bottom layer to top and increased overall amount. 10% less squash and
alliums, though not intentionally. Had to adjust scallion/onion
green/ratios. Used dried thyme and sage instead of fresh. Quinoa flour
instead of oat. Sheep's milk cheeses instead of gruyere.

Ingredients for first layer:

- 1/4 cup olive oil
- 293 g leek greens, chopped small
- 68 g onion greens, chopped small
- 167 g scallion greens, chopped small
- 1/3 tsp dried thyme
- 4 tsp rubbed sage
- 0.75 tsp salt
- 1/4 tsp freshly ground black pepper

Ingredients for second layer, though I split this into two batches:

- 1757 g (10-12 cups) butternut squash, 1 cm dice
- a handful or so of quinoa flour
- 1/4 cup olive oil
- 4 tbsp frozen minced parsley
- 0.75 tsp salt
- 1/4 tsp freshly ground black pepper

Remaining ingredients:

- Oil or butter for pan
- 140 g grated sheep's milk cheeses to replace gruyere (45 g Maremma, 95 g Agour)
- 1.25 cups almond milk (unsweetened, "original flavor"), warmed
- 1.5 cups bread crumbs (gluten free bread, minced)

Recipe:

- Preheat oven to 350° and oil or butter a 4-quart baking pan.
- First layer:
    - Heat oil in skillet over medium heat.
    - Add leek and scallion greens, thyme, and sage and cook, stirring
      frequently, until allium greens are browned at edges, about 15
      minutes.
    - Stir in salt and pepper, then spread in the baking pan.
- Second layer:
    - Heat oil again in skillet over medium/high heat.
    - In a bowl, toss squash in flour, letting excess fall away.
    - Add squash to skillet and cook, stirring occasionally, until
      starting to brown on multiple sides, about 7 minutes.
    - Add parsley, salt, and pepper and cook for 1 more minute, then
      turn off heat.
    - Layer this mix into the baking pan as well.
- Cover with cheese and add almond milk.
- Cover and bake for 25 minutes.
- Add bread crumbs and cook uncovered until top is browned and liquid
  absorbed, about 25 more minutes.

Notes:

- This worked really well! The salt levels in particular are much
  better this time.
- The Agour cheese seemed like a good match for the other flavors. I
  would have used just the Agour, but I didn't have enough. (The
  Maremma wasn't bad, but didn't have the nutty notes the Gruyere
  would have provided.)
