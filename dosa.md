# Dosa

## 2021-08-13

Base:

- 300 g (1.5 cups) brown lentils
- 200 g (1 cup) millet
- 22 g (2 Tbsp) fenugreek seeds
- 3 g (1 tsp) cumin seeds
- 1000 g water

Combined lentils, millet, fenugreek, and cumin and covered with water.

Soaked 12 hours, ground up, then let sit another 8 hours. Took
portions for individual experiments.

### Test batch

- 150 g of base (above)
- 40 g water
- 1 tsp unsweetened shredded coconut
- 1 small scallion, minced (about 5 g)
- 1 curry tree leaflet, broken up
- 1 pinch of frozen cilantro leaves
- 1/8 tsp salt

Made thin pancakes over medium-high heat with a little oil

Nice, a little low on salt. Some bitterness.

### Remaining

Scaled up test batch, but with more salt, and leaving out the water
(for space).

- 850 g base (remaining)
- 14 g (3 Tbsp) unsweetened shredded coconut
- 45 g scallion, minced
- 9 curry tree leaflets, chopped
- 5 g (1 Tbsp) frozen cilantro leaves
- 2 tsp salt

Used a food-processor to process it. Stored in fridge and freezer.

### 2

Ended up kind of grainy, so I had to re-process it with an immersion
blender to get the smooth texture I needed. Added 0.39 water : 1 base
to get up to consistency of test batch. However, this may have been
too watery, based on how it acted on the skillet (stuck more to itself
than to the skillet). I was able to get extremely thin crispy dosas,
but nothing thicker -- the dough would roll up and stick to the
spatula.
