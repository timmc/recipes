# Noodle soups

## 2016-07-01 - Bean pasta vegetable soup

- Sautéed in olive oil: Chard, fennel leaves, garlic, jalapeño pepper,
  lettuce, shell peas (with pod, chopped up)
- Put dry black bean noodles in pot, add water to cover
- Add sautéed vegetables
- Bring to boil, simmer until noodles ready
- Add tamari, lime juice, and toasted sesame oil to taste

Results:

- Really tasty! Similar to some japanese dishes I've had, I think?
- Shell peas are supposed to be shelled first, but the pods are sweet
  and I don't mind the fibrousness.
