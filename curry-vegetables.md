# Curry vegetables

## Aloo dopiaza

- 650 g tofu, cubed and fried in olive and toasted sesame oil
- 450 g pinto potatoes, boiled
- 200 g frozen peas
- 400 g jar of dopiaza curry simmer sauce
- Pinch of cilantro leaves

Simmered for about 15 minutes.

Not much of a fan of this curry sauce flavor.
