# Cough drops

- **WARNING:** It may not be safe to consume these cough drops!
    - Menthol is an anaesthetic. Overuse might impede the cilia in
      your lungs from clearing dust and mucus.
- **WARNING:** When working with concentrated menthol, use forced
  ventilation to draw the fumes away from you.
    - Use a fume hood (or at least a stove with external fan vent)
      when heating menthol.
    - Don't stick your head over the double-boiler, or at least keep
      your eyes closed and avoid inhaling when doing so
- **Caution:** When stored in a sealed container, menthol may
  sublimate and redeposit as crystals.
    - Be cautious not to let these crystals get in your eyes or on
      other sensitive body parts (like with capsaicin)

## Attempt #1

```
4 g xylitol
5 mg peppermint oil
(mass ratio: 800)

* Assuming peppermint oil is 1g/mL
* Xylitol crystals are 4g/tsp

1 teaspoon of peppermint oil
-> 4.9 mL oil
-> 4.9 g oil
-> 3920 g xylitol

2 drops ~ 1/32 teaspoon
-> 3920/32 g xylitol = 122.5 g xylitol
-> 30.625 tsp (0.64 cups) xylitol crystals

25+ drops oil per 1/4 teaspoon

Actual: 75 drops oil + 2/3 cup crystals
```

...and that was *way* too much oil, particularly since the result is more of
a hard goop that dissolves very quickly, releasing all the menthol at once.
I'll see what happens when I freeze it.

After freezing, these seem to be nice crystals, although the outer layers
are more amorphous and the inner layer is more crystalline, with clear
crystal domains ~ 4 mm. Amusingly, now the menthol concentration seems too
*low*.

## Attempt #2

OK, let's try this again, but with menthol crystals.

I can't get accurate masses on the crystalline menthol, so I'll use
dimensional measurements to get an approximate volume.

The first crystal (cylindrical) is 1 9/16 inches long and 1/8 inch thick.
4 cm long, 0.32 cm thick = 0.32 cm^3

At 0.890 g/cm^3, that's 0.283 g. That's 283 mg. By the 800x mass ratio,
we need 226 g xylitol, or 56 tsp = 1.2 cups. That's doable!

We'll use 1 cup.

Pot covered when not stirring.

Samples crystalize quickly in the freezer.

Seems a bit weak. Adding 3.175 cm long x 0.3175 cm x 0.47625 cm cyl = 0.377 cm^3
(0.336 g menthol)

Samples no longer crystalize quickly in the freezer.

Next morning: Still not fully crystalline. Center of pot is crystalline,
but the outer centimeter or so is a clear semi-solid that can be dented
with a fingernail. Perhaps the methol concentration is too high, and is
interfering with crystal formation? This seems unlikely, since the result
tastes less mentholated than the stronger cough drops, which seem more
crystalline.

Eureka! The freezer may be *too cold* for crystallization. As before, I briefly
ran hot water over the outside of the pot to unstick the contents, then pried
it out onto a plate. I let it sit at room temperature overnight under a cover,
and in the morning it was crystalline.

## 2017-11-08

A CVS menthol & eucalyptus cough drop (with glucose) is 3.7 g and
contains 6.5 mg menthol. That's a 570x ratio; attempt #2 was about
300x, but hard to say because I didn't have a milligram scale. But I
do now! Aiming for 300x.

- Melted 227 g xylitol crystals (1 cup) in first a double-boiler and
  then over direct heat to finish melting it.
- Stirred in 300 mg menthol crystals, which immediately produced a
  dense white smoke. Added more, same results. Little remained in the
  xylitol; I think the xylitol was too hot.
- Cooled the xylitol from underneath with ice water (too long) until
  the xylitol formed a highly viscous clear syrup
- Put back over direct heat to thin the xylitol and bring it up to
  menthol-melting temperature
- Added perhaps 1 g more menthol, and then perhaps 20 drops of
  distilled peppermint oil, while stirring vigourously over heat.
- Xylitol turned opaque white, then fragmented into granular
  gel. Removed from heat, set back on ice water.
- Spread what I could on parchment paper to cool; some white smoke
  released as I smoothed it.

So, moderately disastrous, but I learned some things:

- Xylitol must be cooled before stirring in menthol
- Don't over-stir
- Definitely continue using good ventilation! That menthol smoke would
  have been nasty.

## 2023-12-17

- 225 g xylitol crystals
- 1 g menthol crystals
- 20 drops peppermint oil

Didn't work well -- too much menthol escaped. Also stuck badly to the wax
paper, so go back to using parchment paper in the future!

I remelted the xylitol, fished out the shreds of wax paper, let it
cool again to somewhere below 200°F (I think), and aded another 1 g of
menthol and more peppermint oil (to replenish what volatilized off,
and then some) while stirring. The mixture quickly clouded and became
grainy, likely because it was supercooled. I spread it on parchment
paper to cool and harden. There was residue in the pot, so I heated
that back to a thin liquid and poured it out on the parchment paper as
well. (It ended up with a somewhat different texture from the rest.)

The final result is _extremely_ strong. Use sparingly! Great for cough
suppression.
