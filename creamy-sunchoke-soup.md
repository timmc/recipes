# Creamy Sunchoke Soup

From Mark Bittman's "How to Cook Everything Vegetarian", page 130.
It's a variation of Creamy Cauliflower/Broccoli Soup, which calls for
1 head of cauliflower or 1 lb of broccoli.

## 2019-03-16

Ingredients:

- 2 Tbsp salted butter
- 530 g sunchokes, 1 cm dice
- 290 g of yellow onion (2 small) and shallots (2 med), 5 mm sliced
- 10 g of garlic cloves (3 small), 5 mm chopped
- large pinch coarse-grain salt
- a few grinds of black pepper
- 1/2 cup white wine (used Yellowtail Sauvignon Blanc)
- 3 cups vegetable stock (3 tsp Seitenbacher's vegetarian broth mix in
  3 cups water)
- Optional: Plain yogurt (whole milk)

Directions:

- Melt butter over medium heat in deep pan
- Add sunchokes, onion, garlic, salt, and black pepper
- Cook, stirring occasionally, until onion is softened, 5 to 10
  minutes
- Add in white wine and cook for 1 minute
- Add in stock
- Cook until sunchokes are tender or soft, 10-20 minutes (I ended up
  covering the pot after 10 minutes because it was losing water)
- Allow to cool somewhat
- Purée with immersion blender
- Serve with yogurt (or sour cream) to mix in
