# Mushroom bacon

## 2021-01-06

- 4 oz shiitake mushrooms (somewhat dried out), sliced 2-4 mm
- 1/4 tsp liquid smoke
- 1 Tbsp olive oil
- 1 Tbsp soy sauce
- 1 tsp ancho chile powder

Mixed all and let sit in fridge overnight. Heated cast iron skillet to
medium heat and cooked marinated mushrooms, stirring occasionally,
until soft (3-4 minutes.) Increased heat to high and cooked until
drier and starting to brown, perhaps 5-7 minutes.

Tasty, but not very bacon-like.

## 2021-01-08

- 173 g shiitake mushrooms (somewhat dried out), sliced 2-4 mm
- 1/2 tsp ancho chile powder
- 1/2 tsp table salt
- 1 Tbsp soy sauce
- olive oil (1-2 ml?)

- In a large bowl, sprinkle sliced mushrooms with chile powder and
  salt, then mix
- Spinkle soy sauce on the mushrooms, then mix again
    - I think I may have refrigerated them overnight at this point?
- Move mushrooms to the side, pour a small drizzle of olive oil into
  the bare space, then stir mushrooms through the space
    - Try not to let any of the mushrooms individually absorb too much
      oil, since this can give them a soggy texture.
- Spread evenly on a baking sheet and bake at 375°F for 30 minutes,
  stirring every 10 minutes

Pieces actually started burning a bit somwhere between 20 and 30
minutes, but fresher (wetter) mushrooms will likely take 40 minutes.

I forgot to mix liquid smoke into the soy sauce before sprinkling it
in, but I'm not sure it's even needed.

Huge reduction in volume. Could likely increase by 3-4x and still use
a single baking sheet.

Much more bacon-like this time.

## 2021-01-20

2021-01-01 but scaled up, and using fresher mushrooms. Mushrooms were
not just fresh but maybe a little soggy, so I tried to adjust the
other proportions accordingly, but there ended up being too much liquid.

- 428 g shiitake mushrooms including stems, very fresh, sliced 2-4 mm
  (trimmed weight)
- 7.14 g (1 tsp) table salt
- 2.68 g (1 tsp) ancho chile powder
- 2 Tbsp soy sauce
- 6 drops liquid smoke
- ~2 ml olive oil

- In a large bowl, sprinkled sliced mushrooms with salt and chile
  powder, then mixed
- Spinkled soy sauce on the mushrooms, then mixed again
    - Let sit 22 minutes
- Moved aside some mushrooms and mixed liquid smoke into liquid, then
  stirred in to rest of mushrooms
    - Let sit in fridge 12 hours
    - Too much liquid!
- Drained liquid and reserved for other cooking projects
- Moved mushrooms to the side, poured a small drizzle of olive oil into
  the bare space, then stirred mushrooms through the space
    - Tried not to let any of the mushrooms individually absorb too
      much oil, since this can give them a soggy texture.
- Spread evenly on a baking sheet and baked at 375°F for 08:02 -> 08:42 _____ minutes,
  stirring every 10 minutes
    - Pulled out as the bacony smell began to intensify (about 5-10
      minutes after it first started) and as the pieces were speeding
      up in their shriveling, losing the mushroomy flavor.

In the future, if the mushrooms are very fresh, consider letting
mushrooms sit after salting to see if they give off any liquid -- and
be careful how much soy sauce I add. Or add soy sauce first?
