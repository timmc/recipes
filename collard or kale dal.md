# Collards/kale, lentils, and tomato

2020-07-28 is the best version so far.

## v1

- 17 collard leaves
- 2 cups brown lentils
- 3 cups water for lentils (I think!)
- 3+ Tbsp olive oil
- 1 Tbsp mustard seeds
- 2 tsp cumin seeds
- 1 tsp nigella seeds
- 1 green chili pepper (serrano?), minced
- 27 g frozen ginger, minced
- 28 g garlic (4 cloves), crushed
- 1/4 tsp turmeric powder
- 750 g (~26 fl oz) canned plum tomatoes (w/ citric acid)
- ??? salt
- 1 Tbsp lime juice
- 1/2 tsp garam masala powder

Lentils and collard ribs:

- Soak lentils in water for a couple hours
- Separate collard leaf blades from ribs and chop the leaves
- Chop collard ribs 1 cm and add to lentils; cover and bring to boil,
  then simmer until lentils tender

Tempered spices:

- In a skillet, toast mustard, cumin, and nigella seeds in oil over
  medium heat until they begin to sizzle
- Add chili, then 30 seconds later add ginger; turn heat up on account
  of frozen ginger; sauté until spices start popping and ginger starts
  to smell a little singed, about 3 minutes
- Turn down heat to medium and add the garlic, stirring until it
  starts to become translucent (watch out for burning)
- Stir in turmeric, then pour in tomatoes to quench
- Bring up to a simmer

Combined:

- Add chopped collard leaf blades, tempered spices, and salt into
  lentils pot
- Mix, and add water for consistency
- Mix in lime juice and garam masala

## 2020-07-17: Collards, lentils, and tomato

Similar to before, but with lentils cooked previous day.

- 13 collard leaves
- 2 cups brown lentils, soaked and cooked to tender, water discarded
- 3+ Tbsp olive oil
- 1 Tbsp mustard seeds
- 2 tsp cumin seeds
- 1 tsp nigella seeds
- 1 green chili pepper (serrano?), minced
- 18 g frozen ginger, minced
- 31 g garlic cloves, crushed
- 1/4 tsp turmeric powder
- 793 g (~28 fl oz) canned tomatoes with basil (w/ citric acid and salt)
- 2 tsp salt
- 1 cup water for consistency
- 1 Tbsp lime juice
- 1/2 tsp garam masala powder

Tempered spices:

- In a skillet, toast mustard, cumin, and nigella seeds in oil over
  medium heat until they begin to sizzle
- Add chili, then 30 seconds later add ginger; turn heat up on account
  of frozen ginger; sauté until spices start popping and ginger starts
  to smell a little singed, about 3 minutes
- Turn down heat to medium and add the garlic, stirring until it
  starts to become translucent (watch out for burning)
- Stir in turmeric, then pour in tomatoes to quench
- Bring up to a simmer

Main pot:

- Separate collard leaf blades from ribs and chop the leaves
- Chop collard ribs 1 cm and sauté in oil until changing color
- Stir in lentils, leaf blades, tempered spices, salt, and water
- Once everything is up to temperature and leaves are starting to
  become tender, turn off heat and stir in lime juice and garam masala

Notes:

- Much better than last time. Really tasty, with a little bit of
  sweetness.
- The kid liked it, except for the hot pepper, which I could barely
  taste.

## 2020-07-28: Kale, lentils, and tomato

Based on 2020-07-17 but with kale instead of collards, no hot pepper, 25% less salt, more
water, possibly 2-3x the lime juice. Flavor very close.

- 21 lacinato kale leaves
- 2 cups brown lentils, soaked and cooked to tender, water discarded
- 3+ Tbsp olive oil
- 1 Tbsp mustard seeds
- 2 tsp cumin seeds
- 1 tsp nigella seeds
- 19 g frozen ginger, minced
- 32 g garlic cloves, crushed
- 1/4 tsp turmeric powder
- 794 g (~28 fl oz) canned tomatoes (w/ citric acid and salt)
- small sprig of basil, chiffonaded and added with tomato
- 1.5 tsp salt
- 1.5 cup water for consistency
- juice of one lime
- 1/2 tsp garam masala powder

Tempered spices:

- In a skillet, toast mustard, cumin, and nigella seeds in oil over
  medium heat until they begin to sizzle
- Add frozen ginger and turn heat up a little;
  sauté until spices start popping and ginger starts
  to smell a little singed, about 3 minutes
- Turn down heat to medium and add the garlic, stirring until it
  starts to become translucent (watch out for burning)
- Stir in turmeric, then pour in tomatoes to quench
- Bring up to a simmer

Main pot:

- Separate kale leaf blades from ribs and chop the leaves
- Chop kale ribs 1 cm and sauté in oil until changing color
- Stir in lentils, leaf blades, tempered spices, salt, and water
- Once everything is up to temperature and leaves are starting to
  become tender, turn off heat and stir in lime juice and garam masala

Notes:

- I also made a variation using two 12 fl oz jars of roasted red bell
  peppers instead of the tomato. It turned out *really* well.
    - These nominally have 196 g each of actual peppers according to
      the nutrition facts label, but after quickly draining them the
      actual drained weight was 215 g and 286 g. Other ingredients:
      Water, salt, citric acid, calcium chloride.
