# Casserole

## 2022-11-08 - Chard, shiitake, millet, cheese

Roasted mushrooms:

- 253 g shiitake mushrooms, sliced thin, including stems (were 300 g
  before spending 3 days in fridge)
- 80 g red wine
- 52 g olive oil
- 1/2 tsp dried thyme
- 1/2 tsp iodized table salt

Tossed together, spread on baking sheet, and baked on upper rack at
350°F, stirring once, until almost all darkened and losing their
moisture/softness (about 40-50 minutes).

Millet:

- 1 cup dried millet
- 1-2 tsp olive oil
- 2 cups water

Rinsed millet thoroughly, toasted for a few minutes with oil, then
added water. Brought to boil, then simmered covered until water
absorbed, ~20 minutes.

Casserole:

- olive oil
- 24 g garlic, crushed
- 493 g rainbow chard (blade and stem together) chopped to 10-15 mm
- 1/2 tsp iodized table salt
- 1/4 tsp freshly ground black pepper (fine powder)
- 1/4 tsp smoked paprika
- 1 egg
- 138 g sharp cheddar (for mixing in)
- 70 g sharp cheddar (for topping)
- 90 g gluten-free stuffing mix/croutons

Garlic sauteed in olive oil until translucent, then chard added and
cooked, stirring, until well wilted. Salt and mushrooms added near
end.

In a large bowl, mixed millet, greens/mushrooms, black pepper, and
paprika. Then mixed in egg and 138 g cheese. Packed into buttered
casserole dish, then topped with 70 g cheese and the croutons.

Baked at 400°F for 20 minutes.

Notes:

- Consider cooking mushrooms in skillet and then adding garlic and
  greens to that -- might be simpler.
- Flavor is nice, although somewhat concentrated in the
  mushrooms. Maybe chop them smaller, or mince them up before mixing
  with other ingredients?
- Total salt might be a bit high.
- Could use a little more cohesion. Next time, try not rinsing millet
  -- extra starch may help? Or just compact the mixture more tightly
  into the casserole dish.
- It's not entirely clear that the oven time is doing anything useful
  other than browning the croutons. It would probably be more
  important if there were liquid in the mixture, but this recipe
  doesn't use any stock.
- Recipes I consulted:
    - https://www.recklesstarragon.com/2018/05/24/mushroom-polenta-casserole/
    - https://www.fromachefskitchen.com/grains-and-greens-casserole/

