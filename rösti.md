# Rösti

## 2021-12-17: Parsnip and quinoa

From _How To Cook Everything Vegetarian_. Overcooked parsnips a bit,
so I couldn't grate or squeeze liquid from them.

Fell apart and started to burn, so had to improvise. Turned into a
scrambled parsnip thing.

- 97 g (1/2 cup) dried quinoa
- 226 g water (for cooking quinoa)
- 440 g cleaned and trimmed parsnips
- 2 L water + 1/2 tsp salt (for cooking parsnips)
- 1/8 + 1/4 tsp salt
- 0.5 mL (scant 1/8 tsp) freshly, finely ground black pepper
- 85 g (2 + 2 + 2 Tbsp) salted cultured butter, melted

- Cooked quinoa in 226 g water
- Brought plenty of water to boil, then salted water and added parsnips
- Cooked until they could be pierced with the tip of a knife but still
  had some resistance (or maybe more tender than that for the smaller
  ones), 12 minutes
- Drained and set aside to cool
- Lightly mixed quinoa with 1/8 tsp salt, pepper, and first third of butter
- Chopped parsnips to ~5 mm discs then ~5 mm thick strips
- Added parsnips to quinoa and lightly mixed
- Added remaining 1/4 tsp of salt (adjusted for taste) and mixed again
- Melted second third of butter in medium skillet over medium-high heat;
  once hot, firmly pressed parsnip-quinoa mixture into pan to form a
  solid cake
- Turned heat to low and cooked undisturbed until began to smell
  toasted, 27 minutes
- Gently transfered (flipped) onto plate
    - Lost a little cohesion at one edge
    - Definitely more browned in the center, where the flame was
- Turned heat up to medium, added last third of butter, and carefully
  put the rösti back in to cook second side until browned nicely...
    - Sort of fell apart when dropped into skillet; pressed back into
      cake
    - Started to burn in center, but edge was still not well cooked
- Repeatedly scrambled and then pressed back into cake, not leaving it
  alone long enough to burn, until well-cooked.

I think that since I didn't shred the parsnips, I didn't end up with
the long strands that could have held the dish together.
