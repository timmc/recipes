# Sourdough!

Work in progress.

----

I use a pretty relaxed method and get very nice, sour sourdough. No
fancy schedules or ratios, no measuring by weight. 

This recipe is for gluten-free sourdough; to use wheat, just reduce
the stirring to the minimum necessary. (You'll want to anyhow. The
gluten makes it very hard to mix.)

## Making the starter

- Follow http://www.artofglutenfreebaking.com/2010/10/sourdough-starter-gluten-free/
- So that's equal volumes water and flour (packed measure), or 1.25 to
  1 by mass (TODO: check on this)
- Use whole grains to get the enzymes, bacteria, and yeast that reside
  in and on the bran

## Maintaining the starter

- Feed once every day or so. Stir once or twice a day, especially if
  there is liquid ("hooch") on top. (I don't really know what the
  hooch indicates, so I just stir it back in and call it good.)
- Brown rice flour makes for a *lot* of hooch. Nothing else seems to.
- Bean flours can lend a funky flavor, as can buckwheat. I've heard
  that sorghum can as well, but have not observed this. So a broad mix
  of flours is a good idea to make the flavor less startling.
- Keep it covered with cloth and in a warm-ish. That seems like a good
  idea, right? No evidence to the contrary. I've raised starter in a
  house that gets to 55°F at night with no trouble, it just grows more
  slowly.

## Making the bread

Follow http://www.artofglutenfreebaking.com/2010/10/sourdough-bread-boule-gluten-free/ more or less.

- 4 cups starter
- 3 cups flours (packed measure)
- 2 tsp xanthan gum (replaces gluten, sort of)
- 2 tsp salt (for flavor)
- 2 tbsp sugar (for flavor, and I often skip it)
- ~3/4 cup water

Mix dry ingredients, add starter. Mix, adding water until it becomes a
sticky mess. (3/4 cup is about right.) If you lift your hand out, a
big chunk of dough should follow. However, it should also be juuuust
liquidy enough to flow slightly and take the shape of the container
(if coaxed).

Add other ingredients at this step. Chocolate chips? Spices? Minced
hot peppers? Garlic cloves? Up to you.

Grease a dutch oven, pour/drop in the dough, and smooth it out. Carve
something interesting into the top 5-10 mm. This will turn into a
furrow in the surface and is almost entirely decorative. (With
crustier breads this would function as a release point for stresses
that develop in the cooking bread.) Cover and let sit 6-12
hours. Eventually it will stop rising and you can bake it.

(The Art of Gluten-Free Baking has you instead rise the dough in
parchment paper in a different container and then transfer it to a
dutch oven that has also been pre-heated, but this involves squishing
the dough around a bit, and I worry that that destroys some of the
bubble structure of the risen dough!)

UNTESTED:

Preheat the oven to 425°F. Bake for 45 minutes, remove cover and bake
for 15 more minutes. Confirm that the internal temperature is above
200°F (perhaps 205°F?), and bake more if necessary. Remove from oven
and let sit for 15 minutes, then transfer from dutch oven to cooling
rack and allow to *completely* cool before slicing.



Wheat variation:
- 100% hydration by volume (add some whole wheat to make it thicker)
