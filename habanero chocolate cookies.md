# Habanero chocolate cookies

Spicy chocolatey cookies. Minced habanero is heated with fats to
create a sneaky slow burn (10-30 seconds to peak). Mild to
mild-moderate heat.

Makes about 20 cookies.

### 2014-04-21 - First attempt

Actually a variation on the rosemary cocoa sea salt cookies recipe;
removed rosemary and added habaneros, butter, and chocolate.

- 1.5 cup AP wheat flour (or some gluten-free flour)
- 1/3 cup cocoa powder
- 1/2 tsp baking soda
- 1/2 tsp large-grain salt
- 3 tbsp non-dairy "milk"
- 1/2 tsp vanilla extract
- 1/2 cup canola oil
- 1 cup sugar
- 1/4 cup maple syrup
- 4 habaneros
- 1 Tbsp butter
- 50 g baking chocolate

- Remove the stems, seeds, and ribs from 4 habaneros (reduces
  heat-to-flavor ratio)
- Finely mince them, perhaps with gloves on
- Melt butter (1 tbsp) and baking chocolate (50 g) around the minced
  habanero, stir, and let sit for 30 minutes (to spread the capsaicin
  into the fats, theoretically...)
- Add oil and stir (make more amenable to mixing with other
  ingredients)
- Mix dry ingredients, then mix in wet ingredients.
- Bake at 350°F until they look almost but not quite done, about 15-20
  minutes. (They'll harden up as they cool.)

### 2015-09-27 - Hodge-podge

This time I also had some unused portion of the dry ingredients of
another batch of cookies, so I threw that in as well to use it up.

- 1 tbsp cultured butter
- 45 g baking chocolate
- 4 minced habaneros, minus placentas and seeds

Melted together, as above.

- 1.5 cups quinoa flour
- 0.5 cup cocoa powder
- 1/2 tsp large-grain salt
- 1/2 tsp baking soda
- 3/4 cup mixture of: GF rolled oats, sugar, Bob's Red Mill GF flour

Mix.

- 1/2 cup olive oil
- 1/2 cup maple syrup
- 1/2 tsp vanilla extract
- 3 tbsp almond milk (soy-free)

Mix.

Dough was sticky. I couldn't form nice little balls on the
sheet. Placed 18 misshapen wads on sheet. :-)

Baked 15 minutes @ 350°F.

The lumps expanded quite a bit but did not spread. Reasonably
cohesive, but contain some "fault lines". Probably could do with a bit
more flour as well as some moulding. (Not that this recipe is
particularly reproducible...)

Good slow burn, takes up to 15 seconds for full impact.

Stats: GF, low lactose

## 2024-09-29 - Gluten free and brown sugar

Using GF flour, brown sugar instead of
white (and drop to 0.75x), olive oil instead of canola, table salt
instead of large-grain.

- 50 g baking chocolate
- 1 Tbsp unsalted butter
- 4 habaneros, with stems, seeds, and ribs removed; finely minced
- 106 g (1/2 cup) olive oil
- 236 g (1.5 cup) Bob's Red Mill GF AP flour, sifted
- 34 g (1/3 cup) baking cocoa powder
- 114 g (3/4 cup) dark brown sugar (loose, not packed)
- 1/2 tsp table salt
- 1/2 tsp baking soda
- 1/4 cup maple syrup
- 3 tbsp almond milk (unsweetened)
- 1/2 tsp vanilla extract

- Combine oil, butter, baking chocolate, and minced habanero. Melt and
  stir until combined, then let sit for 30 minutes (to spread the
  capsaicin into the fats, theoretically.) Consider using gloves when
  processing habaneros.
- Mix remaining wet ingredients into oil mix.
- Mix dry ingredients, then combine all.
- Form into balls on baking sheet and bake at 350°F until they flatten
  out, about 15 minutes. (They'll harden up as they cool.)

Notes:

- Nice level of heat. Slowly rising and then steady burn, but not
  particularly strong. Could easily double the habanero (unless these
  were just fairly mild peppers, which is possible.)
- Delicate/crumbly. Maybe too little sugar? Probably should have
  packed the brown sugar to make the 3/4 cup, at least. Possibly
  underbaked, since microwaving a cookie a couple days later made it
  much firmer after it cooled. Maybe sugar did not get a chance to
  caramelize, or there was too much moisture from the brown sugar.
    - From later experiments: It was just that there was too little
      sugar. Should have been tight-packed in cup (166 g).
    - Making this again with 166 g brown sugar still resulted in soft
      cookies, even at 20 minutes. (The dough felt unexpectedly soft
      too.)
- Produced about 25 cookies.
