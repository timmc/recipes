# Canning

## Tomatoes 2019-08-31

- ~10 kg of tomatoes ranging from a little underripe, ripe, and
  "seconds" (some spots which need to be cut out)
- citric acid

Process:

- Wash tomatoes to remove any leaves and dirt
- Trim spots and stem end
- Dice 1-2 cm, with peel still on
- Drop diced tomatoes directly into quart jars via funnel
    - If you collect them first in a bowl, the liquid will settle to
      the bottom
- Pack down to 1-1.5 cm headspace; use flat utensil to free trapped
  air along sides
- Add 1/2 tsp citric acid to each jar
    - Should add to bottom, rather than after filling. I did take the
      precaution of using a spoon to put the citric acid down the
      side, not just on top, since it could be expelled during
      canning.
- Screw on lids finger-tight
- Boil 50 minutes
- Remove and let sit on counter

Deviations:

- Batch 1 had seven jars:
    - First four (1.1) were placed in stock pot full of hot water,
      with butter knives on the bottom rather than a rack. I then ran
      out to get a proper canner, and I think some water may have
      infiltrated the jars as they cooled slightly in the water. I
      scooped out some water and, since I was concerned some citric
      acid may have been lost, added 1/4 tsp more to each.
    - They were then put in a proper water-bath canner with the next
      three (1.2) and given a full cycle.
    - Batch was simmered at times rather than always a rolling boil
- Bath 2 has five jars:
    - First four (2.1) were similar to 1.2
    - Last (2.2) contained plum tomatoes rather than regular field
      tomatoes

## Tomatoes 2019-09-14

- About 10 kg of seconds tomatoes, including some plum and non-seconds
  sauce tomatoes of some sort
- citric acid

Note: about 15 minutes to prep the new jars (washing)

### Batch 1

Cold pack. 6305 g of trimmed and diced tomatoes, with 926 g lost as
trim, resulting in 5379 g across 7 jars.

0.5 tsp citric acid at bottom of each jar. 60 minutes at a boil.

### Batch 2

Hot pack. 4513 g of trimmed and diced tomatoes, with 583 g lost as
trim. Added fresh from chopping into pot over high heat, kept at
boiling. Pot left overnight.


Canning:

- Water bath canner filled about half way, with rack in hanging
  position and water just 1 cm over its bottom; put over high heat to
  warm
- Tomatoes heated to boiling
- 1/2 tsp citric acid added to each of 4 jars
- Jars placed in rack of canner to warm (too much water and they'll tip)
- Fire turned off under tomatoes
- Each jar taken out, filled leaving 0.5–0.75 inch headspace, capped,
  and placed back in canner
- Jars lowered in
- Left at boiling for 60 minutes

No liquid separation, but the tomatoes also lost 510 g to some
combination of juice runoff (which there was little of) or
evaporation; 3420 g is in the jars. That's about 1/8th of each jar,
which is about what I see in the raw-packed jars.

## Tomatoes 2020-09-05

### Batch 1

7060 g "heirloom" tomatoes (16 large), hot pack in quart jars.

Tomatoes were seconds, but in good shape. 350 g trimmed during
chopping.

Enough liquid boiled off that I only had enough to fill 6.5 jars, so I
only did 6.

- Water bath canner filled about half way, with rack in hanging
  position and water just 1 cm over its bottom
- 6 jars placed in canner with 1/2 tsp citric acid in each and
  lids/rings very loosely on
- High heat under canner to warm up water
- Tomatoes trimmed and chopped 1 cm dice into stew pot and kept at a
  simmer
- Fire turned off under tomatoes
- Each jar taken out, filled leaving 0.5–0.75 inch headspace, capped,
  and placed back in canner. (Spin ring until it just catches.)
    - Did not use a bubble remover stick; tomatoes were already pretty
      stewed down.
- Jars lowered in
- Left at boiling for 60 minutes

### Batch 2

7281 g San Marzano minus 87 g trim. Same process.

Filled 7 jars and had enough for a little over 1/2 another jar.
