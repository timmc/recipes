# Beet and tahini dip

## 2015-12-15

From: http://www.thekitchn.com/recipe-creamy-beet-and-tahini-dip-recipes-from-the-kitchn-188995

- 350 g roasted beets (2 medium/large beets)
- 9 g garlic (1 medium/large clove)
- 15 mL lemon juice
- 127 g tahini (125 mL)
- 100 mL skim filmjolk (similar to yoghurt or sour cream)
- 3 g large grain salt (1/2 tsp)
- 1/8 tsp ground black pepper

Process until smooth.

Serving suggestion from original recipe: "Drizzle with olive oil and
serve with chips or pita."

## 2017-01-01

1.5x version of 2015-12-15.

- 512 g roasted beets (7 small/medium)
- 15 g garlic cloves (3 medium/large cloves)
- 23 g lemon juice (from concentrate)
- 197 g tahini
- 150 g skim filmjölk
- 4 g (1/2 tsp) iodized table salt
- 1/4 tsp (scant) ground black pepper

Process until smooth.

I tried making it without filmjölk this time, but it really does need
the dairy (at least with these ratios), so I kept it in.

## 2018-09-15

Replicating 2017-01-01.

- 521 g roasted beets (5 small/medium)
- 16 g garlic
- 25 g lemon juice (fresh-squeezed)
- 209 g tahini
- 150 g skim filmjölk
- 3 g (1/2 tsp) iodized table salt
- 1/4 tsp (scant) ground black pepper

Process until texture as desired.

Tastes good. Little sharp on the garlic.

## 2020-08-29

- 520 g roasted beets, skin still on
- [process]
- 200 g tahini
- 20 g lemon juice (bottled key west lemon juice)
- [process]
- 10 g garlic, sliced
- 150 g plain whole kefir
- 3 g (1/2 tsp) iodized table salt
- 1/4 tsp ground black pepper, ground to powder with mortar and pestle
- [process]

Still a little high on the garlic and black pepper.
