# Celeriac and radish stir-fry

## 2014-06-01

- Olive oil, high heat
- 3 small celeriac cut in 4 mm slices

Sautéed for a few minutes, adding:

- A few twists of black pepper
- Minced garlic (3 small/med cloves)
- About 6 fenugreek seeds, ground
- About 12 cumin seeds, crushed
- About 10 coriander seeds, crushed/ground
- 1/2 small daikon radish, cut in 5-6 mm slices
- Sprinkling of salt
- Reduced heat somewhere in here
- Sautéed a few more minutes
- Splash of mustard oil

There was a meaty flavor to the food -- possibly a result of the
burned garlic? I should explore this more.

## 2015-11-05

Like 2014-06-01, but doubled, using red radish instead of daikon, and
adding the mustard oil early.

- 2-3 tbsp olive oil, high heat
- 581 g celeriac, trimmed, sliced 4 mm (3 small/med, 751 g untrimmed)
- 1-2 tbsp mustard oil

Saute, stirring frequently to keep from burning, for about 8 minutes
until becomes soft and slightly browned in places.

Reduce heat to medium-low and add:

- 12 fenugreek seeds, crushed/ground (mortar & pestle)
- 24 cumin seeds, crushed/ground
- 20 coriander seeds, crushed/ground
- 14 g garlic (3 med cloves)
- 167 red radishes (~12 small), sliced 6 mm
- 3 mL tsp salt

Saute, stirring to keep garlic from burning, about 6 more minutes.

Notes:

- Even with the doubling, still worked in a 10-inch cast iron skillet
- I think the meaty flavor comes from the celeriac sauteed at high heat

