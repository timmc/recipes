# Cucumber-Zucchini Soup

## 2015-08-22

Based on http://www.epicurious.com/recipes/food/views/zucchini-cucumber-soup-235460

- 3.8 lb cucumber
- 3.2 lb zucchini
- 1.5-2 cups chopped scallion greens
- 1 clove garlic
- 1/2 jalapeño pepper
- 1 shishito pepper (not a hot one)
- 1 tbsp ground coriander
- 5 tsp salt
- 3/4 cup apple cider vinegar
- 3/4 cup water

Blend it up.

A little heavy on the hot pepper, salt, and possibly the green
onion. Goes well with a dollop of yoghurt.
