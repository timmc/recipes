# Magic Layer Cake

Recipes that produce a dessert that separates itself into cake on top
of custard.

## 2024-04-17 - Lemon, gluten-free, non-dairy

From https://cooking.nytimes.com/recipes/12363-lemon-pudding-cake --
this recipe turns into a cake on top of a custard, using a water bath
technique.

Adapted to use almond milk instead of whole milk, gluten-free flour,
table salt instead of kosher salt, and 25% less sugar.

I also spilled and lost some of the egg whites (maybe 1/2 to 1 egg's
worth), but it turned out fine.

- 4 large eggs, separated
- 1 tsp finely grated lemon zest (~1.5 lemons)
- 1/3 cup lemon juice (~2.5 lemons)
- 1 Tbsp unsalted butter, melted
- 3/4 cup (175 g) sugar
- 1/2 cup (104 g) all-purpose gluten-free flour (Bob's Red Mill)
- 1/2 tsp table salt
- 1.5 cups almond milk

Steps:

- Place a large roasting pan on a center/upper rack of the oven,
  filled about halfway with water, then preheat to 350°F.
- Butter an 8-inch square baking pan.
- In a medium to large bowl, whisk together the egg yolks, lemon zest,
  lemon juice and butter.
- In another bowl, stir together the sugar, flour and salt.
- Whisk half the flour mixture into the egg yolks, then half the
  milk. Whisk in remaining flour mixture, then remaining milk.
- Whip the egg whites until soft peaks form, then gently fold them
  into the batter. They'll mostly float rather than fully combine, and
  that's fine.
- Pour batter into the buttered dish. Place the dish in the pan of
  water in the oven. Bake until the cake is set, about 45 minutes.

Notes:

- Came out great! Should make this again. No changes needed.
- Custard was a little soft for scooping out during serving, at least
  with a knife and spoon. A narrow spatula would have been perfect,
  but a firmer custard wouldn't hurt. Definitely firmer at corners of
  pan.
- The top started to brown fairly early on, which had me a little
  worried, but then it stopped getting darker.

Variations recommended by commenters:

- Buttermilk instead of milk. Or plain yogurt thinned with water for a
  cheesier taste. Or warmed coconut milk.
- Instead of trying to float the custard-filled pan, place it into the
  roasting tray in the over and then pour kettle-boiled water into the
  tray around it.
- Doubling lemon zest and juice works well if you like strong lemon
  flavor.
- Fold in a cup of raspberries.
- Cook it in ramekins, and maybe serve them by inverting onto a plate,
  with berries on top.

## 2024-06-02 - Chocolate, gluten-free, non-dairy

From https://www.tasteofhome.com/article/chocolate-magic-cake/ but
gluten-free and non-dairy. Table salt instead of Kosher salt. Using
espresso instead of equal volume of espresso powder (much milder, of
course, but it's what I had on hand.) Cacao powder rather than cocoa
powder (fattier). Used lime juice to help stiff peaks form.

- 4 large eggs, separated and at room temperature
- 150 g granulated sugar
- 113 g (8 Tbsp) unsalted butter, melted and cooled to room temperature
- 1 teaspoon pure vanilla extract
- 1/2 teaspoon espresso (optional)
- 1/8 teaspoon table salt
- 60 g gluten-free all-purpose flour (Bob's Red Mill)
- 40 g (1/4 cup) cocoa powder
- 2 cups almond milk, at room temperature
- 1/2 tsp lime juice

- Separate the eggs, putting the yolks in one large bowl (4-5 qt) and
  the egg whites in another clean, dry bowl large enough for beating.
- Preheat the oven to 325°F. Then grease an 8×8-inch baking pan.
- Add the sugar to the bowl with the egg yolks and beat until smooth
  and pale in color.
- While mixing on low, drizzle in the melted butter, vanilla,
  espresso, and salt. Beat for 1 to 2 minutes until well incorporated.
- Using a rubber spatula, stir the flour and cacao powder into the
  mixture. Mix thoroughly, scraping down the sides as needed.
- Add the milk to the chocolate mixture, about 1/2 cup at a time,
  beating until the batter is well combined and uniform in color. Set
  aside.
- In a separate, clean bowl, add lime juice to the egg whites (to
  stabilize them) and whip until stiff peaks form.
- Gently fold the egg whites into the chocolate batter. Mix with a
  gentle hand so you don’t deflate the egg whites. The final batter
  should be thin, with distinct lumps of egg whites still visible
  throughout. Do not mix until smooth, or the layers will not form
  correctly.
- Transfer the batter to the prepared baking pan and bake for 40 to 45
  minutes until the top is firm to the touch and the center slightly
  jiggles when shaken gently.
- Leaving the cake in the oven, turn off the heat and crack the door
  to allow heat to escape. Let the cake remain in the oven for 4 to 6
  minutes, then remove and let it cool at room temperature completely,
  about 60 minutes. Transfer to the fridge and chill for at least 4
  hours.

Notes:

- I didn't get good separation. The cake layer was quite thin and I
  didn't see other layer divisions. I may have folded the whites in
  too thoroughly, and the fat of the cacao powder may have done
  unwanted things to the recipe as well.
- Tasty, but I'll try the lemon recipe in the future if I want good
  separation.
