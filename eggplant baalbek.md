# Eggplant Baalbek

2021-09-11 is my current preferred version (correct salt level).

## Recipe as received

Serves 6 (Ha!)

- 2 sm eggplants, peeled, cut into 1/2” chunks
- 2 med onions, chopped
- ½ med green pepper, chopped
- 1/8 tsp garlic powder
- 3 T olive oil
- 2 med tomatoes, diced
- 2 t salt
- 1 t black pepper
- 1 8-oz can tomato sauce
- 2 T butter
- 1 c grated sharp cheese
- 1 egg, beaten
- ½ c whole milk
- ¼ c minced fresh parsley

- In a large skillet, sauté the onions and green pepper in oil with
  garlic powder for 5 minutes, stirring constantly.
- Lower heat and add eggplant cubes, simmer for 10 minutes, stirring
  often.
- Add tomatoes, salt, and pepper, and simmer for 5 minutes more,
  stirring occasionally.
- Add tomato sauce, and mix well.
- Butter a 2-qt casserole and place the vegetable mixture in it.
- Mix together the cheese, egg, milk, and parsley, and pour it over
  the eggplant mixture.
- Dot with butter and bake in a preheated 350 F oven for 30 minutes.

## 2021-08-22: Low FODMAPs

Changes:

- Scallion greens and garlic instead of onion and garlic powder
- Pureed tomato instead of canned tomato sauce, and indicated to drive
  off some of the water in compensation

- 3 Tbsp olive oil
- 24 g garlic, crushed
- 71 g scallion greens (one bunch)
- ~175 g green bell pepper (1 medium), chopped
- 470 g eggplant (4 long, w/o peels or stems), cut into 1/2” chunks
- 386 g tomato, diced
- 2 tsp salt
- 1 tsp black pepper, freshly fine-ground
- 238 g tomatoes, pureed
- 1 egg, beaten
- 93 g (1 cup) grated sharp cheddar cheese
- 127 g (slightly over 1/2 cup) almond milk
- 14 g (1/4 cup, 2 sprigs) fresh parsley, minced
- 2 Tbsp butter

- In a large (12+ inch) skillet, sauté the garlic, scallion greens,
  and green pepper in olive oil for 5 minutes, stirring frequently.
- Lower heat and add eggplant cubes, sauté until eggplant is browning
  and softening (about 10 minutes), stirring often.
- Add tomatoes, salt, and pepper, and simmer until most of the liquid
  is driven off (about 5 minutes), stirring occasionally.
- Add pureed tomatoes and cook off excess water on high, until mixture
  is more of a thick stew than a soup
- Butter a 2-quart casserole and place the vegetable mixture in it.
- Mix together the egg, cheese, almond milk, and parsley, and pour it
  over the eggplant mixture.
- Dot with butter and bake in a preheated 350°F oven for 30 minutes.

Notes:

- Really tasty, especially eaten with dosa or bread. Very rich flavor.
- Black pepper comes through as a bit of heat in the aftertaste. Could
  reduce this, and probably should. Maybe 3/4.
- Salt might be a bit high -- consider 1.5 tsp? It's not unpleasant,
  just may be more than necessary for the flavor. (Confirmed in later
  recipe.)
- Personal notes: AT enjoyed it very much, RT tolerated it on bread.

## 2021-09-11

Basically doubled 2021-08-22. Did not peel eggplants. Eggplants were
borderline overripe, on average. Less scallions than I would have
liked. Less butter, proportionally, although more by surface area.

- ~6 Tbsp olive oil
- 49 g garlic, crushed
- 106 g scallion greens (one bunch)
- 40 g green bell pepper, diced
- 900 g eggplant (with peels but not tops), cut into 1-2 cm chunks
- 787 g tomato, diced
- 3 tsp salt
- 2 tsp black pepper, freshly fine-ground
- 471 g tomatoes, pureed
- 2 eggs, beaten
- 187 g grated sharp cheddar cheese
- 254 g almond milk
- 28 g (1/2 cup, 4 sprigs) fresh parsley, minced
- 3 Tbsp butter

Ended up baking this for 45 minutes instead of 30.

Barely fit in 12" cast iron skillet. (Note to self: Fit well in the
red ceramic casserole dish.)

Salt level is good.
