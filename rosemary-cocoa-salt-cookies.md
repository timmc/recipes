# Cocoa, Rosemary, and Salt Cookies

Here's the recipe as received from Annie, which makes 15-20 vegan
and optionally gluten-free cookies:

- 1.5 cup AP wheat flour (or some gluten-free flour)
- 1/3 cup cocoa powder
- 1/2 tsp baking soda
- 1/2 tsp large-grain salt
- a bunch of rosemary leaves
- 3 tbsp non-dairy "milk"
- 1/2 tsp vanilla extract
- 1/2 cup canola oil
- 1 cup sugar
- 1/4 cup maple syrup

Mix dry ingredients, then mix in wet ingredients. Bake at 350°F until
they look almost but not quite done, about 15-20 minutes. (They'll
harden up as they cool.)

## Variations

The recipe is fairly forgiving; I often make some combination of the
following modifications:

- double the cocoa powder (might make more crumbly?)
- reduce the sugar and/or maple syrup
- add chocolate chips
- substitute olive oil for canola oil
- add butter
- use quinoa flour instead of wheat flour

### 2015-09-27

- 44 g baking chocolate
- 0.75 cup olive oil

Melt together.

- 1.5 cups quinoa flour (possibly slightly over)
- 0.5 cup cocoa powder
- 1/2 tsp large grain salt
- 1/2 tsp baking soda
- several tbsp dried rosemary leaves
- 3/4 cup turbinado sugar

Mix.

- 0.25 cup maple syrup
- 1/2 tsp vanilla extract
- 3 tbsp almond milk (soy-free)

Mix.

Dough was pretty dry, so I increased the evoo from 0.5 to 0.75
cup. Formed 18 1.5–2" balls.

Baked 15 minutes @ 350°F.

They came out pretty dry and crumbly, but retained shape. Should have
increased maple syrup instead of evoo? Could have used a little more
rosemary. However, delicious.

Stats: GF, vegan, soy-free

## 2016-01-30

Trying to use up pecan meal, and also seeing what happens if I cut out
the cane sugar and only rely on the maple syrup for sweetness.

- 350 g pecan meal (from making pecan milk)
- 160 g (1 cup) brown teff flour
- 240 g (2 cups) quinoa flour
- 1 tsp baking soda
- 1 tsp large grain salt
- 74 g (2/3 cup) cocoa powder
- 1 tsp chopped fresh rosemary
- 1/2 cup maple syrup
- 1 cup olive oil
- 1 tsp vanilla
- 6 tbsp almond milk

Notes:

- Tasty enough, but kind of biscuity. Expected with no sugar + pecan meal.
- Extremely wet/oily. Reduce oil?
- Rosemary added nothing to this batch.
