# Drinks

## 2017-04-28 - Flat cream mk1

When I make this right, it tastes like something between cream soda
and root beer. In the process of measuring, I failed to get the
proportions right, so this is just the first try.

- 152 g water
- 2 tsp maple syrup
- 1/2 tsp balsamic vinegar
- 1/4 tsp lemon juice (bottled)
- 1/4 tsp chocolate bitters

Needs more water and maple syrup -- fruity flavors are dominating and
need dilution + more of the burnt flavor of maple syrup. Next time,
try 1 tbsp maple syrup and 200 g water.

## 2017-04-29 - Flat cream mk2

- 200 g water
- 4 tsp maple syrup
- 1/2 tsp balsamic vinegar
- 1/4 tsp lemon juice (bottled)
- 1/4 tsp chocolate bitters

Maybe a little high on maple syrup and a little low on chocolate
bitters? (Overshot on water and maple syrup?)

## 2020-05-30 - Lemonade

- 300 g water
- 50 g lemon juice (fresh-squeezed from 1 lemon)
- 25 g maple syrup, dark

On ice.

## 2020-07-09 - Mint lemonade

- 600 g water
- 150 g ice (8 cubes)
- 3 small sprigs mint (about 12 medium leaves)
- 100 g lemon juice (from bottle - Nellie & Joe's Key West Lemon Juice)
- 54 g dark maple syrup

...not very good! I think the bottled lemon juice tastes weird. Maybe
don't get this brand again.

## 2020-07-12 - Lemonade

- juice, pulp, and zest of two organic lemons (100 g)
- 600 g water
- 50 g dark maple syrup
- 150 g ice (9 cubes)

Lemon flavor was too strong, maybe in part because of the zest. Better
when diluted with maybe 75% more water.
