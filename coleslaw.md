# Coleslaw

Cabbage salads.

Tip: Keep dressing separate from salad until about to serve.

## 2020-12-27 - Simple, no sugar

This makes much more salad than dressing. Probably need 2-3x the
dressing described here.

Salad:

- 894 g green cabbage, shredded (about 10 cups)
- 386 g carrots, shredded
- 11 g scallion, finely chopped

Dressing:

- 176 g (~2/3 cup) mayonnaise (no sugar added)
- 1/4 tsp table salt
- 1/4 tsp black pepper, freshly ground to powder
- 2 Tbsp lemon juice, fresh squeezed (half a large lemon)

I made a second batch of dressing with more lemon juice (2.5 Tbsp) and
1 Tbsp of maple syrup, and I guess it tastes a *little* bit better,
but really the basic version is perfectly nice and doesn't need any
sweetening. From some experimentation with the first batch, adding
some honey might be a good direction to explore, but again that's not
really needed.
