# Jerky

## 2023-12-02 - Tamari beef jerky

- 954 g beef flank (minus 101 g fat trimmings)
- 444 g tamari

Sliced across the grain and marinated 24 hours.

Spread directly on baking sheets (needed 3-4) and dried in oven at
170°F on upper and top racks. Rotated every 45 minutes. Poured off
some liquid after 45 minutes for the first two trays; for the rest of
the meat, I first let it fully drain of marinade in a colander before
laying it out on the sheet.

Notes:

- Wow, way too much soy sauce. Next time maybe just try sprinkling
  salt over the slices laid out on the baking sheet?
- 3 hours was bordering on too dry. But the really dry pieces weren't
  bad! Just a little crunchy, which was fine.

## 2023-12-16 - Plain beef jerky

This is not a shelf-stable recipe -- there's not enough salt, sugar,
or other preservatives for that, and I left the meat a bit moist. It's
for the freezer only.

- 1-2 lbs beef flank
- ~1/2 tsp table salt

After trimming off fat, sliced across the grain and spread across 2
baking sheets. Sprinkled salt across trays. Dried in oven at 170°F on
upper and top racks, rotating trays periodically. Flipped meat pieces
once. Removed from oven when pieces were starting to dry on the top as
well; center of pieces was still light pink and a bit moist. Let cool,
then packaged in freezer.

Scraping the baking sheets afterwards produces a savory mix of dried
meat juices and salt. Probably good mixed into stew.

Notes:

- Flavor is similar to hamburger, especially the pieces that didn't
  really get any salt on them.
- Honestly, this is fine. Way simpler than marinating, and I don't
  really need the spices.

## 2024-02-03 - Plain beef jerky, slightly more salt

- About 1.5 lbs beef flank steak
- 1 tsp table salt

Trimmed off fat, sliced across grain into roughly 15×25×4mm slices,
and tiled onto a baking sheet. Sprinkled with salt. Dried in oven at
170°F on an upper rack until darkening, then flipped pieces and dried
until picnk was mostly gone. Let cool, then packaged in freezer.

Salt level seems right.
