# Celeriac curry

## 2021-12-11

This was an excuse to try finding a use for the chili/tomatillo
ferment I made (see "2021-10-05 - Chili and roasted tomatillo,
fermented" elsewhere in recipes) and use up some celeriac, onions, and
scallion whites. Inspired by a dish that used a Goan curry paste.

Ingredients:

- 2-3 Tbsp olive oil
- 1/2 tsp cumin seeds
- 0.7 g curry leaflets (maybe 70% of a compound leaf)
- 23 g frozen ginger, minced
- 27 g garlic, crushed
- 217 g celeriac, cubed 1 cm
- 190 g yellow onion, sliced 2-3 mm by ~5 mm
- 76 g scallion (mostly whites), chopped 2 mm
- 1/8 tsp turmeric powder
- 1/4 cup of "2021-10-05 - Chili and roasted tomatillo, fermented"
  (very salty and sour salsa sort of thing)
- 1 Tbsp tamarind sauce ("concentrated cooking tamarind" but it seemed
  pretty watery honestly)
- 1 cup water,cold
- 1 cup dried green lentils
- 1 cup water, hot
- 2 Tbsp shredded dried coconut (unsweetened)
- 1/2 cup cold water

Directions:

- Start lentils soaking in cold water
- Heat cumin seeds in olive oil over medium-high heat until they begin
  to sizzle
- Add curry leaves and ginger, reduce heat to medium, and fry until
  ginger is becoming dry or just starts to brown, about 2 minutes
- Add garlic, celeriac, onion, and scallion and sauté until slightly
  softened (about 7 minutes), stirring periodically
- Stir in turmeric
- Stir in chili/tomatillo salsa and tamarind paste
- Add soaking lentils and additional hot water and bring to boil, then
  cover and reduce to simmer. Add shredded coconut.
- Cook until celeriac and lentils are tender, about 1 hour. Add the
  additional 1/2 cup cold water partway.

Notes:

- Flavor is pretty mild, nothing amazing. A little sweet.
- Celeriac was tender long before lentils were; soak and cook lentils
  ahead next time.
