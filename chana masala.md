# Chana Masala

## Savory Indian Chick Peas, by latinmama

Ingredients:

- 1 onion, chopped
- 1 tomato, chopped
- 1 (1 inch) piece fresh ginger, peeled and chopped
- 4 cloves garlic, chopped, or more to taste
- 1 green chile pepper, seeded and chopped (optional)
- 3 tablespoons olive oil
- 2 bay leaves, or more to taste

- 1 teaspoon chili powder
- 1 teaspoon coriander powder
- 1 teaspoon garam masala
- 1/2 teaspoon turmeric powder
- 1 pinch salt to taste
- water as needed
- 1 (15 ounce) can chickpeas
- 1 teaspoon fresh cilantro leaves, for garnish, or more to taste

Directions:

- Grind onion, tomato, ginger, garlic, and chile pepper together in a
  food processor into a paste.
- Heat olive oil in a large skillet over medium heat. Fry bay leaves
  in hot oil until fragrant, about 30 seconds. Pour the paste into the
  skillet and cook until the oil begins to separate from the mixture
  and is golden brown in color, 2 to 3 minutes. Season the mixture
  with chili powder, coriander, powder, gram masala, turmeric, and
  salt; cook and stir until very hot, 2 to 3 minutes.
- Stir enough water into the mixture to get a thick gravy; bring to a
  boil and stir chickpeas into the gravy. Reduce heat to medium and
  cook until the chickpeas are heated through, 5 to 7 minutes. Garnish
  with cilantro.

## 2015-02-15

- 4 cups chickpeas
- 2 28 oz cans whole tomatoes
- 2 inches ginger
- 1.5 heads garlic
- most of a habanero pepper (starting at tip)

reserve liquid from tomatoes

- 5 bay leaves
- 1 tsp cayenne
- 1 tsp cumin
- 2.5 tsp garam masala
- 2.5 tsp coriander

## 2020-03-22

- 380 g dried chickpeas (2 cups)
- 57 g garlic (~13 cloves)
- 24 g ginger (a couple inches)
- 1 small jalapeño minus seeds and ribs

- 855 g home canned tomatoes
- 6 really old bay leaves
- 10 cardamom seeds
- 1 peppercorn
- 1/2 tsp ancho powder
- 3/4 tsp cumin powder
- 1/2 tsp turmeric
- 1/2 tsp sugar
- 1/16 tsp cinnamon powder

add chickpeas

- 3 tsp garam masala
- 1 Tbsp fenugreek leaves

- 1/4 tsp salt
- 1 tsp panela
- 1/2 tsp cumin

Turned out very poorly. Acidic and bitter.
