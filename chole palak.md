Chickpeas w/ Spinach

http://www.manjulaskitchen.com/2009/07/07/chole-palak-chickpeas-with-spinach/
Retrieved 2013-10-21

"Chickpeas and spinach is a great combination and a healthy, protein
rich dish. Chickpeas immersed with spicy spinach gravy. This dish can
be served with roti, naan or any other bread.

Recipe will serve 2 to 4."

# Ingredients

- 1 15oz can of chickpea (chole, garbanzo)
- 3 cups finely chopped spinach (palak)
- 2 medium tomatoes
- 1/2″piece ginger (adrak)
- 1 green chili
- 3 tablespoon oil
- 1/4 teaspoon asafetida (hing)
- 1 teaspoon cumin seed (jeera)
- 1 tablespoon coriander powder (dhania)
- 1/2 teaspoon turmeric (haldi)
- 1/2 teaspoon red pepper adjust to taste
- 1/2 teaspoon salt adjust to taste
- 1/2 teaspoon garam masala

# Method:

1. Drain the liquid out of the chickpeas and rince the chick peas well.
2. Blend the tomatoes, green chilies, and ginger to make a puree.
3. Heat the oil in a saucepan. Test the heat by adding one cumin seed
   to the oil; if seed cracks right away oil is ready.
4. Add the asafetida and cumin seeds.
5. After the cumin seeds crack, add the tomato puree, coriander
   powder, turmeric, red chili powder and cook for about 4 minutes on
   medium heat.
6. Tomato mixture will start leaving the oil and will reduce to about
   half in quantity.
7. Add spinach, and salt and one half cup of water and let it cook
   covered for 4 to 5 minutes on medium heat covered.
8. Add the chickpeas and mash them lightly with a spatula so they
   soften Note: add more water as needed to keep the gravy consistency
   to your liking, and let it cook on low heat for 7 to 8 minutes.
9. Add the garam masala and let it cook for another minute.

# Tim's attempts

## 2013-10-31

- 2 medium and 1 small greenish tomatoes (not very flavorful)
- 1 small jalapeño and 1/4 of a larger hot pepper
- Fresh-ground coriander
- 1 pea of asafoetida resin, crushed to rough powder
- Pressure-cooked chickpeas
    - Cooked 2 cups dried & washed with 7 cup water and 2-3 tbsp evoo,
      producing something like 5 cups cooked chickpeas and 4 cups
      broth to use another time
    - Bring to high heat, cook for 34 minutes, take off heat allow to
      release naturally
- 4 tbsp mustard oil

The cumin seeds didn't really crack, and the mustard oil was smoking
quite a bit, so I put them in when they started to sizzle strongly in
the oil (and crackle slightly). I left them in for about a minute at
high heat.

Turned out great! It was a little heavy on the coriander, but that
wasn't a problem.

## 2013-11-07 - Halloween edition

- evoo
- Pressure-cooked chickpeas, much as before (still 2 cups chickpeas to
  only 6 cups water, and less oil)
- 2 tsp cumin seeds
- 2 peas asafoetida, ground moderately fine
- ____ cups jack-o'-lantern puree
- 1 habanero + 1/2 a larger hot red pepper
- 1 inch ginger
- I was slow getting the spices in:
    - A little less than 2 tbsp coriander
    - 1 tsp turmeric (slightly heaping)
    - 1 tsp smoked paprika (instead of red chili powder, since the
      peppers are probably plenty hot)
- Split for:
    - 3 cups kale
    - 3-ish cups chard
- More like a tsp salt each
- Water as needed
- 1/2 tsp garam masala each

...well, that didn't taste anything like chole palak and it was pretty hot, but definitely edible.
