# Alex's gluten-free bread

## Base recipe

### Ingredients

- 2 Tbsp olive oil

Wet:

- 1.25 cups almond milk
- 1 "flax egg": 1 Tbsp ground flax, 3 Tbsp water

Dry:

- 2 cups Bob's Red Mill gluten-free all-purpose flour
- 1 tsp salt
- 1 tsp sugar
- 1 tsp cream of tartar
- 0.5 tsp baking soda
- 0.5 tsp xanthan gum

Mix-ins:

- 1 cup pecans, chopped (optional)
- 1 cup dried cranberries (optional)
- 1/2 cup chocolate chips (optional)

### Process

- Preheat oven to 375°F
- Mix "flax egg" and let sit in fridge
- Mix dry ingredients and mix-ins
- Coat 10" cast iron skillet with olive oil, preheat over burner (2
  minutes)
- Mix almond milk and flax egg in to form batter
- Before oil starts smoking, pour batter into skillet, and flatten
- Bake 30 minutes

### Variations

- Replace flax egg with real (chicken) egg
- Replace almond milk with water (may need longer bake)
- Add additional eggs, reducing water or almond milk by 0.25 cups per egg
    - At 3 eggs, starts to taste more like a popover or other
      egg-heavy pastry
- Bake in loaf pan instead of skillet
- Savory mix-ins: Olives and rosemary powder, with salt and rosemary
  powder dusted on top

## 2018-06-24 - pizza

- Base recipe, but with real egg
- Heated aluminum baking sheet lightly while spreading dough onto it
- Added sauce and other toppings after 12 minutes (maybe should have
  waited until 15 minutes?)
- Toppings: 1 cup of ground tomatoes, 12 oz of cheddar, 1 bell pepper,
  half a jar of olives, basil leaves, sprinkling of oregano

Took a long time to crisp up, although texture was good.

Consider using higher temperature next time, both for cooking dough
and after toppings are added -- but only suggesting this to reduce
cooking time.

## 2020-03-13 - Low FODMAPs

(Quoting from https://alexandra-thorn.dreamwidth.org/52994.html)

I finally figured out a recipe for an actually tasty gluten free
bread. One variant calls for eggs, the other is vegan, using flax eggs
instead.

I do all the mixing by hand.

Dry ingredients:

- 1 cup millet flour
- 1 cup tapioca starch (corn starch or potato starch also works, but I
  have friends who are allergic to either corn or potatoes)
- 1/2 cup quinoa flour
- 1/2 cup brown teff flour
- 1/4 cup sorghum flour
- 1/4 cup fresh ground flax meal
- 1 Tbs xanthan gum
- 1 1/2 teaspoons salt

Wet ingredients:

- 3 eggs (NOTE: for vegan variant can substitute 3 flax eggs; 1 flax
  egg = 1 Tbs flax + 3 Tbs water, mixed and chilled in the fridge for
  10 minutes or longer)
- 3 tablespoon olive oil
- 1 tablespoon maple syrup
- 1 teaspoon apple cider vinegar

Yeast-proofing ingredients:

- Just under 1 cup hot (110-115F) water (I pour 1 cup and then remove 1 Tbs)
- 3 Tbs maple syrup
- 2 1/2 teaspoons dry active yeast (instant yeast seems to work equally well)

Equipment:

- Baking sheet
- Candy thermometer
- Cooling rack

Steps:

1. Combine dry ingredients in a large bowl and mix
2. Combine wet ingredients in a smaller bowl and mix
3. Combine proofing ingredients and let proof for 7 minutes (set a timer)
4. After the 7-minute proof, mix all ingredients until uniform, shape
   into a loaf on an oiled baking sheet
5. Start oven preheating to 375F
6. Let loaf rise for 30 minutes in a warm place (I use the stove top
   so that it gets the oven heat from the vents; if your setup is
   different, you might need a longer rise)
7. Bake until center of loaf is 200F to 210F is great (about 55
   minutes)
8. Allow loaf to completely cool on cooling rack before slicing

Notes:

- Like most gluten free bread, this will come out pretty crumbly. It
  slices better with a chef's knife or other non-serrated kitchen
  knife than with a bread knife.
- I've had pretty good results coating the outside of the loaf with
  the residual contents of the wet ingredients bowl
- The dough is a bit wetter and stickier when real eggs are used than
  when flax eggs are used
- I once tried the real egg version without xanthan gum; the result
  was a liquid batter, which I poured into a loaf pan; the result was
  satisfactory, and was actually less crumbly, but it was not as sweet
  as usual, which suggests to me that the fermentation went
  further. NB: if you don't have xanthan gum on hand you *need* a loaf
  pan

(This recipe is modified from:
http://www.allergyfreealaska.com/2012/03/12/gluten-rice-free-multigrain-bread/)
