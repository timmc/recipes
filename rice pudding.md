# Rice pudding

## 2015-03-14

- 5 cups over-cooked jasmine rice
- 2 cups nonfat plain kefir
- 2 handfuls dark raisins
- 2 tsp vanilla
- 1/6 tsp ground nutmeg
- 1/6 tsp ground cinnamon
- 1/3 tsp ground cardamom
- 1/2 cup turbinado sugar
- Butter (cultured, unsalted)

Mix kefir, raisins, vanilla, nutmeg, cinnamon, cardamom, and sugar in
a bowl. Mix in rice until there are no clumps. Grease 2-quart baking
pan with butter, pour in mix. Bake at 375°F for 45 or so minutes until
center congeals a bit.

Result: Tastes... citrusy? I think I overdid the vanilla and nutmeg,
just like the squash bread I made that one time. Not a bad flavor,
just unexpected.
