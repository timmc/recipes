# Tomatillo and Lime Jam

Basic recipe, from <https://www.food.com/recipe/tomatillo-and-lime-jam-460331>:

- 1 lb tomatillo (husks removed, rinsed and roughly chopped)
- 1 1⁄2 cups sugar
- 1 cup water
- 4 tablespoons lime juice
- 1 lime rind, with inner membranes removed, chopped into 5 mm squares
- 1 pinch salt

Use organic limes (peels should be safer to eat) with thin skins (not
the thick, puffy-walled kind.)

- Place all of the ingredients into a saucepan set over medium heat.
- Bring them to a simmer and stir occasionally
- Cook until it has thickened and achieved a soft and loose jam
  consistency, about 35 to 40 minutes. Don't wait until it has
  thickened too much, because it thickens considerably as it cools.
- Once it has cooled down, pour it into a container, cover tightly and
  refrigerate.

Notes from experience with this recipe:

- When removing the membrane from the juiced lime to separate it from
  the rind, it's often easiest to peel from one of the equator
  corners, and produces a cleaner peel. But removing the bit around
  the stem/flower end and peeling from there works OK too.
- A medium, thin-skinned, fresh lime contains about 4 Tbsp of juice (45 g).

## 2016-09-27: 3/4 recipe

This is a 3/4 recipe because one of my tomatillos rotted before I
could use it. I also probably overcooked by about 10 minutes, but no
matter.

- 344 g tomatillos (.758 lb)
- 275 mL turbinado sugar
- 180 mL water
- pinch of salt
- 45 mL lime juice (one medium/small thin-skinned lime, very well juiced)
- 3/4 of that lime's peel, medium chopped

Cooked for almost an hour; 45 minutes probably would have been
sufficient. 30 minutes probably would not have been.

It looks and tastes like fig spread!

## 2017-03-18

5x recipe (relative to original from food.com). Could only use rinds of 3 limes because the other two were
not organically grown.

Tomatillos were perhaps a *bit* too roughly chopped, since they were
frozen and thawed and then were too squidgy to cut easily. I ended up
throwing the last 1/3 into the food processor.

- 2270 g tomatillos
- 287 mL lime juice (from 5 limes)
- 3 lime rinds, medium chopped
- 5 cups water
- 1616 g (7.5 cups) white cane sugar
- 5 pinches of salt (something like 40% of a 1/4 tsp)

I ended up splitting the batch and adding a jalapeño (or two?) at some
point to one half, which was a terrific idea.

Simmered for perhaps 1.5 hours, perhaps longer. Overcooked a bit,
particularly since the canning process involved more cooking.

Produced about 15 4-oz jars of jam in total.

## 2019-10-13

Two batches, each a 4.9x recipe of 2016-09-27.

want 2700 mL turbinado (maybe 1 kg?), 440 mL lime juice (10 limes) + skins

### Batch 1

- 1685 g tomatillos, yellow and green
- 1366 g raw cane sugar (~1350 mL)
- 882 g water
- 0.5 g salt (5 pinches)
- 244 g lime juice (4.5 limes; intended 220 g)
- Rind of 4.5 limes

Used food processor to chop and then blend tomatillos. Prepped other
ingredients, mixed all together, brought to a boil and then a simmer.

Simmered/boiled until bubbles seemed to be coming up with more
difficulty, about 70 minutes. An hour or two later, decided to simmer
it for an additional 45 minutes. Doesn't pass the freezer test, but
has now reached 220°F or possibly 221.

Very liquidy. :-/

Packed into 5 narrow-mouth pint jars and canned in a water bath
canner, boiling for at least 45 minutes. One jar failed to seal.

Pretty sure I screwed this up by running the tomatillos through the
food processor and letting them sit; my understanding is that this
liberated the pectinesterase and allowed it to degrade the pectin
before the pectinesterase was itself degraded by heat.

## 2022-09-25 - With shiso

1x, with red shiso leaves. May or may not have forgotten the pinch of salt.

- ~1 lb tomatillos (possibly more), diced
- 344 g (1.5 cups) white granulated sugar
- 1 cup water
- 3 red shiso leaves
- 54 g lime juice (juice of one lime)
- rind of one lime, chopped 5 mm (membrane removed)

Put all but tomatillos into pot, turned on heat, then diced tomatillos
into pot as it came up to temperature. Simmered/light-boiled until
222°F and firm in freezer. (Should have stopped sooner, but that's
OK.) Removed and ate shiso leaves.

Filled 3 small jam jars. Shiso taste not really noticeable.

## 2022-09-28 - With jalapeño

1x, with jalapeño. May or may not have forgotten the pinch of salt.

- 344 g (1.5 cups) white granulated sugar
- 115 g (~1 cup) water
- 31 g jalapeño (including seeds and ribs), minced
- 54 g lime juice (juice of one lime)
- rind of one lime, chopped 3-5 mm (membrane removed)
- 454 g tomatillos (possibly more), diced

Put all but tomatillos into pot, turned on heat, then diced tomatillos
into pot as it came up to temperature. Simmered/light-boiled for about
45 minutes until a bit placed on a cold plate in the freezer was still
soft but showed wrinkles when swiped sideways.

Filled three small jam jars. Mild heat.

## 2022-10-02 - With shiso, redux

1.9x recipe, with red shiso chiffonade.

- 654 g white granulated sugar
- 450 g water
- 103 g lime juice (~2 limes)
- rind of 1.9 limes, chopped 3-5 mm (membrane removed) -- 27 g
- 2 pinches salt
- 863 g tomatillos, diced
- 6 red shiso leaves, chiffonade (well, strips 2×20 mm)

Put all but tomatillos and shiso into pot, turned on heat, then diced
tomatillos into pot as it came up to
temperature. Simmered/light-boiled for about 45 minutes until a bit
placed on a cold plate in the freezer was still soft but showed some
surface tension when swiped sideways. Seemed to take longer than
previously, more than 45 minutes, but not sure (maybe due to larger
volume but same size or narrower pot).

Stirred in shiso shreds just before turning off fire.

I then canned this, although most of the jars didn't seal properly (I
did a sloppy job of it). Roughly 900 mL of jam, about 1120 g.

## 2023-10-22

- 225 g (1 cup) water
- 347 g (1.5 cups) sugar
- 4 tablespoons lime juice
- 1 lime rind, with inner membranes removed, chopped into 5 mm squares
- 1 pinch salt
- 454 g tomatillo (husks removed, rinsed and diced 5-10 mm)

Brought other ingredients up to simmer and diced tomatillos into the
mixture. Boiled until droplet on cold plate in freezer just started to
wrinkle. Water-bath canned for 40 minutes in 2x 3.5 oz jars and 2x 6
oz jars.

Came out fairly sloppy. Had to break the seals and re-boil until the
jam turned thicker and sloppy, and noticeably darker as well. Almost
looked "lumpy" in pot. A drop on a cold plate became sludgy. Texture
was great after water-bath canning a second time. A control container
that I did not put through the second water bath seemed to have the
same texture, which is helpful to know.

Result was, very roughly, 14 fl oz (about two of the 8-oz jars, which
seem to hold about 7 oz when filled for canning.)

I wonder if I can add less water in the future to reduce the boiling
time? (Possibly would allow me to reduce the sugar as well, if that
produces a smaller total volume, but need to research water activity
first.)

## 2023-10-26 - regular

2x of 2023-10-22, although about 5% more tomatillos.

- 450 g water
- 694 g sugar
- 8 tablespoons lime juice
- 2 lime rinds, with inner membranes removed, chopped into 5 mm squares
- 2 pinches salt
- 938 g tomatillo (husks removed, rinsed and roughly chopped)

Produced 1224 g of jam. 45 minute water bath canned, poured into four
8 oz jars and one 4 oz jar with tiny bit left over.

Total mass of ingredients would have been about 2200 g, so cooking
would have involved boiling off almost 980 g of water.

## 2023-10-27 - with jalapeño, less water

Same as 2023-10-26 but with jalapeños (same ratio as 2022-09-28) and water reduced by half.

- 225 g water
- 694 g sugar
- 8 tablespoons lime juice
- 2 lime rinds, with inner membranes removed, chopped into 5 mm squares
- 2 pinches salt
- 938 g tomatillo (husks removed, rinsed and roughly chopped)
- 68 g jalapeño, minced (including seeds and ribs)

Three 8 oz jars, three 4 oz jars. Had to re-seal several.
