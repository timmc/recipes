# Creamy mashed rutabaga

Based on http://www.thekitchn.com/autumn-recipe-creamy-smoky-whi-157478,
with almond milk, butter, cheddar, and olive oil standing in for
whole milk and cream cheese. (Recipe already called for butter.)

2 tbsp cultured butter
3 lb rutabaga, chopped 1 inch
4 med/small potatoes, chopped 1 inch
8 cloves garlic, roughly chopped
2 tsp salt
1.5 cup almond milk (sweetened, oh well)
--
.5 cup squash pulp, minced
.75 cup rough grated cheddar
pepper and more salt and butter

- Melt butter in 4qt pot over medium heat
- Add rutabaga and garlic, stir to coat
- Add salt and almond milk
- Bring to simmer
- Turn to low and cover
- (Some time in there, add squash pulp.)
- Cook for 30 minutes or until rutabaga is tender -- might take much
  longer.
- Add cheese
- Uncover and allow to cool for a few minutes
- Mash
- Add pepper and more salt and butter as desired (and recommended by doctor)
