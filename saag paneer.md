# Saag paneer

## Vegan recipe

http://www.connoisseurusveg.com/2014/09/vegan-saag-paneer.html

- Vegan Saag Paneer
- Author: Alissa
- Recipe type: Entree
- Prep time:  10 mins
- Cook time:  25 mins
- Total time:  35 mins
- Serves: 4

This vegan saag paneer has pan fried tofu cubes that stand in for
cheese and are smothered in spicy curried spinach and coconut milk
mixture.

### Ingredients

- 1 small onion, chopped
- 1 tbsp. grated fresh ginger
- 3 garlic cloves, minced
- 9 oz. fresh spinach
- 2 tbsp. vegetable oil
- 1 lb. extra firm tofu, drained and pressed at least 20 minutes, and
  cut into 1 inch cubes
- 1 tsp. turmeric
- 1 cup vegetable broth
- 1 tbsp. lemon juice
- 1 tsp. ground cumin
- 1 tsp. garam masala
- 1 tsp. Asian chili paste
- 2 tsp. sugar
- ½ tsp. salt
- ¼ cup coconut milk

### Instructions

- Place onion, spinach, garlic and ginger in food processor. Pulse,
  stopping to stir up everything a few times. You may need to do this
  in batches, depending on the size of your food processor bowl. Stop
  when spinach is very finely chopped, just shy of becoming blended.
- Heat oil in a large skillet over medium-high heat. Add tofu. Cook
  about 10 minutes, until tofu cubes begin to brown lightly and crisp
  up, tossing a few times to ensure even cooking on all sides.
- Sprinkle turmeric over tofu and toss a few more times to coat tofu
  cubes.
- Add spinach mixture, broth, lemon juice, cumin, garam masala, chili
  paste, sugar and salt. Stir a few times to mix everything up. Reduce
  heat to medium and allow to simmer about 10 minutes, until most of
  the liquid has cooked off.
- Add coconut milk, and cook another minute. Mixture should be thick
  and creamy.
- Serve with rice and naan.

## 2015-01-29

An attempt at the vegan recipe, except... 5x-ish recipe, way less tofu
(proportionally), too much lemon juice, kefir instead of coconut milk,
chili powder instead of chili paste.

### Tofu:

- olive oil
- 2 tofu blocks: firm, extra firm. Drained.

### Sauce:

- 3 16 oz bags of frozen spinach
- 15-ish garlic cloves coarsely chopped
- 5-6 tbsp grated ginger (a thick inch)
- 5 tsp cumin
- 5 tsp garam masala
- 3 tsp chili powder
- 2 cup water
- tiniest dash of turmeric (pretty much all on one cube, ah well)
- 2 tsp salt
- 1 lemon (too much!)
- 3 tsp sugar
- ~1 cup lactose free kefir

I forgot to squeeze the spinach until after I had added the ginger and
garlic, so I may have lost some ginger juice! This was my first time
cooking with spinach, so it got everywhere.

I should have stuck with half a lemon, and maybe less garam masala and
cumin. Before adding too much of those, I think I was getting pretty
close to something that tasted like what I get in restaurants.

## 2017-03-06

Two versions of the 2015-01-29 recipe, one with coconut milk and one
with lactose-free kefir.

I squeezed the spinach dry after thawing it, but then put that liquid
back in. A little was required for the food processor to run, and if
you're going to add water anyway... (Also, the original calls for
fresh spinach, which you wouldn't drain!)

I used "lite" coconut milk only because it needed to be used up. I
would use the regular stuff normally.

One batch each with these ingredients:

- 3 16 oz bags of frozen spinach, thawed
- 15-ish garlic cloves coarsely chopped (1 large head)
- 5.5 tbsp grated ginger (a couple inches)
- 5 tsp cumin
- 5 tsp garam masala
- 3 tsp chili powder *or* 3 tsp "hot paprika"
- 2-3 cups water
- a few sprinkles of turmeric
- 2 tsp salt
- 0.5 lemon
- 3 tsp sugar
- 1 cup lactose free kefir *or* 1 cup "lite" coconut milk

Process notes:

- Each batch required two runs of the food processor.
- I can't quite believe the "2 cups water" from the last version of
  this, especially since I drained the spinach for that one. Use as
  much as you need so that it doesn't burn!
- Accidentally put the kefir in before the simmering stage, but it
  turned out fine.

Notes for next time:

- Don't bother draining the spinach!
- Process the spinach more. Get closer to finely chopped, so that it
  juuust starts becoming blended.
- Use plenty of water.
