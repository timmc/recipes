# Dan Dan Noodles

## 2024-03-18 - Tempeh variation

Based on https://redhousespice.com/dan-dan-noodles/ with various
substitutions and omissions.

Makes enough toppings & sauce for 2-3 servings of noodles.

Toppings:

- 2 Tbsp toasted sesame oil
- 4 oz tempeh, crumbled (back to individual beans)
- 2 tsp tamari
- 2 tsp rice wine vinegar
- 9 g frozen ginger, minced
- 3/4 tsp yellow mustard seeds

Massaged tempeh with a splash each of the tamari and vinegar, then
fried in oil with ginger and mustard seeds until browned. Added rest
of tamari and vinegar about halfway through.

Sauce:

- 12 g garlic (2 cloves) minced fine
    - Not crushed! See notes.
- 22 g tahini
- 22 g water
- 2 Tbsp tamari
- 1 Tbsp rice wine vinegar
- Three-finger pinch of sugar
- 1 Tbsp chili oil (from the top of a jar of spicy chili crisp)
- 1 tsp chunky peanut butter

Mixed well.

Other toppings:

- Komatsuna greens
- Scallions

Notes:

- I made this another time and ended up with a pretty spicy sauce just
  due to the garlic; this might be because I crushed the garlic
  instead of mincing it, or possibly it was due to a difference in
  garlic varieties. If crushing garlic for this recipe, use 1/4 of the
  recommended amount and taste the sauce before adding more.
