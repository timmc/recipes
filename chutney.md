# Chutneys

# 2019-03-16: Onion chutney

Based on https://www.indianhealthyrecipes.com/onion-chutney-recipe/
with substitutions:

- No chana dal
- Jalapeños instead of red chilis
- Lemon juice instead of tamarind
- Brown sugar instead of jaggery

Ingredients:

- olive oil
- 1/4 cup peanuts
- 2 Tbsp urad dal
- 2 jalapeños, sliced into rounds
- 4 cloves garlic, sliced
- 2 cups cubed onion mix (red onion, shallot, yellow onion)
- 1 tsp cumin
- 11 curry leaflets
- 1 cup water
- 2 tsp brown sugar
- 2 tsp lemon juice
- 1 tsp salt

Directions:

- Heat olive oil in cast iron skillet to medium heat
- Fry peanuts, urad dal, and jalapeños until urad dal is lightly
  golden
- Add garlic and onions; fry 3-4 minutes (until onions are half-done)
- Add cumin and curry leaves
- Continue sauteing until onions are light golden or turn soft
- Allow to cool completely
- Blend with water, brown sugar, lemon juice, and salt

Notes:

- Very watery at first, but thickened up in fridge.
- Also very mild at first, but heat crept in as it sat.
