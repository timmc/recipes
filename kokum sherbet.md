# Kokum Sherbet

An Indian sherbet with kokum fruit (Garcinia indica).

This is a cool, sweet, spiced fruit drink, as opposed to American
sherbet, which is a frozen dessert.

## 2019-05-11

Based on https://www.vidhyashomecooking.com/dried-kokum-sharbat-kokum-sherbat/

Ingredients:

- 44 g dried black kokum fruit, including seeds (1/2 cup)
- 1 cup + 0.5 cup water
- 5 Tbsp white sugar
- 1 tsp black salt
- 1 tsp ground cumin
- 1/2 tsp ground black pepper
- 1/4 tsp salt

Steps:

- Soak kokum in 1 cup water for 24 hours
- Put through smoothie blender and pour into small pot
- Add sugar
- Rinse blender with 0.5 cup additional water, adding rinse water to pot
- Set on low heat and stir occasionally until sugar dissolves
- Stir in black salt, cumin, black pepper, and salt
- Continue cooking on low flame, below a simmer, for about 5 to 10
  more minutes
- Allow to cool, then strained (squeeze the pulp to fully extract the
  liquid)

This concentrate should be stored in the fridge. 2 Tbsp in a glass of
cold water is a pleasant summer drink.
