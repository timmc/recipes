# Gur Wale Shalgam

http://indian-recipe.net/indian-dinner/gur-wale-shalgam.html

- 500 grams (about 20 oz.) turnips peeled and cubed
- 4 small green chillies chopped
- salt to taste and a pinch of turmeric powder
- 2 tablespoon(s) ghee (clarified butter) / butter / oil
- 1 onion(s) finely chopped
- 1 tablespoon(s) grated ginger
- ½ teaspoon(s) red chilli powder
- ½ teaspoon(s) hot spice mix (garam masala) powder
- 1 teaspoon(s) coriander-cumin powder
- 2 teaspoon(s) jaggery or sugar
- 1 teaspoon(s) lime juice
- finely chopped fresh coriander to garnish

Combine turnips, half of the green chillies, salt and turmeric powder
in a heavy-based pan. Add enough water. Cover and cook on low / medium
heat for about 20 minutes or till the turnips have become very soft
and mushy. Mash the cooked turnips with the back of a ladle whilst
stirring on high heat so that it is completely mashed and dry.

Heat the clarified butter in a pan and fry the onion on medium heat
for about 3 minutes or till the onions are brown. Add the ginger and
the remaining green chillies. Fry briefly till the ginger turns
brown. Add the chili powder, garam masala, cumin, and coriander and mix well.

Add the turnips and mash them with the back of a ladle whilst stirring
on high heat so that it is completely mashed and dry. Add the jaggery
or sugar and keep stirring to mix well. Put off the heat and mix in
the lime juice.  Garnish with chopped fresh coriander.

# Actual attempts

- 1 rutabaga
- 1 habanero
- 1 tsp salt
- asafoetida instead of onion
- no turmeric
- no chili powder
- 1 tsp cumin seeds
- 1 tsp coriander seeds

Oh jesus that was too much hot pepper. I added some cheese to try to
cut the heat a bit, but without much luck. I had forgotten just how much heat a single habanero can add.
