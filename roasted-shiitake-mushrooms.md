# Roasted shiitake mushrooms

(As an ingredient.)

I have a love/hate relationship with shiitake mushrooms. On the one
hand, mushrooms smell gross when they're cooking, but really heavily
baked shiitakes that are leathery or even crispy taste wonderful.

## 2019-10-27

Along with brussels sprouts.

- 196 g clean shiitake mushrooms, caps and stems separated
- 120 g white wine (sauvignon blanc)
- 100 g extra virgin olive oil
- 1/2 tsp dried thyme
- 1/4 tsp iodized table salt

Made sauce with wine, evoo, thyme, and salt and poured it over the
caps and stems, making sure plenty ended up inside each cap (soaking
into the gills), and tossed to coat.

Baked at 350°F for 40 minutes with caps upright, along with 14 halved
brussels sprouts (200 g) tossed in excess sauce. Removed brussels
sprouts, flipped caps to expose gills, and baked for 20 more minutes.

Possibly a little overdone, with smaller stems verging from crispy
into charred.

## 2020-01-26

- 257 g clean shiitake mushrooms, caps and stems separated
    - These were a week old and a little dried out, so probably
      weighed a good bit more when fresh
- 120 g white wine (Yellow Tail sauvignon blanc)
- 100 g extra virgin olive oil
- 1/2 tsp dried thyme
- 1/4 tsp iodized table salt

Process:

- Whisked wine and oil until emulsified
    - Wine was cold from the fridge, so the result was viscous
- Stirred in thyme and salt
- Layered caps upside-down in bowl and drizzled sauce to coat the
  gills, adding more caps once the existing ones were coated.
    - Why? Because the gills are the hardest part to wet, but allow
      the best penetration of flavor into the mushroom.
- Added stems and poured in remaining sauce, tossed to coat
- Marinate 35 minutes
- Arrange on baking sheet with gills down and largest caps to outside,
  ands stems towards the center
- Bake at 350°F for about 20 minutes
    - Some signs of browning, especially on stems, but also brown foam
      or residue around a few caps
- Flip all caps gill side up, and try to flip stems as well
- Rotate tray and bake for another 20 minutes
- Check for any stems and caps that have reached a leathery (or even a
  hard) texture; these are done and should be pulled out early
- Remove when caps start to brown and blacken at edges; they'll just
  start to burn after this point. (About 15 more minutes.)

## 2020-02-16

Fresher, but smaller quantity. Long marination, during which almost
all marinade was soaked up. (Excess oil, though.)

- 228 g clean shiitake mushrooms, caps and (trimmed) stems separated
    - About a day old from the market, so still quite fresh
- 100 g white wine (Yellow Tail sauvignon blanc)
- 80 g extra virgin olive oil
- 1/2 tsp dried thyme
- 1/4 tsp iodized table salt

Process:

- Whisked wine and oil until emulsified
    - Wine was cold from the fridge, so the result was viscous
- Stirred in thyme and salt
- Layered caps upside-down in bowl and drizzled sauce to coat the
  gills, adding more caps once the existing ones were coated.
    - Why? Because the gills are the hardest part to wet, but allow
      the best penetration of flavor into the mushroom.
- Added stems and poured in remaining sauce, tossed to coat
- Marinated 3 hours
- Arranged on baking sheet with gills down and largest caps to outside,
  and stems towards the center
- Baked at 350°F for 20 minutes
    - Some signs of browning, especially on stems, but also brown foam
      or residue around a few caps
- Flipped all caps gill side up, and tried to flip stems as well
    - Pulled out any stems and caps that have reached a leathery (or
      even a hard) texture
- Rotated tray and baked for another 23 minutes
- Removed from heat when caps started to brown and blacken at edges;
  they'll just start to burn after this point. (About 15 more
  minutes.)

Good flavor. Could still probably reduce the quantity of oil, since so
much comes back out during baking.

## 2023-12-16

2020-02-16 scaled up, but with 60% reduction in olive oil.

- 341 g shiitake mushrooms, caps and (trimmed) stems separated
- 131 g red wine (Cabernet Sauvignon)
- 65 g extra virgin olive oil
- 2/3 tsp dried thyme
- 1/3 tsp salt

Worked well, flavor-wise. Still some difficulty in figuring out when
to pull which pieces off the tray.
