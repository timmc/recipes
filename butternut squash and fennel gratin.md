Butternut Squash and Fennel Gratin

Based on http://silk.com/recipes/butternut-squash-and-fennel-gratin

- olive oil
- 2 small bulbs fennel, cored, stalks thinly sliced (reserve fronds)
- A small pea's worth of asafoetida resin, ground (replacing 1 small
  onion, thinly sliced)
- 3 cloves garlic, minced
- pinch of nutmeg
- 1 cup unsweetened almond milk
- 1 tsp salt
- 1 med/small butternut squash, peeled, seeded, and thinly sliced
- Salt and freshly ground pepper to taste
- 4 oz grated pecorino romano cheese

1. Preheat oven to 350°F.
2. Butter or oil a 9x13-inch casserole pan.
3. Place a medium-sized skillet over medium-low heat. Add olive oil
   and cook fennel and ground asafoetida until fennel is translucent, about 10
   minutes.
4. Add garlic and cook an additional 2 minutes.
5. Stir in nutmeg, Silk and salt. Bring mixture to a boil and remove
   from heat.
6. Chop 3 Tbsp of the reserved fennel fronds. Spread half of the
   squash in the bottom of prepared casserole pan. Top with half the
   fennel/onion mixture and half the fennel fronds, then lightly salt
   and pepper. Repeat, ending with the fennel/onion mixture and
   remaining chopped fronds. Salt and pepper again.
7. Cover casserole with foil and place on a sheet pan in the middle of
   the preheated oven for 30 minutes.
8. Remove the foil, top with grated cheese and bake for an additional
   15 minutes.
9. Allow to cool for 10 minutes before eating.
