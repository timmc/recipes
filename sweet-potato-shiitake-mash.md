# Sweet Potato and Shiitake Mushroom Mash

Based on https://www.afamilyfeast.com/roasted-mashed-sweet-potatoes/
but with shiitake.

I made something like this in January 2019 but apparently forgot to
write it down.

## 2019-04-03

Ingredients:

- 1 large sweet potato (1020 g untrimmed, 940 g trimmed)
- 100 g fresh shiitake mushrooms
- 1/3 cup mix (50/50) of dry white wine (sauvignon blanc) and red wine
  that was turned part way to vinegar
- 1/3 cup evoo
- 1/2 tsp dry thyme (heaping)
- 1 tsp iodized table salt
- a few grinds of black pepper

Directions:

- Preheat oven to 350°F
- Dice sweet potato to 2 cm
- Separate shiitake stems from caps (and trim any substrate off of the
  stems). Don't throw away the stems, we're using them!
- Mix other ingredients as a marinade (wine, olive oil, thyme, salt,
  black pepper)
- Toss sweet potato and shiitake with marinade
- Spread on baking sheet and roast until sweet potato is tender and/or
  shiitake is starting to change texture/lose that gross
  cooking-mushroom smell. For me, this was 50 minutes with a stir at
  the half-way point.
- Allow to cool somewhat. Chop the shiitake stems small (more finely
  if they're harder/crispier) and chop the caps.
- Roughly chop the sweet potato and recombine it with the mushroom,
  creating almost a mashed consistency.

Notes:

- Really tasty. Family-approved, too.
- There was a lot of extra marinade, which seemed like a
  waste. Nevertheless, it was hard to get the shiitakes sufficiently
  coated in it.
    - Next time: Use 1/4 cup each of wine and oil (as original recipe
      called for) and soak the shiitakes in it *first* before pouring
      the marinade over onto the sweet potatoes and combining all.
- The result was pretty salty! I forgot that shiitake and salt are synergistic.
    - Next time: Use less salt than the original recipe, perhaps 1/2 tsp.

## 2019-04-07

- 1 large sweet potato (1094 g untrimmed, 984 g trimmed)
- 100 g fresh shiitake mushrooms
- Leftover marinade from last time, plus a little more (thrown
  together unscientifically)

Soaked and cooked shiitake first, followed by sweet potato. Minced
shiitake and mashed into sweet potato with spoon. Very tasty.
