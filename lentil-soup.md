# Lentil soup

Version 2023-06-01 is my go-to.

## 2019-02-11

A stripped-down version of https://www.geniuskitchen.com/recipe/egyptian-red-lentil-soup-94673

Ingredients:

- 1 cup brown lentils
- 4 cups water
- ~1 Tbsp olive oil
- 2 tsp ground cumin
- 1 tsp table salt
- 1/2 tsp turmeric powder
- 2 Tbsp fresh lemon juice and pulp

Directions:

- Bring water and lentils to boiling, then simmer 'til lentils are soft
- Temper cumin, salt, and turmeric in olive oil over medium heat (add
  to hot oil, stop when bubbling diminishes, and before spices start
  to burn, maybe 30 seconds)
- Add tempered spices to lentils
- Use immersion blender to process to smooth texture
- Add lemon juice and pulp, blend in

Notes:

- Hearty and delicious.
- A little earthy before the lemon juice, and after 2 Tbsp the
  brightness started to conflict with it. Maybe try just 1 Tbsp next
  time, and less turmeric. The original, full recipe would have had
  more balance, I suppose.
- Thickened up considerably after sitting several minutes.

## 2021-02-20

Another variation on https://www.geniuskitchen.com/recipe/egyptian-red-lentil-soup-94673
with these changes:

- brown instead of red lentils
- using a slow cooker
- slightly more onion
- olive oil instead of canola
- used a frozen lemon instead of fresh, and used some pulp to make up for juice
- no cilantro

This was also an experiment in using efficient electric cooking.

- 5 cups water
- 1 cup dried brown lentils
- 379 g onions, diced small (2.25 cups) -- 3 small yellow onions
- 267 g potatoes, diced small (2 cups) -- 1 large gold potato
- 33 g garlic cloves (9 small/medium cloves), peeled and left whole
- 1 Tbsp extra virgin olive oil
- 2 tsp ground cumin
- 1/2 tsp turmeric
- 1 tsp salt
- 2.5 Tbsp frozen lemon juice + .5 Tbsp pulp

- Added the water, lentils, onion, potato, and garlic to a slow
  cooker; set on high and brought to a light boil (reached after about
  3 hours).
- Lifted out the ceramic cookpot and placed into "fireless cooker" --
  cardboard box lined very thickly with towels on all sides, top, and
  bottom. Left overnight, 10 hours; temperature descended to 120°F.
- Tempered the spices: In a small saucepan, added the oil; warmed over
  low heat until the oil was hot but not smoking. Added in the cumin,
  turmeric, and salt; cooked and stirred constantly for a couple
  minutes until the cumin had stopped bubbling and was
  fragrant. (Smoked a bit -- may have been close to scorching the
  spices.)
- Set spice mixture aside for about 30 seconds to cool, then stirred
  into lentil mixture. (Spattered a bit -- cooling for longer would be
  safer.)
- Pureed, leaving a little bit of texture.
- Added in lemon juice and pulp; stirred to combine.
- Rewarmed as needed.

Notes:

- Overall, quite tasty.
- A bit watery for my tastes. Reduce water by one cup next time.
- Flavor was a bit on the earthy/turmeric side of things; slightly
  more sour from lemon would have been good. But I had to use a frozen
  lemon because I didn't have any fresh on hand, and it had lost some
  sour. Don't go too far in this direction, though.
- When I first opened the slow cooker, I thought the onions were
  undercooked based on the smell -- perhaps because they were never at
  a boil for more than a few minutes. But that might just be the smell
  of onions that are boiled without first having been sautéed. The
  smell did dissipate with time.

## 2023-06-01

Larger version of 2019-02-11, but with less citrus juice and turmeric,
and using lime juice instead of lemon. Add salt after tempered spices.

Ingredients:

- 2 cups brown lentils
- 8 cups water
- ~2 Tbsp olive oil
- 4 tsp ground cumin
- 1/2 tsp turmeric powder
- 2 tsp table salt
- 2 Tbsp fresh lime juice and pulp

Directions:

- Bring water and lentils to boiling, then simmer 'til lentils are soft
- Temper ground cumin and turmeric powder in olive oil over medium
  heat: Add spices to hot oil, stop when bubbling diminishes, and before
  spices start to burn, maybe 30 seconds.
- Add tempered spices and salt to lentils
- Use immersion blender to process to smooth texture
- Add lime juice and pulp, blend in

Notes:

- Really nice, even before I added the lime juice, at least according
  to the small sip I had. (Honestly, just lentils, salt, and cumin
  would probably be fine.)

## 2023-06-06 - Simplest soup/dal

- 2 cups brown lentils, rinsed
- 5 cups water
- 1 tsp salt

Soak lentils overnight, bring to boil, then simmer until tender. Stir in salt.

Sets up to be somewhere between a soup and a stew or something more
solid. Pleasant flavor, a bit earthy; good base for adding spices.
