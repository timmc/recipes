# Sprouted cereal

An attempt to make something along the lines of Food For Life's
wonderful Ezekiel 4:9 cereal.

## 2018-03-02

My first attempt. No wheat or barley, since I'm on a gluten-free trial
diet. Guessing at quantities based on nutritional information and
flavor estimation.

Ingredient sources listed for seeds I sprouted, since some sources are
not great for sprouting. (Germination rates are affected by seed
quality, age, and storage conditions.)

### Ingredients

- 200 g mung beans (Bob's Red Mill)
- 200 g millet (Bob's Red Mill, whole grain)
- 200 g quinoa (organic white from bulk bin at Harvest Co-op)
- 50 g almonds (whole, raw, probably kinda old)
- 25 g chia seed
- 1/4 cup water
- 1/2 tsp iodized table salt

When sprouting ingredients, let them sit in a sieve over a bowl on the
counter with no cover to allow good drainage and air circulation.

### Sprout slower ingredients (mung bean, millet, almonds)

- Rinse in cold water (Morning of March 2nd)
- Soak 10 hours in plenty of water
- Rinse and drain thoroughly
- Let sit ~12 hours in sieve
- Rinse and drain thoroughly (Morning of March 3rd)
- Let sit ~12 hours in sieve
- Rinse and drain thoroughly
- Let sit ~12 hours in sieve
- Rinse and drain thoroughly (Morning of March 4th)
- Let sit ~12 hours in sieve
- Rinse and drain thoroughly
- Let sit ~12 hours in sieve
- Now mung beans and millet have short tails (almonds have none)

### Sprout quinoa

- Rinse quinoa in sieve with high pressure cold water from sink
  sprayer, several minutes. (Morning of March 2nd)
- Soak 1 hour in plenty of water
- Rinse and drain thoroughly
- Let sit ~10 hours in sieve
- Rinse and drain thoroughly
- Let sit ~12 hours in sieve
- Rinse and drain thoroughly (Morning of March 3rd)
- Let sit ~12 hours in sieve
- Now has "tails" (shoots and roots) about 6 mm in length
- Refrigerate while finishing prep of other ingredients (36 more
  hours)

### Make cereal

- Process sprouted quinoa in food processor until doughy
- Process sprouted mung beans, millet, and almond with 1/4 cup water
  (or as needed) until doughy
- Grind chia seed in spice grinder until clumping
- Mix all, including salt
- Spread on baking tray
- Bake until crispy
    - Not recorded particularly well, but basically I wanted to not
      burn it, but also cook it slightly
    - Baked at 220°F for awhile and then at 350°F for awhile more
    - Sheared off tray in strips with a spatula, flipped, and baked
      for longer at 350°F
    - Various experiments with letting it sit/dry and putting it back
      in the oven at 180°F or 220°F
- Pending: How to break it up from crackers into crumbs (put it in a
  bag and step on it? pulse in food processor?)

### Outcome

Flavor is nice, a little more sour than Ezekiel 4:9 is. Could use a
tiny bit more salt, or not. Texture is identical.

Baked unevenly; outside is brown and crispy, inside is lightly
greenish brown/gray and does crumble but with a bit of give to it;
needs to bake/dry more fully. The sheet of material separated into two
sheets along the moister interior. Should have used a much thinner
layer on the baking sheet, maybe 3mm instead of 6mm, but I only had
one baking tray to use and wanted to do just a single batch.

Still trying to figure out the best way of making cereal out of it
rather than crisps.

**Important update**: This is causing really unpleasant flatulence. I
wonder if I didn't sprout the beans enough. -.-
