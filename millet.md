# Millet

Different ways of cooking millet.

## 2022-07-19 -- Fluffy attempt

- 117 g (1/2 cup) millet
- 227 g (1 cup) water

Rinse. Soak 10. Bring to boil, then simmer covered and undisturbed for
15 minutes. Let rest for another 10. Fluff gently with fork.

Probably safer to let sit for much longer, and cool before
fluffing. But worked well enough.

Made 302 g.
