Various gratins, many based on recipes from "How to Cook Everything
Vegetarian".

# 2014-04-13: Celeriac root, green bean, and navy bean

## Ingredients

- 2 celeriac root, sliced into bite-sized, 1/4 inch thick pieces
- 1.5 lb green beans, trimmed
- 1.5 cups dry navy beans
- 2 garlic cloves
- butter
- salt
- sweet paprika (2 tsp?)
- dried oregano (1 tsp)
- ground black pepper
- dried marjoram (1 tsp?)
- olive oil
- 1/2 cup grated parmesan cheese
- 1/2 cup breadcrumbs (gluten-free sourdough)

## Process

- Prepare celeriac:
    - Melt butter in a skillet
    - Cook celeriac over med/high heat until browning and becoming
      tender, 8-12 minutes. Add salt and pepper towards end.
- Prepare green beans:
    - Cook in water until crisp-tender, drain well
- Prepare navy beans:
    - Soak for up to 12 hours (as long as possible)
    - Pressure-cook with splash of olive oil (to reduce foaming) and
      marjoram
    - Drain, but reserve the water the bean were cooked in
- Rub baking pan with cut garlic clove and then butter
- Mix the celeriac, green beans, navy beans, paprika, and oregano in
  the pan
- Add enough bean cooking water to make the mix a bit soupy
- Top with breadcrumbs and grated cheese
- Bake at 400°F for about 30 minutes

# 2014-04-13: Butternut squash and potato

## Ingredients

- Butternut squash, sliced into bite-sized, 1/4 inch thick pieces
- 3 potatoes, sliced into bite-sized, 1/4 inch thick pieces
- 2 garlic cloves
- 3 eggs, lightly beaten
- 1 cup grated pecorino romano
- 4-5 cubic inches of gouda, sliced thin and broken into squares
- several cups soy milk
- butter
- salt
- ground black pepper

## Process

- Grease a baking pan with butter
- Toss the squash, potatoes, egg, and half the grated cheese. Add salt
  and pepper as desired.
- Put in tray and shake it down (try to get it denser)
- Add soy milk to about halfway
- Top with remaining grated and sliced cheese
- Bake at 400°F for 45 minutes

# 2021-04-29: Butternut squash and potato

Filled a 10" x 14" metal baking pan to ~1.5" deep.

- butter (for greasing pan)
- 820 g butternut squash, sliced into bite-sized, 1/8" thick pieces
  (bottom portion of a 2.3 kg squash)
- 836 g Yukon gold potatoes, sliced into bite-sized, 1/8" thick pieces
  (about 5 medium-large potatoes)
- 4 eggs (212 g out of shell)
- 370 g almond milk (1.5 cup)
- 392 g kefir (1.5 cup)
- 5.2 g table salt (~1 tsp)
- 1/4 tsp black pepper, finely ground
- 14 g garlic cloves (~4 medium), finely minced
- 70 g pecorino romano, grated (1 cup)
- 160 g sharp cheddar, grated (2 cups)

- Grease paking pan with butter
- Beat together the eggs, almond milk, kefir, salt, and black pepper
- Add alternating, compact layers of potato and squash. After every
  such pair, add:
    - Enough liquid to fill in
    - A sprinkling of minced garlic and pecorino romano. Aim to use up
      the listed amount by the time the layers are all built.
- Top with cheddar
- Bake at 400°F for 45 minutes

Notes:

- Got 3 pairs of layers in
- Used about 2/3 the garlic, and all of the pecorino romano, between
  the 2nd and 3rd pairs
- Next version of recipe should give instruction to divide garlic and
  pecorino romano after making first layer pair (into number of piles
  equal to number of remaining layer pairs.)
- Kefir seems to have curdled, leaving a lot of liquid. I ended up
  draining some after removing a corner square.
- Potatoes were still a bit firm after 45 minutes; I ended up putting
  it back in for maybe 20 more minutes. Still kind of firm.
- A bit of raw garlic flavor. Maybe sauté before adding next time.
- Overall, I think this recipe would be better done by adding the
  cheese late in the baking process so that the vegetables have
  sufficient time to cook before the cheese starts browning.
