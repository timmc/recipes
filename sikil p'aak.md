# Sikil P'aak

A Mayan appetizer or dip, to be served with tortilla chips. Various
recipes give widely varying textures, from runny to firm.

## 2015-11-30

Based on http://www.seriouseats.com/recipes/2014/04/sikil-paak-from-yucatan.html with these modifications:

- Half recipe
- Forgot to add salt (whoops!)
- Scallion greens instead of onion and chives, and not much at that
- No cilantro

### My recipe

- 1 habanero, charred, seeded, finely minced
- 1 med/large clove of garlic, peeled and charred
- 225 g fire-roasted tomatoes (Muir Glen, San Marzano Style)
- 18 g lime juice
- 9 g orange juice
- 4 g lemon juice

Puree.

- 122 g Blue Hubbard squash seeds, toasted and ground to a powder
- 6 g scallion greens, minced (1-2 tbsp)
- 59 g water

Mix all, adding water to reach desired texture. Dip may thicken over
5–10 minutes, necessitating more water.

### Thoughts

- Surprisingly mild given the habanero, even though it was deseeded
  and deveined.
- Very strong on the squash seed flavor and texture (grainy). Could
  see decreasing it relative to other ingredients, although citrus
  flavor should not be increased too much.
- Consider adding a pinch of cinnamon or canella next time -- it was
  mentioned in another recipe.
