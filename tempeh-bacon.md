# Tempeh bacon

## 2021-04-07

- 4 oz tempeh
- 20 drops liquid smoke (hickory)
- 2 tsp soy sauce (San-J)

- Slice tempeh to 3 mm and put in bowl, still in slab shape (so that
  marinade will wick)
- Mix liquid smoke with soy sauce and drizzle over slab (can be mixed
  in teaspoon -- drip liquid smoke into spoon first, then add soy
  sauce to fill spoon; repeat)
- Turn tempeh so that both sides get marinade
- Let sit 1 hour
- Heat a few Tbsp of a mix of toasted sesame oil and olive oil in a
  medium skillet over medium-high heat until shimmering
- Tile the tempeh slices into the skillet and shallow-fry, flipping
  once, until browned on both sides

Notes:

- I found this did not get the bacon-y flavor I've gotten in the
  past. I think the best flavor comes from having the soy sauce in
  direct exposure to the hot oil, which comes from instead drizzling
  it onto the tempeh once it is almost finished cooking.
- It's possible that the marinade may produce in a darker brown,
  meaning the tempeh was not as well-cooked as I thought. Some did
  have a slight raw-tempeh flavor.
- Still tasty, though.

## 2022-04-28

- 109 g tempeh
- 25 g soy sauce (Eden tamari)
- 13 drops (1/8 t) liquid smoke (colgin brand: has pecan smoke,
  molasses, vinegar, coloring)
- 1/8 t garlic powder (very small granules, not fully a powder)
- 1/4 t smoked paprika (Spanish paprika, oak smoked)

- Sliced tempeh ~3 mm and arranged in container, still in slab shape
  (to support wicking)
- Mixed remaining ingredients, then poured over slab
- Allowed to marinate about 20 hours, flipping tempeh a few times
- Fried in shallow oil

Notes:

- Nice enough
- A little bacon-like

## 2022-04-29: Baked

2022-04-28, doubled, baking instead of shallow-frying.

- 224 g tempeh
- 50 g soy sauce (Eden tamari)
- 1/4 t liquid smoke (colgin brand: has pecan smoke, molasses,
  vinegar, coloring)
- 1/4 t garlic powder (very small granules, not fully a powder)
- 1/2 t smoked paprika (Spanish paprika, oak smoked)

- Sliced tempeh ~3 mm and arranged in container, still in slab shape
  (to support wicking)
- Mixed remaining ingredients, then poured over slab
- Allowed to marinate about 22 hours, flipping tempeh a few times
- Arranged on baking sheet and baked at 350° for 36 minutes, turning twice.

Notes:

- Better at the second flip, when they were still slightly moist, than
  at the end, when they were dry, darkened, and tasting somehow like a
  combination of burnt and still a bit raw.
- Actually a little better after they had sat a bit, cooling and
  drying further. More like "tempeh chips".
- Next time, try just 25 minutes, perhaps flipping once.
