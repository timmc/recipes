# Peanut sauerkraut noodles

## 2020-02-29

- 1/2 lb (227 g) buckwheat spaghetti (100% buckwheat flour)
- 109 g peanut butter (chunky, just peanuts and salt)
- 55 g sauerkraut
- 45 g water
- 21 g ssamjang
- 14 g soy sauce

- Cook pasta, then drain and rinse in cold water (to improve texture)
- Mix remaining ingredients and stir in

I only needed about 2/3 of the sauce.

The ssamjang's flavor only comes through very lightly.
