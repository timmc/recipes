# Pesto

...of various sorts.

Currently preferred basil pesto recipe is 2024-08-07 (though that's
about as small as I can make it in a blender).

## 2016-06-11: Green garlic, walnut, sunflower, pumpkin

- Equal parts (approximately 0.5 cup each):
    - Raw, plain, shelled sunflower seeds
    - Raw, plain, shelled pumpkin seeds
    - Toasted walnut pieces
- Tops of 3 large green garlics, cut into 5mm or shorter segments
- 1 tsp salt
- 1/2 tsp black pepper
- A fair amount of olive oil (1-2 cups? add as needed for texture and
  ability to blend)
- Small bunch of arugula

Process (see notes before attempting:

- Process the nuts and seeds together in a food processor
- Add chopped green garlic, some olive oil, salt, black pepper
- Blend and continue adding olive oil and mixing with spatula until
  thick but still turns over somewhat
- Add arugula and blend until smooth

Notes:

- Next time consider adding all ingredients in at once, even tossed
  first, to prevent layer separation. (Or maybe that was because I
  used a tall squarish food processor first, instead of the
  cylindrical type I moved to partway through.)
- I chopped the green garlic tops into somewhat small pieces because
  they are fibrous and may otherwise clog the food processor (I've
  seen the fibers wrap over the cutting blade previously.)
- I processed the nuts and seeds without any of the other ingredients
  to make sure the sunflower seeds got ground up. I'm not sure if it
  was necessary, but the first processor I was using didn't mix well,
  and the nuts and seeds formed a sticky, viscous layer that did not
  come off the blades readily.
- The arugula was a late addition to add some water, since additional
  olive oil was not making things blend more easily. I should have
  added it early.

## 2017-07-01: Garlic scape pesto

Added water because I didn't have greens like arugula to add.

- 308 g garlic scapes
- 234 g (1.5 cup) sunflower seeds (raw, plain, shelled)
- 186 g (1.5 cup) pecan halves
- 1 tsp salt
- 1/2 tsp black pepper
- 130 mL water
- 60 mL lemon juice (from bottle)
- fair amount of olive oil (keep adding more!)

Tasty, but more like a garlic-flavored nut butter. :-)

## 2020-07-10 - Basil pesto

- 76 g basil leaves (Italian Genovese) -- about two double fistfuls of
  tops, and just about loosely fills 14 cup blender
- 8 g garlic (2 cloves), sliced 1 mm
- 1/2 tsp iodized table salt
- scant 1/4 tsp black pepper, ground to powder in mortar and pestle
- 76 g pecans
- 50 g pepitas
- 25 g sunflower seeds
- 110 g olive oil

Heavy on nuts and oil. Probably over-blended, too.

## 2020-08-15 - Basil pesto

- 100 g sunflower seeds
- 100 g pepitas
- 100 g pecans
- 21 g garlic cloves

- 309 g Italian basil leaves, relatively fresh (medium size bowl full!)
- 182 g olive oil
- 1 tsp salt

Nice flavor. Ended up more of a paste as a result of overblending but
I think that's kind of inevitable.

## 2020-10-30 - Basil pesto

About 10 full plants of basil, harvested as the cold weather came in.

- 430 g basil leaves (~6 qt)
- 241 g olive oil

Shredded in food processor, adding in leaves and oil as space became
available, just until all leaves were shredded.

Stored in fridge overnight so I could do the rest in the morning.

- 24 g garlic cloves
- 340 g pecans
- 1.5 tsp salt
- 11 g pepitas
- 45 g sunflower seeds
- 27 g olive oil

Blended garlic, seeds, and salt until well-chopped, then added
basil/oil mixture and added more oil and pecans until flavor was
right.

Great flavor. Made a little over a quart of pesto.

## 2021-06-26 - Basil, mortar and pestle

Approximate recipe.

- About 3 or 4 grinds of pepper

Grind fine in mortar.

- 1 small round garlic clove, about 15 mm across

Peel and add to mortar, mash into paste

- About 4 pecan halves

Break up into pieces, toast for a minute or two in thin skillet. Add
to mortar in thirds, mash into paste.

- 1 cup basil leaves, not wet, stems removed
- 2 drizzles of olive oil
- small pinch of salt

Add leaves to mortar in batches and grind, crush, and pound until
pieces are small.

(I ended up taking it all out onto a cutting board and mincing fine
with a chef's knife to get it finished.)

Add salt and oil periodically.

- About 6-8 grates of pecorino romano

Grind it in.

Notes:

- Tasty enough, although the garlic is quite strong
- Not worth the work

## 2021-07-25 - Basil, w/cheese, food processor

Started with 2020-10-30 at 86% scale, but added more pepitas and less
sunflower than intended, increased salt by ~50%, and added lemon juice
and cheese.

Basil leaves were left on the stem in a plastic bag overnight (to
retain moisture) until they could be used, unrefrigerated (to avoid
blackening). Separated leaves from stems, and rinsed by putting them
in a bowl, filling it with water, swishing around, and pulling leaves
out into new bowl. (The leaves float, the dirt sinks.)

The leaves were still a bit wet from rinsing, even after letting drain
for a bit, which likely affects the consistency of the pesto.

- 20 g garlic cloves
- 292 g pecan halves
- 21 g pepitas
- 29 g sunflower seeds
- 14 g (~2 tsp) table salt
- 370 g basil leaves
- 231 g olive oil
- 15 g lemon juice, fresh-squeezed
- 66 g pecorino romano cheese, fresh grated

Ran garlic, seeds, and salt through food processor until it started
clumping, then added basil leaves and olive oil in batches.

Then adjusted salt (to final value above) and added lemon juice and
cheese.

Notes:

- Very nice flavor
- I think I've generally been adding too little salt
- Skip the pecorino romano next time and see how it goes -- I added it
  because I wanted a saltier flavor, before realizing I could just add
  more salt, and I don't thing the cheese flavor itself is
  contributing much here. This could easily be vegan.

## 2021-08-10 - Basil, vegan, food processor

Intended 2021-07-25 but without cheese, at roughly 1/4 scale, and
rebalancing the seeds and nuts to a 1:1:1 ratio. Total seed/nut
portion includes the missing mass of cheese; salt slightly increased
to compensate as well. Accidentally added 1.5x as much olive oil and
3x as much lemon juice as intended.

Basil leaves were left on the stem in a plastic bag at room
temperature for three days until I could get around to using
them. Very few leaves spoiled. (Refrigerator would have blackened
them.) Rinsed by putting them in a bowl, filling it with water,
swishing around, and pulling leaves out into new bowl. (The leaves
float, the dirt sinks.) Leaves then pulled off of stems.

The leaves were still a bit wet from rinsing, even after letting drain
for a bit, which likely affects the consistency of the pesto.

- 5 g garlic cloves
- 34 g pecan halves
- 34 g pepitas
- 34 g sunflower seeds
- 1/2 tsp (~3 g) table salt
- 81 g basil leaves
- 90 g olive oil
- 12 g lemon juice, fresh-squeezed

Ran garlic, seeds, and salt through food processor until it started
clumping, then added basil leaves and olive oil in batches, and
finally lemon juice near the end.

Flavor was good, better than 2020-10-30. Ended up blending into a
fairly fine paste; that's probably why the extra olive oil hasn't been
seeping out.

## 2022-08-19 - Basil, vegan

Like 2021-08-10, but restoring to intended evoo and lemon juice
quantities to see how that goes. Pecans accidentally high. Increased
salt by 50%.

- 6 g garlic cloves
- 43 g pecan halves
- 33 g pepitas
- 34 g sunflower seeds
- 3/4 tsp table salt
- 116 g basil leaves (wet, stripped weight)
- 60 g olive oil
- 5 g lemon juice, fresh-squeezed

Very thick and fine paste, but good flavor.

## 2022-09-04 - Basil, vegan (and better measurements)

Like 2022-08-19 but restoring relative total quantity of seeds;
removed pecans and redistributed mass to other seeds; increased lemon
juice 50%.

This time I'm being careful to weigh the basil both before and after
rinsing, and to use fairly fresh basil so it hasn't dehydrated
appreciably before the first measurement. I'm not sure which
measurement other recipes used!

- 14 g garlic cloves
- 100 g pepitas
- 100 g sunflower seeds
- 1.5 tsp table salt
- 183 g fresh basil leaves (266 g after rinsing; 10 g water left in bowl after removing basil)
- 121 g olive oil
- 16 g lemon juice, fresh-squeezed

This recipe is on the salty side, appropriate for using in pasta
(without adding more salt). Might be more salty than people want on
bread.

## 2023-08-24 - Basil, vegan

Like 2022-09-04 but 0.45x and with fresh basil with dry leaves (not rinsed).

- 5 g garlic cloves
- 45 g pepitas
- 45 g sunflower seeds
- 5/8 tsp table salt
- 82 g fresh, unwashed basil leaves
- 55 g olive oil
- 7 g bottled lime juice

Garlic, pepitas, sunflower, and salt ground relatively small;
processed more with olive oil; then added the basil leaves; finally
added the lime juice.

This is about the smallest amount I could possibly make in the 14-cup
food processor. (2.5x this is about the most it can fit.)

Salty, like you'd want on pasta.

## 2024-08-07 - Basil and nettle, vegan, less pastey

1.75x of 2023-08-04 with fresh nettles (to pad things out) and about
half the oil stirred in rather than blended.

- 10 g garlic cloves
- 90 g pepitas
- 90 g sunflower seeds
- 1.1 tsp table salt
- 16 g fresh nettle leaves and tender stems
- 12 g bottled lime juice
- 126 g fresh, unwashed basil leaves
- 40 + 60 g olive oil

- Blended garlic, pepitas, sunflower, and salt until couscous-like
- Added nettle and lime juice and blended well
- Added basil leaves and 40% of olive oil and blended in short bursts
  with scraping until basil was in small pieces but not yet pastey
- Moved to bowl and stirred with remaining olive oil.

Notes:

- Better texture than previously; rather than a paste, it's more like
  the loose oily texture I'm used to with commercial pesto.
- This is a salty recipe like you'd want on pasta.
- I tried 2023-08-24 again with the same aim, but a different
  approach: 1) toasted the seeds; 2) chopped the basil to 5-10 mm; 3)
  doubled the olive oil. Blended as much as before. This actually came
  out much softer than usual, but unfortunately wasn't very spreadable
  on bread, as it was somewhat granular and not very cohesive and
  would fall apart off of the bread.
