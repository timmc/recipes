# Rumbledethumps

## 2019-02-03

- 1117 g fingerling potatoes
- 700 g white cabbage
- 4-5 scallion greens, fine-chopped
- 2 tsp butter
- 1/4 tsp salt
- 1/4 tsp black pepper
- olive oil
- 150 g cheddar, shredded

Directions:

- Boil potatoes until soft, maybe 20 minutes
- Slice cabbage into 1/4 inch strips
- Preheat oven to 230°C/450°F
- Boil cabbage no more than 5 minutes (can re-use potato water)
- Drain cabbage and let cool
- Chop cabbage
- Mash potatoes and cabbage with scallions, butter, salt, black pepper
- Pack into oiled casserole pan or dutch oven
- Spread cheese over top
- Bake, uncovered, until cheese begins browning in spots (not just at
  edge)

Notes:

Original recipe[1] called for more butter (quite a bit!) and milk. I
used little butter, and no milk. The top was good, but the middle was
lacking in fats.

We ended up sprinkling more shredded cheddar over what we had on our
plates, but consider using whole-milk kefir in the dish in the future,
and/or a good deal more butter.

[1] https://www.geniuskitchen.com/recipe/rumbledethumps-celtic-potato-cabbage-cheese-gratin-211815

## 2019-11-03

With shiitake mushrooms. Also, about double the cheese because I
accidentally mixed in the cheese intended as a topping, and had to
shred more.

- 1100 g red potatoes (before trimming)
- 812 g green cabbage (before trimming)
- 263 g shiitake mushrooms
- olive oil
- 40 g leek (green parts)
- 40 g scallion (green parts)
- 2 tsp butter
- 1/4 tsp salt
- 1/4 tsp black pepper
- 150 g + 115 g cheddar, shredded

Directions:

- Boil potatoes until soft, then set potato aside (reserve hot water)
- Slice cabbage into 1/4 inch strips
- Bring potato water to boiling again, add cabbage, and bring to boil again
    - I did this rather than "boil cabbage for no more than 5 minutes"
      because cabbage was cold from fridge
    - This reuses the potato water for efficiency
- Drain cabbage and let cool
- Separate shiitake stems and caps. Drizzle olive oil into shiitake
  caps (gill side) and toss with stems and a little more oil to coat;
  roast at 350°F for about 40 minutes until cripsing at edges of caps,
  turning once.
- Chop cabbage and shiitake
- Chop leek and scallion thinly and sauté in olive oil
- Add butter, salt, black pepper, and 150 g cheese
- Mash all together (minus second batch of shredded cheese)
- Pack into oiled dutch oven
- Sprinkle remaining 115 g shredded cheese on top
- Bake at 450°F, uncovered, until cheese begins to brown in spots (not
  just at edge), 35 minutes.

Very creamy.

## 2021-11-25

2019-02-03 but larger batch (about 1.9x), way more butter (closer to
source recipe's ratio), and a bunch of leek greens. Doubled salt since
I was skeptical of the quantity.

Did not quite double the cheese, but doubling was probably the wrong
approach anyhow, since there's the same surface area in a single
recipe.

This turns out to be much potatoes as will fit for boiling in a 5 qt
pot, and about as much as I'd like to fit in the 5 qt Dutch oven.

- 2170 g gold and white potatoes, chopped roughly (aim for 3-5 cm)
- 1290 g green cabbage, sliced ~6mm chunks
- 94 g leeks (green part), chopped ~3x10 mm
- 37 g scallions (green part), chopped ~5 mm
- ~2 Tbsp olive oil
- 6 Tbsp salted cultured butter
- 1/2 tsp  + 1/2 tsp salt
- 1/2 tsp freshly and finely ground black pepper
- 228 g cheddar, shredded

Directions:

- Boil potatoes (starting with cold water) until soft, about 30
  minutes, reserving hot cook water. Keep potatoes covered and warm
  until it is time to mash them.
- Sauté leek and scallion greens in plenty of olive oil until softened
  (not until soft)
- Turn off fire and melt/soften butter in skilled with leeks and
  scallions using remaining heat, then mix in 1/2 tsp salt and black pepper
- Preheat oven to 230°C/450°F
- Bring potato water to boil, add cabbage and additional water to
  cover, then bring to a boil for no more than 5 minutes -- aiming for
  bright green color
    - Should have added additional water first and brought to boil
      before adding cabbage. Also probably left them in too long. I
      took them out just a minute or two after the water started
      simmering and they had long since turned a bright green and
      softened, but by the time I drained and chopped them they had
      cooked more due to the residual heat, they were turning a duller
      green and a slight sulfurous note was creeping in.
- Drain cabbage and let cool
- Chop cabbage ~1/4"
- Mash potatoes and cabbage with leek/scallion mixture and remaining 1/2 tsp salt
- Pack into oiled casserole pan or dutch oven
- Spread cheese over top
- Bake, uncovered, until cheese begins browning in spots (not just at
  edge), about 1 hour
