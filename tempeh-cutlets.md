# Tempeh cutlets

## 2022-07-19

- 2 Tbsp soy sauce
- 1 Tbsp balsamic vinegar
- 20 drops liquid smoke (pecan)
- 1 lb uncooked tempeh (two 8 oz packages in standard "slab" shape)
- Toasted sesame oil
- Olive oil

Directions:

- Sliced tempeh thin, maybe 5 mm -- sliced each slab down the
  middle. Then cut across to make something more sandwich-sized.
- Mixed soy sauce, balsamic vinegar, and liquid smoke on a plate.
- Marinated slabs in stacks on plate, rotating the bottom piece to the
  top periodically, until marinade absorbed/dried.
- Pan-fried in about equal amounts olive and toasted sesame oil,
  maintaining enough oil to keep the bottom surface covered, until
  browned.

Kid-approved, especially in a sandwich with melted cheese.
