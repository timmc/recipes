# Kimchi fried rice (or millet)

2022-09-11 is my current preferred recipe.

## 2022-06-12

Based on Maangchi's recipe
<https://www.maangchi.com/recipe/kimchi-bokkeumbap> and some
variations from an acquaintance. For this one, no gochujang; white
kimchi; no gim; added carrots and scallion.

### Rice

More than needed for usual recipe:

- 282 g (1.5 cups) dried brown basmati rice
- 555 g (2.25 cups) water

Rinse. Soak 20. Bring to boil, then simmer covered and undisturbed for
40 minutes. Let rest another 10. Fluff with fork.

(Rinsing the rice only added 26 g of water.)

Made about 5 cups.

Using 190 g brown basmati rice and 374 g water made 471 g. So maybe
plan for a 2.5x increase in mass of rice after cooking.

### Ingredients

- 1 tsp olive oil
- 181 g (1 medium) carrot, slices (maybe 15x15x2 mm)
- 42 g (3 large) scallion leaves, chopped
- 163 g (1 cup) chopped kimchi
- 400 g (3 cups) day-old rice
- ¼ cup kimchi juice
- ¼ cup water
- 1 Tbsp toasted sesame oil
- 1 Tbsp roasted sesame seeds

### Directions

- Heat oil in skillet over medium-high heat.
- Add the carrot and scallion, and stir-fry for a few minutes.
- Add the kimchi and stir fry for 1 minute.
- Mix rice with kimchi juice and water, and massage into liquid so
  that all grains are separate. Add to skillet and stir for about 7
  minutes.
- Add sesame oil and remove from heat.
- Sprinkle with sesame seeds. Serve right away.

### Notes

- Basically good flavor, and satisfying.
- I liked the sourness of the kimchi but felt there was a missing
  flavor. A little tamari was nice, but wasn't really the direction I
  wanted to go either. Probably gim and egg would help add balance.
- Will use gochujang in a future version.

## 2022-06-13

2022-06-12, but:

- Smaller batch
- Sesame oil in at the beginning of the stir-fry
- Added a small amount of diced purple cabbage after carrots and scallions
- Dropped two duck eggs in with the cabbage
- Possibly less kimchi juice
- 1-2 Tbsp of fermented chili paste in just after the rice
- No sesame seeds
- A little bit of tamari

I liked this better, but Alex didn't like it as much. A little hot for
the kid.

Future variations:

- Millet instead of rice
- No egg, or less, or cooked separately (did it absorb and suppress flavors?)
- Make sure there's enough kimchi + juice to give saltiness
- Add greens (late? early?)

## 2022-06-14

2022-06-12 again, but with wild greens.

Ingredients:

- 1 tsp olive oil
- 186 g carrot, sliced thin (maybe 10x10x2 mm)
- 28 g scallion leaves, chopped
- 1 cup chopped kimchi
- 400 g day-old rice
- ¼ cup kimchi juice
- ¼ cup water
- 27 g wild greens, chopped
- 1 Tbsp toasted sesame oil
- 1 Tbsp roasted sesame seeds

Directions:

- Mix rice with kimchi juice and water and massage into liquid so that
  all grains are separate.
- Heat oil in skillet over medium-high heat.
- Add the carrot and scallion, and stir-fry for a few minutes.
- Add the kimchi and stir fry for 1 minute.
- Add rice to skillet and cook, stirring for about 7 minutes.
- When rice is halfway done, add chopped greens
- Add sesame oil and remove from heat.
- Sprinkle with sesame seeds. Serve right away.

Notes:

- I think this worked well!
- Can add a little more kimchi juice at end if sour taste is too mild

## 2022-07-26 - Seaweed and millet

### Millet

- 117 g (1/2 cup) millet
- 227 g (1 cup) water

Rinse. Soak 10. Bring to boil, then simmer covered and undisturbed for
15 minutes. Let rest for another 10. Fluff gently with fork.

(Probably safer to let sit for much longer, and cool before
fluffing. But worked well enough.)

Made 302 g.

### Ingredients

- 1 tsp olive oil
- 196 g carrot, sliced thin (maybe 10x10x2 mm)
- 30 g scallion leaves, chopped
- 1 duck egg, 2 chicken eggs
- ~40 g fermented seaweed salad (has kimchi juice in it)
- 302 g week-old fluffy millet
- 45 g of juice from a beet/carrot/ginger ferment
- 45 g water
- 1 Tbsp toasted sesame oil

### Directions

- Mix millet with ferment juice and water and massage into liquid so
  that all grains are separate.
- Heat oil in skillet over medium-high heat.
- Add the carrot and scallion, and stir-fry for a few minutes.
- Add the eggs and scramble until solid
- Add the seaweed salad and stir fry for a few minutes.
- Add millet to skillet and cook, stirring for about 7 minutes.
- Add sesame oil and remove from heat.

### Notes

- Prety good proof of concept.
- Flavor combination was fine, nothing amazing. Might benefit from
  some heat.
- I don't think the ferment juice added anything to this one; it was
  mild flavored and the seaweed salad was quite strong.

## 2022-09-11 - Kimchi-fried millet

Changes since 2022-06-14: Millet instead of rice, 1.4x carrot, 2x
kimchi juice (and no water), no sesame seeds, added soy sauce.

Wasn't tasting savory and strong enough during cooking, which is why I
added the soy sauce. This kimchi might have been too mild.

### Millet

- 155 g millet
- 300 g water

Rinse. Soak 10. Bring to boil, then simmer covered and undisturbed for
25 minutes. Let rest for another 20. Fluff gently with fork.

Made 435 g.

### Ingredients

- ~1 tsp olive oil
- 260 g carrot, sliced thin (maybe 10x10x2 mm)
- 25 g scallion leaves, chopped
- 1 cup kimchi, chopped (mild white)
- 435 g day-old fluffy millet
- 101 g (1/2 cup) kimchi juice
- 23 g various greens, chopped (evening primrose, horseradish, tomato)
- 1 Tbsp soy sauce
- 1 Tbsp toasted sesame oil

### Directions

- Mix millet with kimchi juice and massage into liquid so that
  all grains are separate.
- Heat oil in skillet over medium-high heat.
- Add the carrot and scallion, and stir-fry for a few minutes.
- Add the kimchi and stir fry for 1 minute.
- Add millet to skillet and cook, stirring for about 7 minutes.
- When millet is halfway done, add chopped greens
- When almost done, stir in soy sauce
- Add sesame oil and remove from heat.

### Notes

- Turned out well! Should use this again, maybe scale up. If using
  stronger kimchi, can probably skip the soy sauce.
- Adding considerably more scallions works out well. (3x for instance.)
- Goes well with cubed tempeh fried in olive and toasted sesame oil.
