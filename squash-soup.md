# Squash soup

## 2023-12-09

- 2.9 kg trimmed squash (1 kabocha and 1 koginut, halved and minus
  seeds -- 380 g of trimmings)
- ~2 Tbsp olive oil
- 51 g crushed garlic (8 cloves)
- 1 Tbsp butter
- 4 cups water
- 4 cups vegetable broth (from "Better Than Bouillon" vegetable base)
- 3/4 or 1 tsp ground cumin
- 3/4 or 1 tsp smoked paprika
- 2 tsp salt

- Roasted squash halves face-down on baking sheet at 475°F until very
  tender and juices caramelizing on baking sheet, around 40-50 minutes
- Sautéed garlic in olive oil for about 5 or so minutes, until tiny
  pieces starting to brown. Added butter and turned off heat.
- Sizzled cumin and smoked paprika in olive oil over low/medium heat
  for a minute or so
- Working in batches, combined squash (without skins), garlic mixture,
  tempered spices, water, broth, and salt in a food processor and pureed until
  smooth

I found that there was a burnt-garlic flavor even though I didn't let
the garlic visibly burn -- apparently I let it go too long
regardless. It cuts through the mild squash flavor and shows up in the
aftertaste in particular. I added cumin and paprika (lost track of
exact quantities) to try to mask it or pair with it as much as
possible, and that helped. By the time it had sat a day, the
unpleasant aftertaste had cleared up entirely. However, I had a cold
at the time, so no guarantees someone else would have tasted the same
thing.

After sitting for a day, the soup had a lovely butterscotch flavor.

Next time:

- Saute the garlic only for a minute or two (just enough to cut the
  heat) to avoid the surprise burnt-garlic flavor.
- Make less! This was rather a lot of soup, especially to puree in
  batches.

## 2024-09-29 - With shallot

About a 0.5x version of 2023-12-09 using a kabocha squash. Differences:

- Roasted a shallot with the squash halves, then diced it.
- Added the cumin and smoked paprika towards the end of sauteeing the
  garlic (when I already knew there was only a minute or so left, and
  the garlic wouldn't brown) and then added the butter and the sauteed
  shallot to finish cooking it.

Notes:

- This was the right amount to make -- about 2L of soup.
- The butterscotch flavor was present when the soup was freshly made,
  but receded somewhat by the next day, with a stronger savory
  component (no doubt from the shallot). Still tasted amazing, though.

## 2024-11-12 - Red kuri

About a 0.5x recipe of 2023-12-09 although with a red kuri squash.

- 1338 g red kuri squash (trimmed weight, with skin)
- ~1 Tbsp olive oil
- 25 g crushed garlic
- 1.5 tsp butter
- 2 cups water
- 2 cups vegetable broth (from "Better Than Bouillon" vegetable base)
- 1/2 tsp ground cumin
- 1/2 tsp smoked paprika
- 1 tsp salt

## 2024-11-27

Like 2023-12-09, but 0.75x, used only kabocha, a little less water,
cooked spices with garlic.

Divided, and made one half with vegetable broth and the other with a
hopefully equivalent amount of salted water.

- 2 kabocha squash, halved and seeds removed (2520 g unprepped; 2089 g
  after trimming; 1330 g cooked flesh)
- 1.5 Tbsp olive oil
- 2.5 tsp butter
- 38 g crushed garlic
- 3/4 tsp ground cumin
- 3/4 tsp smoked paprika
- Half #1:
    - 2.5 cups water
    - 7.371 g table salt
- Half #2:
    - 2.5 cups water
    - 1.5 tsp "Better Than Bouillon" vegetable base
    - 0.75 tsp salt

- Roasted squash halves face-down on baking sheet at 475°F until very
  tender and juices caramelizing on baking sheet, around 40-50 minutes
- Sautéed garlic in olive oil and butter on medium/low for about 5 or
  so minutes until starting to become translucent.
- Added cumin and smoked paprika and sizzled it for a minute or so,
  then turned off heat.
- Working in batches, combined squash (without skins), garlic/spice
  mixture, water, salt, and (in one half) broth in a food processor
  and pureed until smooth.

Notes:

- Salt-only seemed slightly less rich and is worth doing in the
  future. (Was accommodating allergies this time.)
- Made 2 kg of soup

## 2024-12-07 - Butternut

Like 2024-11-27 (broth version) with butternut. Ran out of cumin
powder, had to use some chili powder.

This butternut is actually a butternut cross, containing Seminole
Pumpkin and/or Chinese Tropical Pumpkin, courtesy of Edmund
Frost. (Not sure which of the unstabilized lines this was.)

- 2 butternut squash, halved and seeds removed (2443 g unprepped; 2159 g
  after trimming; 1250 g flesh after cooking, removing skin)
- 1.5 Tbsp olive oil
- 2.5 tsp butter
- 35-40 g crushed garlic
- ~1/4 tsp ground cumin
- ~1/2 tsp chili powder
- 3/4 tsp smoked paprika
- 5 cups water
- 3 tsp "Better Than Bouillon" vegetable base
- 1.5 tsp salt

- Roasted squash halves face-down on baking sheet at 475°F until very
  tender and juices caramelizing on baking sheet, around 40-50 minutes
- Sautéed garlic in olive oil and butter on medium/low for about 5 or
  so minutes until starting to become translucent.
- Added cumin, chili powder, and smoked paprika and sizzled it for a minute or so,
  then turned off heat.
- Combined squash (without skins), garlic/spice mixture, water, salt,
  and broth and pureed with an immersion blender until smooth.

Notes:

- Much more savory than sweet. Very pleasant.
- Flavor dramatically improved by the next day, as usual.
