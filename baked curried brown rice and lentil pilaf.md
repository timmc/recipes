# Baked Curried Brown Rice and Lentil Pilaf

- Yields ~ 1.5 - 2 quarts
- Measured time to oven: 26 min
- Measured bake time: 55 min
- Source: Ken Burris
  http://www.eatingwell.com/recipes/baked_curried_brown_rice_lentil_pilaf.html

Goes well with kefir or yogurt.

## Ingredients

- 1 tablespoon butter
- 1 cup brown basmati or brown jasmine rice
- 4.25 cups water
- 1 cup brown lentils
- 6 cloves garlic, peeled (I sometimes cut large cloves in half)
- 1 stick cinnamon or 1 tsp Ceylon cinnamon
- 8 slices (1/8-inch-thick) peeled fresh ginger
- 2 tbsp red curry paste
- 1 *drop* Dave's Insanity Sauce
- 1.25 teaspoon salt
- (opt) 4 scallions, trimmed and sliced

## Modifications from original

- Salt: was 1/2 tsp
- Red curry paste: was 1 tsp

## Ingredient lists of OTS ingredients

For allergy info and general reference, here are the ingredient lists
printed on the off-the-shelf ingredients I use:

- Thai Kitchen Red Curry Paste: Red chili pepper, garlic, lemongrass,
  galangal (Thai ginger), salt, shallot, spices, Kaffir lime
- Dave's Insanity Sauce: Red habanero, hot pepper extract, red chiles,
  tomato paste, salt, onions, cane vinegar, acetic acid, garlic,
  vegetable oil, xanthan gum, citric acid.

## Directions

1. Place rack in lower third of oven; preheat to 350 degrees.
2. Melt butter over medium-high heat in a large ovenproof Dutch oven;
   add rice and cook, stirring, until lightly toasted, about 1 1/2
   minutes. (If using curry powder, add it now and cook, stirring,
   until fragrant, about 15 seconds.)
3. Add water. Stir in lentils, garlic cloves, cinnamon stick, ginger,
   curry paste, if using, and salt; bring to a boil, stirring to
   dissolve the curry paste.
4. Cover the pot tightly with a lid or foil. Transfer to the oven and
   bake until the rice and lentils are tender and all the water is
   absorbed, 50 to 55 minutes.
5. Fluff with a fork, removing the cinnamon stick and ginger slices
   (or not.) Serve garnished with scallions.

## Variations

### First

- Original recipe
- Pretty bland according to Alex

### Second

- Doubled salt and curry paste from original, to 1 and 2 tsp respectively. (updated)
- Still kind of bland according to Alex
- I found the first bite bland, but successive bites quite tasty.

### 2011-02-02

- Differences:
    - > 1/2 to 3/4 tsp ground Vietnamese cinnamon (had no sticks)
    - > 1 tbsp (heaping) curry paste (updated)
    - + dollop Spicy Thai Chili (updated)
    - > roughly 1.5 tsp salt (updated)
    - > 8 slices ginger (updated)
    - > 6 medium cloves garlic (updated)
    - + 1 drop of Dave's Insanity Sauce (updated)
- Time to oven: 26 minutes
- Bake time: 50 minutes
- Had some mild but persistent heat.
- Worked well with grated Dubliner Irish cheddar from Whole Foods.
- Could use less cinnamon.

### 2011-02-06

- Made roughly double the last variation
    - Used 1 long cinnamon stick
    - Whole medium head of garlic, half a small head
    - Maybe 1/2 tbsp salt
- Bake time was only about 10-15 minutes longer
- With rice and butter in heating pot + 4 cups water on side, can prep
  all other ingredients during toasting and bringing to boil.
- Could have used more salt, perhaps 2.5 tsp. (updated)

### 2011-02-20

- Using a different brand of Basmati brown rice now (Sugat)
- 70 minutes in oven (ETA: this was probably a double recipe)

### 2011-07-09

- Double recipe
- replace 1 drop Dave's with extra dollop Spicy Thai Chili sauce
- replace 1/2 of brown basmati rice with whole grain red rice (Wehani)
- dutch oven had sticky oil residue to start with, probably oiled
  without heating during clening last time
- properly preheated, unlike last couple of times
- cooked 73 minutes, which was too long -- lentils were hard
    - attempted to remedy by adding 1/2 cup water and putting back in oven

### 2011-11-06

- Double recipe
- Used own ginger
- 2 heads garlic
- Brown basmati rice, 7 lb lentils
- 1 tsp preground Ceylon cinnamon
- Cooked 1.5 hr, two stirrings

A bit soft. Too much water + time?

More cinnamon would work.

### 2011-12-07

- double recipe
- 2 tsp Ceylon cinnamon
- no Spicy Thai Chili (missing!) -- also, turns out to have dried
  shrimp, so will replace with something else in the future
- therefore, doubled red curry paste (to 2 tbsp/per)
- Cooked 1.5 hr, opened once at 35 minutes

Good consistency (soft, but not overly gluey). Not quite as flavorful.

### 2011-12-30

- same as last time (double, etc.)
- 3 med-small heads garlic

### 2012-03-31

- 1.5 recipe
- Lundberg Wehani red rice
- ~2 cubic inches ginger
- 1 lg head garlic
- 09:15 PM -> 10:21 PM

Perhaps slightly underdone? Or maybe it is just right! Lentils still
hold shape, but all are soft... nope, the rice at the bottom is
undercooked.

### 2012-08-04

2x recipe

### 2012-10-06

- 2x recipe
- Vegan (1 tbsp evoo, 1 tbsp toasted sesame oil)
- Khao Deng Ruby Red rice
- green lentils
- only 1 drop insanity sauce (1/2 usual amount for 2x recipe)
- Checked after 1:15; very thin layer of soupiness at bottom. Back in oven
      for a bit.

### 2014-10-19

Alternative spice mixture with no garlic. Approximate ratios:

- 1 tsp lemongrass powder (possibly old)
- 1/3 tsp microplane-grated dried galangal
- 1 tsp makrut lime powder
- 1 tsp coriander powder
- 10 black peppercorns, ground fine
- 1 jalapeño, mashed fine
- 1-2 tsp toasted sesame oil
- 1 tsp lemon juice
- 1/4 tsp salt

### 2016-03-07

- Double recipe
- 4 strips of some sort of very thin, brittle cinnamon bark, labelled
  "canella" (bought at a Latin American grocery)
- no hot sauce

### 2016-03-17 - Quinoa

- 1 cup black quinoa instead of 1 cup rice
    - Toasted quinoa dry instead of in the butter, then added water
      and other ingredients (including the butter)
- 1 pea-sized lump of my habanero hot sauce/spread instead of 1 drop
  hot sauce
- Cooked only 40 minutes

Worked well!

### 2017-01-16

Double recipe, with evoo instead of butter and no hot
sauce. Interestingly, only took 50 minutes of oven time to finish.

### 2020-03-17 - Millet

- Substitutes 1 cup millet for 1 cup brown rice
- Garlic cloves (6) were roughly chopped
- 3 small jalapeños (minus stems, cores/ribs, and seeds) rather than
  hot sauce

Turned out great. Millet is a little overdone—somewhat gluey, although
the rice often turns out that way too.
