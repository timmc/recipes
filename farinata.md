# Farinata

From http://www.foodandwine.com/recipes/farinata

See 2017-02-04 for current best recipe.

## Original recipe

- 2 cups water
- 1.5 cups (7.5 oz) chickpea flour
- 1.5 tsp kosher salt
- 1/2 tsp rosemary leaves, minced
- 3 + 2 tbsp olive oil
- Optional: Black pepper

Instructions:

- Whisk chickpea flour into water. Let stand 2-12 hours.
- Preheat oven and skillet to 500°F
- Stir salt, rosemary, and 3 tbsp of olive oil into batter
- Pour 2 tbsp on skillet, swirl to coat
- Pour batter into skillet (quickly, before oil starts to smoke!)
- Bake 25-30 minutes until crisp around edges and starting to brown on top
- Serve fresh, optionally sprinkled with black pepper

## 2016-01-06

- 204 g (7.2 oz) chickpea + fava flour
- 2 cups water
- 1.5 tsp kosher salt
- 5 tbsp olive oil

## 2016-12-12

- 470 g (2 cups) water
- 200 g chickpea/fava flour
- 2 tsps chopped fresh rosemary
- 1 tsp coarse salt
- 0.5 tsp table salt
- 5 tbsp evoo

Let chickpea/water batter sit 5 hours. Skimmed foam.

Oven temperature was variable, 450°F–550°F.

Turned out very tasty, as usual.

## 2017-02-04 - Rosemary in early

A variation putting dried rosemary in the flour/water mixture early,
with the thought that the flavor would seep out during the soak.

- 470 g (2 cups) water
- 200 g chickpea flour
- 1.5 tsp minced dry rosemary
- 1.5 tsp fine-grained sea salt
- 5 tbsp evoo

Let chickpea/water/rosemary batter sit 9.5 hours. Skimmed some
foam. Rosemary turned the batter brown in spots.

Allowed oven to heat longer this time. I think it got the skillet
hotter, because this time it smoked coming out of the oven after
preheat, and the farinata was done after only 22 minutes.

Quite tasty, A bit saltier than necessary. Maybe use lower volume of
salt when using fine-grained stuff?

## 2017-02-10 - Rosemary in early, screwing up the salt

Again putting dried rosemary in the flour/water mixture early.

However, this time I forgot to add the salt to the batter! So I
sprinkled it on top after a few minutes, when I remembered, but I
don't know how this is going to turn out.

- 470 g (2 cups) water
- 200 g chickpea flour
- 1.5 tsp minced dry rosemary
- 0.5 tsp table salt
- 5 tbsp evoo

Let chickpea/water/rosemary batter sit 14 hours. Skimmed foam.

Allowed oven to properly preheat to 500°F again.

## 2017-02-18 - Rosemary early, less salt

Rosemary in early, but this time putting in less salt than on
2017-02-04 because that one turned out fairly salty. (Table salt and
other fine-grained salt probably packs more salt per teaspoon, and the
original recipe calls for the large-grained kosher salt.)

- 470 g (2 cups) water
- 200 g chickpea flour
- 1.5 tsp minced dry rosemary
- 1.0 tsp table salt
- 5 tbsp evoo

Let chickpea/water/rosemary batter sit 9 hours. Did not skim foam.

Allowed oven to preheat fully to 500°F again.

Cooked only 21 minutes, since the top had browned and the sides were
pulling away already, but it turned out wetter/doughier than
usual. OK, next time make sure to go at least to 25 minutes.

Not as salty as I'd like, but the wetness might have contributed to
that. Next time I use table salt, I'd like to use 1.25 tsp and see how
that goes.

## 2017-09-04

- 470 g water
- 206 g chickpea flour (nominally 200 g)
- 1.084 g mostly dried rosemary, minced (about 1 scant tsp)
- 1 tsp table salt
- 5 tbsp evoo

Let chickpea/water/rosemary batter sit 5 hours. Did not skim foam.

Allowed oven (and skillet, as usual) to preheat fully to 500°F. Stayed
at temp for at least 15 minutes.

Cooked only 18 minutes, since the top had browned and the sides were
pulling away already, but it turned out wetter/doughier than
usual. OK, next time *actually* make sure to go at least to 25
minutes.

Stuck to skillet somewhat in the center. I wonder if the skillet was
less seasoned there, or if it was so thoroughly heated that the oil
was pushed away to the edges when I poured in the batter.

Definitely delicious. Saltiness seemed fine.

Next time:

- Try 25 minutes minimum
- Try 1 tsp table salt
- Try pouring the batter in a circle around the edge instead of into
  the center.

## 2017-11-11

Doubled rosemary, poured in circle.

- 202 g chickpea flour
- 470 g water
- 2 tsp minced dry rosemary leaves
- 1 tsp salt
- 5 tbsp evoo

Let chickpea/water/rosemary batter sit 17.5 hours. Did not skim foam,
stirred once halfway through.

Allowed oven (and skillet, as usual) to preheat fully to 500°F. Stayed
at temp for only a couple minutes before adding batter.

Poured batter in a circle twice around the edge; this resulted in the
oil being pushed around in a circle and overlapping up onto the
batter.

Cooked 25 minutes; crust at very rim was blackening and crust was
browning overall; had pulled away from edge.

Farinata was somewhat stuck to skillet and I was worried it wouldn't
come off cleanly, but after a few seconds it seemed to unstick. There
was a 4 mm hole in the center where I could see the skillet. Perhaps
pouring in a circle allowed this hole to form? Not sure if it was a
problem.

Flavor was good.

Next time:

- Pour in a circle again, see if the hole forms
- Consider allowing farinata to sit in pan for a minute after removing
  from oven to see if it sticks less

## 2017-11-27

Rosemary powder and poured in a circle.

Same as last time, but used 1/4 tsp rosemary powder. Different oven
than usual, and different skillet.

Did not see hole form in middle, but also noticed that when batter was
poured in circle, it quickly cooked around the edges and the "tube"
that formed in the middle as a result was all twisted and folded by
the motion of the batter. Might require special circumstances to form.

If using powder again, suggest using a full tsp.

## 2019-05-12 - Fresh rosemary, early

- 200 g chickpea flour
- 470 g water
- 1/4 to 1/2 tsp minced fresh rosemary leaves
- 1 tsp salt
- 5 tbsp evoo

Soaked with rosemary 12 hours, baked 30 minutes, worked out great.

## 2022-05-29 - Rosemary salt, weighed

Using finely ground rosemary salt, weighed to match 1.25 tsp of table
salt. (Grinding the salt expands it.)

- 470 g (2 cups) water
- 200 g chickpea flour
- 7.98 g rosemary salt, fine-ground (a little over 1.75 tsp)
- 5 tbsp evoo

23 minutes with a well-preheated skillet. Turned out great!
