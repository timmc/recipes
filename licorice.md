# Licorice candy

## 2020-04-04: Tiny trial run

Extract:

- 3.3 g shredded licorice root (2 tsp)
- 15 g ethanol 95% v/v (190 proof Everclear)
- 15 g water

Let steep 17 days, the filter. Yielded 21 g extract.

- 21 g licorice root extract
- 39 g molasses ("Grandma's Molasses", 2 Tbsp)
- 45 g whole wheat flour (King Arthur, about 1/4 cup)

- Heated extract in small pot until bubbled, then until darkened and
  sticking to bottom somewhat
- Added molasses and brought to a boil, scraping the bottom to mix in
  the extract
- Stirred in wheat flour
- Mashed it together once it was cool enough to work by hand.

Failure! Gritty, grainy, medium brown color, only a hint (or
aftertaste) of licorice. I think the molasses was slightly burnt as
well?

Edible, and even tasty, but "almost, but not quite, entirely unlike
licorice".
