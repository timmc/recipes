Miscellany.

# Toasting walnuts

(Raw walnuts hurt my mouth.)

## 2016-06-11

250°F for 10-15 minutes in a convection toaster oven on broil.

- Small pieces plus walnut dust
- At 10 minutes a piece became edible
- At 13-14 minutes the pieces were starting to darken

# Aloo Paratha with Mustard Seed–Scented Eggplant

Adapted from
https://www.purplecarrot.com/vegan-recipes/aloo-paratha-with-mustard-seed-scented-eggplant
with major modifications and unsure changes to ratios (since the
original doesn't give some of the measurements!)

## 2016-08-14

Modifications:

- Added carrot to aloo paratha filling
- Added beet greens and bell pepper to eggplant-tomato stew; excluded
  onion
- No mint garnish, it tastes terrible on this dish
- No actual parathas! Served aloo on corn tortillas, topped with stew

Aloo paratha filling:

- Water, salt, olive oil
- 646 g potatoes, chopped
- 266 g carrots, chopped (8 med/small, cut 2-3 cm)
- 24 mL lemon juice
- 16 g ginger, minced (1.5–2 cm chunk)
- 5 mL curry powder
- 20–25 mL frozen cilantro

- Bring salted water to boil in medium pot, about 2L
- Add potatoes and carrots
- Keep covered at steady bubble
- Drain when fork tender (10-15 minutes)
- Add lemon juice
- Mash

- Saute minced ginger at medium heat, sprinkled with salt, until
  fragrant (1-2 minutes)
- Remove from heat and mix in curry powder
- Mix into potato/carrot mash
- Mix in cilantro

Stew:

- Olive oil, salt
- 410 g eggplant, 1-2 cm slices
- 183 g beet greens, medium chopped
- 19 g garlic, medium chopped (5 small cloves)
- 241 g bell peppers, medium chopped (2 med yellow/white peppers)
- 580 g tomatoes (about 7 plum tomatoes)
- 20 mL mustard seeds

- Medium-salt and saute eggplant until browning and tender (5–10 minutes)
- Add garlic and beet greens, sauté until beet stems become tender
- Stir in bell pepper
- Add diced tomatoes and mustard seed, bring to boil
- Cook until starts to thicken (10–15 minutes)

Results: It was OK. Needed salt for sure. Maybe it was my
modifications, but this just didn't seem that flavorful a dish.

# 2020-04-18 - Sauerkraut millet bowl

Millet with mix-ins. Kind of adjacent to sushi flavors, what with the
starchy grains, soy sauce, mustard/cabbage, and acid.

Millet:

- 1/4 cup millet
- 1/2 cup water

Bring millet and water to boil, then cover and simmer on lowest heat
for about 20 or 30 minutes, until water is gone.

Tempeh:

- Half package of tempeh
- Dash of soy sauce
- Several drops of liquid smoke (optional)
- 1-2 tsp toasted sesame oil

Cut tempeh into 5 mm chunks and marinate in soy sauce and liquid
smoke. Fry in sesame oil, stirring occasionally, until browned on
several sides.

Other mixins:

- Several forkfuls of sauerkraut
- Squirt of brown mustard
- Dash of lemon juice
- Several shakes of nettle seed gomasio (nettle seeds, sesame seeds,
  garlic, salt) -- mostly tastes like garlic salt

Mix all.

# 2020-06-14 - Wild greens millet bowl

Millet with mix-ins.

Millet:

- 1/4 cup millet
- 1/2 cup water

Bring millet and water to boil, then cover and simmer on lowest heat
for about 20, until water is gone.

Mix in:

- Handful of wild greens, chopped: Chickweed (Stellaria media),
  dandelion, broad-leaf and narrow-leaf plantain (Plantago major,
  Plantago lanceolata), young leaves of broad-leaf dock (Rumex
  obtusifolius), violet
- 2-3 Tbsp of chopped pecans
- 1-2 Tbsp of sunflower seeds
- 1-2 Tbsp of pepitas
- 1/4 tsp salt, maybe?
- Generous drizzle of olive oil

# Zucchini ribbons

Based on
https://food52.com/recipes/5505-summer-squash-ribbons-with-thai-basil-and-peanuts
but with a number of substitutions; that one is in turn reportedly
based on Chayote Squash Salad with Peanuts and Lime from Sally
Schneider's A New Way to Cook.

- 1 medium zucchini, shaved into ribbons lengthwise, excluding core
  (538 g zucchini produced 391 g ribbons)
- 2.5 Tbsp Italian basil chiffonade

Sauce:

- 2 large scallion leaves (11 g)
- 2 Tbsp olive oil
- 2 Tbsp key west lemon juice
- 1/2 tsp sugar
- 1/4 tsp salt
- 1/8 tsp ground black pepper (6 grinds), ground to powder in mortar and pestle
- 2 tsp chunky salted peanut butter

Whisk together sauce and mix gently into squash. Let stand 5 minutes,
then gently mix in basil.

Notes:

- Pleasant. Surprisingly noodle-like. Did not taste like zucchini
  usually tastes to me, although a little of that "squashy" flavor
  came through.
- The salt pulled a good deal of water out of the zucchini. Could
  likely reduce the amount of lemon juice to make the sauce less
  watery and without much affecting the flavor.
    - After refrigeration, either some of the water was pulled back in
      (maybe along with salt) or the oil thickened enough to make it
      less liquidy
- I sliced up the left over zucchini core into disks and sautéed them
  with olive oil and tamari until they were meltingly soft.
