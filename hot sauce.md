# Hot sauce

## 2014-04-12

I had some frozen habaneros to use up.

### Ingredients

- 18 habaneros, frozen, washed, de-stemmed
- 6 cloves garlic, peeled
- 2 med/large carrots, roughly chopped
- olive oil
- 1 lime, frozen
- 2 tsp salt
- ~1/2 cup Bragg's apple cider vinegar (it's non-pasteurized)
- ~1/4 cup red wine vinegar

### Process

- Look up safety info for working with hot peppers in
  quantity. *Strongly* consider wearing gloves, or your hands will be
  on fire all night. Glasses are a must.
- Sauté carrots and garlic in olive oil on medium-high heat for 8
  minutes
- Cut off blackened skin from garlic (oops!)
- Combine carrots, garlic, and habaneros in food processor
- Add juice of lime
- Add salt
- Puree until fairly smooth, adding vinegar for consistency. The
  carrot will prevent it from becoming entirely smooth.
- Cook on low to medium heat, stirring *carefully*. Avoid boiling,
  since it can cause splashes. Add red wine vinegar for consistency.

Ended up as more of a paste. Intensely hot. Maybe next time remove the
seeds so the habanero flavor can be stronger.

## 2019-08-04

Again using up frozen habaneros, now that the previous batch was all
gone. (Note to self: Not gluten-free, and may have other allergens;
carrots and garlic sautéed in cast iron that had not been cleaned
after making grilled cheese.)

- 143 g carrots, chopped into 7 mm thick discs
- 26 g garlic cloves, peeled whole
- 38 frozen habaneros
- 13 g scallion greens, minced
- juice of one lime (3.75 Tbsp)
- 3 Tbsp Bragg's apple cider vinegar
- 3 Tbsp water
- 2 tsp table salt
- 1 Tbsp olive oil + a little for sautéing

Wear gloves if you don't want your hands to suddenly feel as if
they've caught fire several hours later. (Washing your hands will be
too late.) Wear glasses or safety goggles.

Saute carrots and garlic in olive oil over medium heat for 14
minutes. Carefully pour in 1/2 cup of water and cook until the water
boils off.

Remove stems, seeds, and ribs from habaneros. Lay the skins flat on a
baking sheet, and bake at 400°F for 14 minutes. Remove and mince.

Combine carrots, garlic, scallions, pepper skins, lime juice, vinegar,
salt, water, and olive oil in a small bowl. Use an immersion blender
to puree it, then regret your choices as small bits of potent hot
sauce go everywhere.

Notes:

- Makes an orange/yellow paste
- Hot, but not as blastingly hot as the previous one, presumably
  because I removed the seeds and ribs
- Good flavor

## 2021-10-05 - Chili and roasted tomatillo, fermented

I did this recipe in stages because I wasn't sure what I was going to
make at first. Theory is that roasted tomatillos will retain their
pectin and will help thicken the sauce.

- 423 g diced chilies and other peppers: Jalapeño, anaheim, poblano;
  one ají dulce and one large sweet Italian. (Weight without stems,
  ribs, or seeds. Diced 5 mm.)
- 17 g iodized table salt (4%)

- Evening of 10/5: Sprinkled peppers with salt and mixed a few times;
  after an hour or so a brine had formed so I packed it into a quart
  jar.

2021-10-07 evening:

- 750 g tomatillos, mix of green-unripe and yellow-ripe; husked and washed
- 46 g scallion greens
- 40 g garlic cloves
- 6 g salt to mix in (bringing mixture to 2%)

- Roasted tomatillos 400°F top rack until all had become somewhat
  translucent and dimmed in vibrancy towards a dull yellow-green
  color, and some had developed a sour caramel where they oozed (about
  17 minutes). 648 g after roasting.

Blended all (except for cap salt) into slurry and divided evenly into
two jars.

- 18 g + 18 g salt for salt cap (bringing total to 5%)

Wiped sides of each jar clean above the fill line, then carefully
topped each with 18 g salt. Lightly closed jars and set in place where
they would not be jostled.

Fermentation has been slow to start; by 10/17 bubbles are fairly
obvious and liquid/mash separation starting in bottom cm of jar.

2021-12-11: Opened a jar and tasted some. Very strongly salty and
sour. Zero heat. Blended further, which didn't really improve it, and
may have added a bitter note.

## 2021-11-03 - Half-roasted with carrot and garlic

All peppers had stems, ribs, and seeds removed, leaving just the
flesh. (Serranos are hot enough as it is without the seeds...)

Treated roasted pepper as "vegetable" for purposes of calculating salt
content.

- 44 green serranos (330 g fresh and whole; roasted flesh ended up
  being 62 g)
- 200 g flesh of assorted other chilies -- poblano, probably some
  Hungarian wax, maybe Anaheim, others
- 72 g flesh of some sweet red pepper
- 109 g garlic cloves
- 188 g orange carrot
- 7 g + 25 g iodized table salt (mix-in + salt cap)

- Roasted serranos whole under broiler until blackened (maybe 20-30
  minutes?) and then placed in covered bowl to steam themselves for
  about 20 minutes.
- Put fresh peppers and garlic through food processor until ground.
- Grated carrot fine and added to food processor, blended further.
- Peeled blistered skin off of serranos and removed seeds and ribs as
  best I could; reserved to dry for another recipe. Removed and
  discarded stems. (Remaining flesh was 62 g; other materials a bit
  over 57 g.)
- Put roasted serrano skins in food processor with first 7 g of salt,
  blended until skin chunks disintegrated.
- Divided into two 1-pint jars, tapped the contents level, wiped down
  the glass above the mash, and capped each with 12.5 g salt. Loosely
  sealed and left to ferment.

Notes:

- Pretty decent. Not a sauce, something more like a grainy salsa. Some
  heat. Good flavor, a little bitter.

## 2022-09-18 - With carrot and garlic, moderately fermented, strained

- 569 g various chilis, roughly chopped (no stems, but left seeds and ribs in)
- 22 g garlic (2 cloves), cut in half crosswise
- 70 g carrot, diced (6-8 mm)
- 916 g water (to cover, and create 2-3 cm headroom)
- 23 g (~4 tsp) apple cider vinegar
- 32 g iodized table salt (2% of wet weight)

- Combined all and let sit for 1 week
- Drained and reserved brine, resulting in 962 g solids and 622 g brine
- Pureed all solids with 330 g of brine

Did not filter; would have been too liquidy. Filled a 1 qt Ball jar
almost to the brim.

Flavor is fine, nothing special. Reasonably hot.

## 2022-11-01 - With more garlic

- 1433 g various chile peppers, roughly chopped (no stems, but left seeds and ribs in)
- 255 g garlic
- 255 g carrot
- 3 Tbsp (~45 g) apple cider vinegar (Bragg's -- raw)
- 1454 g water (to cover, and with 1 cm headroom)
- 176 g iodized table salt (5.1% of wet weight)

Packed into 5 jars -- tight at first, but became loose as salt
softened the ingredients. Next time, salt the vegetables by weight,
wilt them, and then pack and cover with brine?

- 2022-11-13: Opened smallest jar containing 170 g solids and 261 g
  brine. Combined all solids with 50 g brine. Pureed. Pretty nice,
  although *quite* salty.
- 2022-11-22: Opened two of the four regular-sized jars. 792 g solids, pureed with
  101 g brine.
    - Blueberry hot sauce: Combined 493 g of that puree with a few
      Tbsp brown sugar and 153 g frozen blueberries. Pureed, heated to
      145°F to stop fermentation. Really nice!
    - 400 g puree + 45 g more brine (so about 355 g solids + 90 g
      brine total) in another bottle, pureed. Alex liked this one.
- 2022-11-28: Opened remaining two jars. One had a tiny bit of kahm
  yeast and smelled like it too.
    - Jar without yeast: 429 g solids (leaving the seeds behind,
      mostly) + 100 g brine (same ratio as previously), pureed.
    - Jar with slight yeast smell: Drained, rinsed solids. 314 g
      solids, 85 g brine (from other jar).

## 2024-09-12 - Fermented with carrot and garlic

Similar to 2022-11-01, but minimizing brine (by getting as much as
possible out of the peppers themselves).

- 1471 g various chile peppers, roughly chopped (no stems, but left seeds and ribs in)
    - Habanero, jalapeño, others
- 260 g carrot, shredded
- 260 g garlic, crushed
- 3 Tbsp (~45 g) apple cider vinegar
- 104 g iodized table salt (5.1% of weight other ingredients)

Tossed chiles and carrots with 2% their weight in salt overnight, then
added rest of ingredients the next day (not on purpose, just how it
worked out.) Packed into several jars with weights. Added a bit of
5% brine to last jar just to ensure there was the right amount of
headroom.

- Small jar: 217 g solids, 131 g brine. Pureed 217 g solids with 75 g
  brine.
- Mid jar 1: 597 g solids, 214 g brine. Pureed these together on
  2024-10-30.
