# Wheat Bread

Various attempts at making bread.



Current favored recipes:

- 2019-06-16 for 100% whole wheat sandwich loaf, regular sourdough
- 2019-06-14 for 100% whole wheat sandwich loaf, sourdough/soda
  hybrid, good for using up excess starter
- 2020-01-26 for 100% whole wheat sandwich loaf, single-day recipe
  (less than 7 hours total)
- 2020-05-09: Buttery sandwich bread

- 2022-04-04 for 100% whole wheat Dutch oven boule with long room
  temperature proof, well suited for winter overnight rise.  Published as
  <https://www.brainonfire.net/blog/2022/04/05/100-percent-whole-wheat-sourdough/>
- 2022-06-05 for 100% whole wheat Dutch oven boule, single-day (summer recipe)

## 2019-05-15

Trying to follow https://www.seriouseats.com/recipes/2019/01/100-whole-wheat-sandwich-bread.html

Autolyse:

- 425 g Bob's Red Mill organic stone ground whole wheat flour
- 320 g cold water (deviation: should have been cool room temperature)

Combined in bowl until uniform, covered and left for 3.5 hours.

Dough:

- Hydrated flour from above
- 50 g dark brown sugar (Domino)
- 11 g iodized table salt (salt, calcium silicate, dextrose, potassium
  iodide)
- 8 g instant yeast (Red Star Quick-Rise, 2 rounded tsp)
- ~55 g water
- 2 Tbsp sesame oil
- A little olive oil and sesame oil for oiling the bowl

I combined the hydrated flour, sugar, salt, and yeast in the food
processor, but it wouldn't mix them properly, just roll a big lump of
it around in a circle. I took it out, tried to knead it for a bit,
then attempted to add water, which was fair disastrous—it was too
stiff and wouldn't absorb the water. I put it back in the food
processor, along with the remaining water, and had another go at
it. It was pretty gluey and wanted to run around the edge again, and
this time managed to gum up the spindle. However, at this point it was
fairly elastic, so I called it good enough, oiled the bowl, and dumped
it in.

Let sit covered 4 hours until doubled in volume.

Shaping the loaf:

Scooped out dough onto floured board, folded in half, and pushed down
on end to seal it. Shaped into square with seam on underside and
dropped into 8 inch loaf pan.

Let sit loosely covered 3 hours.

Only rose to about 2 cm at most above rim before it was ready (as
evidenced by poking it and the dimple not coming back very quickly, or
at all).

Started preheat of oven to 350°F 10 minutes before putting bread in (20:05).

10 minutes in (22:25) I realized I'd forgotten to slash the bread. I
managed to sort of gouge it, even though a crust was
forming. (Probably a terrible idea!) I also added a small cast iron
skillet with some water in the bottom of the oven. (This seemed to
work out fine.)

The bread baked for 55 minutes and I turned it out onto a wire cooling
rack when the internal temperature reached 200°F.

Results:

- Flat-topped, probably partly because I broke the crust while it was
  cooking, which is not a substitute for slashing it beforehand. But
  this never had the rise it needed to be the classic sandwich loaf
  shape.
- Reasonably open crumb: Not overly dense, not light and airy, but I
  don't *like* light and airy because that's hard to work with for
  sandwiches. So this is actually a success.
- Good flavor, although the sugar comes through altogether too much,
  leaving the bread somewhat sweet. (Would it be less sweet if the
  yeast had been more active?)

This worked, but I never want to put dough in a food processor again.

## 2019-05-21

First bread with starter by the name of "glarp". Tried to make 100%
hydration bread, entirely out of starter. Dough didn't rise, so I
added flour, more sugar, and yeast to the existing dough (now 80%
hydration) and tried it again. Worked out well.

- 800 g starter (100% hydration of 100% whole wheat)
- 10 g iodized table salt
- 26 g brown sugar
- olive oil (for pan)

Layered salt and sugar on starter, mixed slightly, then kneaded about
15 times. Left to rise in oiled loaf pan, somewhat covered, 13+
hours. Did not rise appreciably. (Cool weather didn't help, but 100%
hydration and young starter didn't either.)

Added:

- 100 g whole wheat flour (brings down to 80% hydration)
- 2 tsp (8 g) Red Star Quick-Rise instant yeast
- 24 g brown sugar

Became incredibly sticky. Was able to "knead" for a couple minutes
with a metal fork, which didn't stick. Set in refrigerator overnight,
10 hours. Kneaded maybe 30 times on a floured board, tightly
jellyrolled it, pinched under the ends, and placed in covered, oiled
loaf pan in 22°C room 3.5 hours. Moved to warm, sunny porch, 2
hours. Dough rose to top of bread pan, just touching cover. When
touched, dimples remained.

Preheated oven to 450°F (took 12 minutes), then put in loaf pan
(mid-lower rack setting), metal bowl with 100 g water, and a few mLs
of water dashed onto the floor of the oven to create some initial
steam. Baked 14 minutes. Reduced heat to 380°F, baked for 26 more
minutes. When removed, internal temperature was 205°F.

Crumb was dense, and a little sticky. Bread did not rise much above
top of loaf pan and had smooth, flattish top. Flavor was deliciously
sour, very mildly sweet. Overall, a success!

## 2019-05-24

Second try with "glarp", going for 80% hydration and single knead.

- 600 g starter (100% hydration of 100% whole wheat)
- 200 g whole wheat flour
- 100 g water
- 50 g brown sugar
- 10 g iodized table salt
- olive oil (for pan)

Mixed in bowl, and left covered 10 hours 50 minutes. However, it rose a
fair amount overnight, even in the cooler kitchen. Interestingly, it
had what would have been a fairly open crumb, so this might have been
a good candidate for no-knead if I had shaped it last night and let it
sit in the loaf pan, or retarded it in the fridge. Anyway, I wasn't
sure if I would get enough rise during the day if I shaped it now,
so...

Added:

- 100 g whole wheat flour
- 80 g water

Kneaded about 25 times, then jellyrolled with ends tucked under and
placed in oiled loaf pan 5 hours 20 minutes. Probably left too long;
top was starting to spread at cracks and becoming more flat instead of
rounded.

Placed in 380°F oven on mid-lower rack setting, with metal bowl with 1
cup of water and a bit of the water dashed on the oven floor. Baked 52
minutes, then turned off heat (by accident) and replaced for 12
minutes.

Somewhat dark and thick top crust, otherwise very similar to previous
loaf; slightly sticky inside, sour and well-developed
flavor. (Generally well-liked.) Fairly dense, but not brick-like.

Good recipe overall, might just need tightening up of recipe. Could
also work on developing the gluten better.

## 2019-05-29

"glarp" again, 80% hydration, single knead.

- 830 g starter (100% hydration of 100% whole wheat)
- 104 g whole wheat flour
- 40 g brown sugar
- 10 g iodized table salt
- olive oil (for pan)

Mixed in bowl, left covered 3 hours. Moved to refrigerator for the
night, 8 hours. Alternated kneading (with variously bare, oiled, and
floured hands) and short rests for one hour until maybe getting close
to forming a nice windowpane, but had to leave for work. Let rise in
70°F room in loaf pan 10.5 hours (too long). Put in oven preheated to
400°F, then turned oven down to 375°F. Left in for 1.5 hours, because
I got distracted. 210°F internal temperature, thick and dark
crust. Stuck to pan a bit on the bottom.

Flavor was fine.

## 2019-06-09

From "glarp" sourdough culture, trying to adapt
http://www.make-your-own-bread.com/the_recipe.html

73% hydration, 100% whole wheat flour.

- 650 g starter (100% hydration of 100% whole wheat)
- 200 g whole wheat flour
- 9 g iodized table salt (1.25 tsp)
- 35 g brown sugar (3 Tbsp)
- 19 g extra virgin olive oil (+ more for pan)
- 56 g water

- Mix flour, salt, and brown sugar, and break up the clumps in the sugar
- Make indentation and pour in olive oil, push some flour on top
- Add starter and water
- Mix and knead
    - Initial mixture was a little tougher than probably recommended
      for a non-sourdough—mild to moderate resistance. I figured that
      the starter would already have some gluten development, so this
      might be the right amount of resistance, pre-kneading.
    - I mostly air-kneaded. That is, I kneaded it between my hands, in
      the air, not on a surface.
- Fold under, place in oiled loaf pan
    - Pan had an excess of oil in it, which pooled into corners.
- Brush oil onto surface of dough
- Let rise
    - Let sit uncovered in warm room 3.5 hours
    - Moved to warm oven to speed up rise since night was coming, 50
      minutes
- Turned on oven to 375°F with pan inside. Preheat took 10 minutes;
  baked 40 minutes. Internal temperature reached 208°F. Stuck badly to
  bottom of pan.

Top crust looked nice. Crumb is reasonable—somewhat dense, not gluey,
strong enough for sandwiches. Flavor is pretty nice.

## 2019-06-14

Soda bread with excess starter, adapted from
https://www.recipetineats.com/no-yeast-bread-irish-soda-bread/

Original calls for flour (mix of white and whole) and buttermilk. I
substituted sourdough starter and whole wheat flour for the flour, and
kefir plus the water in the starter for the buttermilk. I also kneaded
for more than the recommended 10 seconds to try to make the dough ball
more manageable.

Ingredients:

- 827 g starter from "glarp" (100% whole wheat at 100% hydration)
- 87 g skim milk kefir
- 152 g whole wheat flour
- 1.5 tsp baking soda
- 1.5 tsp iodized table salt
- olive oil (for pan)

Directions:

- Preheat oven to 430°F
- Prepare an oiled baking sheet
- Mix dry ingredients (flour, baking soda, salt) and wet ingredients
  (starter, kefir) separately
- Mix together until well combined
- Knead about 15 times on unfloured work surface
    - Was pretty hard to get it to stop sticking, and I thought that
      more kneading might help. If I'd used the recommended floured
      work surface, maybe this wouldn't have been a problem!
- Form dough into ball and place on baking sheet
    - It fell flat to about 2.5 cm thick on its own, and pretty wide
- Score a shallow "X" into the surface
- Bake 22 minutes
- Reduce oven thermostat to 390°F and bake 19 minutes
    - Bottom tapped hollow, but internal temperature only reached 195°F

Reasonably tasty. Thin crust, although bottom was a little thicker and
crunchier, probably because of the oil. Perfect crumb for sandwiches,
although the shape was fairly flat, only a couple inches
tall. Slightly less elastic and tough than a regular sourdough loaf,
but much more structural integrity than regular soda bread.

Consider trying this in a loaf pan next time.

## 2019-06-16

From "glarp" sourdough culture, trying to improve on 2019-06-09, 1.2x
recipe, and ran dry ingredients through a sifter to break up small
chunks of brown sugar and disperse it a bit better. I also did not oil
the surface.

73% hydration, 100% whole wheat flour.

- 780 g starter (100% hydration of 100% whole wheat)
- 240 g whole wheat flour
- 11 g iodized table salt (1.5 tsp, slightly rounded)
- 42 g brown sugar
- 23 g extra virgin olive oil (+ more for pan)
- 67 g water

- Mix flour, salt, and brown sugar
- Pass through a sieve and break up any clumps in the sugar, returning
  it to the mix
- Make indentation and pour in olive oil, push some flour on top
- Add starter and water
- Mix and knead
    - Kneaded on the counter for about 25 minutes
    - Kneading seemed complete before dough stopped being sticky
- Fold under, place in very lightly oiled loaf pan
- Let rise 6 hours (until soft to touch, doesn't spring back much)
    - Let sit covered in warm room about 3 hours
    - The dough stuck to the cover during the rise, and when I removed
      the cover it was stretched pretty dramatically upwards. :-(
    - Uncovered and left for about 3 more hours
- Bake in an oven preheated to 375°F for 43 minutes; internal
  temperature reached 205°F

Good sour flavor, nice crumb. Soft crust on sides, darker and thicker
on top.

Top was a little sunken, probably because of damage to the structure
when I removed the cover during the rise. Overflowed the edges a
little, which might not have happened if the top weren't damaged.

Very reasonable sandwich bread!

## 2019-06-22

Following 2019-06-16, but this time uncovering early enough to not
screw up the proof.

73% hydration, 100% whole wheat flour.

- 780 g starter (100% hydration of 100% whole wheat)
- 240 g whole wheat flour
- 11 g iodized table salt (1.5 tsp, slightly rounded)
- 42 g brown sugar
- 23 g extra virgin olive oil (+ more for pan)
- 67 g water

- Mix flour, salt, and brown sugar
- Pass through a sieve and break up any clumps in the sugar, returning
  it to the mix
- Make indentation and pour in olive oil, push some flour on top
- Add starter and water
- Mix and knead
    - Kneaded 20 minutes, plus a 5 minute rest in the middle, during
      which the dough came together.
    - I used more of a shove/smear than a stretch for about half the
      kneads this time, and I think it worked better.
- Fold under, place in very lightly oiled loaf pan
- Let rise 5–6 hours
    - Let sit 20m uncovered; 2h15m covered; 3h5m uncovered
    - Room temperature was 23°C initially, rising to 24.5°C
    - Rose *very* well, maybe 2+ cm above edge of loaf pan
- Bake in an oven preheated to 375°F for 50 minutes; internal
  temperature reached 195°F

The dough rose fantastically well during proof, and developed a lovely
crust and crumb. No complaints there.

The only serious problem is that the loaf's sides stuck to the pan,
and when I tried to free it with a knife, the knife cut a bit into the
loaf. I don't know why it released relatively easily last time, and
not this time (or why the final internal temperature was so different,
for that matter!)

Additionally, towards the end of the rise, the top started getting
"stretch marks" up to 1 cm wide, and at least one hole appeared. The
poke test was inconclusive, because the surface was getting a little
leathery and would pull down a larger area when I poked. Some holes
and stretching appeared at sides, just above lip of pan. This may
indicate insufficient kneading.

Also consider going back to a smaller recipe (down from 1.2x to 1x)
since the "shoulders" that formed over the lip of the loaf pan made it
hard to release the bread. (Look into release techniques, too, of
course.)

## 2019-07-03

Trying a refrigerated rise before the proof.

- 780 g starter (100% hydration of 100% whole wheat)
- 240 g whole wheat flour
- 11 g iodized table salt (1.5 tsp, rounded)
- 37 g brown sugar (3 Tbsp)
- 23 g extra virgin olive oil (+ more for pan)
- 67 g water
- [1.5 tsp baking soda, late addition]

- Pass brown sugar through a sieve to break up any clumps, using flour
  to help coat and divide it.
- Mix in salt and remaining flour
- Make indentation and pour in olive oil, push some flour on top
- Add starter and water
- Mix and knead
    - Kneaded about 20 minutes, with some rests
    - Didn't really come together like I wanted it to
- Refrigerate 10 hours
- Let sit covered 8 hours

At this point, I found that the gluten cloak had fallen apart. I tried
kneading for 10–20 minutes, but it seemed to be making matters worse;
the dough was actually getting slightly more slack, if anything. I
added 1.5 tsp of baking soda, kneaded it in, and dumped it all into an
oiled loaf pan.

Baked in a 375°F oven with a bowl of water for 50 minutes. Internal
temperature reached 204°F. Okay rise, nicely browned crust, released
fairly easily from pan. Covered on cooling rack with dish towel.

Somewhat dense and overly moist crumb, but nicely strong and
elastic. Decent sour flavor.

## 2019-07-07

Following 2019-06-22. 73% hydration, 100% whole wheat flour.

- 781 g starter (100% hydration of 100% whole wheat) -- intended 780 g
- 240 g whole wheat flour
- 10 g iodized table salt (1.5 tsp, rounded)
- 36 g brown sugar (3 Tbsp)
- 23 g extra virgin olive oil (+ more for pan)
- 67 g water

let sit 7 minutes

kneaded about 90 times

wet down counter, jellyrolled

proof 5 hours, too long (collapsed on top, with stretch marks,
although had also stuck to cover)

baked 50 minutes, internal temp 203°F, stuck to pan

## 2019-07-22

Using up some old sourdough starter discard, not using any added fresh
flour, based on 1.25x the proportions of 2019-06-14.

- 1260 g very mature starter (discarded excess that was kept in the
  fridge)
- 81 g whole wheat flour
- 2 tsp iodized table salt
- 2 tsp baking soda
- peanut oil (for pan)

Mixed all ingredients, baking soda last, and poured into oiled baking
pan. (The discarded starter was very loose, so I wasn't able to knead
it.) Baked at 430°F for 22 minutes (tossed some water in the bottom
after 3 minutes), then at 390°F for 19 minutes.

Came out looking like an undecorated cake. Flavor was surprisingly
mild and pleasant.

## 2019-12-14

Whole wheat sponge dough, based on half recipe of "Basic Whole Wheat
Bread" p. 80 of Laurel's Kitchen Bread Book, 2003 edition with sponge
adaptation.

### Sponge

#### Ingredients

- 0.5 g instant yeast (Red Star Quick-Rise, heaping 1/8 tsp)
- 60 g warm water for yeast
- 225 g whole wheat flour (Bob's Red Mill)
- 7.6 g salt (1.25 tsp)
- 18 g honey (raw wildflower honey, 1 Tbsp)
- 101 g ice cold water

#### Process

- Yeast added to warm water
- Measured out flour and salt into mixing bowl
- Added honey to (now tepid) yeast water
- Mixed and fluffed wheat and salt
- Made well in dry ingredients; poured in yeast/honey water and the
  ice cold water. Mixed in well to smooth batter, then folded in rest
  and worked by hand into a ball.
- Left in mixing bowl, covered with a plate, for 18.5 hours, at about
  70°F

### Dough

#### Ingredients

- Sponge, above
- 160 g lukewarm water
- 225 g more flour
- 2.5 g more yeast (tried for 7/8 tsp, might be less)
- Salted butter (for loaf pan)

#### Process

- Poured water over sponge, and mixed by hand (flattening and folding)
  until sponge was softening and starting to fall apart
- Added flour and yeast
- Mixed, and kneaded 600 times over the course of 65 minutes (includes
  breaks of various lengths)
- Rounded and let sit in clean bowl covered with damp towel for 2.5 hours
- Deflated and rounded again, left to sit again under cover of damp
  towel for 3 hours
- Deflated and shaped into loaf, placed into 8"x4" loaf pan greased
  with butter, left to rise under bowl until a finger poke dent
  rebounded slowly, 2.5 hours
- Preheated oven to 425°F, and placed pan in oven just as heating
  cycle turned off
- After 15 minutes, reduced heat to 325°F
    - Meant to do 10 minutes; left oven door open longer than
      necessary to partially compensate
- Continued baking 30 more minutes
- Dumped bread out of pan onto cooling rack and covered with towel to cool

### Notes

- Dense crumb, but not moist; good for sandwiches
- Reasonably soft crust, a little chewy
- Could have risen more, I think, but I needed to go to bed. The high
  initial heat probably didn't help; I wonder if I could have gotten
  bit more rise in the oven if I'd used lower heat and added steam.
- The bottom 5 mm was more dense than the rest. Not sure what to make
  of it. Cold countertop?
- Yeast proofed a little sluggish, but still seems viable.
- Flavor pleasant, a little sour. Kid-approved.

## 2019-12-21

Whole wheat with very little kneading. 71% hydration, same ingredients
as 2019-12-14 but different process.

- 320 g warm water
- 19 g honey (raw wildflower honey, 1 Tbsp)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 450 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (1.25 tsp)
- butter (for loaf pan)

Directions:

- Mix 220 g of water with honey and yeast; let stand a few minutes
  while measuring other ingredients
- Mix flour and salt in large mixing bowl
- Add liquid mixture and stir, then mash, into a dough
- Add the rest of the water and knead it in
    - (Because you forgot at first how much water you were supposed to
       add.)
- Form into an approximate ball shape
- Let sit in bowl, covered with plate, for 10.75 hours at temperatures
  rising from 66°F to 75°F (probably around 70°F for the bulk of the
  time)
- Deflate, knead 7 times, form into a ball, and let sit covered again
  for 2 hours at 74°F
- Deflate thoroughly, jellyroll several times, and shape into a log
  with the ends tucked under and sealed
- Place seam side down in buttered 8"x4" loaf pan and let sit, covered
  with large bowl, 2.5 hours at 72°F
- Preheat oven to 425°F with metal bowl containing 1 cup of water off
  to one side on a lower rack
- Preheat 1/4 cup of water in microwave
- When oven is preheated and reaches second heating cycle, place loaf
  pan in oven and toss water into floor of oven (watch out for steam
  cloud)
- After 2 minutes slash loaf and add 1/8 cup hot water to oven
- After 2 more minutes, add another 1/8 cup hot water to oven
- 6 minutes later, set oven down to 325°F
- Bake 45 more minutes, with a couple removals to check internal
  temperature
    - Never went appreciably above 200°F, but it looked done, 55
      minutes is a long time, and the thermometer was coming out
      clean.
- Tap out of loaf pan and let cool on a wire rack, covered with a
  towel to allow internal steaming to finish

Notes:

- Noticed a large bubble forming on top around 2 hours into proof,
  which I pricked and deflated. Decided it was time to bake.
- The second rise was more vigorous than the proof; maybe should have
  just done a single pre-proof rise.
- Crust was very thin, a little chewy. Crumb dense but fairly fine and
  dry, kind of wobbly when slicing. Similar to last time, but maybe
  wobblier; may have to do with weaker crust.

Next time, try with just one rise and then proof, and no steaming.

## 2019-12-27

Trying 2019-12-21 again (71% hydration, warm water)

Still aiming for a good overnight, minimal-knead, whole wheat bread.

- 320 g warm water (about 110°F)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 450 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- butter (for loaf pan)

Process:

- Mixed warm water with yeast and honey; let stand for 10-15 minutes
  while measuring other ingredients so yeast could revive
- Mixed flour and salt in large mixing bowl
- Added liquid mixture and stirred, then mashed and kneaded, into a dough
- Formed into an approximate ball shape
    - Dough felt only mildly sticky at this point, interestingly
- Let sit in mixing bowl for 2 hours at 72°F
- Deflated and let sit again, 10.3 hours overnight at 70°F
    - Smelled very sour in the morning
- Deflated, kneaded about 20 times.
    - I accidentally got butter on it halfway through, and also wet my
      hands
    - The dough started falling apart and losing its gluten sheath,
      getting kind of shaggy with little rough cavities
- Formed into log, placed in buttered loaf pan and let sit 7.5 hours
  at 72°F
- Baked at 325°F for one hour

Notes:

- About half height. Pleasant texture, nice sour flavor.
- I suspect this recipe could be done with a bit more water (since the
  dough never goes through a truly sticky phase) to get more rise.
- This dough was *way* more active than I had expected. I had not
  intended to deflate the dough after just two hours, but it had
  already doubled. I suspect the water was warmer than last time, and
  I let the yeast sit in the honey water for 10-15 minutes instead of
  just 5-10. And then it sat in a covered container overnight in warm
  temperatures and I think the gluten started to break down.

Lesson: Next time it's warm out, don't let it sit overnight!

## 2020-01-20

Working from 2019-12-27 but trying for single-day by keeping bread in
a warm environment.

- 320 g warm water (cold water microwaved one minute, allowed to cool to 110°F)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 450 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- Butter (for loaf pan)

Process:

- Mixed warm water with yeast and honey; let stand for 10-15 minutes
  while measuring other ingredients so yeast could revive
- Mixed flour and salt in large mixing bowl
- Added liquid mixture and stirred, then mashed and kneaded, into a dough
- Kneaded 25 times
- Formed into an approximate ball shape
    - Dough mildly sticky, cratered
- Let sit in covered mixing bowl on warming blanket (low setting) for
  2 hours at 78°F
- Deflate and shape into a ball again
    - Dough now soft, smooth, and supple, only about as sticky as a
      post-it note
- Let sit again 2 hours
- Knead a few more times and shape into jelly roll (pinch ends), then
  place in buttered loaf pan
- Let sit (covered with box this time) for 1.5 hours
    - Nicely tall in loaf pan
- Baked at 375°F for 1 hour
    - Tried to slash the loaf first, but just sort of dented it in a
      line and very slightly deflated it; knife was not as razor-sharp
      as I had thought it was.
    - Meant to cook for only 45 minutes, but lost track of time.

Flavor is definitely on the bland side, which I attribute to the quick
rise. Might consider combining this with a colder sponge.

Crumb is fairly dry, and crust is kind of leathery and chewy. I
definitely overcooked this (note: internal temperature reached 210°F)
and should probably have set an alarm for 45 minutes.

On the whole, this is a good base to work from, since it was fast and
easy.

## 2020-01-26

Trying to replicate 2020-01-20, but with 45 minute bake time.

- 320 g water
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 450 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
    - Dough will probably be mildly sticky, and cratered
- Let sit in covered mixing bowl, maintained at 80°F until doubled in
  size and does not fill in after being poked, about 2 hours
    - I set the bowl on a heated blanket set on low; putting a towel
      over top can help as well
    - Left 2 hours 10 minutes this time, which was slightly too long
- Deflate and shape into a ball again
    - Dough now soft, smooth, and supple, only about as sticky as a
      post-it note
- Let sit again until doubled, about 2 hours
    - 2 hours and 10 minutes again
- Knead a few more times and shape into jelly roll (pinch under ends),
  then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, about 1.5 hours
- Bake at 375°F for 45 minutes
    - Internal temperature reached 205°F

Notes:

- Not as impressive a rise in proof as the previous time
- Crust is pleasant, slightly crunchy; crumb is well-formed, dense
  enough for spreading jam on, but not overly dense; flavor is
  pleasant

Fast to make, and decent loaf. This one is a winner!

## 2020-02-09

Replicating 2020-01-26, but with 73% hydration instead of 71%, and
increasing flour slightly to make it a full pound (454 g instead of
450 g). All other ingredients left alone.

Unfortunately, this also mixes the dregs of the flour from the old bag
(276 g, which appears to have been milled March 2019) with a new bag
(October 2019). Hopefully that doesn't matter too much.

It is also possible the salt quantity is off by about a gram, since
the milligram scale didn't show the same tare after I dumped in the
salt.

- 331 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 454 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
    - Dough somewhat sticky; small amounts stuck to my hands, more
      than last time. Not cratered.
- Let sit in covered mixing bowl, maintained at 80°F until doubled in
  size and does not fill in after being poked, about 2 hours
    - I set the bowl on a heated blanket set on low; putting a towel
      over top can help as well
    - Left exactly 2 hours this time. Stuck to bottom and stretched
      into network of thin strands when removed.
- Deflate and shape into a ball again
    - Dough now soft, smooth, and supple, a little stickier than a
      post-it note.
    - Surface smooth and slightly webbed; gluten sheath
      developing. Bran flecks highly visible against pale dough.
- Let sit again until doubled, about 2 hours
    - 1 hour 20 minutes this time
- Knead a few more times and shape into jelly roll (pinch under ends),
  then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, about 1.5 hours
    - 1 hour 40 minutes this time
- Bake at 375°F for 45 minutes
    - Internal temperature reached 210°F

Notes:

- Higher internal temperature
- Collapsed slightly in oven, on one side
- Crust found to be hard and thick when I tried to insert the
  thermometer; had to punch it in
- Reasonably moist and strong crumb
- Crust is tough, but thin

Good recipe overall.

## 2020-02-15

First loaf using my new starter, "pickle". 73% hydration dough with
1/5th of the flour coming from the starter, using the 2020-02-09
recipe otherwise.

(Yes, it is both sourdough and has added yeast—for comparison.)

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
    - Dough pretty sticky; my hands were coated with dough.
- Let sit in covered mixing bowl, maintained at 80°F until doubled in
  size and does not fill in after being poked, about 2 hours
    - I set the bowl on a heated blanket with a plate and towel over top
    - Except it was really cold in the house, so I cranked up the
      heated blanket... and it was about 90°C under the bread, even
      though it was maybe 78°C on top!
    - Left 2 hours this time.
- Deflate and shape into a ball again
    - Deeply stringy, pretty sticky
- Let sit again until doubled, about 2 hours
    - 1.5 hours this time
- Knead a few more times and try to shape into jelly roll, then place
  in buttered loaf pan
    - But it was so sticky and soft that I didn't really manage to
      shape it
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, about 1.5 hours
    - 2 hours this time
- Bake at 375°F for 45 minutes
    - Actually about 49 minutes
    - Internal temperature reached 211°F

Notes:

- Well, I screwed up the temperature. Just use the "low" setting on
  the heated blanket in the future, and keep the thermometer closer to
  the blanket. (Or get a dedicated baker's rising chamber, or
  whatever.)
- Really hard to work with the dough. Probably related to the too-high
  temperature.
- Rise was none too impressive. Kind of a short loaf. Looks compressed
  along the sides and bottom, maybe because I reduced the temperature
  towards the end.
- Flavor was good, nicely sour.

Try this again, but with right temperature.

## 2020-02-22

Trying 2020-02-15 again, but hopefully getting the rise temperature
right: I set the blanket to low, and kept the thermometer next to the
bowl, not on the plate that covered it.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 3.0 g instant yeast (Red Star Quick-Rise, a bit less than 1 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
    - Dough pretty sticky; my hands were coated with dough.
- Let sit in covered mixing bowl, maintained at 88°F until doubled in
  size and does not fill in after being poked, about 2 hours
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
    - Left 2.25 hours this time.
- Deflate and shape into a ball again
    - Very stringy, fairly sticky
- Let sit again until doubled, about 2 hours
    - Just under 2 hours this time
- Knead a few more times and try to shape into jelly roll, then place
  in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, about 1.5 hours
    - 2.5 hours this time
- Bake at 375°F for 45 minutes
    - Internal temperature reached 209°F

Notes:

- Just like last time, not very tall. Nice sour flavor, though!
- I think the first two rises are too long, and the yeast and gluten
  are both losing a lot of their rise by the 4 hour mark.
- Alternatively, try adding less yeast, since the starter is already
  full of it.

## 2020-02-29

Reducing the amount of yeast added in the 2020-02-15 recipe, and again
setting the blanket to low.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 1.25 g instant yeast (Red Star Quick-Rise, about 1/2 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
    - Dough pretty sticky; my hands were coated with dough.
- Let sit in covered mixing bowl, maintained at 80°F for 2 hours
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
    - Doubled in size, probably overrisen (sighing when poked)
- Deflate and shape into a ball again
    - Very stringy, fairly sticky
- Let sit again 2 hours
    - Doubled in size, probably overrisen (sighing when poked)
- Knead a few more times and try to shape into jelly roll, then place
  in buttered loaf pan
    - Extremely sticky and soft, hard to shape
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 2 hours 25 minutes
    - Temperature was somehow higher for this part, around 90°F
- Bake at 375°F for 45 minutes
    - Internal temperature reached 210°F

Notes:

- Halving the yeast did not make an appreciable difference in the outcome
- I think the best next change is to check in on the first two rises
  every 20 minutes so that I catch them before they're starting to go
  loose and alcohol-heavy

## 2020-03-07

2020-02-29 but being more careful about rise times.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 1.25 g instant yeast (Red Star Quick-Rise, about 1/2 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 1 hour 37 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into a ball again
- Let sit again until doubled and sighing (whoops!), 1 hour 48 minutes
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 1 hour 55 minutes
- Bake at 375°F for 45 minutes
    - Internal temperature reached 209°F

Notes:

- Better rise than last time. Keeping the rises from going too long seems key.
- Next, try kneading more at the beginning.
- Very tasty!

## 2020-03-14

2020-03-07 but with more kneading.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 1.25 g instant yeast (Red Star Quick-Rise, about 1/2 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat
  starter)
- Butter (for loaf pan)

Process:

- Microwave water one minute
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times, let rest 20 minutes, knead 25 more times
- Form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled and fingerpokes do not rebound, 1 hour 48 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into a ball again
- Let sit again until doubled, 1 hour 5 minutes
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 2 hours
- Bake at 375°F for 45 minutes
    - Internal temperature reached 210°F

Notes:

- Timing was a little wonky this time; I let the yeast mixture sit and
  cool for longer than intended. This should be replicated before
  moving on to other variations.
- Rise was decent, a little better than last time.

## 2020-03-21

2020-03-07 but only doing one rise before shaping, on the theory that
the gluten is developing too early.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 1.25 g instant yeast (Red Star Quick-Rise, about 1/2 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water 45 seconds (to over 110°F)
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 1 hour 45 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into jelly roll with ends turned under, then place
  in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 2 hours 21 minutes
- Bake at 375°F for 45 minutes
    - Internal temperature reached 213°F

Notes:

- Pretty decent rise! Got a little ovenspring.
- Fairly dark, thick top crust

## 2020-03-28

Replicating 2020-03-21.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 1.25 g instant yeast (Red Star Quick-Rise, about 1/2 tsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water 45 seconds (to over 110°F)
- Stir in honey and allow to cool to 110°F
- Add yeast, cover, and let stand 10-15 minutes (stir in yeast if it
  floats) while preparing other ingredients -- should start to see
  bubbling by end of that time
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 1 hour 53 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into jelly roll with ends turned under, then place
  in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 1 hour 54 minutes
- Bake at 375°F for 45 minutes
    - Internal temperature reached 213°F

Notes:

- Not as much rise this time
- Again with the dark, thick top crust! No idea why a shorter rise
  would produce this.

## 2020-04-04

Back to 2020-03-07 with the two pre-proof rises, but not adding any
yeast -- relying entirely on the sourdough starter.

Accidentally used 21 g honey instead of 20.

- 241 g water (cold from the tap)
- 21 g honey (raw wildflower honey, 1 Tbsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water until warm, about 45 seconds
- Stir in honey and allow to cool to 105°F
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 2 hours 42 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into a ball again
- Let sit again until doubled, 1 hour 32 minutes
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 4 hours 22 minutes
- Bake at 375°F for 45 minutes
    - Internal temperature reached 212°F

Notes:

- Let the proof rise go too long; fell a bit in the oven
- Dark crust

## 2020-04-11

2020-04-04 again (relying entirely on the sourdough starter) but only
one pre-proof rise, and baking at a lower temperature (350°F).

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 364 g whole wheat flour (Bob's Red Mill)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water until warm, about 45 seconds
- Stir in honey and allow to cool to 105°F
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 3 hours 3 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 3 hours 18 minutes
- Bake at 350°F for 45 minutes
    - Internal temperature reached 211°F

Notes:

- Lighter crust than I've been getting recently—and more similar to
  what I *used* to get. (Why have I been getting darker crusts? Warmer
  weather? Bad oven thermostat?) I guess I'll go with 350°F going
  forward.
- Absolutely lovely flavor.
- Still not a hell of a lot of rise. Just barely peeked over the edge
  of the loaf pan.
- Future experiments:
    - More sweetener to get a faster ramp-up
    - Build up yeast first: Pull the 180 g starter in the morning, but
      just add 90 g water and 90 g flour at first; let it sit to build
      yeast, and when it reaches maturity, add the rest of the
      ingredients to make the dough.
    - 1.3x recipe—just make it taller that way!

## 2020-04-18

2020-04-11 again, but a mix of Bob's Red Mill (about 15 or 16%
protein?) and King Arthur (organic 100% whole wheat, hard red, 14%
protein). Slightly higher water temperature as well.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 364 g whole wheat flour (199 g Bob's Red Mill, 165 g King Arthur)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water until warm, about 45 seconds
- Stir in honey and allow to cool to 110°F
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 25 times and form into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 3 hours 10 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 3 hours 48 minutes
- Bake at 350°F for 45 minutes
    - Internal temperature reached 211°F

Notes:

- Proof rise was too long, and was starting to hollow and become
  leathery on top as it sometimes does
- Great flavor, not noticeably different from before

## 2020-04-25 - Prebuild, plus hotter oven

New approach! Pre-build yeast, then make final dough.

Also entirely on King Arthur flour now. Used corn oil to grease the pan.

- 241 g (90 + 151) water (cold from the tap)
- 20 g (5 + 15) honey (raw wildflower honey, 1 Tbsp)
- 364 g (90 + 274) whole wheat flour (King Arthur)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Corn oil (for loaf pan)

Process:

- Microwave 90 g of water for 20 seconds and add 5 g honey, then let
  cool to 109°F
- Mix with 90 g flour, and then starter
- Let sit covered 1 hours 12 minutes, then put on heated blanket to help
  things along, 2 hours 20 minutes
    - Was bubbling at least 40 minutes earlier
- Microwave 151 g of water for 20 seconds and add 15 g honey, then let
  cool to 95°F and add to mixture
- Mix in 274 g flour, and then salt
    - Should have mixed dry separately!
- Knead 60 times
- Jellyroll and put in greased loaf pan
- Let sit, covered on warm blanket 5 hours 50 minutes
    - Electric blanket was off for some portion of this time :-(
- Preheated oven to 425°F
- Threw a half cup of hot water onto bottom of oven, and put in the
  loaf pan and a metal bowl of about a half cup more of hot water
- Baked at 425°F for 39 minutes, then at 350°F for 6 minutes
    - Meant to turn down sooner!

Notes:

- Well, that was kind of a disaster! It only rose to about level with
  the loaf pan rim, or not even that. And then it fell somewhat in the
  oven, and I left it on high heat for almost the entire time, not the
  intended first ten minutes or so.

## 2020-05-02

Going back to 2020-04-11, but with AP flour (so, different kneading
and rise times), and an underfed starter. :-/

This time the starter has been fed only King Arthur whole wheat (hard
red) and the fresh flour will be almost entirely One Mighty Mill
stone-ground "whole wheat" AP flour.

I also missed last night's starter feeding, so I fed the starter
immediately before taking some starter out, but it will likely be less
active than it should be.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 364 g flour ("whole wheat", but also marked AP, and I don't see bran)
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water until warm, about 45 seconds
- Stir in honey and allow to cool to 105°F
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead about 15 times and let rest 31 minutes
    - Extremely gluey
- Knead about 30 times and let rest 36 minutes
- Knead about 30 times and shape into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 3 hours 33 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate, fold a few times, then let sit covered and heated again
  2 hours 30 minutes
- Deflate and pour into buttered loaf pan; try to make roughly level
- Let sit (covered with box this time) until fingerpokes only rebound
  very slowly, 2 hours 34 minutes
- Bake at 450°F for 7 minutes with splash of water in oven for steam
  at start, then 350°F for 38 minutes
    - Internal temperature reached 210°F
    - Top looks lightly toasted

Notes:

- So gooey and loose that I wasn't able to shape it at all
- Saw bubbling and sighing during initial high heat segment of baking;
  if I'd been able to develop a gluten sheath, that would have been
  some good ovenspring!
- Short and brick-like

## 2020-05-09 - Variation on KAF classic sandwich bread

All I have is AP flour, so I'm going to try
https://www.kingarthurflour.com/recipes/classic-sandwich-bread-recipe
-- but I don't have milk, and I want to use my starter instead of
yeast.

I also added 22 g additional flour partway through kneading since the
dough was too slack to work, and ended up putting it on a heated
blanket to get it to rise at all.

- 292 g One Mighty Mill "all-purpose whole wheat" flour
- 6.6 g (1.25 tsp) iodized table salt
- 26 g (2 Tbsp) granulated sugar ("raw cane sugar", blonde)
- 165 g hot water
- 56 g (4 Tbsp) cultured unsalted butter
- 180 g sourdough starter from "pickle" (100% hydration AP flour starter)
- Corn oil, for oiling rise bowl, loaf pan, hands, and work surface

Directions:

- Mix flour, salt, and sugar
- Put butter in water and heat in microwave 1 minutes, then wait until
  butter melted
- Add butter and water and then starter to dry ingredients
- Mix and then knead with oiled hands for about 5 to 10 minutes, until
  soft and supple (still sticky)
- Transfer to oiled bowl and cover wih plate
- Let rise at 70°F for 2 hours
- Put on heated blanket (maybe 90°F) for 3 hours
- Jellyroll on oiled surface and place in 8.5" x 4.5" loaf pan and let
  sit on heated blanket under high plastic cover 3 hours 30 minutes
- Bake at 350°F for 35 minutes
    - Interior registered 210°F
- Remove from pan and let sit to cool, covered

Notes:

- Sweet, buttery, and soft, with a pretty soft crust
- Only rose to just below the rim of the pan, but had a little ovenspring
- Next time, use a heated pad for entire rise

## 2020-05-16

2020-04-11 again, but starter contained mix of One Mighty Mill AP and
whole wheat from Oeschner Farms, and flour was again from Oeschner.

Used lower rise temperature (no heated blanket) so I could be out of
the house longer. More kneads were required. Longer rise than
necessary because I was out.

- 241 g water (cold from the tap)
- 20 g honey (raw wildflower honey, 1 Tbsp)
- 364 g whole wheat flour
- 7.5 g salt (should be about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration AP & whole
  wheat starter)
- Butter (for loaf pan)

Process:

- Microwave water until warm, about 45 seconds
- Stir in honey and allow to cool to 110°F
- Mix flour and salt in large mixing bowl
- Add sourdough starter
- Add liquid mixture and stir, then mash, into a dough
- Knead 75 times and form into a ball
- Let sit in covered mixing bowl maintained at ~75°F until
  doubled, 4 hours 21 minutes
- Deflate and shape into jelly roll, then place in buttered loaf pan
- Let sit (covered with box this time, and heated to 80–90°F) until
  fingerpokes only rebound very slowly, 3 hours
- Bake at 400°F from 16 and then 350°F for 24 minutes
    - Internal temperature reached 211°F

Notes:

- Bubbled and sighed a bit in oven; let rise too long. Gluten probably
  just not developed enough, too.
- Wonderful texture and flavor, though!

## 2020-05-22 - 3 day low-knead

Based on https://sourdough.com/recipes/100-whole-wheat and
https://www.jocooks.com/recipes/no-knead-cast-iron-whole-wheat-bread/

86% hydration no-knead sourdough in a Dutch oven.

Using hard red whole wheat flour from Oeschner Farm, NY and 100% whole
wheat 100% hydration starter "pickle" fed on same.

### First preferment

Day 1, evening (22:00):

- 1 g of sourdough starter from "pickle" (100% hydration whole wheat starter)
- 37 g flour
- 22 g water

Mixed all together, then kneaded a bit to make sure starter was evenly
distributed. Formed into ball and left on counter in covered jar.

### Second preferment

Day 2, morning (10:15):

- first preferment
- 128 g flour
- 64 g water

Again mixed all and kneaded to homogeneity, formed into a ball, and
left covered.

### Dough

Day 2, afternoon (17:15):

- second preferment
- 534 g flour
- 514 g water
- 15 g iodized table salt (2.5 tsp)
- butter for greasing bowl

Mashed up preferment into water, then mixed with flour. Belatedly
added salt, then kneaded about 15 times to make sure everything was
mixed well.

Formed into ball and placed in greased bowl, covered fairly tightly
with a plate.

22:04: Fairly voluminous. Placed into fridge for the night.

### Bake

- flour
- 2-3 tsp corn meal

Day 3, morning (08:55):

- Preheated oven with cast iron to 475°F
- Sprinkled dough liberally with flour
- Took out Dutch oven and sprinkled corn meal across bottom to reduce
  burning
- Lifted dough out of the bowl as gently as possible (working down the
  edges with floured hands) and dropped into Dutch oven
- Baked for 30 minutes with lid on
- Baked for another 15 minutes with lid off
    - Internal temperature reached 211°F

Notes:

- Really nice rise! Could get a lot taller, I think, but this is
  better than what I get out of the loaf pan.
- The dents I left when getting it into the Dutch oven filled in just
  fine, although one was still visible as a very narrow put. I guess
  it created "scar tissue" that the rest of the dough rose around.
- The crust cracked in several places. Good candidate for slashing
  next time.
- Really gummy inside. Next time, bake longer, maybe lowering the
  temperature when I take the lid off.

## 2020-05-23

Going back to 2020-03-21 but kneaded longer because dough seemed a lot
more wet and slack.

Starter was past-ripe likely because of higher night-time temperatures
(and late start)

- 241 g water (cold from the tap)
- 21 g honey (raw wildflower honey, 1 Tbsp)
- 364 g whole wheat flour (from Oeschner Farms, hard red wheat)
- 7.5 g salt (about 1.25 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)

Process:

- Microwaved water 40 seconds (to over 110°F)
- Stirred in honey and allow to cool to 110°F
- Mixed flour and salt in large mixing bowl
- Added sourdough starter
- Added liquid mixture and stirred, then mashed, into a dough
- Kneaded 85 times, then used fingertips to rapidly roll into a ball
- Let sit in covered mixing bowl, maintained at 80 to 90°F until
  doubled, 2 hours 52 minutes
    - I set the bowl on a heated blanket on low with a plate and towel
      over top
- Deflate and shape into jelly roll with ends turned under, then push
  into shape in buttered loaf pan
- Let sit (covered with plastic-lined box) until bubbles starting to
  appear, 2 hours 26 minutes
- Bake at 350°F for 45 minutes with a splash of water on oven floor to
  produce initial steam, and a bowl of water to keep humidity up
    - Internal temperature reached 211°F

Notes:

- Decent rise relative to previous loaves, crested about 5 mm over
  loaf pan rim
- Very little flavor! Definitely can tell it's sourdough and whole
  wheat, but still extraordinarily mild.
- Crumb is surprisingly dark in color.

## 2020-05-28

Doing 2020-05-22 again, but with longer lid-off bake to drive off more
water and reduce gumminess.

86% hydration no-knead sourdough in a Dutch oven.

Using hard red whole wheat flour from Oeschner Farm, NY for
preferments and 100% whole wheat 100% hydration starter "pickle" fed
on same. Fresh flour for dough from Ground Up Grain, 100% whole wheat
from hard red spring and hard red winter wheat. (Huge flakes of bran
in this one.)

First preferment (day 1, evening):

- 1 g of sourdough starter from "pickle" (100% hydration whole wheat starter)
- 37 g flour
- 22 g water

Mixed all together, then kneaded a bit to make sure starter was evenly
distributed. Formed into ball and left on counter in covered jar
overnight, about 12 hours.

Second preferment (day 2, morning):

- first preferment
- 128 g flour
- 64 g water

Mixed flour and water, then kneaded in first preferment. Formed into a
ball, and left covered for about 5 hours.

Dough (day 2, afternoon):

- 534 g flour
- 15 g iodized table salt (2.5 tsp)
- second preferment
- 514 g water
- butter for greasing bowl

Mixed flour and salt, added preferment and water, then mixed and
kneaded a bit to make sure everything was mixed well.

Formed into ball and placed in greased bowl, covered fairly tightly
with a plate. Let sit at room temperature until large and puffy, about
5 hours. Then refrigerated overnight, about 19 hours.

Bake (day 3, afternoon):

- flour
- 2-3 tsp corn meal

Removed from refrigerator and let sit 1 hour 30 minutes, then baked:

- Preheated oven with cast iron to 475°F
- Sprinkled dough liberally with flour to reduce stickiness
- Took out Dutch oven and sprinkled corn meal across bottom to reduce
  burning (quickly started smoking)
- Lifted dough out of the bowl as gently as possible (working down the
  edges with floured hands) and dropped into Dutch oven
- Baked for 30 minutes with lid on
- Lowered temperature to 420°F and baked for another 25 minutes with
  lid off
    - Internal temperature reached 211°F

Notes:

- Even better rise this time
- Big crack, definitely should slash next time to see what happens.
- Not very gummy, could go slightly longer with lid off (maybe even
  lower temperature)

## 2020-06-04

2020-05-28 but with no refrigeration overnight, and entirely on Ground
Up Grain flour now.

86% hydration no-knead sourdough in a Dutch oven.

First preferment (day 1, evening):

- 1 g of sourdough starter from "pickle" (100% hydration whole wheat starter)
- 37 g flour
- 22 g water

Mixed together thoroughly, formed into ball, and left in covered jar
overnight, 14 hours.

Second preferment (day 2, morning):

- first preferment
- 128 g flour
- 64 g water

As before, and left until late evening, 10 hours.

Dough (day 2, evening):

- second preferment
- 514 g water
- 15 g iodized table salt (2.5 tsp)
- 534 g flour
- butter for greasing bowl

Mixed thoroughly, formed into ball, and placed in greased bowl,
covered fairly tightly with a plate. Let sit at room temperature
(~79°F) overnight, 11 hours.

Bake (day 3, afternoon):

- a few Tbsp of flour
- 2-3 tsp corn meal

- Preheated oven with cast iron to 475°F
- Sprinkled dough liberally with flour to reduce stickiness
- Took out Dutch oven and sprinkled corn meal across bottom to reduce
  burning (quickly started smoking)
- Lifted dough out of the bowl as gently as possible (working down the
  edges with floured hands) and dropped into Dutch oven
    - Failure! It was loose and came out in chunks
- Baked for 33 minutes with lid on
    - Meant to do 30 minutes
- Lowered temperature to 420°F and baked for another 27 minutes with
  lid off
    - Meant to do 25 minutes
    - Internal temperature reached 210°F

Note:

- Pretty flat :-/

## 2020-06-06 - Two-day Dutch oven, low-knead

Trying to do a 2-day version of 2020-05-28.

(Meant to do one-day, but ran out of time and had to refrigerate.)

- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 510 g water
- 15 g iodized table salt (2.5 tsp)
- 610 g flour, 100% whole wheat
- butter (for greasing bowl)
- 2 Tbsp flour for dusting
- 2 tsp corn meal for bottom

- Prepped a large bowl greased with butter
- In another bowl, added starter and salt to water, then stirred to mix
- Added flour, and stirred and kneaded until dough was roughly homogeneous
- Placed dough in buttered bowl, covered, until well-risen but not
  bubbling through or starting to fall (6 hours, at 82°F)
- Refrigerated 14 hours 30 minutes
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Used plastic scraper to gently work down sides of dough and unstick
  it from the bowl, allowing flour to fall down edges and prevent more
  sticking, until dough ball was loose
- Placed dough ball into Dutch oven, slashed lightly
- Baked with lid on 30 minutes
- Removed lid and baked for another 24 minutes
    - Oops! Meant to lower temperature to 420°F for this part

Notes:

- Crust ridges are somewhat blackened; actually remember to lower
  temperature next time

## 2020-06-14 - One-day Dutch oven, no-knead

Trying to do a 1-day version of 2020-06-06.

House is colder. Adding salt to water before adding starter. (Not sure
if either approach is better for yeast, in terms of osmolarity. Theory
is that stirring starter into water better distributes it into the
flour in the absence of kneading.)

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- butter (for greasing bowl)
- 2 Tbsp flour for dusting
- 2 tsp corn meal for bottom

- Prepped a large bowl greased with butter
- In another bowl, mixed salt into water, then added starter and
  stirred to mix
- Added flour and stirred until dough was roughly homogeneous
- Placed dough in buttered bowl, covered, until well-risen but not
  starting to fall or showing too many bubbles (7 hours 50 minutes, at
  70°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Used plastic scraper to gently work down sides of dough and unstick
  it from the bowl, allowing flour to fall down edges and prevent more
  sticking, until dough ball was loose
- Placed dough ball into Dutch oven, slashed lightly
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another
  25 minutes

Notes:

- Great success!

## 2020-06-20 - One-day Dutch oven, no-knead

Repeat of 2020-06-14, but house is warmer again. Also, didn't mix in
starter the same way.

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- butter (for greasing bowl)
- 2 Tbsp flour for dusting
- 2 tsp corn meal for bottom

- Prepped a large bowl greased with butter
- In another bowl, added salt and starter into water, then added flour
  and stirred/floded until dough was roughly homogeneous
- Placed dough in buttered bowl, covered, until well-risen but not
  starting to fall or showing too many bubbles (6 hours 9 minutes, at 80°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Used plastic scraper to gently work down sides of dough and unstick
  it from the bowl, allowing flour to fall down edges and prevent more
  sticking, until dough ball was loose
- Placed dough ball into Dutch oven, slashed lightly
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another
  25 minutes

Notes:

- Tasty!
- A little less rise than last time; maybe because of time and
  temperature and whatnot, maybe because I was too rough with
  transferring it to the Dutch oven. (Consider lining the bowl with
  butter *and* flour? Would that help? Maybe a stretch and fold
  partway through to make it more ball-like, and then line with
  flour?)

## 2020-06-28 - One-day Dutch oven, cold start

Repeat of 2020-06-14, but with a stretch-and-fold and transfer to a
greased Dutch oven partway through. (Trying to reduce sticking.)

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- butter (for greasing Dutch oven)

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Formed into rough ball and let sit covered in mixing bowl until
  starting to show signs of fermentation and maybe relaxation (6 hours
  at 80°F)
- Poured out of bowl onto counter. Flattened, folded in thirds with
  spatula and again but 90 degrees off; repeated two more times.
- Formed dough into ball and plopped into greased Dutch oven
- Let sit covered until well-risen but not starting to fall or
  showing too many bubbles (2 hours 10 minutes, at 80°F)
- Placed Dutch oven into oven and set oven to 475°F
- Oven took 25 minutes to reach target temperature; baked for 20
  minutes after that
- Lowered temperature to 420°F, removed lid for one minute, then put it back
    - This was a timing accident
- Baked another 9 minutes
- Removed lid baked for another 25 minutes

Notes:

- Dark, burnt bottom, 214°F interior
- Low and puck-like; very little oven spring
- Try again, but with these changes:
    - Oil as a release agent rather than butter (butter has water in
      it, and I don't want that sitting against the raw cast iron for
      hours)
    - Transfer to Dutch oven sooner, maybe after only 4 hours; rise
      was already high and gorgeous by the end of 6 hours, and I
      probably lost a lot of the rise potential (or just didn't wait
      long enough?)
    - Bake for less time in lid-on segment
    - Sprinkle corn meal into bottom of Dutch oven before adding dough?

## 2020-07-05 - One-day Dutch oven, cold start

Attempting something like 2020-06-28 again, but better.

Still doing a stretch-and-fold and transfer to the larger, greased
bowl partway through, but with different timing. (Also using oil and
corn meal.)

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat
  starter)
- 610 g flour, 100% whole wheat
- 2 tsp corn meal
- corn oil (for greasing Dutch oven)

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Formed into rough ball and let sit covered in mixing bowl until
  starting to show signs of fermentation but not yet anywhere near
  doubled (2 hours at 77°F)
- Poured out of bowl onto counter. Flattened, folded in thirds with
  spatula and again but 90 degrees off; repeat two more times.
- Formed dough into ball and plopped into oiled Dutch oven with corn
  meal sprinkled in bottom
- Let sit covered until well-risen but not starting to fall or showing
  too many bubbles (8 hours 46 minutes, at 77°F)
- Placed Dutch oven into oven and set oven to 475°F; baked for 35
  minutes
    - Preheat portion took 19 minutes
- Lowered temperature to 420°F, removed lid, and baked another 30
  minutes

Notes:

- Cylindrical again! Not really much taller, either. Not burnt, so
  that's good. Top looks kind of like a bran muffin -- glossy, and
  caramel in color.
- One more experiment to do: Proof in the Dutch oven again, but
  preheat the oven to 500°F before putting the Dutch oven in. Maybe
  that will be a fast enough rise in temperature that I can get the
  best of both worlds. (Or maybe I'll just get another puck.)

## 2020-07-12 - One-day Dutch oven, preheat oven

Attempting 2020-07-05 but placing the Dutch-oven-proofed dough into a
hot oven.

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- 2 tsp corn meal
- corn oil (for greasing Dutch oven)

- In a medium-large mixing bowl, dissolved salt into water, then added
  starter and stirred to mix
- Immediately added flour and stirred/folded until dough was roughly
  homogeneous
- Formed into rough ball and let sit covered in mixing bowl until
  starting to show signs of fermentation but not yet anywhere near
  doubled (2 hours 35 minutes at 83°F)
- Poured out of bowl onto counter. Flattened, folded in thirds with
  spatula and again but 90 degrees off; repeat two more times.
- Formed dough into ball and plopped into oiled Dutch oven with corn
  meal sprinkled in bottom
- Let sit covered until well-risen but not starting to fall or
  showing too many bubbles (3 hours 52 minutes, at 84–85°F)
- Preheated oven to 500°F
- Placed Dutch oven into oven and baked for 16 minutes at 500°F and
  then 14 minutes at 470°F
- Lowered temperature to 420°F, removed lid, and baked another 25 minutes

Notes:

- Another puck!
- Back to dropping dough into a hot Dutch oven if I want a rounded
  boule. Although honestly, this isn't bad for getting rectangular
  slices at a reasonable density; maybe I could just scale up the
  recipe if I wanted it taller.

## 2020-07-19 - Cold start in Dutch oven, but with lower hydration

Like 2020-07-05, but with lower hydration, to see if that helps hold
in more bubbles. This takes the hydration down from 85.7% to 82.6%.

- 488 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- 2 tsp corn meal
- corn oil (for greasing Dutch oven)

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Formed into rough ball and let sit covered in mixing bowl until
  starting to show signs of fermentation but not yet anywhere near
  doubled (2 hours 24 minutes at 81°F)
- Poured out of bowl onto counter. Flattened, folded in thirds with
  spatula and again but 90 degrees off; repeat two more times.
- Formed dough into ball and plopped into oiled Dutch oven with corn
  meal sprinkled in bottom
- Let sit covered until well-risen, showing some bubbles (meant to
  catch it earlier) -- 5 hours 36 minutes at 84°F
- Placed Dutch oven into oven and set oven to 475°F; baked for 42 minutes
    - Meant to do 35 minutes for this segment, but lost track of time
    - Preheat portion took 16 minutes
- Lowered temperature to 420°F, removed lid, and baked another 23 minutes
    - Was supposed to be 30 minutes, but previous segment ran long

Notes:

- Still a puck! But this shape is growing on me. (Makes for
  rectangular slices.)
- Dark, crunchy, thick crust on the sides and bottom. Probably from
  some combination of the lower hydration and the over-long 475°F
  segment.

## 2020-07-26 - One-day Dutch oven, no-knead

Back to 2020-06-14. Still with Ground Up Grain in the starter, but
using Red Fife whole wheat flour from Misty Brook Farm in Albion, ME
(organic, stone-ground). *Huge* amount of bran.

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 610 g flour, 100% whole wheat
- butter (for greasing bowl)
- 2 Tbsp flour for dusting
- 2 tsp corn meal for bottom

- Prepped a large bowl greased with butter
- In another bowl, mixed salt into water, then added starter and
  stirred to mix
- Added flour and stirred until dough was roughly homogeneous
- Placed dough in buttered bowl, covered, until well-risen but not
  starting to fall or showing too many bubbles (7 hours 22 minutes, at
  84°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Used plastic scraper to gently work down sides of dough and unstick
  it from the bowl, allowing flour to fall down edges and prevent more
  sticking, until dough ball was loose
- Placed dough ball into Dutch oven
    - Pretty rough handling, looked cratered and fissured after transfer
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another
  25 minutes

Notes:

- When I first mixed the dough, it was like sandy mud, hardly cohering
  at all. The bran content was absurd, or maybe it just wasn't milled
  very fine, so large amounts of endosperm were stuck to it. I wonder
  if this flour was intended for a different use, such as mixing into
  white flour.
- Turned out surprisingly well, given that. About 50-60% the height I
  would normally expect from this recipe.
- Dark, rich brown color in crust.

## 2020-08-01 - One-day Dutch oven, cold start

Trying 2020-07-05 again, but with mix of Ground Up Grain and remaining
Misty Brook flour (to use up the latter).

...also I forgot to add corn meal.

- 510 g water
- 15 g iodized table salt (just over 2.5 tsp)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat
  starter)
- 610 g flour, 100% whole wheat
    - 261 g Misty Brook Farm's Red Fife
    - 349 g Ground Up Grain
- corn oil (for greasing Dutch oven)

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Formed into rough ball and let sit covered in mixing bowl until
  starting to show signs of fermentation but not yet anywhere near
  doubled (4 hours 25 minutes at 84°F)
- Poured out of bowl onto counter. Flattened, folded in thirds with
  spatula and again but 90 degrees off; repeat two more times.
- Formed dough into ball and plopped into oiled Dutch oven
- Let sit covered until well-risen but not starting to fall or showing
  too many bubbles (3 hours 28 minutes, at 84°F)
- Placed Dutch oven into oven and set oven to 475°F; baked for 35
  minutes
- Lowered temperature to 420°F, removed lid, and baked another 30
  minutes

Notes:

- Still looked pretty wet at lid removal time; probably should have
  delayed that by about 10 minutes.
- Stuck to Dutch oven bottom and sides for a while. I don't know why
  it stuck to the sides, but I was able to pry it away with a butter
  knife after letting it sit for some time. Sticking to the bottom was
  more serious, and I suspect it's because I forgot the corn
  meal. However, it did eventually come away intact.

## 2020-08-04 - Hot Dutch Oven, low knead, lower hydration

2020-07-19 still seemed kind of high hydration, so I want to try
lowering it more. Plenty of other differences, though; I'm going to
try a preheated Dutch Oven and doing the proof rise in a cloth-lined
bowl (a first for me). 78.6% hydration.

First ferment, first day:

- 50 g whole wheat flour
- 50 g water
- 1 g sourdough starter ("pickle")

Mixed and left covered 16 hours 34 minutes.

Dough, second day:

- 500 g water
- 15 g iodized table salt (about 2.5 tsp)
- First ferment
- 650 g flour, 100% whole wheat
- a little extra flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 25 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat two more times.
- Lined the mixing bowl with flour-impregnated cloth (from flour sack)
  and a sprinkling of extra flour
- Covered until well-risen but not starting to fall or showing too
  many bubbles (4 hours 40 minutes, at 83°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Gently tipped dough out of bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Bread looks great. A proper gluten sheath developed just from the
  stretch-and-folds.
- Stuck in places to cloth and had to be gently pried free. It
  flattened out like a jellyfish on the counter and I had to sort of
  gather it together to drop it in. That was OK, but I should really
  get a proper banneton. (This at least demonstrates that it would be
  useful to me.)
- Try some kneading next time and see if it makes a difference.

## 2020-08-09 - Hot Dutch Oven, some knead

Trying 2020-08-04 again but with some kneading. 78.6% hydration
again. Longer rest.

First ferment, first day:

- 50 g whole wheat flour
- 50 g water
- 1 g sourdough starter ("pickle")

Mixed and left covered 13 hours 20 minutes.

Dough, second day:

- 500 g water
- 15 g iodized table salt (about 2.5 tsp)
- First ferment
- 650 g flour, 100% whole wheat
- a little AP flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 54 minutes
- Scraped onto counter and kneaded 20 times
- Lined the mixing bowl with flour-impregnated cloth (from flour sack)
  and a sprinkling of extra flour
- Covered until well-risen but not starting to fall or showing too
  many bubbles (6 hours 6 minutes, at 86°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
    - The dough stuck really badly to the cloth
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- The dough stuck quite badly to the cloth, and was torn, lumpy, and
  deflated when I finally dumped it into the Dutch oven.
- Astonishingly, it kind of looked like bread when it came out. With
  ridges and crevasses and taller on one side than the other, but
  bread nonetheless.

## 2020-08-16 - Hot Dutch oven, buttered/floured proof bowl

I didn't get around to making bread on the usual day, so I removed the
amount of starter I would have used, fed it to 100% increase, and made
bread with it the next day. This is partially based on 2020-08-09
recipe, with the same hydration (78.6%).

Preferment, first day:

- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 90 g 100% whole wheat flour
- 90 g water

Stored in 40°F refrigerator overnight 08 hours 42 minutes.

In the morning, set out on counter to warm up 6 hours 43 minutes.

Dough, second day:

- 370 g water
- 15 g iodized table salt (about 2.5 tsp)
- First ferment
- 520 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little AP flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  preferment and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 1 hour 40 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat two more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour as best I could
- Covered until well-risen but not starting to fall or showing too
  many bubbles (5 hours 7 minutes, at 75°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Good rise
- Didn't have too much trouble getting it out of the bowl. Only a thin
  layer of flour stuck to the butter, but I was able to flour the
  bottom more thickly, and I think the dough stuck more to the sides
  than the bottom. I was able to get it out still in a ball shape
  using a spatula.
- Try an even lower hydration next time just to see what happens!

## 2020-08-22 - 78% hydration no-knead hot oven

Like 2020-08-16 but without the overnight preferment, and one more
stretch & fold. Using whole wheat flour for dusting bowl and
dough. Still 78.6% hydration.

- 460 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 2 hours 12 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered until well-risen but not starting to fall or showing too
  many bubbles (3 hours 35 minutes, at 81°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Large fissures opened in the lid-on phase. Good rise, should try
  slashing again.
- I was able to release the dough from the bowl by scraping down the
  sides just a couple of cm and flipping the dough out into my hands,
  placing it into the Dutch oven upside-down.

## 2020-08-30

Following 2020-08-22, but ran out of time and had to refrigerate
overnight and then let sit out all morning the next day and most of
the afternoon before it was ready to bake.

- 460 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 1 hour 52 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Let rise, covered, for 3 hours 38 minutes at 76°F
- Refrigerated for about 11 hours
- Left on counter to warm up and finish rising, about 7 hours
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Did not seem to have suffered for its time in the refrigerator

## 2020-09-05 - 78% hydration no-knead hot oven

2020-08-22 but baking one day early with a longer rest than usual, and
with 528 g of the fresh flour being whole wheat flour I found in the
freezer, and the remainder the usual Ground Up Grain. The flour in the
freezer looks finer-ground and kind of yellow, and I also suspect it
is desiccated from having been frozen for about 10 years.

- 485 g water
- 15 g iodized table salt (about 2.5 tsp)
- 130 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 635 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 3 hours 26 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was as well covered as possible
- Covered until well-risen but not starting to fall or showing too
  many bubbles (7 hours 30 minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Dought was pleasantly pliable. I suspect this is due to a finer
  grind or lower bran percent.
- Dough took a looong time to rise, and it was getting late, so I
  baked it maybe 30 to 60 minutes earlier than I should have.

## 2020-09-13 - 78% hydration no-knead hot oven - Red Fife

Like 2020-08-22 but using Misty Brook Farm's stone ground Red Fife
whole wheat for fresh flour.

- 460 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 3 hours
- Scraped onto counter and did 4 stretch-and-folds (Each: Flatten to
  lengthen, fold in thirds, turn 90 degrees and repeat)
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was as well-covered as possible
- Covered until well-risen but not starting to fall or showing too
  many bubbles (7 hours 20 minutes, at 75°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Misty Brook's Red Fife was again very bran-heavy, although maybe not
  as much as last time. Maybe I got some of the same lot. It felt like
  wet sand when I made the dough, although I could feel it tighten up
  considerably during the stretch-and-folds.
- If I use this flour again, maybe I should try proper kneading and/or
  a heated rise. The rest period seems important, though.

## 2020-09-19 - Red Fife, moderately disastrous.

Starting from 2020-09-13 but using warm water (as the weather is
getting colder) and doing some kneading. Still using Misty Brook
Farm's stone ground Red Fife whole wheat for fresh flour.

Baking a day early, so using less starter than usual.

Had to use a heated blanket to make rise not take forever.

Baked at 375° with maybe not fully preheated Dutch oven because of
timing and coordination issues.

- 485 g water heated to about 85°F
- 15 g iodized table salt (about 2.5 tsp)
- 130 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 635 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
    - Dough temperature was about 71°F at start
- Let rest 5 hours 11 minutes
- Scraped onto counter and kneaded 32 times
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was as well-covered as possible
- Covered until well-risen but not starting to fall or showing too
  many bubbles (5 hours 31 minutes)
    - A little too far risen, maybe? Very sloppy.
    - Left on a heated blanket (on low) with a towel over top
- Preheated oven with Dutch oven (including lid) inside to 375°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
    - Peeked, did not look ready yet -- still wet and low
- Set oven to 475°F and baked with lid on for another 15 minutes
    - Took lid off again to check on loaf
- Set oven to 420°F and baked with lid still on for a final 18 minutes

Notes:

- Red Fife probably wants even lower hydration than this, given how
  sloppy the dough was -- it flopped, bunched, and folded as I took it
  out of the bowl
- Low rise, not very even distribution of bubble sizes
- Not one to repeat, even if I wanted to

## 2020-09-27 - 78% hydration no-knead hot oven - KA

Following 2020-08-22 but with King Arthur whole wheat.

Much longer first rise than intended.

- 460 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let rest 5 hours 27 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered until well-risen but not starting to fall or showing too
  many bubbles (3 hours 34 minutes, at 79°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Very nice rise; dark, smooth crust with only a minor crack
- I think the Dutch oven was fully preheated this time, which would
  create the dark crust. At this higher initial temperature, slashing
  is probably more important for maximizing rise.
- Should do this recipe again, but with a first rise of 2 hours

## 2020-10-04 - 78% hydration no-knead hot oven - KA

Following 2020-09-27 but with 2 hour first rise.

I sifted some of the badly milled Red Fife and used the bran in place
of the corn meal for the bottom of the Dutch oven, and the sifted
flour for dusting the proof bowl.

- 460 g water
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
- 2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let rest 2 hours 5 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered until well-risen but not starting to fall or showing too
  many bubbles (9 hours at 74°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- The dough released very easily. The grainy Red Fife probably helped
  a good deal with that.
- The rise was so slow! I wonder if I'm using too much salt, which is
  retarding the yeast, and I've only been getting away with it
  previously due to warm weather.

## 2020-10-10 - Preferment open pan

First time in a while doing an open-pan bake. Ground Up Grain with
overnight starter-doubling preferment. Heated blanket for proof.

Preferment, first day:

- 150 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 75 g 100% whole wheat flour
- 75 g water

Stored at room temperature (about 74°F) overnight 10 hours 18 minutes.

Dough, second day:

- 400 g water
- 15 g iodized table salt (about 2.5 tsp)
- First ferment
- 550 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little coarse flour for dusting
- several Tbsp wheat bran for pan

- In a medium-large mixing bowl, mixed salt into water, then added
  preferment and stirred to mix
- Added flour and stirred/folded until dough was roughly homogeneous
- Let rest 1 hour 30 minutes
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat two more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour as best I could
- Placed on heated blanket and covered until well-risen (5 hours 31 minutes)
    - Too far risen, as it turns out
  minutes, surface temperature rising from 71°F to 82°F)
- Preheated oven to 375°F
- Coated pan with wheat bran and dusted top of dough with coarse wheat flour
- Removed dough from bowl and placed onto pan
- Baked for 58 minutes

Notes:

- Pretty much immediately flattened out into a foccacia when
  transferred to the pan
- Probably would have been great in the Dutch oven
- Great flavor -- either the preferment, or the Ground Up Grain in
  contrast to the King Arthur

## 2020-10-17 - Overnight no-knead with mid-morning bake

Baked one day early (thus with less starter than usual) and with
partially refrigerated (overnight) proof.

- 420 g water
- 15 g iodized table salt (about 2.5 tsp)
- 140 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 630 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Formed dough into ball and placed in mixing bowl
- Let rest covered in refrigerator 9 hours 33 minutes
- Let sit at room temperature 8 hours 26 minutes, surface temperature
  rising from 38°F to 62°F
- Placed covered bowl on heated blanket set on low with towel on top
  until well-risen, 4 hours 46 minutes, surface temperature rising
  from 62°F to 76°F
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Didn't have time for full rise
- Kinda flat, but not quite a foccaccia :-)

## 2020-10-25 - 78% hydration warm dough no-knead hot oven

Following usual 78% no-knead 4-fold, but with warmed dough (water + blanket).

Ground Up Grain for the fresh flour.

- 460 g water warmed to 80-100°F
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
    - Let water cool to 91°F
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 3 minutes on heated blanket on low
    - Surface temperature rose from 75°F to 78°F
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (4 hours 52
  minutes, surface temperature rising from 73°F to 79°F)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Crust kinda dark, even though I put in the dough right after the
  oven came up to temperature (so the Dutch oven wasn't fully
  preheated, as I sometimes let it get)
- Rise OK. Probably could have waited longer, not sure.

## 2020-11-01 - 78% hydration warm dough no-knead hot oven

2020-10-25 again.

I've discovered that the infrared thermometer I'm using is low by
about 4 or 5°F. :-(

- 460 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 56 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (4 hours 25
  minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Removed dough from bowl and placed into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Overproofed. Tore a bit during transfer.
- Next time: Try going back to higher hydration but keep recipe
  otherwise the same

## 2020-11-08 - 83% hydration hot Dutch oven direct drop

2020-10-25 but raising hydration to 82.6%. Flour from Ground Up Grain.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 16 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (5 hours 16
  minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Kind of shaggy and very loose for stretch-and-folds
- Dropped straight out of bowl into Dutch oven -- may have shocked too
  much air out of it
- Good rise anyhow!

## 2020-11-15 - 83% hydration hot Dutch oven direct drop

2020-11-08 again.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 33 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (4 hours)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Great rise, again, despite being dropped directly from bowl into
  Dutch oven and ending up somewhat lopsided.
- I think the combination of heated blanket, butter, and coarse flour
  allows for an easy release.

## 2020-11-22 - 83% hydration hot Dutch oven direct drop

2020-11-08 again.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 18 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until badly
  overproofed (4 hours 40 minutes)
    - Hissed and collapsed when I checked on it
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Note:

- Badly overproofed
- Turned out OK anyhow!

## 2020-11-29 - Olive bread - 83% hydration hot Dutch oven direct drop

2020-11-08 again, but with olives. Lots of olives. Probably too many!

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- 176 g whole pitted kalamata olives (entire jar, drained)
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp corn meal

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and olives and stirred/folded until dough was basically
  homogeneous
- Cover and let rest 3 hours on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (3 hours 42 mins)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal on bottom to reduce sticking
  (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- A little flatter than previous (olive-free) iterations of this recipe
- Really, really tasty
- About 9 olives all ended up in a line down the side, which I think
  corresponds to the seam from when I formed a ball for the proof
- Consider using about 2/3 the quantity of olives, and chopping half
  of them in eighths or so so that they incorporate better
- Definitely try adding some chopped rosemary, and maybe a mix of
  olive types

## 2020-12-05 - 83% hydration hot Dutch oven direct drop, but King Arthur

2020-11-08 again, but with King Arthur instead of my usual Ground Up
Grain -- and wheat bran instead of corn meal for coating the bottom of
the Dutch oven. (I've actually been using a mix recently.)

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2.5 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours 35 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (3 hours 28 minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered wheat bran on bottom to reduce
  sticking (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Bran seems to work just as well as corn meal. And in both cases,
  most of it brushes off right away (or falls off later during
  handling).
- Not much apparent difference with the different flour, despite King
  Arthur being a finer grind.

## 2020-12-12 - 83% hydration hot Dutch oven direct drop

2020-11-08 again. I'm baking a day early, so I gave the starter
tonight's feeding this morning, less than an hour before making the
dough (so the starter is effectively 10% smaller.)

Using wheat bran as anti-stick.

First rise was at room temperature, since I forgot to get out the
heated blanket.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife
- 2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 3 hours 11 minutes at room temperature
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (5 hours 31
  minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered bran on bottom to reduce sticking
  (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

## 2020-12-20 - 83% hydration Dutch oven proof

Another attempt at doing the proof rise in the Dutch oven and then
placing it into a hot oven. Based on 2020-11-08. Flour from Ground Up
Grain. Only three stretch-and-folds.

Accidentally baked for 7 minutes longer before taking lid off (same
total time.)

Starter was smelling a bit odd today -- more of a cheese & brassica
smell, not great. Hopefully doesn't affect rise or flavor much.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of sunflower oil

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Cover and let rest 3 hours on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat two more times.
- Oiled the Dutch oven, shaped dough into ball with seam down, and
  placed dough into Dutch oven.
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (7 hours 30
  minutes)
- Preheated oven to 475°F
- Baked with lid on 37 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 18
  minutes

Notes:

- Puck
- Stuck
- Not doing this again

## 2020-12-27 - Half rye

2020-11-08 again (83% hydration hot Dutch oven direct drop) but with
about half the flour being organic rye flour from Freedom Food Farm
in Raynham, MA. (Looks like whole grain, although not labeled as
such.)

I took the lid off during baking 5 minutes later than intended.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 300 g whole wheat flour
- 310 g whole rye flour
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife for bowl
- 2 tsp corn meal and wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flours and stirred/folded until dough was basically homogeneous
- Cover and let rest 2 hours on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Cleaned the mixing bowl and greased with butter, then dusted with
  coarse flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (6 hours 50
  minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with wheat flour, especially around edges, to
  prevent sticking to hands
- Removed Dutch oven, scattered corn meal and wheat bran on bottom to
  reduce sticking (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 35 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 20
  minutes

Notes:

- Slow to rise, didn't rise particularly well; dough was very gooey
  and did not develop strength during stretch-and-folds.
- Bread ended up relatively flat.

## 2021-01-02 - 83% hydration hot Dutch oven direct drop, two rise

2020-11-08 but with two rises before the proof rise.

Starter was very active. I had inoculated 180 g of flour/water mixture
two nights previous and then did a "discard half, double" the previous
night.

- 488 g water, warmed
- 15 g iodized table salt (about 2.5 tsp)
- 180 g 100% whole wheat sourdough starter, 100% hydration (from "pickle")
- 610 g flour, 100% whole wheat
- a few grams of unsalted butter
- a little extra whole wheat flour for dusting
    - Used sifted Red Fife for bowl
- 2 tsp corn meal and wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Covered and let rest 3 hours 41 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat three more times.
- Covered and let rest another 2 hours 29 minutes and performed
  stretch-and-folds again
- Cleaned the mixing bowl and greased with butter, then dusted with
  flour until butter was almost entirely covered
- Covered and let rest on heated blanket on low until well-risen but
  not starting to fall or showing too many bubbles (3 hours 7 minutes)
- Preheated oven with Dutch oven (including lid) inside to 475°F
- Dusted top of dough with flour, especially around edges, to prevent
  sticking to hands
- Removed Dutch oven, scattered corn meal and wheat bran on bottom to
  reduce sticking (smoked a bit)
- Dropped dough straight out of bowl into Dutch oven
- Baked with lid on 30 minutes
- Removed lid, lowered temperature to 420°F, and baked for another 25
  minutes

Notes:

- Huge rise, but collapsed after the direct drop and then rose to a
  modest height during the bake -- no real advantage.

## 2021-01-10 - Loaf pan, 78% hydration, no sweetener, full sourdough

Taking inspiration from 2020-03-21 loaf pan recipe, which was 73%
hydration, but without the yeast and honey and taking a middle ground
of 78% hydration. Keeping the 2.1 baker's percent of salt from
2020-11-08 and other recent Dutch oven recipes rather than the 1.6
from the loaf pan recipe.

- 339 g water, warmed
- 12 g iodized table salt
- 460 g whole wheat flour (100% whole wheat, Ground Up Grain)
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- Butter (for loaf pan)
- Wheat bran and corn meal (for loaf pan)

Process:

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Covered and let rest 4 hours 12 minutes on heated blanket on low
- Scraped onto counter and used fingertips and scraper to stretch and
  fold: Flattened, folded in thirds with spatula and again but 90
  degrees off; repeat two more times.
- Covered and let rest another 3 hours 14 minutes
- Greased loaf pan with butter and sprinkled corn meal and wheat bran
  on bottom and sides
- Deflated dough, performed stretch-and-folds again, and shaped into
  jelly roll with ends turned under, then placed in coated loaf pan
- Covered with plastic-lined box and let rest on heated blanket on low
  until until fingerpokes only rebound very slowly, but not starting
  to fall (3 hours 51 minutes)
- Bake at 375°F for 45 minutes

Notes:

- Ran out of time at end of day for proof rise, had to bake early. On
  the other hand, there appears to have been some minor slump, so
  maybe that was as much as it was going to rise.
- Rose about 1 cm over rim
- Next time just try scaling up the recipe?

## 2021-01-17 - Loaf pan, 78% hydration, but larger

Scaling up 2021-01-10 and adding some stretch-and-folds at intervals.

Forgot to add corn meal and wheat bran to loaf pan.

- 398 g water, warmed
- 13.4 g table salt
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 535 g whole wheat flour (100% whole wheat, Ground Up Grain)
- Butter (for loaf pan)

Process:

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Covered and let rest 5 hours 45 minutes on heated blanket on low,
  performing two stretch-and-folds at intervals (including one at very
  end.)
    - One stretch-and-fold: Scrape onto counter and flatten/stretch in
      one direction, then fold in thirds; rotate 90 degrees and then the
      same.
    - Performed stretch-and-folds at 1 hour, 2 hours, 3 hours, and 5
      hours 45 minutes
- Greased loaf pan with butter
- After last stretch-and-fold, shaped into jelly roll with ends turned
  under, then placed in greased loaf pan
- Covered with plastic-lined box and let rest on heated blanket on low
  until until fingerpokes only rebound very slowly, but not starting
  to fall (5 hours 13 minutes)
- Baked at 375°F for 45 minutes
    - Tossed a splash of water onto the bottom of the oven before
      closing the door

Notes:

- Rose about 2 cm over rim, then fell to about 1 cm during baking
- Some definite muffin-topping. A stiffer dough would likely work better.
- Not sure the hourly folds actually helped.

I recently discovered that the table salt we've been using recently is
*not* iodized, even though I've been recording it that way. I don't
know if it makes a difference at all, but it does mean that the
ingredients for some recent recipes have been incorrect.

## 2021-01-31 - Overnight proof, cold into hot Dutch oven

New recipe after discussion with coworker K McG 2021-01-19.

81% hydration, 2.22 b% salt, 20% of flour prefermented.

- 360 g whole wheat flour
- 275 g water, warm
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10 g table salt
- ~2 tsp wheat bran

- Mix flour and warm water in medium bowl for autolyse, let sit 40 minutes
    - Intended 20-30 minutes
- Mix starter and salt into autolyse, folding repeatedly to combine well
- Let sit in covered bowl for 6 hours 33 minutes at cold room
  temperature (55-60°F), every hour tensioning the dough. Tensioning:
  Pull one edge inward, then rotate 1/8th; repeat 8 times total for
  full circle. Place back in bowl, seam downward.
    - Actually: Tensioned at beginning and then at intervals of 2:48,
      1:45, 1:00, and 1:00
- Remove from bowl. Prep bowl for overnight rise by lining with
  heavily floured cloth taken from flour sack.
- Shape dough: Press ends in, pinch seam closed; repeat other
  direction; pull center together. Place in bowl, seam side up, in
  refrigerator overnight 18 hours 36 minutes. Lightly floured on top
  and down sides intermittently to avoid sticking.
- Remove from refrigerator and let sit on counter again 24 hours 13
  minutes.
- Preheat oven to 450°F with Dutch oven inside for at least an hour.
- Sprinkle wheat bran into Dutch oven.
- Flour top of dough, turn out onto hand, and set into Dutch oven,
  spraying new top liberally with water.
- Bake covered for 20 minutes, then uncovered for 25 minutes.

Notes:

- OK rise...
- ...except there was a 2-3 cm tall cavern under the top of the crust,
  most of the way across the loaf. This is way beyond "mouse holes".
- Top of crust was fairly dark, likely because of cavern. Crust also
  has large patches of 2 mm thick flour that came with the dough out
  of the proofing bowl. These formed their own crust thanks to the
  water.
- No noticeable rise in refrigerator even after 18 hours.
- Flavor is surprisingly mild -- still sourdough, but not as sour as I
  would expect after 48+ hours. Curious smell.
- Dough was a joy to work with. Slightly sticky, but didn't stick to
  hands. Easy to work, but nice and firm.

I think the refrigerated step is pointless, except that it might be
useful as a way to chill the dough and then let it slowly warm up to
cool room temperature over course of proof -- maybe make the dough in
the late morning,tension in the afternoon, refrigerate until bedtime,
then let sit on counter overnight and bake the next afternoon. Might
as well scale up to 700 g flour.

## 2021-02-05 - Hourly folds, overnight proof, hot Dutch oven, 81% hydration

Based on 2021-01-31 but I accidentally scaled up part of the recipe
and did not do a refrigeration. (Using cold water instead.)

Going back to usual approach of dissolving salt and then starter into
water.

81% hydration, 1.8 b% salt, 9% of flour prefermented.

- 505 g whole wheat flour
- 400 g water, cold from tap
- 100 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.0 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 3 hours 48 minutes at cold room
  temperature (~67°F), every hour tensioning the dough. Tensioning:
  Pull one edge inward, then rotate 1/8th; repeat 8 times total for
  full circle. Place back in bowl, seam downward.
    - Actually tensioned at: 1:23, 1:53, 2:48, 3:48 (end)
- Removed from bowl. Prepped bowl for overnight rise by lining with
  heavily floured cloth taken from flour sack.
- Shaped dough (final tension) and placed in bowl, seam side up and
  lightly floured.
- Covered and let sit overnight, again at room temperature (~65°F), 18
  hours 10 minutes. Lightly floured on top and down sides
  intermittently to avoid sticking.
- Preheated oven to 450°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, and set into Dutch oven,
  spraying new top liberally with water.
- Baked covered for 25 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- Big cavern under the top again. I think it's related to spraying the
  water; try not doing that next time.

## 2021-02-14 - Hourly folds, overnight proof, hot Dutch oven, 81% hydration

Based on 2021-02-05 but no spraying with water. More starter (the usual amount). 460°F
instead of 450°F by accident.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 515 g whole wheat flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 5 hours at cold room temperature (~64°F), tensioning the
  dough at the beginning of each hour.  Tensioning: Take out of bowl,
  seam side up; pull one edge inward, then rotate 1/8th; repeat 8
  times total for full circle. Place back in bowl, seam downward.
- Removed from bowl. Prepped bowl for overnight rise by lining with
  heavily floured cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature (~65°F), 15
  hours 30 minutes. Lightly floured on top and down sides
  intermittently to avoid sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 25 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- Faster proof than last time, presumably because of the larger
  fraction of starter.
- Still had a wide cavity underneath the top crust, but not as
  deep. Maybe something to do with the tensioning pattern. Try a
  different approach next time, maybe folding in thirds.
- Moister than I'd like -- might mold more easily. Increase the time a
  little.

## 2021-02-21: Hourly folds, overnight proof, hot Dutch oven, 81% hydration

2021-02-14 but with a different tensioning method and a slightly
longer bake during the covered segment.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 515 g whole wheat flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 5 hours at cold room temperature (~64°F), tensioning the
  dough at the beginning of each hour.  Tensioning: Take out of bowl,
  seam side up; flatten a bit and fold in thirds, then turn 90 degress
  and do the same. Shape into a ball and place back in bowl, seam
  downward.
- Removed from bowl. Prepped bowl for overnight rise by lining with
  heavily floured cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature (~64°F), 21
  hours. Lightly floured on top and down sides intermittently to avoid
  sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- No cavity, no crust separation this time! But this is also the
  longest rise so far.
- Things to try next:
    - Aim for timing that allows a 20-24 hour rise
    - After dumping dough into hand, take the opportunity to slash the
      top

## 2021-02-28: Hourly folds, overnight proof, hot Dutch oven, 81% hydration

2021-02-21 again, but with slashing.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 400 g water, cold from tap
- 10.9 g table salt
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 515 g whole wheat flour
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 5 hours 20 minutes at cold room temperature (~69°F),
  tensioning the dough at the beginning of each hour.  Tensioning:
  Take out of bowl, seam side up; flatten a bit and fold in thirds,
  then turn 90 degress and do the same. Shape into a ball and place
  back in bowl, seam downward.
    - (Accidentally let sit a bit longer than intended for last
      "hour", and was not super precise about the hour intervals this
      time.)
- Removed from bowl. Prepped bowl for overnight rise by lining with
  heavily floured cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight at room temperature (~69°F) until well
  risen (stops rebounding as well when poked), 15 hours 20 minutes.
  Lightly floured on top and down sides intermittently to avoid
  sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, slashed top once, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- When I turned the dough out onto my hand, it started slumping out
  over the edges, and ended up slumped upwards a bit when placed into
  Dutch oven. Might be better to turn out onto floured counter, then
  scoop up and into Dutch oven.
- Slashing doesn't seem to have helped much, and made the transfer
  harder.

## 2021-03-13: Hourly folds, overnight proof, hot Dutch oven, 81% hydration

2021-02-21 again but back to the previous "eighths" tensioning
method. Clarified instructions about flouring the cloth liner.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 515 g whole wheat flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 5 hours at cold room temperature, tensioning the
  dough at the beginning of each hour.  Tensioning: Take out of bowl,
  seam side up; pull one edge inward, then rotate 1/8th; repeat 8
  times total for full circle. Place back in bowl, seam downward.
- Removed from bowl. Prepped bowl for overnight rise by lining with
  flour-saturated cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature (~68°F),
  until well risen (stops rebounding as well when poked), 13 hours 47
  minutes. Lightly floured on top and down sides intermittently to
  avoid sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- The cavity under the top of the crust is back! But not very tall --
  less than a centimeter.
- This was a shorter proof by the clock, so maybe it was underproofed
  (explaining the cavity), but it was warmer out and the dough *did*
  seem ready to bake.
- Later uses of this recipe only sometimes had a cavity. It's probably
  caused by underproofing.
- Total work time is about 30 minutes. (Starter maintenance is about
  10 minutes per week.)

This ended up being my standard recipe, more or less, for about a
year. See 2022-04-04 for the stable version.

Later experiments:

- Can get major oven spring with: Less proofing (hard to explain how
  much less... more firm when removed from bowl?), spray with water
  just before lid goes on, and 35 minutes at 460°F instead of 30. Not
  sure which parts matter yet. Sizeable cracks, may benefit from
  scoring.
    - Proofing time seems to matter most. Too little and it gets tall
      with explosive growth, but isn't as wide and gets hollows. "Just
      right" is hard to communicate, but there is a certain firmness
      remaining in the dough when transferred.
    - Longer at high heat just creates thicker crust.

## 2021-03-21: Overnight rise with accelerated folds, hot Dutch oven, 81% hydration

Ingredients of 2021-03-13 but I didn't have enough time for 5 hourly
tensionings before bed, so I'm going to do some before bed on shorter
intervals.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 515 g whole wheat flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 2 hours at cold room temperature, tensioning the dough at
  the beginning of each half hour.  Tensioning: Take out of bowl, seam
  side up; pull one edge inward, then rotate 1/8th; repeat 8 times
  total for full circle. Place back in bowl, seam downward.
- Removed from bowl. Prepped bowl for overnight rise by lining with
  flour-saturated cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature, until well
  risen (stops rebounding as well when poked), 14 hours 43
  minutes. Lightly floured on top and down sides intermittently to
  avoid sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- Very well risen in proof -- was pressing against underside of plate
  that was covering the bowl.

## 2021-04-04: Overnight rise with accelerated folds, hot Dutch oven, 81% hydration

Like 2021-03-21 -- shorter intervals for folds.

81% hydration, 1.8 b% salt, 14.9% of flour prefermented.

- 515 g whole wheat flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 2 hours at cold room temperature, tensioning the dough at
  the beginning of each half hour.  Tensioning: Take out of bowl, seam
  side up; pull one edge inward, then rotate 1/8th; repeat 8 times
  total for full circle. Place back in bowl, seam downward.
- Removed from bowl. Prepped bowl for overnight rise by lining with
  flour-saturated cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature, until well
  risen (stops rebounding as well when poked), 17 hours 7 minutes.
  Lightly floured on top and down sides intermittently to avoid
  sticking.
- Preheated oven to 460°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- Not as tall as it sometimes is. Maybe not as well risen. (Held
  together very well during transfer, not as floppy as usual.)

## 2021-04-12: Hourly folds, overnight proof, hot Dutch oven, 81% hydration -- part rye

2021-03-13 with some rye flour from Freedom Food Farm in Raynham,
MA. (Looks like whole grain, although not labeled as such.)

Rye is about half the fresh flour (which is still 515 g total.)

Ended up raising bake temperature as well since I felt it might need
more help.

- 260 g whole wheat flour + extra... maybe 50 g? 100 g?
- 255 g whole rye flour
- 400 g water, cold from tap
- 180 g sourdough starter from "pickle" (100% hydration whole wheat starter)
- 10.9 g table salt
- ~2 tsp wheat bran

- In a medium-large mixing bowl, mixed salt into water, then added
  starter and stirred to mix
- Added flour and stirred/folded until dough was basically homogeneous
- Let sit in covered bowl for 15 minutes
- Let sit 5 hours at cold room temperature, tensioning the
  dough at the beginning of each hour.  Tensioning: Take out of bowl,
  seam side up; pull one edge inward, then rotate 1/8th; repeat 8
  times total for full circle. Place back in bowl, seam downward.
    - Dough was way too sticky; had to fold in additional wheat flour
      during tensionings
- Removed from bowl. Prepped bowl for overnight rise by lining with
  flour-saturated cloth taken from flour sack.
- Shaped dough: Seam side up, pressed ends in, pinched seam closed;
  repeated other direction; pulled center together. Rolled around on
  lightly floured counter to reduce sticking to cloth. Placed in bowl,
  seam side up and floured dough lightly and fabric moderately.
- Covered and let sit overnight, again at room temperature, until well
  risen (stops rebounding as well when poked), 13 hours 34
  minutes. Lightly floured on top and down sides intermittently to
  avoid sticking.
- Preheated oven to 475°F with Dutch oven inside for at least an hour.
- Sprinkled wheat bran into Dutch oven
- Floured top of dough, turned out onto hand, gently peeled cloth off
  the dough, and set into Dutch oven
- Baked covered for 30 minutes.
- Uncovered, reduced heat to 425°F, and baked another 20 minutes.

Notes:

- Decent rise, several wide cracks
- When the dough went into the Dutch oven, it was still fairly dense
  and firm and had a number of holes or short fissures that made me
  think it was going to lose all of its bubbles. But I guess that
  didn't happen.
- All in all (and assuming 50 g extra wheat flour) this was probably
  around 39% rye, 61% hydration (!), 1.7% salt

## 2021-05-09 - higher temperature

2021-03-13 but with oven starting at 480°F -- darker, harder
crust. Not a good idea.

## 2021-07-13 - with seeds in

Basically 2021-03-13 but with 2 Tbsp of sunflower seeds and 1 tsp of
flax seeds mixed in. Used King Arthur Flour and some extra water to
compensate for texture differences.

Outcome: Could have used a lot more seeds, maybe 3x or more.

## 2021-07-18

2021-03-13 but with King Arthur whole wheat flour, so added 27 g extra
water.

Fairly flat and extremely sour. This was pretty old flour, though.

## 2021-08-15: Tensioning, overnight proof, hot Dutch oven, 81% hydration, high-proportion-fed starter

2021-03-13 but with starter having been fed at high proportion (80%
discard, aka 400% increase) each night, and increased the morning of
baking. Also missed some hourly folds and tried to make up for it
later, resulting in a longer, less consistent tensioning period.

Starter: Took 40 g of starter 12 hours after last feeding, increased
to 180 g and let sit about 8 hours before use.

The dough never really rose properly, and I instead had to add flour
and water and make a (sour, dense-crusted) pizza. This might be
because of the screwed-up tensioning, but I've done that before and
ended up with a decent loaf. It might be because the starter had only
been out of the fridge for a week, but I doubt it. I'm inclined to
blame the extremely high-proportion feeding. Switching to a 100%
increase feeding schedule.

Retrospective: It's also possible that the yeast grew *too fast*, not
too sluggishly, and I waited too long to go into proof.

## 2021-11-06: Olive and tomato bread

2021-03-13 with:

- 73 g Castelvetrano olives, halved
- 73 g Kalamata olives, halved
- 35 g "sun-ripened dried" tomatoes in oil, diced 3 mm

The proof rise took exceptionally long (~16 hours), perhaps because of
salt leaching from the olives. Good rise, tasted great.

## 2022-03-21: Olive bread

2021-03-13 with:

- 160 g Kalamata olives (pitted)

Cut olives into thirds and then squeezed handfuls under moderate force
to drive out excess brine. Added to dough.

Turned out great! Very flavorful, pretty high olive density but not too high.

## 2022-04-04: Hourly folds, overnight proof, hot Dutch oven, 81% hydration

My use of the 2021-03-13 recipe has accumulated some small changes and
optimizations, and this is a record of how I actually do the recipe at
this point.

### Ingredients

- 400 g water
- 10.9 g (2 t) table salt
- 180 g sourdough starter (100% hydration, whole wheat)
- 515 g whole wheat flour, plus extra for dusting
- ~2 tsp wheat bran or corn meal

### Directions

Times and their repetitions in bold.

- Measure out water, then mix in salt until dissolved.
- Add starter and stir to dissolve, then add and mix in flour right
  away.
- Once dough is approximately uniform, cover with a plate and let sit
  for about **15 minutes**.
- Repeat the following **5 times** in order to tension the dough:
    - Remove from the bowl, turn upside down, and place on the
      counter.
    - Flatten the ball to about 2-3 cm thick.
    - Stretch one edge out and then fold and press into the
      center. Move 1/8th of the way around and repeat for a total of 8
      times, a full circle.
    - Pull edges up a bit and shape back into a ball.
    - Turn upside down again (seam downward) and place back in bowl.
    - (Tip: Sponge the counter clean now.)
    - Let sit, covered, for **1 hour**.
- Prep for proof: Remove dough from bowl. (Tip: Clean and dry the bowl
  now.) Line the bowl with a few layers of flour-saturated
  cloth. Lightly sprinkle with extra flour.
- Shape the dough: Place on counter with seam side up. Flatten a bit
  and fold two opposite sides in, pressing and pinching the seam
  closed. Move 90° around and repeat. Pull up into a ball. Place in
  cloth-lined bowl, seam side *up* this time. Sprinkle some flour down
  the sides of the ball.
- Cover with a plate and let sit **overnight** at room temperature for
  the proof rise.
- The next day, watch for the dough to proof to the correct
  stage. This is difficult to describe, but it should be roughly
  doubled or tripled in volume, yet still moderately firm. (It will
  expand considerably in the Dutch oven. Don't wait for it to be
  spongy and loaf-sized!)
- Before the dough has reached the correct stage, place the Dutch oven
  with lid ajar on an upper oven rack and preheat to 460°F. Leave
  enough time for the Dutch oven to absorb heat after the oven reaches
  the target temperature, perhaps 15-30 minutes.
- When ready to bake:
    - Dust the top of the dough ball with more flour
    - Move the Dutch oven to the stove top and sprinkle the wheat bran
      or corn meal evenly across the bottom to avoid sticking. It will
      smoke somewhat.
    - Gently pick up the cloth and turn the dough out onto one hand,
      peeling the cloth back, then lower the dough carefully (seam
      side down) into the Dutch oven.
    - Close the lid, and slide the Dutch oven back into the oven.
- Bake for **30 minutes**. (Tip: Lay out floured cloth on warm stove
  top to dry.)
- Reduce oven thermostat to 425°F, remove the lid from the Dutch oven,
  and bake for another **20 minutes**.
- After removing from oven, allow the bread to cool in the Dutch oven
  for a few minutes, then gently transfer it to a cooling rack.

## 2022-04-10: With olive oil

2022-04-04 with ~20 g (2 Tbsp) of extra virgin olive oil added at 2nd
tensioning (when the gluten is starting to develop). My goal is to see
if there's an improvement in rise, but I'm also curious how it affects
flavor and texture.

To add oil: Flattened dough ball more than usual, put 2 Tbsp of evoo
on center, spread it around on the surface, then did the 8
fold-ins. Then I flattened it again and did the folds again, more or
less. Pretty messy. I mopped up stray oil with the dough ball.

Notes:

- The oil makes the counter and my hands harder to clean, and I think
  some of it was drawn into the floured cloth.
- The dough was stretchier and softer on subsequent tensioning steps.
- Not any better rise, possibly worse, although it may have
  overproofed so I'm not sure it's a valid comparison.
- Crust was definitely harder to cut, which is worse.

Not doing this again.

## 2022-04-19: Long and low bake, with rye

Inspired by pumpernickel and similar. It's still flour, not cracked
grains, but I'd like to see what happens with a long bake.

2022-04-04 but used 100 g of rye flour and 415 of wheat for the fresh
flour (so, about 16% rye in total).

Baking:

- Usual steps: Preheated (though only to 350°F), floured dough,
  sprinkled cornmeal.
- Baked at 350°F for 1 hour
- Lowered temperature to 215°F and baked for 4 hours
    - Measured internal temperature one hour in -- about
      210°F. Sprayed with water (to restore lost steam from removing
      lid) and put back in oven.
- Opened lid again and checked temperature, about 215°F. Closed lid
  and put back into oven, turned off heat.
- Left in oven to use residual heat for another 5 hours.
    - Longer than necessary -- I just forgot to remove it earlier.

Notes:

- Bread was quite flat (no surprise), fairly dark crust and crumb,
  nice strong flavor. I can taste the rye and also I think some
  caramelization.
- Inside of Dutch oven was very wet, beaded with water. Some rust
  spots, high up. This may be destructive to do too often, or maybe
  letting it cool for so long was a problem.
- Given the amount of moisture remaining in the Dutch oven at the end,
  spraying with water was unnecessary and probably
  counterproductive. It retains water just fine.

## 2022-05-15: Standard, with higher salt for summer

2022-04-04 but with 12 g salt (~2.25 tsp) instead of 10.9 g. Also had
longer rest between mixing the dough and starting to tension it (37
minutes instead of 15) but that's unlikely to matter.

Outcome: Didn't really seem to slow the proof overnight. Dough was
also pretty sticky, although maybe not outside the usual range.

## 2022-06-05: Single-day, warmer weather

2022-04-04, but single-day: Started in morning and used 45-minute
tensioning intervals to account for warmer weather. (Dough relaxes
faster in warmer weather, and yeast is more active as well.)

Proofed 6 hours. Came out great!

Later notes:

- 30 minute intervals work fine too. With 45, sometimes bubbles appear
  in the dough; may be losing rise.

## 2022-07-04: Olive rosemary bread

2022-06-05 with:

- 160 g Kalamata olives (pitted)
- 2.4 g rosemary leaves (about a 5-finger pinch, or maybe 30 leaf-bundles)

Cut olives into thirds and then squeezed handfuls under moderate force
to drive out excess brine. Minced rosemary. Added to dough after
mixing it.

Used ~30 minute tensioning intervals. Proofed 4 hours.

Worked well.

## 2024-04-14: Trifold

Usual winter recipe (2022-04-04) but for the proof, I flattened the
dough out fairly hard, then folded into thirds (both edges in) and
folded again in thirds the other direction. This seemed to be able to
capture gas *much* better—the dough was huge and airy when I went to
bake it, yet didn't collapse much when I transferred it to the
oven.

I forgot to turn down the heat after removing the lid, so this ended
up much darker than I like. The height was good, but not stellar. I'd
like to try and replicate this but bake it a little sooner.

Update: I did another run, and the bread was pretty flat. Not a
technique I'm going to explore further.
