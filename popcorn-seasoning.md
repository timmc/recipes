# Popcorn seasoning

Also see `spice mixes.md` and `consolation-spice.md`.

## 2021-05-19

- 1/4 cup popcorn, air-popped
- liberal application of olive oil
- 1/8 tsp table salt
- 2 tsp carob powder

Very nice!
