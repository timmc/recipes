# Tofu

Tofu preparations, usually as an ingredient for something else.

## 2020-02-17 - Baked tofu

- 800 g extra firm tofu (two 14 oz blocks)
- 2 Tbsp olive oil
- 1 Tbsp sesame oil

- Cut tofu into ~2 cm cubes (for standard grocery store tofu blocks,
  that's 2 x 4 x 5 across)
- Coat with olive and sesame oil
- Spread out on a baking half sheet
- Bake at 400°F for 1 hours, turning cubes several times

Turned out nicely. Could also see adding some salt and pepper.
