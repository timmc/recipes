# moong dal tadka

## Original

From: http://www.vegrecipesofindia.com/moong-dal-tadka/

moong dal tadka recipe - mung dal cooked with onion, tomatoes, ginger
and then tempering with cumin, garlic, green chili and some indian
spice powders.

Author: dassana amit
Serves: 3-4 servings

### Main ingredients:

- ¾ cup moong dal/spilt skinned mung lentils
- 1 medium size onion, finely chopped
- 1 medium size tomato, chopped
- ½ inch ginger/adrak, finely chopped or grated
- ¼ tsp red chili powder/lal mirch powder
- ⅓ tsp turmeric powder/haldi
- 3 cups water
- salt as required

### Ingredients for tempering:

- 1 tsp cumin seeds/jeera
- 4-5 garlic/lahsun, crushed lightly
- ¼ or ½ tsp garam masala powder
- ¼ tsp red chili powder/lal mirch powder
- 1 or 2 green chilli/hari mirch - slit
- a pinch of asafoetida/hing
- 2-3 tbsp oil or ghee or butter

### Instructions

- Put main ingredients (except for salt) in pressure cooker
- Stir well and pressure cook till the dal is cooked and soft.
- Once the pressure settles down, remove the lid and stir the dal.
- If the dal looks thick, then add some water and simmer for 1-2
  minutes. add salt and keep aside.
- In a small pan, heat oil or ghee or butter.
- Fry the cumin seeds.
- Add the garlic and green chili and fry for some seconds.
- Don't brown the garlic. Switch off the flame.
- Now add the garam masala powder, red chili powder and
  asafoetida. Switching off the flame ensures that the spice powders
  don't get burned. You can also fry the spice powders on a low
  flame. Make sure you don't burn them.
- Stir and immediately pour the tempering mixture in the dal.
- Stir the moong dal and serve hot moong dal with steamed rice or
  chapatis.
- The moong dal tastes better as it is and there is no need to garnish
  or add cilantro leaves to it.

## 2014-08-10

Only sort of following the recipe. Triple recipe, but I just eyeballed
the measurements. I replaced the tomato with random vegetables I had
lying around:

- 1.25 pattypan squash, diced
- a few handfuls of different types of green beans
- an apple
- 2 bell peppers

Other notes:

- I also minced the green chili (a jalapeño) instead of slitting it,
  but I only used one, total.
- Frying the jalapeño and garlic took more than a few seconds -- more
  like 2 minutes.
- The dal was mush, more like chowder. That's fine. I don't remember
  how long I cooked it.

On the whole, I'm pretty happy with it! I look forward to trying again
but following more closely.
