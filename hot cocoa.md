# Hot cocoa and hot chocolate

## 2023-01-08

- 14 g Taza 85%
- 2 tsp cold water
- 3 tsp brown sugar
- 4 tsp cocoa powder
- 268 g heated milk

Heated all but milk together, stirred, added heated milk, whisked.

Seemed fine. Not as chocolatey as I might like, but that's my fault
for using cocoa powder.

## 2024-01-01

- 34 g Taza chocolate 85%
- 2 Tbsp cold water
- 2 Tbsp brown sugar
- 2 Tbsp cocoa powder
- 370 g heated milk (2%)

Heated all but milk together, stirred, added heated milk, whisked.

The cocoa powder clumped, which ended up being a big problem -- the
clumps would melt on the outside and then stay that way, not melting
further or dissolving.

Extremely rich -- too much for the kiddo.
