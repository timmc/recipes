# Kimchi

General recipe:

- Mostly cabbage, one leaf reserved as a cap
- Optionally some carrot or maybe radish, scallions, or apple
- Garlic to 5% of cabbage (assuming mostly cabbage)
- Ginger to 2.5% of cabbage
- Salt to 2% of total produce mass
- 1600 g total is a good fit for a 2 qt canning jar, but still expect
  some overflow if it's vigorous.

## 2019-10-20 - Pink kimchi

A mild white kimchi inspired by several sources.

- 1126 g head of napa cabbage
- 1.5 Tbsp small-grain sea salt
- 28 g garlic cloves, finely minced
- 13 g frozen ginger, finely minced
- 17 g apple, finely minced
- 14 g scallion greens, chopped
- 107 g carrot, cut to flat matchsticks
- 170 g purple daikon radish, cut to 3×10×10 mm slabs
- 73 g watermelon radish, cut to 3×10×10 mm slabs

Directions:

- Removed outer leaves of cabbage to wash them, and reserved a few of
  the largest inner leaves for later. Chopped non-reserved leaves 2-3
  cm.
- Tossed chopped cabbage with salt and waited for it to wilt and produce
  liquid (about 8 hours)
- Combined all ingredients (except reserved leaves) in quart jar and
  packed down below the brine that the salt and cabbage has formed
- Used reserved leaves to form a cap to the other ingredients, holding
  them under the brine.
- Covered and let sit at room temperature for 6 days (bubbling mostly
  stopped after 5 days), then refrigerated

Notes:

- Started bubbling after about 12 hours
- Might consider using more salt to reduce chances of pre-fermentation
  spoilage, although it's at a fine level for flavor
- The cabbage turned pink due to the radishes
- Flavor is good, although less acidic than I am used to. I did use
  far less apple than was called for, and I suspect that the apple's
  sugar is important for the production of lactic acid.

## 2020-07-02

Similar to 2019-10-20, but used blender to puree garlic, ginger,
apple, and salt. Salt was not mixed with cabbage before other
ingredients, but instead all were tossed together to let wilt.

- 1.6 Tbsp small-grain sea salt
- 29 g garlic cloves
- 26 g apple
- 18 g frozen ginger
- 850 g napa cabbage leaves (kinda wet), chopped 15 to 25 mm
- Two squarish cabbage leaf ends (25 g) reserved for capping the jar
- 403 g carrot, cut to flat matchsticks
- 200 g purple daikon radish, cut to flat matchsticks

Directions:

- Pureed apple, ginger, garlic, and salt
- Tossed puree with chopped vegetables (cabbage, carrot, radish)
- Waited for cabbage to wilt and brine to form, about 8 hours (tossed
  occasionally)
- Moved into 2L glass jar and packed until brine covered and air
  bubbles removed
- Pushed reserved leaves down into brine to keep pieces from floating to
  the surface and molding
- Placed an empty small glass jar on top of leaves as a weight,
  covered with a plastic bag to keep lid from corroding, and screwed
  lid on loosely
    - This was a bad idea -- it overflowed a little overnight. Make
      sure there's enough headroom in the jar.
- Let sit until it stops bubbling, maybe a week

## 2021-07-14: Minimalist kimchi

Salt 2% of vegetables; ginger and garlic each about 2.5% of cabbage.

- 967 g Napa cabbage, diced 5-8 mm
- 45 g garlic, minced
- 24 g frozen ginger, minced
- 21 g iodized table salt

Mixed and let wilt overnight, then packed into jars the next day.

Sampled 2021-08-24. A perfectly acceptable kimchi.

## 2021-10-24: With purple-skinned radish

Ginger and garlic to 3% of cabbage, salt to 2% of vegetables.

(All produce was locally grown -- Massachusetts or New Hampshire.)

- 1508 g Napa cabbage, diced 6 mm, + 37 g cap leaf
- 45 g young ginger, diced 2 mm
- 45 g garlic (elephant garlic? huge cloves), crushed
- 208 g dark-purple-skinned radish, cut into matchsticks 2x2x15 mm
- 37 g iodized sea salt

Diverted some to a small jar with a serrano pepper slit open.

## 2022-09-10

Based on 2021-07-14, 1.3x and with some carrots.

Salt 2% of vegetables; garlic ~5% of cabbage; ginger ~2.5% of cabbage.

- 1288 g Napa cabbage, diced 5-8 mm (with a leaf reserved)
- 278 g carrot, shredded
- 61 g garlic, minced
- 32 g frozen ginger, minced
- 33 g iodized table salt

Mixed and let wilt overnight, then packed into jars the next day.

## 2022-01-06

Salt 2% of vegetables; garlic 5% of cabbage; ginger 2.5% of cabbage.
Substantional amount of radish this time.

Used glass fermentation weights for the first time in this batch.

- 805 g Napa cabbage, diced 5-10 mm (with a leaf reserved)
- 181 g carrot, shredded
- 41 g garlic, crushed
- 21 g frozen ginger, minced
- 5 g scallion greens, chopped
- 450 g watermelon radish, cut 2×2×12 mm
- 72 g leek greens, sliced 2×15 mm strips
- 32 g iodized table salt

## 2024-04-01 - with green cabbage

Targeted: Garlic 3% of bulk vegetables; ginger 1.5% of bulk
vegetables; salt 2% of all vegetables. Total 1700 g
ingredients. Originally had about 1600 g of ingredients, but I wanted
to fill the jar more, so I added 100 g more cabbage and 2 g more salt
(resulting in the above quantities). This meant I had a bit less
garlic and ginger than intended.

- 1200 g Napa cabbage, diced 5-10 mm (with a leaf reserved)
- 400 g carrot, shredded
- 45 g garlic, crushed
- 22 g frozen ginger, minced
- 33 g iodized table salt

Used a glass fermentation weight on top of a leaf.
