# Roasted chickpeas

2020-03-30 is the recipe to use.

## 2020-02-15

This one started out with me trying to use a modified baking soda
method to cook the chickpeas after soaking them, but before they got
to boiling I decided to just roast them, since that would probably
cook them well enough anyhow.

Don't use this one.

Ingredients:

- 400 g dried chickpeas (2 cups)
- 1 tsp baking soda
- 35 g olive oil
- ~1/2 tsp salt
- 5 tsp Consolation Spice (see other recipe file)

Process:

- Soaked chickpeas 24 hours
- Drained, rinsed, and drained again
- Set chickpeas and baking soda in cookpot over high heat, stirring
  continuously, until residual water was driven off and sizzling
  started (about 4 minutes)
- Immediately removed from heat and added water. Stirred in the water
  for about a minute to wash off the baking soda, drained, then rinsed
  and drained again twice.
- Spread in single layer on baking tray (fit just perfectly on a half sheet
  pan)
- Baked 30 minutes at 375°F (190°C), shaking/stirring occasionally, to
  dry and begin to dry roast (about 30 minutes)
- Tossed with olive oil and salt in big bowl
- Baked again until golden and most were crispy or at least not very
  chewy, about 50 minutes
- Tossed in same bowl to pick up remaining oil and salt, and then with
  spices (about 5 tsp of Consolation Spice)
- Allowed to cool in oven, with door cracked open, to retain crispness
- Stored in air-tight container

Notes:

- Kind of unpleasantly hard and crunchy, though still edible. Next
  time, turn off the heat *before* they reach the right texture, since
  they will continue to dry out as they cool in the oven.
- Good flavor
- ...but I'm pretty sure this gave me bad gas, so next time let's go
  with boiling and then roasting

## 2020-03-30

This time, boiling the chickpeas first, with a heated abrasion step
between soaking and boiling.

Using corn oil because we're trying to conserve olive oil during
self-isolation.

Ingredients:

- 380 g dried chickpeas (2 cups)
- 31 g corn oil
- ~1/2 tsp salt
- 4 tsp Consolation Spice (see other recipe file)

Process:

- Soaked chickpeas 11 hours
- Drained, then set in cookpot over high heat, stirring continuously,
  until residual water was driven off and sizzling started (about 4
  minutes)
- Immediately added water to cover, then brought to a boil until 45
  minutes from addition of water
- Let sit in hot water for an hour or so (I was off doing other things)
- Spread in single layer on baking tray (just barely fit on a half
  sheet pan)
- Baked 30 minutes at 350°F to dry and begin to dry roast
- Tossed with oil and salt in big bowl
- Baked 30 minutes, stirred (outside starting to toast now)
- Baked 17 minutes, stirred (outside good, inside still mealy, but less moist)
- Baked 23 minutes -- now crunchy and light, golden in color
- Tossed with spices
- Stored in air-tight container

Notes:

- Nice flavor
- Great texture, relatively delicate crunch
- Very dry, although there's nothing to be done about that and still
  have them be crispy

Later notes:

- Upper rack (2nd from top) works well for this

## 2020-04-06

2020-03-30 but doubled the oil.

Ingredients:

- 380 g dried chickpeas (2 cups)
- 60 g corn oil
- ~1/2 tsp salt
- 4 tsp Consolation Spice (see other recipe file)

Process:

- Soaked chickpeas 12-18 hours
- Drained, then set in cookpot over high heat, stirring continuously,
  until residual water was driven off and sizzling started (about 4
  minutes)
- Immediately added water to cover, then brought to a boil until 45
  minutes from addition of water
- Let sit in hot water for an hour or so
- Spread in single layer on baking tray (just barely fit on a half
  sheet pan)
- Baked 30 minutes at 350°F to dry and begin to dry roast
- Tossed with oil and salt in big bowl
- Baked 40 minutes, stirred (outside starting to toast now)
- Baked 20 minutes (slightly moist inside, but
  overall crunchy)
    - If you want it crunchy all the way through, stir and bake for 10
      more minutes
- Tossed with spices
- Stored in air-tight container
