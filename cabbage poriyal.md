# Cabbage Poriyal

Also check out thoran, a similar dish.

Based on: http://www.indianfoodforever.com/cabbage/cabbage-poriyal.html

## Original

Ingredients:

- 3 tbsp oil
- 2 tsp mustard seeds
- 1/2 tsp urad dal
- 6-7 dried curry leaves
- 1 green chili, minced
- 1 onion, diced
- 1/2 tsp turmeric powder
- 1 small sized cabbage, chopped
- 1 tsp salt
- 1/4 cup water
- 1/4 cup fresh grated coconut

Procedure:

- Heat oil in a pan at medium-hot.
- Add mustard seeds and urad dal and let it splutter. Don't let it
  brown much.
- Add curry leaves, green chili, and onion. Fry till golden brown and
  then mix in turmeric powder.
- Add chopped cabbage and salt and mix well.
- Add 1/4 cup water and cook covered on low for 5 mins.
- Once cabbage is done add grated coconut and stir.
- Serve hot

## 2014-08-10

Major modifications.

- Instead of 1/2 tsp urad dal, I used about a cup of moong dal. (I
  know, I know, different thing.)
- Instead of slitting the chilis, I minced them.
- I used dried curry leaves, not fresh.
- I used "reduced fat" (whatever that means) shredded coconut instead
  of fresh coconut.

It turned out well nonetheless!

(Update: I make this *all the time* and haven't appreciably changed
anything else about the recipe, except eventually getting my hands on
some urad dal—and using way more mustard seed, urad dal, and coconut
than the recipe calls for.)

## 2020-05-29

Larger batch than usual. Added curry leaves after turmeric because I
forgot to add them earlier.

- 3 Tbsp olive oil
- 4 tsp mustard seeds
- 2 tsp urad dal
- 2 small compound curry leaf (about size of 4 mature leaflets)
- 1 frozen jalapeño, minced (with seeds)
- 2 onions, diced (maybe 400 g?)
- 1/2 tsp turmeric powder
- 1000 g cabbage, chopped small (a few very small cabbages)
- 1.5 tsp salt
- 80 g water (a little over 1/4 cup)
- 1 cup fresh grated coconut
