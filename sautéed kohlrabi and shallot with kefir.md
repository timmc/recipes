Originally "Sauteed Kohlrabi with Onions and Cream"
from http://www.marthastewart.com/354689/sauteed-kohlrabi-onions-and-cream

# 2014-02-02

- Dice kohlrabi and shallots
- Sauté in cultured butter over medium-high heat until almost tender
- Add a good splash of whole goat's milk kefir and cook for a minute, stirring, to reduce and thicken
- Season with salt, pepper, and a very small amount of nutmeg
