# Chilaquiles

Or, Mexican casserole. :-)

## 2015-04-22

Based on
http://pickyeaterblog.com/healthy-vegetarian-black-bean-chilaquiles/
but leaves out onions, onion powder, ancho chili powder (didn't have),
and chili powder (might contain onion). Subs garlic for garlic powder
and pureéd tomatoes for tomato sauce and vegetable broth.

### Ingredients

For sauce:

- 2 28 oz cans whole peeled tomatoes
- 5 cloves garlic
- 1 tsp ground cumin
- 2 tsp ground chipotle
- 1 tsp paprika
- 2 tsp dried oregano

For main body:

- Olive oil
- Red bell pepper, diced
- Frozen corn
- ~12 chard stems, chopped
- Tomato, diced
- Black turtle beans soaked and cooked with epazote and salt
- 1 tsp ground cumin
- 1/2 tsp salt

Additional:

- Olive oil
- ~15 small/medium corn tortillas
- Block of cheddar cheese, grated
- 2 more tomatoes, diced
- Handful of scallions, chopped (the green part)

Optional:

- Sour cream
- Chopped cilantro (leaves)

### Recipe

- Preheat oven 400°F, prep 9"x13" (4 qt) baking pan
- Mix all sauce ingredients and simmer
- Fry tortillas on thin layer of oil
- Heat corn and sauté bell pepper in oil, then mix in chard, tomato,
  beans, cumin, and salt and cook through
- Rip tortillas into quarters and lay half into baking pan
- Layer on half of cooked veggies
- Pour in half of sauce
- Sprinkle on half of cheese
- Make second layer, same way
- Cook 15 minutes covered, 10 minutes uncovered until cheese is melted
  and starting to brown and liquid is bubbling up the sides
- Top with remaining diced tomato and scallion
- Optional: Serve with sour cream and cilantro

## 2015-08-24

Doubled and split into two dishes, the first with garlic and hot
pepper and the second without. I may have slightly boosted some other
spices in the second dish. :-)

I replaced half the ground chipotle with ground ancho because I wanted
to reduce the heat and also had some ancho this time.

Sauce:

- 4 28oz cans crushed tomatoes w/ basil
- 5 cloves chopped garlic (in dish #1)
- 4 tsp dried oregano
- 2 tsp Hungarian sweet paprika
- 2 tsp ground cumin
- 1 tsp ground chipotle (in dish #1)
- 1 tsp ground ancho (in dish #1)

Filling:

- Olive oil
- 4 cups frozen corn
- most of a bok choy, chopped
- 2-3 cups chopped green beans
- 4 15.5oz cans black beans
- 1 large tomato, diced
- 2 tsp ground cumin

Additional:

- Olive oil
- 16-20 small/medium corn tortillas
- 2 2/3 blocks cheddar, grated
- 3 large tomatoes, diced
- handful of tops of scallions/green onions, chopped

Made in a rush towards the end, quantities may be sloppy. I didn't
make enough filling (by about 25%) and I should have had more cheese.

Heat level was good in the garlic + hot peppers dish. Maybe go with 3
tsp hot peppers for that dish next time.
