# Spice mixes

## 2015-11-26: Chili powder w/ no onion

- 1/2 tsp salt
- 1/2 tsp garlic powder
- 2 tsp cumin powder
- 2 tsp anaheim chili powder
- 1/8 tsp chipotle chili powder

## 2020-11-11: Popcorn za'atar plus

- 2 tsp nutritional yeast
- 1 tsp thyme
- 1/2 tsp sumac
- 3/8 tsp salt
- sprinkle of cayenne

Grind to powder.

## 2020-11-16: Popcorn za'atar

- 1 tsp black sesame
- 1 tsp thyme
- 1/2 tsp sumac
- 1/2 tsp salt

Grind to powder.

Overly salty, maybe because the salt was ground up.

## 2020-11-22: Popcorn za'atar

Reducing salt. Ran out of sesame, or would have used more.

- 1 heaping tsp black sesame
- 2 tsp thyme
- 1 tsp sumac
- 1/4 tsp salt

Much better!
