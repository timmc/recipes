# Alex's gluten-free skillet pizza

A deep-crusted pizza to be made in a cast iron skillet (or in later
variations, a baking sheet).

Current best recipes:

- 2022-07-11 quickbread (baking soda and cream of tartar) in a skillet
- 2023-03-24 sourdough on a baking sheet (especially the listed variations)

## 2022-06-08

Based on a recipe Alex developed.

Oregano is miscategorized here as a fresh/delicate spice (second stage
topping) but it seems to work either way.

### Ingredients

Tomato sauce:

- Olive oil (3 Tbsp?)
- 36 g garlic cloves, minced (3 large cloves)
- 1/2 tsp oregano
- 1 cup canned crushed tomatoes
- 1 thick 1-finger pinch of salt (pinch between forefinger and thumb)

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 205 g (about 1 and 1/2 cups) millet flour
- 1 tsp (~7 g) salt
- 1.5 tsp xanthan gum
- 1/2 tsp cream of tartar
- 1/4 tsp baking soda
- 1 egg (48 g)
- 179 g water
- 53 g (1/4 cup) olive oil

Additional millet used in forming dough to make it less sticky: 118 g

Baked toppings:

- 8 oz cheddar, shredded
- Half jar of pitted kalamata olives, halved

Fresh/delicate toppings:

- A few pinches of oregano
- Various weeds, chopped up (chickweed, broad-leaf plantain, violet)

### Directions

Sauce:

- Sauté garlic in olive oil until slightly transparent
    - 8" cast iron skillet worked well for this
- Stir in oregano, let cook for a few more seconds, then add tomatoes
  and a pinch of salt
- Stir and reduce heat to let simmer, stirring occasionally while
  preparing and cooking dough.
- Cook until sauce is reduced and thickened

Dough:

- Start oven preheating to 275°F; rack in upper position (2nd to top)
- Combine dry ingredients and mix well (sift if needed to break up
  clumps, especially in baking soda)
- Add wet ingredients and form into ball
- On countertop, mix additional millet flour into dough until not
  excessively sticky.
- Add about 1 tablespoon of olive oil to center of 12" cast iron skillet
- Place ball of dough onto center of oil puddle and add maybe 1 tsp of
  oil on top to reduce stickiness
- Repeatedly flatten dough, working from center out, until it reaches
  the edges of the skillet, adding more oil as needed. Ensure the
  surface is not too bumpy.

Pizza:

- Once oven reaches temperature, bake in skillet for 15 minutes
    - This should be enough time to prep the first set of toppings.
- Remove skillet and set oven to 475°F
- Spread sauce all the way to the edge
- Evenly distribute cheese, olives, any other "robust" toppings
- Bake pizza for 20 minutes more or until cheese is browning well
    - It's OK to start before the oven has reached its new
      setpoint. Today, the oven had only reached 365°F and it took 6
      more minutes to reach 475°F.
- Remove skillet and add greens, oregano, any other delicate toppings
  while cheese is still sizzling.
- May want to let cool a little before cutting with sharp spatula.

### Notes

- 8 oz cheese might be a *little* excessive. It's basically "extra
  cheese", and isn't too thick, but you could probably get away with
  just 6 oz.
- Pressing on the dough after flouring it increases the
  stickiness. The gentle folding in the original recipe avoids
  this. However, I think the entire step can be skipped in the future
  by mixing the right amount of millet into the dough in the bowl,
  then dropping the ball directly into the skillet. Alex thinks that
  she might have used 1/4 cup millet flour, so split the difference
  and try 75 g additional millet next time?

## 2022-06-13

2022-06-08, with simplification of dough steps (straight from bowl to
skillet) and various experiments, mostly adjusting quantities:

- Less millet (halfway between original and 2022-06-13, probably)
- 30% less garlic; I think I overshot the original recipe last time
- Mix of sesame oil and olive oil
- 25% more tomato
- A larger egg
- 75% of cheese
- Guidance on reduced timing on 2nd bake

### Ingredients

Tomato sauce:

- Sesame and olive oil (3 Tbsp?)
- 27 g garlic cloves, minced (3 large cloves)
- 1/2 tsp oregano
- 320 g (1.25 cups) canned crushed tomatoes
- 1 thick 1-finger pinch of salt (pinch between forefinger and thumb)

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 280 g (~2 cups) millet flour
- 1 tsp (~7 g) salt
- 1/2 tsp cream of tartar
- 1/4 tsp baking soda
- 1.5 tsp xanthan gum
- 1 egg (57 g)
- 180 g water
- 53 g (1/4 cup) olive oil

Baked toppings:

- 6 oz cheddar, shredded
- 50 g (1/3 jar?) pitted kalamata olives, halved

Fresh/delicate toppings:

- A few pinches of oregano (1/2 tsp)
- Various weeds, chopped up (chickweed, broad-leaf plantain, violet)

### Directions

Sauce:

- Sauté garlic in olive oil until slightly transparent
    - 8" cast iron skillet worked well for this
- Stir in oregano, let cook for a few more seconds, then add tomatoes
  and a pinch of salt
- Stir and reduce heat to let simmer, stirring occasionally while
  preparing and cooking dough.
- Cook until sauce is reduced and thickened

Dough:

- Start oven preheating to 275°F; rack in upper position (2nd to top)
- Combine dry ingredients and mix well (sift if needed to break up
  clumps, especially in baking soda), then add wet ingredients and
  form into ball.
- Add about 1 tablespoon of olive oil to center of 12" cast iron skillet
- Dump out ball of dough onto center of oil puddle and add maybe 1 tsp of
  oil on top to reduce stickiness
- Repeatedly flatten dough, working from center out, until it reaches
  the edges of the skillet, adding more oil on top as needed. Ensure
  the surface is not too bumpy.

Pizza:

- Once oven reaches temperature, bake in skillet for 15 minutes
    - This should be enough time to prep the first set of toppings.
- Remove skillet and set oven to 475°F
- Spread sauce all the way to the edge
- Evenly distribute cheese, olives, any other "robust" toppings
- Bake pizza until cheese is browning well, 15-20 minutes. (It's OK to
  start before the oven has reached its new setpoint.)
- Remove skillet and add oregano, greens, any other delicate toppings
  while cheese is still sizzling.
- May want to let cool a little before cutting with sharp spatula.

### Notes

- More rise of the dough this time; probably captured more CO2 from
  the baking soda by moving the dough into the skillet faster.
- Try putting the oregano onto the cheese after the first bake, and
  check if it burns. (Probably won't.)
- Cheese still covered the pizza just fine at 6 oz instead of 8.
- Crust was fine.
- Less savory than last time, probably because of reduced cheese and
  garlic. (More tomato too, but I cook it down pretty thick, so that
  might not matter.)
- Next time:
    - Go back to 1 cup of tomato
    - Go back to larger amount of garlic
    - Keep 6 oz cheese to see if we can continue with that amount and
      still have good savoriness.
    - Maybe experiment with other umami sources. Tempeh bacon would
      probably be amazing.

## 2022-07-11

2022-06-13's simpler dough directions but back to larger amounts of
tasty things, as in 6/8.

Accidentally increased olive oil in dough and did a ~12 minute first
bake instead of 15.

Trying tempeh crumbles and basil as additional toppings.

### Ingredients

Tomato sauce:

- Sesame and olive oil (3 Tbsp?)
- 36 g garlic cloves, minced (~4 large cloves)
- 1/2 tsp oregano
- 264 g (1 cup) canned crushed tomatoes
- 1 thick 1-finger pinch of salt (pinch between forefinger and thumb)

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 280 g (~2 cups) millet flour
- 1 tsp (~7 g) salt
- 1/2 tsp cream of tartar
- 1/4 tsp baking soda
- 1.5 tsp xanthan gum
- 1 egg (49 g)
- 180 g water
- 60 g (~1/4 cup) olive oil

Baked toppings:

- 8 oz cheddar, shredded
- 50 g (1/3 jar?) pitted kalamata olives, halved
- Half of a batch of tempeh crumbles (8 oz tempeh, diced small,
  sautéed until browning in olive and toasted sesame oil, then
  splashed with 2 parts soy sauce, 1 part balsamic vinegar, and a
  squirt of liquid smoke; sautéed further until dry.)

Fresh/delicate toppings:

- A few pinches of oregano (1/2 tsp)
- Basil
- Various weeds, chopped up (chickweed, broad-leaf plantain, violet)
- Other half of tempeh crumbles

### Directions

Sauce:

- Sauté garlic in olive oil until slightly transparent
    - 8" cast iron skillet worked well for this
- Stir in oregano, let cook for a few more seconds, then add tomatoes
  and a pinch of salt
- Stir and reduce heat to let simmer, stirring occasionally while
  preparing and cooking dough.
- Cook until sauce is reduced and thickened

Dough:

- Start oven preheating to 275°F; rack in upper position (2nd to top)
- Combine dry ingredients and mix well (sift if needed to break up
  clumps, especially in baking soda), then add wet ingredients and
  form into ball.
- Add about 1 tablespoon of olive oil to center of 12" cast iron skillet
- Dump out ball of dough onto center of oil puddle and add maybe 1 tsp of
  oil on top to reduce stickiness
- Repeatedly flatten dough, working from center out, until it reaches
  the edges of the skillet, adding more oil on top as needed. Ensure
  the surface is not too bumpy.

Pizza:

- Once oven reaches temperature, bake in skillet for 12 minutes
    - This should be enough time to prep the first set of toppings.
- Remove skillet and set oven to 475°F
- Spread sauce all the way to the edge
- Evenly distribute cheese, olives, any other "robust" toppings
- Bake pizza until cheese is browning well, 15-20 minutes. (It's OK to
  start before the oven has reached its new setpoint.)
- Remove skillet and add oregano, greens, any other delicate toppings
  while cheese is still sizzling.
- May want to let cool a little before cutting with sharp spatula.

### Notes

Didn't record notes at the time, but I think it turned out well. I
vaguely recall that the tempeh bacon ended up dominating the flavor.

## 2022-10-16 - Sourdough

Same sauce and dough as 2022-07-01 but using 250 g of millet sourdough
starter (100% hydration) instead of some of the flour and water. No
baking soda or cream of tartar.

### Ingredients

Tomato sauce:

- 3 Tbsp olive oil
- 36 g garlic cloves, minced (~4 large cloves)
- 1/2 tsp oregano
- 257 g (1 cup) canned crushed tomatoes
- 1 thick 1-finger pinch of salt (pinch between forefinger and thumb)

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 155 g millet flour
- 1 tsp (~7 g) salt
- 1.5 tsp xanthan gum
- 1 egg (53 g)
- 250 g sourdough starter (millet at 100% hydration)
- 55 g water
- 60 g (~1/4 cup) olive oil

Baked toppings:

- 8 oz cheddar, shredded
- 50 g (1/4 jar) pitted kalamata olives, halved

Fresh/delicate toppings:

- A few pinches of oregano (1/2 tsp)
- Basil
- Some fresh greens, chopped up (evening primrose, tomato)

### Directions

Dough:

- Combine dry ingredients and mix well (sift if needed to break up
  clumps), then add wet ingredients and form into ball.
- Add about 1 tablespoon of olive oil to center of 12" cast iron skillet
- Dump out ball of dough onto center of oil puddle and add maybe 1 tsp of
  oil on top to reduce stickiness
- Repeatedly flatten dough, working from center out, until it reaches
  the edges of the skillet, adding more oil on top as needed. Ensure
  the surface is not too bumpy.
- Let rise for about 2 hours. In the meanwhile, make sauce and prepare
  toppings.

Sauce:

- Sauté garlic in olive oil until slightly transparent
    - 8" cast iron skillet worked well for this
- Stir in oregano, let cook for a few more seconds, then add tomatoes
  and a pinch of salt
- Stir and reduce heat to let simmer, stirring occasionally while
  preparing and cooking dough.
- Cook until sauce is reduced and thickened

Pizza:

- Place rack in upper position (2nd from top) and heat oven to
  275°F. Bake in skillet for 12 minutes.
- Remove skillet and set oven to 475°F
- Spread sauce all the way to the edge
- Evenly distribute cheese, olives, any other "robust" toppings
- Bake pizza until cheese is browning well, 15-20 minutes. (It's OK to
  start before the oven has reached its new setpoint.)
- Remove skillet and add oregano, greens, any other delicate toppings
  while cheese is still sizzling.
- May want to let cool a little before cutting with sharp spatula.

### Notes

- Dough is very pleasant, slightly flaky, not too dense.
- This is still using the higher oil (60 instead of 53 g) and shorter
  first bake (12 instead of 15 minutes) of 2022-07-11. Could
  experiment with reverting one or both.
- Should try putting oregano in as a first-stage topping next time
  (which I think has worked well in the past.)

## 2022-12-25 - Sourdough, some starch, baking sheet

2022-10-16, but substituting tapioca starch in for 25% v/v of the
total millet flour (including what's in the starter) to see if there's
more rise. Moved oregano from second-stage to first-stage toppings;
increased olives and oregano a little. Spread on baking sheet instead
of in skillet.

### Ingredients

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 85 g millet flour
- 48 g tapioca starch
- 1 tsp (~7 g) salt
- 1.5 tsp xanthan gum
- 1 egg (50 g)
- 243 g sourdough starter (millet at 100% hydration)
- 55 g water
- 62 g (~1/4 cup) olive oil, plus some for spreading dough
    - Aimed for 60 g

Tomato sauce:

- 3 Tbsp olive oil
- 35 g garlic cloves, minced (~6 medium cloves)
- 1/2 tsp oregano
- 260 g (1 cup) canned crushed tomatoes
- 1 thick 1-finger pinch of salt (pinch between forefinger and thumb)

Baked toppings:

- 8 oz cheddar, shredded
- 70 g (1/4 jar) pitted kalamata olives, halved
- A sprinkling of oregano (3/4 tsp)

Fresh/delicate toppings:

- Some fresh greens, chopped up

### Directions

Dough:

- Combine dry ingredients and mix well (sift if needed to break up
  clumps), then add wet ingredients and form into ball.
- Add about 1 tablespoon of olive oil to center of baking half-sheet.
- Dump out ball of dough onto center of oil puddle and add maybe 1 tsp of
  oil on top to reduce stickiness
- Repeatedly flatten dough, working from center out, until it reaches
  the edges of the pan or as far as possible, adding more oil on top
  as needed. Ensure the surface is not too bumpy.
- Let rise for about 2 hours. In the meanwhile, make sauce and prepare
  toppings.

Sauce:

- Sauté garlic in olive oil until slightly transparent
    - 8" cast iron skillet worked well for this
- Stir in oregano, let cook for a few more seconds, then add tomatoes
  and a pinch of salt
- Stir and reduce heat to let simmer, stirring occasionally while
  preparing and cooking dough.
- Cook until sauce is reduced and thickened

Pizza:

- Place rack in upper position (2nd from top) and heat oven to
  275°F. Bake for 12 minutes.
- Remove baking sheet and set oven to 475°F
- Spread sauce all the way to the edge
- Evenly distribute cheese, olives, any other "robust" toppings
- Bake pizza until cheese is browning well, 15-20 minutes. (It's OK to
  start before the oven has reached its new setpoint.)
- Remove from oven and add oregano, greens, any other delicate toppings
  while cheese is still sizzling.
- May want to let cool a little before cutting with sharp spatula.

### Notes

- Crust was very thin and just barely reached all the edges. Could
  increase dough by 1.5x or even 2x, but it's actually pretty filling
  even now.
- How to make it more like restaurant pizza? Perhaps move some salt
  from the crust to the sauce, or just reduce salt. One garlic clove
  in sauce instead of 6? Mozarella or other cheeses.
    - Probably restaurants add sugar to their sauce...

## 2022-12-26 - Sourdough, some starch, baking sheet, less salt

2022-12-25 but with less salt and garlic. Moved some salt from crust to sauce.

### Ingredients

Dough:

- 27 g (1/4 cup) ground flax seed
- 69 g (1/4 cup) cornmeal
- 85 g millet flour
- 48 g tapioca starch
- 1/4 tsp salt
- 1.5 tsp xanthan gum
- 1 egg (50 g)
- 240 g sourdough starter (millet at 100% hydration)
- 55 g water
- 60 g (~1/4 cup) olive oil, plus some for spreading dough

Tomato sauce:

- 3 Tbsp olive oil
- 6 g garlic cloves, minced (1 medium clove)
- 1/2 tsp oregano
- 260 g (1 cup) canned crushed tomatoes
- 1/4 tsp salt

Baked toppings:

- 8 oz cheddar, shredded
- 70 g (1/4 jar) pitted kalamata olives, halved
- A sprinkling of oregano (3/4 tsp)

Fresh/delicate toppings:

- Some fresh greens, chopped up

### Notes

- Yes, more like a "restaurant pizza"!
- Might try doubling the crust. It's so thin on a baking sheet...

## 2023-01-22 - Baking sheet with less thin crust

2022-12-26 with doubled crust and sauce. Starter was not doubled
(unintentionally) so additional water and millet were added to
compensate. Also increased olives by about 50%, and added
pineapple. Used top rack of oven rather than upper (2nd) rack.

Still using directions of 2022-12-25. Upgraded to ~10" skillet.

### Ingredients

Dough:

- 54 g (1/2 cup) ground flax seed
- 138 g (1/2 cup) cornmeal
- 290 g millet flour
- 96 g tapioca starch
- 1/2 tsp salt
- 3 tsp xanthan gum
- 2 eggs (111 g)
- 235 g sourdough starter (millet at 100% hydration)
- 230 g water
- 120 g (~1/2 cup) olive oil, plus some for spreading dough

Tomato sauce:

- 6 Tbsp olive oil
- 12 g garlic cloves, minced (2 medium clove)
- 1 tsp oregano
- 520 g (2 cup) canned crushed tomatoes
- 1/2 tsp salt

Baked toppings:

- 8 oz cheddar, shredded
- 108 g (~1/2 jar) pitted kalamata olives, halved
- 120 g pineapple chunks
- A sprinkling of oregano (3/4 tsp)

Fresh/delicate toppings:

- None

### Notes

- Pretty nice! More filling. Seems like *more* than 2x the dough and
  sauce, somehow?

## 2023-01-27 - Reducing crust agin

2023-01-22 aiming for 0.75x crust and 1.25x cheese (and different
toppings). Still using 2 eggs, so removing 27 g of water after
scaling. Accidentally used 1x starter rather than 0.75x.

Still using directions of 2022-12-25 with ~10" skillet for sauce.

### Ingredients

Dough:

- 40 g ground flax seed
- 104 g cornmeal
- 218 g millet flour
- 72 g tapioca starch
- 3/8 tsp salt
- 2.25 tsp xanthan gum
- 2 eggs (112 g)
- 237 g sourdough starter (millet at 100% hydration)
- 145 g water
- 90 g olive oil, plus some for spreading dough

Tomato sauce:

- 6 Tbsp olive oil
- 12 g garlic cloves, minced (2 medium clove)
- 1 tsp oregano
- 520 g (2 cup) canned crushed tomatoes
- 1/2 tsp salt

Baked toppings:

- 10 oz cheddar, shredded
- 108 g (~1/2 jar) pitted kalamata olives, halved
- fried tempeh: Most of 2 8oz packages, cubed, marinated in 2 tsp soy
  sauce and 0.5 tsp balsamic vinegar, fried in olive and toasted
  sesame oil
- A sprinkling of oregano (~1 tsp)

Fresh/delicate toppings:

- chopped komatsuna

### Notes

- This seems like a reasonable amount of crust. Tender and a bit flaky
  (i.e. not brick-like) but substantial.
- Cheese and sauce seem about right as well, from a quantity
  perspective.
- Should explore reducing the amount of olive oil in the sauce --
  probably excessive, deriving from an overestimate of an earlier
  version's unmeasured quantity.

## 2023-03-24 - Reducing olive oil

2023-01-27 but with 51 g oil for the dough instead of 90 g (was aiming
for 45) and 3 Tbsp of oil for the sauce instead of 6. Inadvertantly
increased tomatoes from 520 g to 548 g.

Using directions of 2022-12-25 with ~10" skillet for sauce... except I
put the oregano in after the tomato sauce. Whoops.

### Ingredients

Dough:

- 40 g ground flax seed
- 104 g cornmeal
- 218 g millet flour
- 72 g tapioca starch
- 3/8 tsp salt
- 2.25 tsp xanthan gum
- 2 eggs
- 237 g sourdough starter (millet at 100% hydration)
- 145 g water
- 51 g olive oil, plus some for spreading dough

Tomato sauce:

- 3 Tbsp olive oil
- 12 g garlic cloves, minced (2 medium clove)
- 1 tsp oregano
- 548 g canned crushed tomatoes
- 1/2 tsp salt

Baked toppings:

- 10 oz cheddar, shredded
- 108 g (~1/2 jar) pitted kalamata olives, halved
- A sprinkling of oregano (~1 tsp)

Fresh/delicate toppings:

- chopped komatsuna

### Notes

- Worked well. Crust still seems just fine. Could likely reduce oil
  more, if desired.
- Later variations that worked out great: 15 minute crust bake; 195 g pesto
  instead of tomato sauce; halved cherry tomatoes as a topping.
- I usually double this recipe and make two pizzas.
- Allow for about 4 hours elapsed prep time.
