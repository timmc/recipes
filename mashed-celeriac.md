# Mashed celeriac (or puree)

## 2017-01-12 - Vegan puree

Based on an Alton Brown recipe, but 2x and minus the dairy.

http://www.foodnetwork.com/recipes/alton-brown/celeriac-puree-recipe.html

- 3-4 tbsp olive oil
- 2.4 kg celeric (3 large; 1.8 kg once trimmed and any spongy parts
  removed)
- 37 g garlic (7 medium to large cloves)
- 2 tsp large-grain salt
- 1/2 tsp ground black pepper
- 10 cups water

Roughly chop garlic, cut celeriac into 2-3 cm dice. Heat oil in an 8L
or larger pot, add celeriac, garlic, salt, and pepper. Stir
periodically on medium heat for 5-10 minutes, then add 10 cups
water. Bring to boil and cover, cook until celeriac is tender.

Drain the celeriac (reserve liquid as stock) and puree.

Tastes fluffy and smooth. I bet adding too much oil and not draining
too thoroughly contributed to that. :-)

## 2020-12-23 - Non-vegan

Again based on the Alton Brown recipe, but this time following it a
bit more closely. No heavy cream, so using whole-milk kefir.

https://www.foodnetwork.com/recipes/alton-brown/celeriac-puree-recipe-1939419

- 1.36 kg celeriac (9 med/small, 965 g trimmed)
- 30 g garlic (sprouting
- 1 Tbsp olive oil
- 1 tsp fine sea salt
- 5 cups water
- 2 Tbsp unsalted butter
- 1/4 cup kefir

- Trim and peel celeriac. Cut into ~1 cm dice.
- Heat the olive oil in a 4-liter saucepan over low heat just until it
  shimmers. Add the celeriac, garlic, salt and pepper and cook,
  stirring frequently, just until it begins to soften, approximately 5
  minutes.
- Increase the heat to medium-high and add the water. Bring to a boil
  and cook until the celeriac is tender and easily pierced with a
  fork, approximately 20 minutes.
- Drain the celeriac through a colander, reserving liquid as stock,
  and return to the pot. Using a stick blender, puree until no lumps
  are present, approximately 1 minute.
- Place the butter and heavy cream into a microwave proof bowl and
  heat just until the butter is melted, approximately 45 seconds. Add
  the cream and butter and continue to puree with the stick blender
  for another minute.
- Serve warm.

...except I overheated the kefir and butter, and the kefir curdled, so
I had to take the liquid fraction of that and mix it with more
kefir. But that actually worked rather well.

Very... celeriac-flavored. Almost cabbage-y. But pleasant.
