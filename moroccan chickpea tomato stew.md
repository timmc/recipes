# Moroccan chickpea tomato stew

## 2020-05-06

https://www.thefullhelping.com/moroccan-chickpea-tomato-stew/ but subbed:

- 1/2 tsp allspice instead of 1/4 tsp cloves
- Substituted an apple cider vinegar/red wine mixture for red wine vinegar

Ingredients:

- 1 Tbsp olive oil
- 1 yellow onion diced (230 g)
- 29 g garlic, crushed (4 largish cloves)
- 28-ounce can of crushed tomatoes (contains salt)
- 1 Tbsp sugar
- 1 tsp ground coriander
- 1 tsp ground cumin
- 1/2 tsp ground allspice
- 1/2 tsp crushed pepper (1 dried, whole piri-piri pepper, minced)
- 1/4 tsp cinnamon
- 2 cups vegetable broth (contains salt)
- 1 tablespoon tahini
- 3 cups cooked chickpeas (190 g dried chickpeas, soaked 24 hrs and
  then cooked)
- 1.5 tsp apple cider vinegar
- 1/2 tsp red wine

Directions:

- Heat the olive oil in a pot over medium heat. When the oil is
  shimmering, add the onion. Cook onion for five minutes, stirring now
  and then, or until the onion is soft and clear.
- Add the garlic and cook for 2 more minutes, stirring frequently and
  adding a tablespoon or two of water if the garlic starts to stick.
- Add the tomatoes, sugar, coriander, cumin, red pepper flakes,
  allspice, and cinnamon. Cook, stirring frequently, for 10 minutes, or
  until they're thickening up and very bubbly.
- Add the broth or water, lower the heat to a simmer, and cook,
  uncovered, for another 10 minutes
- Stir in the tahini, chickpeas, vinegar, and wine
- Cook for another 5 minutes and adjust seasonings as desired

"Serve the stew with a grain of choice and a sprinkle of parsley or
cashew cream, if you like."

Really quite pleasant. Would like to amp up some of the spices next time.
