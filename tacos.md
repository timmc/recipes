# Tacos

## 2020-03-28

Beans:

- 570 g (3 cups) dried black beans, soaked and then cooked
- 1 or 2 Tbsp oil
- 1 tsp salt
- 7 cloves garlic, crushed
- 3 small jalapeños, minus seeds and ribs and stems, minced

Fry garlic and peppers in oil over high heat, then add beans and
salt. Gently mix, and then cook for another 5 minutes.

Salsa:

- 25.5 oz tomato purée (Red Fire farms, contains citric acid and calcium chloride)
- juice of 1 lime
- 1 tsp ground cumin seed
- 1/2 tsp salt

(Mix.)
