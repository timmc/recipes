# Beans and Rice

## 2019-06-07

Ingredients:

- olive oil
- 4 medium-large garlic cloves, crushed
- 1 large jalapeño, fine dice
- 130 g carrot (1 medium), small dice
- 100 g celery sticks (3 small)
- 1 Tbsp ground cumin
- 1/2 tsp dried oregano
- 1/2 tsp ancho chili powder (could sub paprika)
- 1/4 tsp chipotle chili powder
- 175 g crushed tomato in puree (contains salt)
- 420 g dried black beans (500 mL), soaked
- 380 g brown basmati rice (500 mL)
- 2.5 L water

Directions:

- Saute garlic and jalapeño over medium high heat
- Before garlic starts to brown, add carrot
- When carrot stops sizzling as much, add celery
- After a minute or so, added spices (cumin, oregano, ancho, and
  chipotle), and stir for 20–30 seconds
- Stir in crushed tomato to quench cooking
- When tomato starts to bubble, add beans, rice, and water to cover
- Cover pot and bring to boil, then reduce to simmer

Cooked for 1 hour or so, then turned off fire for the night. In the
morning:

- Put half into crockpot on high (since running out of room in main
  pot); turned fire to medium heat
- Added additional 1 L water and 2 tsp salt (split across the two)
- After an hour or two, beans were mostly cooked. Uncovered pots,
  added 1 tsp of salt, 1 tsp of oregano, and 2 tsp of ground cumin
  (split across the two) and let excess water simmer off (took several
  hours).

Tasted fine. Next time, cook beans first and separately; can reserve
cook water for rice.
