# Pizza

## 2021-08-17 - Sourdough pizza

I had a bread loaf that never rose properly, so I added water and
flour to the dough to use as pizza crust. It wasn't great pizza, but
better than making an inedible puck of a loaf.

Used a 12"x17" baking sheet.

Crust:

- Original dough: 10.9 g salt, 400 g water, 515 g whole wheat flour,
  180 g 100% hydration starter; had fermented for about 48 hours.
- About 20 g additional water
- 150 g additional flour

Oiled the baking sheet and pushed/stretched dough out from center to rim.

Sauce:

- 5.05 oz/144 g (1/2 cup) crushed tomatoes, no salt (Eden)
- 1 tsp (6 g) iodized table salt
- 1/4 tsp black pepper, freshly ground
- 1 tsp garlic powder (fine granule)
- 1.5 tsp of a minced mix of fresh oregano, fresh thyme, and somewhat
  less fresh rosemary leaves
- 1/2 tsp of frozen parsley leaves, minced

Other toppings:

- About 8 oz cheddar, maybe 10, shredded
- Halved pitted kalamata olives
- Roasted eggplant, coins
- Basil leaves

16 minutes at 545°F lower rack.

Sauce a bit too salty/garlicky, and not quite enough; should dilute to
maybe 2x with additional crushed tomatoes.

Crust should have been allowed to rise before baking, was very
dense. Also very sour, but that's a consequence of using a failed
loaf.
