# Mayonnaise

## 2018-07-08 -- Olive oil mayo

From https://www.kitchenkonfidence.com/2013/04/homemade-olive-oil-mayonnaise

Not following the *original* instructions was a bad idea. Don't do this.

- 1 egg, straight from fridge
- 1 Tbsp lukewarm water
- 1 Tbsp lemon juice (from plastic squeeze jar)
- 1 tsp "spicy brown mustard"
- Pinch of iodized salt
- 1 cup evoo

Combine in jar just narrow enough to accomodate immersion blender. Put
immersion blender all the way down, then turn on. Move slowly up and
down once the mayo starts emulsifying.

...and wow, definitely bitter. :-/

The recipe said not to use extra virgin olive oil, and I ignored it,
but it turns out that evoo is the *one* oil you shouldn't use. When it
emulsifies, the polyphenols are released from their fatty acid
bindings and create a bitter flavor.

Other than the flavor, this worked suberbly. Next time, use refined
olive oil, and mix in evoo gently at the end.

*Update*: I let the mayo sit out in hot temperatures for a day or two,
then put it in the fridge. I think this gave it a chance to
de-emulsify somewhat, maybe just shy of breaking; the bitter flavor is
now greatly reduced.

## 2020-01-26 -- Sunflower oil mayo

Sunflower oil is a little old, want to use it up before it goes rancid.

- 1 egg, straight from fridge (67 g with shell, 57 g without)
- 15 g lukewarm water (1 Tbsp)
- 17 g fresh squeezed lemon juice, little bit of pulp (1 Tbsp, rounded)
- 5 g spicy brown mustard (1 tsp) - Gulden's
- 0.1 g iodized salt (1 pinch)
- 217 g sunflower oil (1 cup)

Same process as 2018-07-08.

Tastes reasonably good, and seems to hold together fine. A little loose.

## 2020-12-14 - Garlic-infused mayo

- 4 g garlic clove, crushed
- 12+ g olive oil
    - 12 g is how much made it in; more was still stuck to skillet

- yolk of one egg (69 g egg, 18 g yolk)
- 12 g apple cider vinegar
- 4 g lemon juice from bottle (Key West)
- 5 g spicy brown mustard
- 1 pinch iodized table salt
- 1 pinch freshly powdered black pepper (2 grinds, then mortar & pestle)
- 175 g refined sunflower oil

- Brought olive oil just to the start of a shimmer (fine dimpling) over
  low heat
- Turned off the heat and added crushed garlic to oil; stirred
  frequently to prevent burning
- Added yolk, vinegar, lemon juice, mustard, salt, pepper, and
  garlic/oil mixture to bowl; mixed all
- Belatedly realized that the garlic was still chunky; crushed it
  awkwardly with my fingers
- Poured into jar just narrow to accomodate immersion blender (glass 1
  lb peanut butter jar)
- Poured sunflower oil on top
- Put immersion blender all the way down, then turn on. Move slowly up
  and down once the mayo starts emulsifying
    - Still pretty liquidy

...it broke, and I couldn't fix it.
