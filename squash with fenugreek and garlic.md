# Winter squash, fenugreek, and garlic

## Original

Retrieved 2013-10-21 from http://tastykitchen.com/recipes/sidedishes/acorn-squash-with-fenugreek-seeds-and-garlic/

"Our family favorite way of cooking acorn squash in fenugreek seeds
and garlic with a tang from lemon juice or aamchur powder and a hint
of sweetness."

Ingredients

- 1 Tablespoon Oil (mustard, Olive Or Vegetable Oil)
- 1 teaspoon Fenugreek Seeds
- 1 Tablespoon Minced Garlic
- 2 whole Dried Chili
- 1 whole Medium Sized Acorn Squash (cut In 1/2 Inch Cubes)
- 1 teaspoon Turmeric Powder
- Salt To Taste
- 1-½ teaspoon Aamchur Powder Or Dried Mango Powder (optional)
- 1 Tablespoon Lemon Juice (optional)
- ½ Tablespoons Brown Sugar
- 1 Tablespoon Coriander Powder

Preparation Instructions

> Heat oil in a wok or thick-bottomed pan. Add fenugreek seeds. As
> they start to sizzle, add garlic and whole chili (broken into
> pieces).

> When garlic starts to brown, add acorn squash. Add turmeric. Mix it
> all together and cover with a lid, stirring occasionally until it is
> half-cooked.

> Once squash is half-cooked, uncover and add salt. Now cook it
> without the lid until squash is cooked through.

> At a certain point, squash starts to get mushy, so go easy while
> stirring or it will break. Not that there will be any change in
> taste, but it just won’t look as pretty. So just try not to mash
> everything up while stirring.

> Once it’s cooked through, add aamchur (or lemon/lime juice), sugar
> and coriander powder. Mix everything well. Cook for another minute
> and then turn the heat off.

> My mom serves it with hot rotis and lentil soup. You can even use it
> as a spread on your bread or serve it with rice.


## 2013-10-21

- ~1.5 tbsp mustard oil
- 1 tsp "Ethiopian fenugreek" seeds (not actually, see notes!)
- 1 tbsp minced garlic
- 1 finely minced serrano (?) (with seeds)
- 1/2 kuri squash, peeled and seeded
- 1/2 tsp turmeric powder
- 2 medium pinches salt
- ~1 tbsp lemon juice
- ~1 tbsp orange juice (sub for sugar, although squash was
  quite sweet anyhow)
- ~1 tbsp fresh-ground coriander

Notes:

- Bright and sweet, texture mushy around the edges
- Fenugreek crunches here and there
- It didn't seem to matter that the garlic was well burnt

## 2013-11-05 - Carving pumpkin -- double recipe

- Mustard and canola oil
- 2 serranos
- 5 garlic cloves
- 1/2 of a jack-o'-lantern (about 2 acorn squash worth?)
- 1 tbsp turmeric powder (rather than 2 tsp)
- 2 tbsp fresh-ground coriander
- ~2 tbsp lemon juice (a little less)
- ~1 tbsp demerara sugar (a large-grain blonde sugar)

Notes:

- The pumpkin is a little bland, but I can't stop eating!
- Update 2014-06-03: Turns out that wasn't "Ethiopian Fenugreek"
  as the hand-written jar label claimed, but korarima (Ethiopian
  cardamom).
