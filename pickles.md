# Pickles

## 2020-07-21

- 714 g cucumber with ends trimmed off (5 medium cukes)
- 1000 g water
- 16 g sea salt (1 heaping Tbsp)
- 28 g garlic cloves (4 medium), lightly crushed
- 1/2 tsp mustard seeds
- 1/2 tsp fennel seeds
- 2 small bay leaves, fresh picked
- 2 grinds of black pepper

## 2020-08-10 - Dill

- 400 g cucumber with ends trimmed off (3 small)
- 400 g water
- 6.4 g sea salt
- 8 flowering sprigs of dill
- 10 g garlic cloves (2 small/medium) partially crushed
- 5 grinds of black pepper

## 2021-07-25 - Cored dill

An attempt at using very large cucumbers.

- ~860 g cucumber "rinds" (that is, quartered cucumbers with seeds and
  core mostly removed)
- 9 g (4) grape leaves
- 12 g garlic, sliced 3 mm
- 10 g dill tops
- 4 small bay leaves, bruised
- 5 sections of dried intact mace
- 1/4 tsp caradamom seeds
- 1 tsp brown mustard seeds
- 1/2 tsp coriander seeds
- 1/8 tsp black pepper coarse ground (5 grinds)
- 2 g frozen ginger, sliced 2-3 mm
- 886 g water
- 36 g table salt

Removed stems from grape leaves, bruised them, and slid spears of
cucumber into jar with some with a grape leaf wrapped around them. Had
to cut a bit off the ends of the cucumbers to fit them into a
half-gallon jar.

Shoved garlic, dill, and other spices (except for salt) down the sides
as best I could.

Added water until cucumbers were covered, then added 4% (72 g) salt.

...which was wrong, because what I really want is 2% salt for the
*total*, which means an initial 4% *brine*. So the next morning I
diluted it: I poured out the brine into a separate jar, poured 886 g
water in with the pickles (to pick up some salt), and poured that in
with the brine; then mixed the water and brine well and poured about
half the mixture back in with the pickles (by level, not by weight).

On 2021-08-06 I returned from vacation and found a cap of sporulating
mold. Two days later I had time to deal with it. The mold came off in
a solid chunk. The cucumbers underneath had some over-softness on the
top that I cut off (also because of mold risk), but were overall about
as firm as I would expect cucumbers this old to be. They fared in this
better than the Sour Teacumbers (2021-07-26) that used tea as a tannin
source instead of grape leaves.

Too much mace, though!

Refrigerated the same day as trimming, 2021-08-08.

## 2021-07-26 - Sour teacumbers

Most of the brine is the excess from making 2021-07-25, and may have
picked up spices or have a higher or lower salt amount than written.

- ~1013 g tap water
- ~38 g table salt
- 787 g of cored cucumber quarters (as in 2021-07-25)
- 1 Tbsp loose black tea
- 1 kohlrabi leaf as a cap leaf

On 2021-08-06 I returned from vacation and found a cap of sporulating
mold. Two days later I had time to deal with it. The mold came off in
a solid chunk with the kohlrabi leaf. The cucumbers underneath had
red-brown discoloration on both ends, mostly the top. This is likely
from the tea leaves, since they were also softer on both ends, again
mostly at the top end. I removed the top of each down to where it
stopped being sludgy and become merely soft. Soft is expected since
these were large and not generally suitable for fermentation. I
removed a little from the bottom where they were too soft as well.
Flavor is good, quite sour. No particular tea flavor.

Refrigerated the same day as trimming, 2021-08-08.
