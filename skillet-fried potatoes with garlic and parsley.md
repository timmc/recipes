# Skillet-fried potatoes with garlic and parsley

Based on "Pommes de Terre a Cru" (Skillet-fried potatoes) from The New
York Times 60—Minute Gourmet by Pierre Franey, 1980 edition, page
175. Modifications by Mary Furman and myself.

- Slice potatoes very thinly, 1/8 inch if possible. Consistency is
  good. Wait until the last moment the potatoes are to be cooked to
  peel them. Approx 3 cups when sliced.
- Heat oil in a 12" skillet, preferably one with rounded sides. When
  the oil is hot add the potatoes in layers (starting from the edge
  to even out the cook times.) Cook about 15 minutes or until golden,
  turning them gently to prevent breaking the slices.
- Remove excess oil. Heat butter in bare spot of span, then mix in
  garlic. Let garlic become slightly golden.
- Mix with potatoes and add salt, pepper, and parsley. 
- Continue cooking, stirring and tossing gently for several minutes.

This recipe does not scale up well; the potatoes don't have a chance
to lay flat and fry.

## 2015-10-18

- 1/2 cup oil (cover 12" skillet 1/16 inch deep)
- 1.5 lbs potatoes
- 2 tbsp cultured butter
- 1 clove garlic, minced
- 2 tbsp parsley, chopped
- 1-2 tsp salt
- black pepper, ground

The potatoes formed about 4 layers (too much!) and there was a lot of
excess oil.

## 2015-10-28

Halved oil relative to previous.

- 60 mL (1/4 cup) oil
- 679 g (1.5 lbs) potatoes, sliced 2 mm
- 30mL (2 tbsp) cultured butter
- 1 large clove garlic, minced
- 30 mL (2 tbsp) parsley, chopped
- ~5 cc salt
- black pepper, ground

This is the right amount of oil for the potatoes... but too much
potatoes. Recommend halving the potatoes and only slightly reducing
the amount of oil.

Lost track of time. Not sure how long I cooked for.

Potatoes stick to each other quite a bit, difficult to
separate. Consider tossing in the oil first, then adding potatoes and
oil to med-hot skillet? Large slices also seem to stick to each other
more.
