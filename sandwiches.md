# Sandwiches

## 2016-08-24: Roasted beet & celery

- Roasted beets
- Black beans cooked with jalapeño, garlic, salt, and oil
- Celery leaves
- Brown mustard
- Peanut butter
- Six-grain bread (When Pigs Fly)

The celery, roasted beet, and mustard are the key in the sandwich. The
celery tastes more like parsley than usual. (I believe this is the
variety "Tango" from the Waltham CSA.)

The peanut butter was mostly there to glue the beans to the bread, and
the mustard served double-duty to glue the celery leaves to the
beets. The beans were mostly there to bulk it out and get used up. :-)

Something to invent later: Borscht sandwich

## 2021-04-15: "Turkey club" (umeboshi and mayo)

Tastes just like a turkey club sandwich, or at least how I remember
them tasting. (I haven't had one in 10 or 15 years.)

- Homemade 100% whole wheat/rye sourdough bread (30-40% rye)
- Mayonnaise
- Umeboshi paste (ume fruit pickled with salt and shiso leaves)
- Spinach leaves

Also works with arugula leaves.

## 2021-05-16: Tempeh, umeboshi, ketchup, and arugula

- Tempeh sliced to half-thickness, shallow-fried in sesame and olive oil
- Umeboshi paste
- Fermented ketchup (BBQ-sauce-like flavor)
- Arugula

Ketchup from Short Creek Farm: "Heirloom tomatoes, Ailsa Craig onions,
molasses, brown sugar, salt, chili, mustard seed, garlic, black
peppercorn, coriander seed, allspace, cinnamon"

This was a hit with the kid.

## 2021-09-15: Tempeh, kimchi, and four condiments

- Tempeh, sliced 5 mm and pan-fried in olive oil and toasted sesame oil until browned on both sides
- Minimalist kimchi (a lacto-ferment of cabbage, ginger, garlic, and salt)
- Chunky miso
- Spicy fermented ketchup
- Tahini
- Mayonnaise

I first put the miso and a thin layer of ketchup on one slice, then
the tempeh on that. On top I put the kimchi, with most of the liquid
squeezed out. I then spread the tahini on the second slice, enough to
thinly coat it, then a thin layer of mayo on that, and put it all
together. The tahini helps protect the bread from getting soggy on the
kimchi side.

This, also, was very popular with the kid.

## 2021-10-19: Squash, tempeh, umeboshi, bean paste

- Tempeh, sliced 5 mm and pan-fried in olive oil and toasted sesame oil until browned on both sides
- Honeynut squash, halved, cleaned, oiled, and oven roasted; sliced 1 cm
- Umeboshi paste, thin layer
- Soybean paste, thin layer

## 2021-11-15: Tempeh, seaweed, tahini, dried tomato

- Black sesame butter
- Fermented seaweed salad (includes kimchi juice and sesame oil)
- Tempeh, sliced 5 mm and pan-fried in olive oil and toasted sesame oil until browned on both sides
- Oil-packed dried tomatoes

## 2021-12-21

From Alex:

- Tempeh pan-fried in olive and toasted sesame oil and then marinated in soy sauce and balsamic vinegar
- Cheddar cheese
- Salsa (crushed tomatoes, cumin, salt, lime juice)
- Bok choy (or other greens), chopped
