# Kentucky coffeetree

_Gymnocladus dioicus_ is a North American tree that bears large,
poisonous pods. When the pods are roasted, the seeds become edible and
coffee-flavored.

## 2021-02-02

3 seeds roasted at 325°F for 1 hour 20 minutes, 1 hour 40 minutes, and
1 hour 50 minutes respectively. (Last one might have been 1:55, was
supposed to be 2:00). The seeds were placed on a metal tray and
covered with an upside-down metal loaf pan in case the seeds
exploded. I'm not sure what the correct roasting time is for our oven
to produce something both safe to eat and not burnt, so I'm going from
most roasted to least.

I also roasted the pod, wrapped in aluminum foil, but it seems kind of
charred and I won't be using it. It does smell like coffee, though.

Notes for different preparations for each seed:

### 1 hour 50 minutes

2021-02-02: I cracked the 1:50 seed and found the shell was still hard
and glassy. Inside was the seed itself, which was black in color and
hard, but fractured like chalk in the mortar and pestle. Both tasted
of coffee, but I think a fairly dark roast. I ground up the seed into
a fairly fine powder using a mortar and pestle.

2021-02-02: Alex took some of the powder and added it to water, then
boiled it in the microwave. « The [1:50] batch seems fine.  I think it
would take a number of beans to give a decent-flavored beverage out of
this.  It's reminiscent of coffee but definitely doesn't taste like
coffee.  The smell is difficult to place.  Kind of like roasting
coffee without the more pungent notes?  And mild. The beans taste
absolutely nothing like coffee beans. »

2021-02-03: Alex again: « This morning I boiled the rest of the ground
Kentucky coffee tree seed in a small amount of water for... a goodly
period... long enough that over half of the liquid boiled off, and
then add almond milk and microwaved the mixture, and I'm pretty
satisfied with the result.  It doesn't really taste like coffee, but
it's an improvement on plain heated almond milk and tastes fine
without sweetener. »

### 1 hour 40 minutes

2021-02-08: I cracked the 1:40 seed using a vise across the second
longest axis. The seed + shell was 2.000 grams, and the bare seed was
0.837 grams. I ground the seed to a powder and made a little drink
with some of it:

- 25 g cold tap water
- 250 mg coffeetree powder
- 1-2 g carob powder
- 60 g cold whole milk

I put the powder and the water in a mug and boiled it in the microwave
(2 minutes, most of the water driven off), then let it sit covered 10
minutes. Dissolved carob powder (forgot to measure properly) and then
stirred in milk. Tasted reasonably good, but flavor was too mild. I
think I need to wait until I have larger quantities to work
with. However, a pinch of carob powder + a smaller pinch of cofeetree
powder taken straight seemed promising.

2021-02-09: 0.511 g coffeetree powder with 189 g almond milk, heated
in microwave. Kind of tastes like coffee smells; less bitterness.
