# Sautéed kale

From http://www.food.com/recipe/my-favorite-sauteed-kale-364252

## Original recipe

Ingredients:

- 1 1⁄2 lbs kale, stems and leaves coarsely chopped
- 2 tablespoons olive oil
- crushed red pepper flakes, to taste (optional)
- 2 garlic cloves, finely sliced
- 1⁄2 cup vegetable stock or 1⁄2 cup water
- salt and pepper
- 2 tablespoons balsamic vinegar

Directions:

- Heat olive oil in a large saucepan over medium-high heat.
- Add crushed red pepper flakes, if using, and let them sizzle a bit
  in the oil.
- Add the garlic and cook until soft, but not colored.
- Raise heat to high, add the stock and kale and toss to combine.
- Cover and cook for 5 minutes.
- Remove cover and continue to cook, stirring until all (most) the
  liquid has evaporated. I like the kale to still remain bright-ish
  green.
- Season with salt and pepper to taste and add vinegar.

## 2017-02-19 - Something like my usual

Pepper powder instead of flakes; onion in addition to garlic; lacinato
kale (usually use the curly kind though). This also uses less kale
than the original, since I'm only cooking in a 10" cast iron
skillet. I also reduce the vinegar to match that.

I always let the kale cook beyond the bright green stage, since it
still smells sulfurous to me at that point.

- 2 tbsp olive oil
- 1/8 tsp chipotle chile pepper powder
- 175 g red onion, chopped (1/4 of a large one)
- 4 garlic cloves, crushed
- 338 g lacinato kale, chopped (one bundle from grocery store)
- 1/2 cup vegetable broth (from Seitenbacher broth mix)
- 1/4 tsp table salt
- 1/4 tsp black pepper
- 1 tbsp balsamic vinegar
