# Lentil Salad

## 2015-11-29

Based on http://www.skinnytaste.com/2014/07/lentil-salad.html with the following changes:

- Doubled the celery
- Removed 1/4 cup finely diced red bell pepper; 1/4 cup finely diced
  red onion; 1/4 cup minced parsley; 1 clove garlic, minced
- Reduced the lemon juice from 5 to 2.3 tbsp
- Sextupled the recipe

Actual recipe:

- 1.5 L green lentils, soaked 8 hours
- 6 bay leaves
- 12 sprigs thyme
- 2 L water

Bring to boil, then simmer until lentils are edible but
firm. Drain. Remove bay leaves and thyme sprigs.

- 1.5 L carrots, diced (845 g)
- 1 L celery, diced (8 large stalks, 612 g)
- 205 g radish (daikon & watermelon), diced (350 mL)
- 6 tbsp kosher salt (NOTE: Really? No! I probably accidentally wrote
  down the same as the amount of olive oil. -.-)
- 6 tbsp olive oil
- 270 mL lemon juice
- 2 tsp black pepper

Mix all.

For reference, lentils absorbed/held 1 L of water from soaking. Also,
dicing carrots is a *lot* of work.

## 2020-05-14

No bay leaves and thyme, partly because my bay leaves are totally
stale now, and partly because I forgot.

- 204 g brown lentils (1 cup), soaked 7 hours
- 2 cups water

Drain and rinse lentils, then simmer in water, uncovered, until
lentils are reasonably soft. Drain if excess liquid.

- 265 g shredded carrot (with mandoline)
- 70 g shredded purple daikon radish (with mandoline)
- 2 Tbsp evoo
- 1 tsp iodized table salt
- 45 g lemon juice (from bottle)
- 1/8 tsp cracked black pepper, ground to powder

Notes:

- Taste was on the mild side; 5yo thought the lentil flavor was too strong

## 2020-05-17 - with balsamic vinaigrette

Recipe recorded as performed; should be simplified.

- 200 g brown lentils (1 cup), soaked 9 hours
- water for cooking lentils
- 265 g shredded carrot (with mandoline)
- 70 g shredded purple daikon radish (with mandoline)
- 3 Tbsp balsamic vinegar
- 2 Tbsp olive oil
- 2 tsp dijon mustard
- 10 g minced garlic (1 small/medium clove)
- 1 tsp iodized table salt
- 27 g frozen chopped parsley

Directions:

- Add water to lentils in pot, almost enough to cover
- Cover lentils and bring to light boil; cook until water is absorbed
  and driven off. When finished, should be tender but discrete. Drain
  any remaining water.
- Dump lentils into shredded vegetables and toss lightly
- Whisk together vinegar, oil, mustard, garlic, and salt until roughly
  mixed, then heat gently (e.g. 10 sec in microwave); then whisk to
  emulsion.
- Mix parsley into vinaigrette
- Combine all and gently toss and stir

Notes:

- A little on the spicy side; didn't check how hot the mustard was
  before using it
- Amazing as part of a warm millet bowl with wild greens, sunflower
  seeds, salt, olive oil

## 2020-05-20 - with balsamic vinaigrette

2020-05-17 again but doubled. Less garlic.

Lentils were refrigerated overnight after soaking and before cooking,
but I don't think it makes a difference.

- 400 g brown lentils (2 cups), soaked 9 hours
- 2 cups water for cooking lentils
- 530 g carrot, shredded
- 140 g purple daikon radish, shredded
- 6 Tbsp balsamic vinegar
- 4 Tbsp olive oil
- 16 g finely minced garlic (1 medium/large clove)
- 2 tsp iodized table salt
- 4 tsp dijon mustard
- 54 g frozen chopped parsley

Directions:

- Add water to soaked lentils in pot
- Cover lentils and bring to light boil; cook until water is absorbed
  and driven off. When finished, should be tender but discrete. Drain
  any remaining water if cooking happens faster.
- Vinaigrette: Whisk together vinegar, oil, mustard, garlic, and salt
  until roughly mixed, then heat gently (e.g. 10 sec in microwave);
  then whisk to emulsion.
- Combine lentils, carrot, daikon, vinaigrette, and parsley and gently
  toss and stir

## 2020-06-20

2020-05-20 but half; subbed in some turnip; different vegetable
ratios. Low on parsley.

- 200 g brown lentils (1 cup), soaked 23 hours
- 1 cup water for cooking lentils
- 174 g carrot, shredded
- 164 g purple top turnip, shredded
- 91 g purple daikon radish, shredded
- 3 Tbsp balsamic vinegar
- 2 Tbsp olive oil
- 9 g finely minced garlic (1 medium clove)
- 1 tsp iodized table salt
- 2 tsp dijon mustard
- 18 g frozen chopped parsley

Directions:

- Add water to soaked lentils in pot
- Cover lentils and bring to light boil; cook until water is absorbed
  and driven off. When finished, should be tender but discrete. Drain
  any remaining water if cooking happens faster.
- Vinaigrette: Whisk together vinegar, oil, mustard, garlic, and salt
  until roughly mixed, then heat gently (e.g. 10 sec in microwave);
  then whisk to emulsion.
- Combine lentils, carrot, daikon, vinaigrette, and parsley and gently
  toss and stir

## 2020-12-16

- 200 g brown lentils (2 cups) soaked overnight
- 400 g carrots, shredded

4 Tbsp evoo
1/8 tsp salt
1 Tbsp spicy brown mustard (way too much!)
2 Tbsp lime juice and pulp

## 2021-03-14

- 400 g lentils (~2 cups) soaked overnight
- 0.84 g bay leaves, fresh-picked (5 small)
- 400 g chiogga (candy-stripe) beet, shredded
- 3 Tbsp balsamic vinegar
- 2 Tbsp olive oil
- 4 g garlic, finely minced
- 1 tsp table salt
- 2 tsp spicy brown mustard

- Drained lentils, added 2 cups water and bay leaves, and cooked
  covered over low heat until liquid mostly gone, then uncovered to
  drive off remaining liquid.
- Vinaigrette: Whisk together vinegar, oil, garlic, salt, and mustard
  until roughly mixed, then heat gently (e.g. 10 sec in microwave);
  then whisk to emulsion.
- Combine lentils, beets, and vinaigrette and gently toss and stir

Pretty nice!
