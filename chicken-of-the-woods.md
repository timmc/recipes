# Chicken of the Woods

## 2022-10-03 - Sulfur shelf with garlic, wine, lemon, and parsley

Based on https://www.thesophisticatedcaveman.com/sauteed-chicken-of-the-woods/

- 2 tsp extra virgin olive oil
- 157 g tender Chicken of the Woods cleaned and sliced into 1/2' strips
- 17 g garlic (2 cloves) sliced thin
- 1/8 tsp (heaping) iodized table salt
- 1/8 tsp (scant) freshly ground black pepper
- 2.5 Tbsp red wine
- 3 g fresh lemon juice
- 2 tsp frozen chopped parsley

- Heat the olive oil in a large skillet over medium high heat.
- Spread the mushrooms out in an even layer in the skillet. Cook,
  stirring occasionally, until the mushrooms start to slightly brown
  (about 5 minutes).
- Reduce heat, stir in the garlic, and season with salt and
  pepper. Cook, stirring to keep garlic from burning, until the garlic
  is translucent (4-5 minutes).
- Pour the wine into the skillet and stir. Cook until wine has been
  absorbed by the mushrooms and some of the alcohol has a chance to
  cook off, a couple minutes.
- Stir in the lemon juice and parsley.

Tasted fine, although the wine was a little assertive (I don't like
alcohol). Wine was absorbed almost instantly, as were the oil and
lemon juice; might have done better to soak the mushrooms a bit, as
they'd been in the fridge a few days and had probably dehydrated
somewhat. Would prefer a recipe in the future that highlights the
natural flavor of the mushroom a bit more.
