# Taco fillings

## 2018-02-10 - Turkey and spices

- olive oil
- 1.12 lb ground turkey (mix of light and dark meat)
- 29 g garlic, minced/crushed
- 1 tsp dried oregano
- 1 tsp salt
- 1 tsp chipotle chili powder
- 2 tsp cumin powder
- 1 tsp ancho chili powder

Directions:

- Overheat cast iron skillet
- Add turkey and garlic and notice the turkey instantly sear, then
  belatedly add olive oil
- Cook until almost completely "browned" (whitened?)
- Add in the remaining spices (oregano, salt, chipotle, cumin, ancho)
- Continue stirring, turn off heat after a minute

A bit too salty (salt is dominant.) Tastes mostly like chili powder.

## 2018-08-07 - Turkey and spices

Attempting the 2018-02-10 recipe again, toning down some of the spices
(dividing all dry spices by half except for oregano.)

- olive oil
- 555 g (1.2 lb) ground turkey (mix of light and dark meat)
- 28 g garlic, minced/crushed
- 1 tsp dried oregano
- 1 tsp cumin powder
- 0.5 tsp chipotle chili powder
- 0.5 tsp ancho chili powder
- 0.5 tsp salt

Directions:

- Heat olive oil in cast iron skillet
- Add garlic and turkey
- Cook, stirring, until almost completely whitened
- Add in the remaining spices (oregano, salt, chipotle, cumin, ancho)
- Continue stirring, turn off heat after a minute

Reactions:

- Turkey flavor more prominent this time
- A noticeable amount of liquid came out after I added the salt, and I
  let a little of it cook off

## 2020-03-28 - Beans

(Copied from tacos.md file.)

- 3 cups dried black beans, soaked and then cooked
- 1 or 2 Tbsp oil
- 1 tsp salt
- 7 cloves garlic, crushed
- 3 small jalapeños, minus seeds and ribs and stems, minced

Fry garlic and peppers in oil over high heat, then add beans and
salt. Gently mix, and then cook for another 5 minutes.
