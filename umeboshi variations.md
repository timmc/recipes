# Umeboshi variations

I don't have access to ume, so...

## 2021-09-27: Tomatillo umeboshi

- 206 g tomatillos, mostly on the green side; husked, but not washed
- 31 g iodized table salt (15%)

- 10:30: Put tomatillos into quart jar and sprinkled/poured salt over
  them. Added 363 g weight (trying for 2x weight of tomatillos) onto
  fruits. Capped lightly to keep fruit flies out.
- 9/29: Salt is now wet and caked -- brine formation is still
  extremely slow, but increasing.
- 10/1: About 7 mm of brine on bottom. Lots of undissolved
  salt. Gently swirled the jar to coat lower surfaces of fruits with
  brine, although that may have washed some salt off instead.
- 10/5: Only about 1 cm of brine has formed, and one tomatillo has
  started going moldy. I've cut out the small mold spot and halved all
  of the fruits, then stirred them to properly coat with salt. (They
  quickly formed a deeper brine.)

## 2021-10-08: Tomatillo umeboshi, fresh, pierced

- 428 g tomatillos, mix of ripeness; husked and washed
- 65 g salt
- 18 g green shiso leaves (30 leaves)

Pierced each fruit, placed in jar, and poured salt over. Packed shiso
leaves into sides and top. Capped and periodically swirled liquid in
jar and gently pressed down.

Shiso leaves were harder to manage without being first salted and wrung.

- 10/14: Two fruits that had been sitting above the surface of the
  brine had starter to mold, so I removed those and repacked
  everything into a smaller, necked jar where everything would stay
  submerged.
- 2022-07-30: This was the only jar that did not end up with Kahm
  yeast. I tried one today: Firm, *intensely* salty, mild fruity
  flavor, mild sourness. Refrigerated. Should try drying some out.

## 2021-10-09: Tomatillo umeboshi, using the freezer

- 502 g tomatillos, mix of color (green unripe to yellow ripe);
  husked, washed, and frozen whole
- 20 g green shiso leaves
- 78 g iodized table salt

Packed in layers of tomatillos and shiso leaves, then added salt on
top. Capped and periodically swirled liquid in jar and gently pressed
down. Brine formed *very* quickly.

Shiso leaves were harder to manage without being first salted and wrung.

## 2021-10-09: Tomatillo umeboshi, fresh, halved

- 362 g tomatillos, mix of ripeness; husked and washed
- 55 g salt
- 15 g green shiso leaves
- a few extra grams of salt for shiso

- Sprinkled salt among shiso leaves and mashed and squeezed them until
  maybe 1 mL of liquid came out. (Discarded liquid.)
    - This made them much, much easier to work with in the jar with
      the tomatillos, since once wilted they didn't block brine or
      air.
- Halved tomatillos and placed in jar, then packed shiso in around the
  top. Poured in salt (55 g).
- Capped and periodically swirled liquid in jar and gently pressed down.
- 10/14: Repacked into a smaller jar with a weight to keep things
  submerged. Smelled like alcohol when opened, then changed after a
  few minutes into pleasant floral fruity notes.
