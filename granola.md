# Granola

## Low-FODMAP - Mark Bittman, Alexandra Thorn

Quoting from https://alexandra-thorn.dreamwidth.org/52660.html

« When I needed to drop wheat from my diet as part of the low-FODMAP
diet, it was a strong motivator to figure out how to make my own
granola.

The goal was to have something that provided more than just starch,
was tasty, made with relatively unprocessed ingredients, and was not
*too* high in sugar. I don't have an electric mixer, so I do all
mixing by hand. With some trial and error, this is the recipe that
I've come up with, adapted from the granola recipe in _How to Cook
Everything Vegetarian_ by Mark Bittman.

The below is a double recipe relative to Bittman's recipe. I go
through single recipes too quickly!

Ingredients:
* 2 lb rolled oats
* 4 cups nuts and seeds (I use two heaping cups of pecans, which I
  chop up, one cup of sesame seeds, and one cup of pumpkin seed
  pepitas)
* 2 cups fresh ground almond butter
* 1 cup pure maple syrup (you can substitute a simple syrup if price
  is an issue)
* 2 cups sweetened dried cranberries (make sure that the sweetener is
  low-FODMAP, e.g. sugar or pineapple juice)
* 34 g (~1/4 cup) unsweetened chocolate (I use Pascha brand 100%
  chocolate chips, but you can also chop up your favorite baking
  chocolate)
* 1 teaspoon salt
* 1 teaspoon ground cinnamon

Supplies:
* Two large mixing bowls (life is easiest if one is at least
  2-gallons; I suppose you could make do with one 4-gallon mixing
  bowl)
* One small (1 quart or larger) mixing bowl
* Two 12x16 cookie sheets (you want the kind with a lip so the granola
  doesn't go anywhere) or equivalent area in baking pans
* Nice big spoon for stirring with

Steps:
1. Combine all nuts in one bowl, including any chopping you need to
   do, and set aside.
2. In small bowl, combine almond butter, maple syrup, salt, and
   cinnamon. Stir to a uniform consistency.
3. Put rolled oats into one cookie sheet on stove top across two
   burners on low, low heat.
4. Heat oats for 4 minutes while stirring to keep the oats above the
   burners from burning.
5. Add nuts to oats, and heat for another 4 minutes while stirring.
6. Turn on oven to pre-heat to 300F
7. Transfer oat-nut mixture into a 2-gallon mixing bowl, and pour on
   almond-butter syrup mixture. Stir until uniform. Beware of pockets
   of dry oats that sometimes hide in the center of the bottom of the
   bowl. This is a fair amount of stirring. It will look like granola
   by the time you are done mixing.
8. Spread out the granola mixture on the two cookie sheets and bake
   for 20 minutes.
9. While the granola is baking, prepare the two mixing bowls by adding
   half of the cranberries and half of the chocolate to the bottom of
   each.
10. Immediately after the the granola is finished baking, scoop the
    granola from each cookie sheet into one of the prepared bowls, and
    mix until uniform. The heat of the cooked granola will melt the
    chocolate so that it mixes into the granola.

Notes:
* Using nice large containers really reduces headaches, because there
  is a lot of stirring involved and granola has a tendency to go
  flying.
* The original recipe, which is *not* designed for a low-FODMAP diet,
  also calls for 2 cups of shredded coconut, added during the stovetop
  step, two minutes after the nuts. I think coconut granola tastes
  great but it is also a source of FODMAPS (see
  https://www.fodmapeveryday.com/is-coconut-low-fodmap/). Don't add it
  unless you are sure it is consistent with your diet.
* The recipe is pretty forgiving. Most ingredients can easily be
  dropped, and it works just as well with different dried fruits (chop
  the larger ones; and note that many fruits are high-FODMAP) or
  different seeds and nuts. However, in my experience it doesn't stick
  together properly with less than 1 cup of syrup. If you're trying
  for something lower sugar than this, you'll need to make additional
  modifications. »
