# Creamy Swedish Cabbage Soup

## 2020-01-25

Based on a (doubled) recipe from The Enchanted Broccoli Forest, but
using garlic and fried cabbage instead of onion, and almond milk,
butter, and cheese instead of milk and sour cream, to accommodate a
low-FODMAPS diet.

### Ingredients

- 1083 g adirondack blue potatoes, trimmed
- 4 + 8 cups water
- olive oil
- 150 g green cabbage, finely chopped (4-8 mm)
- 1583 g green cabbage (1/2 extra large head), trimmed and shredded
    - 18 packed cups
- 30 g garlic, minced
- 4 tsp caraway seeds (whole)
- 4 tsp iodized table salt
- 2 cups almond milk (Almond Breeze "Original" unsweetened)
- 1 Tbsp butter
- 56 g sharp cheddar (Smith's Farmstead of Winchester, MA; raw
  cheddar, with a bite, but buttery)
- 1.125 g black pepper (1/2 tsp), ground fine

### Directions

Do potatoes and cabbage pots in parallel:

Potatoes:

- Chop potatoes roughly and bring to boil in the first 4 cups of water
  (just enough to cover) until tender, about 50 minutes
- Drain most of cook water into pot with cabbage
- Add butter, cheese, and almond milk
- Use immersion blender to whip

Cabbage (main):

- Finely chop 150 g of cabbage and saute in olive oil over high heat
  in large stockpot, stirring to keep from burning, until most pieces
  are browned at edges
- Turn heat to low, add garlic, salt, and caraway, and saute until
  garlic is no longer pungent, about 5 minutes
- Add shredded cabbage and remaining 8 cups of water, cover, and
  simmer until tender, about 30 minutes (adding potato water part way
  through)
- Mix in whipped potato mixture and black pepper

### Notes

- The soup is hilariously bad looking, with yellow cabbage pieces
  floating in a lilac-colored broth (thanks to the purple potatoes),
  although this color fades a bit with storage
- The flavor is quite good, though a bit watery, so maybe reduce the
  amount of water added to the cabbage next time, and compensate by
  stirring more
- Also, have a higher potato/cabbage ratio so it's heartier
- Probably should have sauteed garlic separately so that I could tell
  how far along it was (was difficult to see if it was burning)
- Consider frying *all* of the cabbage, and then adding the potatoes,
  whipped in their own water?

Total cook and prep time was 1 hour 15 minutes.

## 2020-12-04 - Original version

This is much more accurate to the Creamy Swedish Cabbage Soup
described in The Enchanted Broccoli Forest.

Potatoes probably increased by 50% or so, given the amount of water I
needed for boiling them. Used yogurt instead of sour cream, and
full-fat dairy.

- 703 g white potatoes, trimmed (6 of varying size; 750 untrimmed)
- 3 cups water for potatoes
- 2 Tbsp or so of olive oil
- 226 g (2 cups) chopped onion (2 med/small white onion)
- 2 tsp whole caraway seed
- 2 tsp salt
- 800 g (8 packed cups) shredded green cabbage (1/2 largish head, chopped 6 mm)
- 4 tsp Seitenbacher vegetable broth powder
- 3 cups water for broth
- 1/2 cup whole milk
- 1/2 cup whole milk yogurt
- 1/8 tsp black pepper, freshly powdered with mortar and pestle

Directions:

- Cut potatoes into 3 cm chunks and set to boil in 3 cups water.
  Boiled until tender (about 25 minutes), reserving water.
- In a large pot, began cooking the onions over medium low heat in the
  olive oil.
- After a few minutes, added caraway and salt.  Let cook over
  medium-low heat about 10-15 minutes, stirring occasionally.
    - Was supposed to leave covered, but left uncovered for at least
      first 10 minutes.
- By now the potatoes were tender, so I added the shredded cabbage,
  the 2.5 cups of reserved potato water, 3 cups water, and vegetable
  broth powder to the onions; covered and simmered until cabbage was
  tender, about 30-40 minutes.
    - Initially added only 1.5 cups water; liquid was about 1 cm below
      top of cabbage
    - About 10 minutes later added additional 1.5 cups water
- Roughly whipped the hot potatoes with milk and yogurt
- Added the potato mixture and black pepper when the cabbage was
  tender, mixing thoroughly

Notes:

- Really tasty!
- Definitely try a double batch of this next time. This one only made
  something like 3 quarts.

## 2023-02-23 - Low FODMAPs, more potatoes

2020-01-25 with 1.5x potato mixture (and black pepper) and some recipe
streamlining.

Cabbage was ~1800 g before trimming.

### Ingredients

- 1593 g Belmonda gold potatoes, 1501 g after trimming
- 6 cups water for potatoes (to cover)
- olive oil
- 300 g green cabbage (part 1), finely chopped (4-8 mm)
- 1320 g green cabbage (part 2), trimmed and shredded
- 30 g garlic, crushed
- 4 tsp iodized table salt
- 4 tsp caraway seeds (whole)
- 5 cups water for stew
- 1.5 Tbsp butter
- 3 cups (707 g) unsweetened soy milk (Silk)
- 84 g sharp cheddar (Thatcher's farm)
- 1.7 g black pepper, ground fine

### Directions

- Chop potatoes roughly and bring to boil in water (just enough to
  cover) then simmer covered until tender, about 40 minutes
- Saute first portion of cabbage in olive oil over high heat in large
  stockpot, stirring to keep from burning, until most pieces are
  browned at edges
- Turn heat to low, add garlic, salt, and caraway, and saute until
  garlic is no longer pungent, about 5 minutes
- Add rest of shredded cabbage, the potato cook-water, and additional
  water (to cover, and for thickness)
- Cover and simmer until tender, about 30 minutes
- Add butter, soy milk, and cheese to the drained potatoes, and use
  immersion blender to whip
- Mix whipped potato mixture and black pepper into cabbage

### Notes

- Maybe *too* creamy. I think I over-mixed the potatoes and made them
  a bit gluey, or there's just too much. This is also a different kind
  of potato than I used before; the Adirondack Blue are less starchy,
  I think.
- Decent flavor. Black pepper might be a tad strong. (I did increase
  it along with the potato mixture.)
