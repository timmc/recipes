# Falafel

Remember: *Always* start with dried chickpeas. Canned or cooked
chickpeas won't work.

## 2019-01-29

Based on https://smittenkitchen.com/2018/12/falafel/

- 415 g 24 hour soaked chickpeas (should be 1.25 cups/190 g dried)
- 49 g garlic cloves (9 medium-large), peeled
- 20 g scallions (green parts only), chopped (1/2 cup)
- 19 g curly parsley, finely chopped (1/4 cup)
- 24 g flat parsley[1], finely chopped (1/4 cup)
- 1 tsp iodized table salt
- 1 tsp ground cumin
- 1/2 tsp (heaping) ancho chili powder
- olive oil

Directions:

- Run the garlic cloves, scallions, and parsley through
  a food processor until they're fairly evenly shredded.
- Add the chickpeas, salt, cumin, and chili powder and pulse to a fine
  chop, about like couscous. You should be able to form balls from it.
- Cover mixture in fridge for several hours.
- Make tightly compacted balls and fry in oil.

[1] This was supposed to be cilantro, but there was flat parsley in
the cilantro bin at the grocery store!

Notes:

- Quite tasty, although a little light on the spices. I tried some
  variations as I went. Yotam Ottolenghi suggests adding cardamom and
  lemon zest, which was pleasant but a little distracting. I also
  tried adding whole coriander seeds to the mixture, which worked
  *very* well.
- Making puck-like patties reduced the depth of oil I needed, but they
  formed corners that over-cooked (burned) and the flat faces were
  weirdly green in the middle.
- Making spheres produced a nicer effect, but I had trouble getting
  some of them to cook all the way through, leading to the flavor of
  uncooked chickpeas (starchy, and probably bad for digestion). It's
  important to cook them to a darker brown, not just a light
  brown. The oil also may not have been the right temperature. (I was
  just below the smoke point of the olive oil, which means it was
  probably way too low. Should use a different oil.)
- Next time I'm just going to bake them.

## 2019-02-05

No pepper, no scallions, doubled cumin, added coriander seeds.

- 190 g dry chickpeas, soaked 24 hours (441 g wet)
- 48 g garlic cloves, peeled
- 20 g curly parsley, finely chopped
- 23 g flat parsley, finely chopped
- 2 tsp ground cumin
- 1 tsp iodized table salt
- 1 Tbsp coriander seeds

I made two versions of this. In both, I added the coriander seeds at
the very end, after the chickpeas were chopped (quick pulse just to
mix in).

In variation 1, I added 50 g of the soaked chickpeas after the garlic
& herbs but before the rest of the chickpeas and spices, and processed
to very fine pieces, then proceeded with the recipe. This was to see
if I could use near-pureed chickpeas to the same effect as
flour. (Variation 2 was left as a control.)

The two versions had little noticeable difference; if anything,
variation 2 was actually easier to form into balls, and I think I
might have processed it more finely. I suspect that getting a chunkier
texture entails bringing in flour, or possibly processing a full half
of the chickpeas to a finer texture and then adding in the rest. (My
guess is that larger chickpea pieces mean longer cooking time.)

## 2019-02-06

Variation:

- Double recipe
- ...except for scallions, since I didn't have enough (just a little
  more than 1x of 2019-01-29 recipe)
- No chili or other pepper
- Put about 1/3 of the chickpeas in early
- Doubled cumin (4x of original) since doubling worked well on 2019-02-05
- Coriander seeds, like 2019-02-05

Ingredients:

- 380 g dry chickpeas, soaked 24 hours
- 100 g garlic cloves, peeled
- 80 g curly parsley, finely chopped
- 23 g scallion greens, finely chopped
- 4 tsp ground cumin
- 2 tsp table salt
- 2 Tbsp coriander seeds
- 0.6 g cardamom seeds, freshly ground (about 3 pods)
- 1/2 tsp lemon juice, fresh-squeezed

Directions:

- Process garlic, parsley, and 1/3 of the chickpeas fairly fine
- Add rest of chickpeas, scallions, cumin, and salt, processing until
  chickpea chunks approach size of couscous grains
- Mix in coriander, cardamom, and lemon juice

## 2019-07-08

Usual recipe, with:

- 333 g dry chickpeas
- 15 g half-dried parsley
- 100 g garlic cloves
- 20 g scallion greens
- 4 tsp ground cumin
- 1.75 tsp iodized table salt
- 1 tsp ancho chili powder
- 1 tsp lemon juice and pulp
- 3 Tbsp coriander seeds

## 2019-09-29

Two batches, one with fresh parsley and one with frozen.

Used 380 g dry chickpeas, which became 780 g after soaking
overnight. Divided evenly into the two batches. Each batch:

- 53 g scallion greens, chopped
- 50 g garlic cloves, peeled
- 39 g parsley
    - Batch #1: fresh parsley, finely chopped
    - Batch #2: frozen chopped parsley
- 190 g dry chickpeas (390 g soaked)
- 4 tsp ground cumin
- 2 tsp Ancho chili powder
- 1 tsp iodized table salt
- 3 cardamon pods worth of seeds, ground
- 1 tsp fresh lemon juice
- 2 Tbsp coriander seeds, whole

Directions:

- Process garlic, parsley, scallions fairly fine
- Add chickpeas, cumin, chili powder, and salt and process until
  chickpea chunks are the size of couscous grains (actually a little
  pastey by the end, not sure why)
- Briefly blend in cardamom powder, lemon juice, and coriander seeds

Results:

Did not notice any real difference between the two batches.

## 2020-05-12

First time using cilantro.

- 380 g dried chickpeas, soaked 24 hours (792 g wet)
- 93 g garlic cloves (1 large head), peeled
- 40 g flat parsley, chopped and frozen (from 2019-07-22)
- 40 g cilantro leaves, chopped and frozen (from 2019-07-22)
- 4 tsp ground cumin
- 1.75 tsp iodized table salt
- 1 tsp ancho chili powder
- 1 tsp lemon juice and pulp
- 3 Tbsp coriander seeds

Directions:

- Run the garlic cloves, parsley, and cilantro through
  a food processor until they're fairly evenly shredded.
- Add the chickpeas, cumin, salt, chili powder, and lemon juice and
  pulse to a fine chop, about like couscous. Towards end, add
  coriander seeds. You should be able to form balls from it.
- Pack into container and refrigerate 4 hours
- Make tightly compacted balls and fry in oil at around 325°F until
  golden brown or darker

Later notes:

- 320°F seems to work well. Too low and the oil soaks in and then
  fragments the ball; too high and the outside is dark brown before
  the inside cooks.
- Batter *perfectly* fits in a 1 qt yogurt container.
- Makes about 35 2–3 cm balls
- Need about 450 g of oil to cover 7 balls; expect to use 30–40% of
  the oil in frying the entire batch. (At least 4 g oil used up per
  ball.)
