
Guided by recipe at http://www.chefscatalog.com/recipe/detail/773-kohlrabi-and-leek-soup.aspx retrieved 2013-12-03

# Variations

## 2013-12-03

Heavy use of leftovers, so not likely reproducible.

- 2 tbsp cultured butter
- 6 leeks: Trim away dark green (use for other recipe), slice into thin
  discs, wash away sand, spin dry.
- 20-30 scallions: Trim away dark green (use for other recipe), chop
- 1 med/large kohlrabi (6 inches across?) peeled and diced into 1/4 inch cubes
- 1 cup potato juice from squeeze-drying potato shreds (about 1/5 starch)
- 3 cups stock: 2.5 cups "chickpea broth" + 2.5 tsp Seitenbacher vegetable broth and seasoning powder + .5 cup water
- 1 tbsp chopped fresh thyme leaves
- 1.5 cup water
- 1 cup filmjölk (a drinkable fermented milk product, like kefir)

1. Melt butter (started to brown a little), add leeks and scallions
   and cook, stirring, until translucent and tender (10 minutes) -- pot
   started to brown/blacken
2. Stir in kohlrabi. Cook, stirring, 3 to 4 minutes longer.
3. Stir in stock, thyme, potato juice. Simmer over low heat until
   kohlrabi very tender, about 30-40 minutes. (Larger kohlrabi may not
   get tender.) Stir occasionally; potato starch settles out of
   solution and sticks to bottom.
4. Add 1.5 cup water for consistency.
5. Stir in filmjölk, heat for 10 more minutes.

Thick, perhaps overly so. (Potato squeezings added too much starch?)
Not amazingly flavorful -- the leeks weren't super-evident -- but
tasty and a decent winter meal. The kohlrabi refused to get tender; it
might have been too big for that. Perhaps some pre-steaming would have
helped.
