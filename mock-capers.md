# Mock capers

## 2023-07-21 - Nasturtium capers, trial run

Based very roughly on a recipe from
<https://hildaskitchenblog.com/recipe/pickled-nasturtium-seeds/>. Scaled
down, stripped down. Increased brine amount to ensure all pods
covered, since there were so few.

- 7.3 g unripe seedpods (13 count, about 15 mL dry volume)
- 8 g water
- 8 g distilled vinegar
- 2.2 g iodized table salt

Dissolved salt into water and vinegar, washed seedpods and poured
brine over them, weighed down with glass weight, and kept covered.

Tried some 2023-08-11. They were a bit tough; I only had one that was
the same size as a caper, and that one had the right texture. The
flavor isn't exact, but it's somewhat close.
