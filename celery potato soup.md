# Celery potato soup

Pureed, heavy on potatoes. Also see celery garlic soup, which involves
roasting.

- olive oil
- 583 g soup celery ribs and leaves, chopped
- 116 g scallion greens
- 31 g garlic, crushed
- 1 + 1 tsp salt
- 1118 g red potatoes, diced
- 2246 g water
- small handful of thyme sprigs
- 4 small bay leaves
- 1-2 Tbsp frozen parsley

Directions:

- Saute celery about 10 minutes
- Add garlic, scallions, and 1 tsp salt and cook for another 10
  minutes or so (aiming for tenderness)
- Add potatoes, water, thyme, bay, parsley, and remaining 1 tsp salt
- Bring to a boil, then cover and simmer (or insulate) until
  vegetables are very tender
- Remove bay leaves and thyme stems
- Puree with stick blender
- Strain out any remaining strings, if desired (e.g. fragments of
  thyme stem)

Notes:

- Nicely creamy
- A little edge of bitterness, from using soup celery and including
  the leaves, but it's not unpleasant
- Serving suggestion: Grated cheese on top. Pecorino romano was a
  little sharp, maybe go with something milder.

## 2022-10-28

Lighter on the celery, heavier on the scallion greens, a little
heavier on the potatoes. Used fresh bay leaves, and bigger than last
time. (Probably used dry ones last time, and likely a bit stale, in
retrospect...)

- olive oil
- 490 g soup celery ribs and leaves, chopped
- 150 g scallion greens
- 36 g garlic, crushed
- 1 + 1 tsp salt
- 1124 g red potatoes, diced
- 1810 g water
- small handful of thyme sprigs
- 4 medium bay leaves
- 2 Tbsp frozen parsley

Notes:

- Still a bit bitter, and better with pecorino romano... but that
  cheese is sharp and salty, so I instead tried adding a pinch of
  salt, and that dramatically improved the flavor.
- Next time: Consider adding 1/2 tsp of salt to recipe.

## 2023-10-17

Basically 2022-10-28 but with "regular" celery, not soup celery;
scallion greens + whites.

- olive oil
- 700 g celery ribs and leaves, chopped
- 355 g scallion whites and greens
- 28 g garlic, crushed
- 1 + 1 tsp salt
- 1000 g potatoes, diced
- 1800 g water
- small handful of thyme sprigs
- some stale bay leaves
- 3 Tbsp frozen parsley

Notes:

- Still low on salt, since I didn't adjust that. Adding soy sauce when
  serving is a nice touch.
