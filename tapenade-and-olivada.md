# Tapenade and olivada

(The difference, apparently, being mostly one of texture.)

## 2024-08-23 - Olivada

From Passionate Vegetarian by Crescent Dragonwagon.

- 1 cup pitted kalamata olives
- 2 cloves garlic, minced
- 1 Tbsp capers, drained
- 2 Tbsp extra virgin olive oil

Combine all in a food processor; pulse-chop until well-mixed but still
chunky (couscous-like pieces).
