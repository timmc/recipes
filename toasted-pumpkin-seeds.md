# Toasted pumpkin seeds

## 2019-11-03

Seeds from a funny white-yellow pumpkin that seemed like a cross
between an orange pumpkin and a spaghetti squash.

- ~1 cup of large seeds
- Olive oil
- 1/2 tsp iodized table salt

Washed seeds. Sprinkled with salt and drizzled with olive oil on
baking sheet, then baked in preheated 325°F oven for 25 minutes.

Very tasty. Some browned on outside, all tan on inside.

## 2019-11-13 - butternut

Seeds from large butternut squash. (~3/8 cup)

- Seeds extracted but not rinsed
- Sprinkled with a few pinches of salt
- Refrigerated for several days
- Drizzled with water to break up clumps
- Drizzled with a little olive oil and mixed around
- Spread on baking sheet (shared with another batch)
- Baked in preheated 325°F oven for 26 minutes

## 2019-11-13 - honeynut

Seeds from 4 honeynut squash. (~3/8 cup)

- Seeds extracted but not rinsed (and fairly gooey)
- Sprinkled with 1/8 tsp salt
- Drizzled with a little olive oil and mixed around
- Spread on baking sheet (shared with another batch)
- Baked in preheated 325°F oven for 26 minutes

25 minutes probably would have been safer; the seeds were just about
to start getting an unpleasant burnt flavor.
