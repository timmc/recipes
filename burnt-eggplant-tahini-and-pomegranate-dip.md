# Burnt eggplant, tahini, and pomegranate (dip)

Adapted from Plenty by Yotam Ottolenghi

From: http://veggiesandgin.com/home/2013/07/31/ottolenghis-burnt-eggplant-wtahini-pomegranate

## 2015-12-19

Changes: Too little eggplant, no water, no pomegranate seeds or other toppings, a little less tahini, too much pomegranate molasses.

- 2 medium eggplants
- 1/4 cup tahini paste
- 2 tbsp pomegranate molasses (was supposed to be 2 tsp!)
- 1 tbsp lemon juice
- 1 garlic clove, crushed
- 4 tbsp chopped parsley
- sea salt and black pepper

Poke holes in eggplant and char directly under broiler for 1 hour,
turning every 15 minutes, such that skin is blackened and crispy and
flesh is deflated. Remove skin. Let drain ~20 minutes, then chop roughly.

Mix well with tahini, lemon juice, pomegranate molasses, garlic,
parsley, and some salt and pepper.

Results:

- Tasty!
- Very strong flavor. Needs more eggplant to dilute and add body.
- Added 3x the molasses that was called for. That added a bit much
  tanginess.
- Might used food processor next time to get better blending.

## 2016-01-19

Right amount of pomegranate molasses, probably wrong amount of lemon
juice, no parsley.

- 3 small/med eggplant
- 1 tbsp lemon juice
- 2 tsp pomegranate molasses
- 1 garlic clove, crushed
- Juice of 1 small lemon
- 1/4 cup water
- 1/3 cup tahini paste
- salt and black pepper

The eggplants mildly exploded because I forgot to poke holes in them.

## 2016-08-06 - simpler

No lemon juice, garlic, or black pepper, but plus dairy.

- 330 g pan-roasted eggplant
    - 7 Fairytale (tiny)
    - 1/2 of a medium/small Italian eggplant
- 32 g parsley (including stems), finely chopped
    - small bunch
- 107 - 118 g tahini
    - lost track of measurement
- 30 mL pomegranate molasses
- 30 mL filmjölk (skim)
    - very similar to kefir, could use yogurt
- 2.5 mL table salt

Food-process until mixed (about 30-60 seconds, may need to scrape down
sides).

## 2016-09-06

- Heavily broiled eggplant: 3 skinny, 1.5 small globe
- 1/4 cup water
- 1/3 cup tahini paste
- ~4 tsp pomegranate molasses
- 1 garlic clove, crushed
- ~4 tsp lemon juice
- ~1/2 tsp salt
- ~1/8 tsp black pepper
- 5 tbsp minced parsley

(No record of flavor, but I seem to recall it was tasty.)

## 2021-08-20

Instead of pomegranate molasses, I mixed molasses and frozen blueberries.

- 4.5 skinny eggplants, pan roasted, stems and blackened skin removed
  (320 g remaining)
- 9 g garlic clove, some chopped up
- 20 g lemon juice
- 20 g molasses
- 10 g frozen blueberries
- 18 g frozen chopped parsley
- 1/2 tsp iodized table salt
- 1/8 tsp black pepper, fresh ground, fine
- 76 g tahini
- 15 g water

A bit much garlic. Otherwise pretty nice.

## 2022-08-01

Eggplants were burnt under the broiler, but not quite blackened and
crumbling-skin as they should have been, so I didn't add any water.

Used citric acid (and increased pomegranate molasses) to make up for
lack of lemon.

- 4 long, skinny eggplants
- 71 g organic tahini paste
- 21 g pomegranate molasses
- 1/16 tsp citric acid
- 4 g garlic clove, minced
- 10 g frozen chopped parsley
- salt (big 2-finger pinch)
- black pepper (pinch)

I'd like to make this without the bitter, astringent note. Not sure
how.
