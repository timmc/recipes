# Gumbo

Or at least gumbo-adjacent things.

## 2019-09-27

Inspired by http://www.gumbocooking.com/okra-tomato-gumbo.html but no
actual okra involved.

- 391 g soup celery (very leafy, strong-flavored variety) - 5.5 cups chopped
- 24 g minced parsley (1 small bunch, mostly minus stems)
- 341 g green bell pepper, trimmed - 2 peppers, 3 cups chopped
- 109 g green peppers of some sort, maybe very mild hot peppers, 1 cup chopped
- 1 small jalapeño, chopped fine
- 431 g fresh white onions, 3.5 cups chopped
- 162 g scallions, mostly the white parts, 2 cups chopped
- 15 g garlic, diced (4 small/medium cloves)
- 2 Tbsp salted butter
- 6 Tbsp evoo

Melt butter with olive oil, stir with vegetables and cook on medium
heat until soft. (Medium-high at first to get everything up to
temperature.)

- 5 cups stock (5 cups water + 5 tsp Seitenbacher's vegetable broth &
  seasoning powder)
- 1.25 tsp dried oregano
- 1.25 tsp iodized table salt
- several bay leaves
- 1/4 tsp ground black pepper

Stir into pot, cover and turn heat to low.

- 814 g chopped tomatoes, mostly plum (5 cups)

Stir into pot. Continue cooking covered on low, stirring
occasionally. Barely even at a simmer.

After about an hour I called it done because it looked and smelled
cooked. Tasted good. This would be a good thing to try when I actually
have okra. Also, next time try starting with a roux.
