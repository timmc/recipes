# Gluten-free sourdough banana bread

With guidance from:

- http://www.gfzing.com/2011/gluten-free-sourdough-banana-bread/
- http://hanmadestuff.wordpress.com/2013/10/12/sourdough-banana-cake/
- http://amandeleine.com/2013/04/03/gluten-free-chocolate-chip-banana-bread/

## 2014-07-24

Mix in, more or less an item at a time:

- 3.5 overripe bananas (414 g; 1.75 cups when pulped down)
- 67 g extra virgin olive oil (1/3 cup)
- 236 g demerara (like turbinado) sugar (1 cup)
- 2 med/large eggs (115 g, with 13 g of shell)
- 1 rounded tsp cinnamon
- 1/4 tsp sea salt
- 1/2 teaspoon xanthan gum
- 2 cups gluten-free sourdough starter (Flora 2, 2014-07-24 @ 20:30) (560 g)
- 172 g gluten-free oat flour (1-2 cups, enough to start thickening
  the mix and holding peaks, very briefly)
- 194 g semi-sweet chocolate chips -- very sweet, containing sugar,
  cacao, milkfat, soy lecithin, vanilla (1 cup)
- 2 tsp baking soda
- 2 tsp cream of tartar

Baked at 350°F for 60—70 minutes. Excellent rise, browned top, comes
away easily from ungreased metal tin, holds together reasonably well.
