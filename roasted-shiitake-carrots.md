# Roasted Shiitake Mushroom and Carrots

Based on
<https://www.wewantveggies.com/roasted-carrots-shiitake-mushrooms/>
(warning: that recipe is not consistent in what it tells you to do)

## 2019-01-23

Ingredients:

- 860 g carrots
- 190 g fresh shiitake mushrooms, including stems
- 22 g garlic (3 large cloves)
- olive oil
- half a small lime
- 2 clementines, peeled
- Some kind of Chinese-ish sauce

Directions:

- Preheat oven to 450°F (230°C)
- Heat the olive oil in a medium-large cast iron skillet on medium-high heat
- Add the garlic cloves, carrots, and shiitake mushrooms
- Squeeze lime over the contents of the pan
- Toss the pan to coat the vegetables
- Place the pan in the oven until the vegetables are roasted, perhaps
  30 minutes. You may wish to stir or turn the vegetables once or
  twice; keep an eye on the garlic especially so that it doesn't
  burn. (Possibly take it out early, when it becomes soft.)
- Transfer vegetables to a plate and allow to cool
- Chop the carrots
- Separate the stems from the shiitake mushrooms; slice the caps and
  finely slice (possibly even mince) the stems, which are tougher
- Chop the 2 clementines (de-seed if necessary) and mix in
- Add the sauce (see below)

I added a sauce that I'd made for another dish, the "Essential Bean
Salad, Chinese Style" out of How to Cook Everything Vegetarian. It's
more than was needed for this recipe, so don't use the whole thing!

- 2 Tbsp toasted sesame oil
- 2 Tbsp sunflower oil
- 2 Tbsp minced garlic (called for 1 Tbsp)
- 1.5 Tbsp minced ginger (called for 1 Tbsp)
- 1 Tbsp rice wine vinegar
- Soy sauce to taste (be cautious; shiitake accentuates salt flavor)
- A few grinds of black pepper (not in original)
- Some chopped scallion

Results:

- Good flavor, although garlic (and ginger) were very prominent and
  other flavors lagged behind
- Shiitake umami flavor was less prominent than in the shiitake and
  sweet potato dish, perhaps because the shiitakes were not as
  thoroughly roasted in this one

## 2019-09-22

Similar to before, but with potatoes, and mistakenly adding the olive
oil differently. No clementines, so I used orange juice.

Ingredients:

- 108 g shiitake mushrooms, including stems (about a week in refrigerator)
- 21 g garlic cloves, peeled (5 med/small cloves)
- 377 g trimmed carrots, cut in half crosswise (to fit in pan)
- 348 g potatoes, quartered (3, white flesh and red skin)
- 26 g olive oil
- Juice of 1.5 small limes
- A few splashes of orange juice

Directions:

- Started oven heating to 450°F
- Poured oil over carrots, garlic, and shiitake in bowl
- Heated oiled cast iron skillet over medium-high heat
- Dumped bowl into skillet, poured lime juice over everything, and
  mixed
- Moved skillet to oven
- After 15 minutes, stirred once and removed garlic
- After 15 more minutes, pulled out skillet and let cool
- Chopped garlic, carrots, and potatoes; minced shiitakes
- Combined with sauce and orange juice

Sauce, similar to before, and again in excess (about 60% more than
needed):

- 2 Tbsp toasted sesame oil
- 2 Tbsp sunflower oil
- 2 Tbsp minced garlic
- 3 Tbsp minced ginger
- 1 Tbsp rice wine vinegar
- A drizzle of soy sauce (be cautious; shiitake accentuates salt flavor)
- A few grinds of black pepper

Tasted quite good, although the shiitakes were pretty charred. Should
have added a bit less ginger.
