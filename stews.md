Various stews (mostly for my own records)

# 2014-03-29/30: Parsnip, squash...

2 largish parsnips, 1 kobocha squash, 1 purple-top turnip (or
rutabaga?) (peeled), 1 Spanish black radish, 4 carrots, 4 stalks
celery, 2 potatoes (1 peeled to get the green off, but was not
bitter), rainbow chard stems, carrot stems, 1 medium head of garlic,
salt, powdered sage, powdered and dried rosemary, dried thyme, ground
grains of paradise

# 2015-06-27: Batata/tomato with greens

Like African Batata and Peanut Stew, minus peanut butter and plus greens.

- Olive oil
- 1 "pea" asafoetida

Heat in oil

- 1 head garlic
- 2-ish in^3 ginger

Sauté.

- 4 large and 4 small batatas, diced 1 cm

Stir and sauté more.

- 3/4 tsp ground cayenne
- 2-3 tsp ground cumin
- 1.5 tsp ground cinnamon

Mix to cook, heat.

- 4 28oz cans tomatos, chopped
- frisee endive
- spinach
- chard
- radish greens
- bok choy
- salt (2-3 tsp?)

Simmer until batatas starting to become tender.

- 5 or 6 14 oz cans black beans

Heat through.

# 2015-07-06: African Batata and Peanut Stew with endive and chard

Two variations.

- olive oil
- 1 "pea" asafoetida
- 6 lb batatas, diced 1 cm
- 4 tsp ground cumin
- 2 tsp ground cinnamon
- 1/8 tsp smoked serrano chili powder (in pot #1)
- 6 28oz cans crushed tomatoes with basil
- 1 head garlic, fine chopped (in pot #1)
- 2-3 cubic inches ginger, minced
- 1 head frisee endive, chopped
- 1 large bundle rainbow chard, chopped
- 2-3 tsp salt
- 6 14oz cans black turtle beans
- 1/2 to 3/4 cup peanut butter (in pot #2)

PB also goes in at end.

# 2015-07-25: Hot chard stew

Mild/moderate spicy, sort of bitter.

- amaranth
- bok choy
- chard
- turnips + greens
- carrots
- pattypan squash
- asafoetida
- black turtle bean broth with epazote
- hubbard squash pulp
- banana pepper
- unknown chile pepper
- cilantro
- snow peas
- olive oil
- salt
- water
- ginger
- cumin
- black peppercorn
- cardamom
- fennel seeds
- cloves
- cinnamon
- bay leaf
- coriander seed
- celery seeds

# 2015-09-17: Hot and sour stew

A little hard to remember after the fact, but roughly:

- olive oil
- 3 cloves garlic
- 1 jalapeño chili
- 1 anaheim chili
- 4 carrots
- 1 butternut squash
- 3 potatoes
- 1 28oz can crushed tomatoes w/ basil
- 1/2 slicing tomato
- 8 cherry tomatoes
- water (several 28 oz cans)
- salt (possibly 1-2 tsp?)
- batata sprouts (not much)
- 4 underripe tomatillos
- greens of 8 carrots
- greens of 4 beets
- 2 purple bell peppers

Smelled bitter at first whiff, but tasted *really* good, especially
with pecorino romano grated on top. Aged well in fridge.

Beet greens tasted more sour than earthy, and had green stems speckled
with red -- both related to being orange beets? probably relevant to
sour flavor, along with underripe tomatillos. Sweetness probably due
to squash.

Probably would have been fine without garlic, if need be.

## 2016-01-17 - Tart chard and chickpea stew

Based on
<http://www.theguardian.com/lifeandstyle/2009/jan/10/new-vegetarian-yotam-ottolenghi> except doubled, no tamarind, less tomatoes, no onion.

Stew ingredients:

- 850 g chard, cut into 1 cm pieces
- 4 tsp whole caraway seeds
- olive oil
- half a small can of tomato paste
- ~375 g canned ground tomatoes
- 750 mL water
- 3 tbsp cane sugar
- 850 g cooked chickpeas (from soaked dried chickpeas)
- 1 tbsp whole coriander seeds
- salt and black pepper
- 2 lemons
- plain yogurt (for topping)
- cilantro leaves (for topping)

Rice ingredients:

- 700 g short-grain brown rice
- 50 g butter
- 1400 mL water

Directions:

Prep

- Toast and grind coriander seeds
- Blanch chard in salted water 2 minutes

Stew

- Sizzle caraway seeds in olive oil over medium heat for a minute or two
- Add tomato paste and cook, stirring, for about a minute
- Add the tomatoes, water, sugar, chickpeas, ground coriander, chard,
  and any salt or pepper
- Bring to a boil, cover and simmer for 30 minutes, by which time it
  should be the consistency of thick soup

Rice

- Put the rice, butter and a pinch of salt in a Dutch oven
- Over medium heat, stir to coat the rice with melted butter, add the
  water and bring to a boil.
- Cover with a tight lid and simmer for 20-25
  minutes.
- Remove from the heat and set aside, still covered, for five more
  minutes.

Finishing

- Add the lemon juice to the stew
- Serve stew over rice, with yogurt, a drizzle of oil, and cilantro

Notes:

- Soaked 500g dried chickpeas 15 hours, turned into 1115 g when cooked
- Quite tasty!

## 2016-07-17: Butternut squash, rhubarb, and pigweed stew

- Large butternut squash (2 kg)
- Bundle of rhubarb stems (390 g)
- Potatoes, 5
- Pigweed, several quarts chopped leaves
- Leeks, just the green parts of 3 small ones
- Olive oil
- Salt (1 tsp?)

Chopped and sautéed leeks, potatoes, and pigweed stems in olive
oil. Chopped and added squash and rhubarb, then water to cover. Salt.

The pigweed stems were a bad idea -- though the cores softened, the
edges did not, leaving discs that broke into fibrous masses in the
mouth. These were pretty mature plants, though -- the stems of small
plants might be fine.

Flavor was decent, a little sour, Could have been sweeter. Pigweed
added a nice edge of bitterness, but quite mild. Good with cheese and
a bit of hot sauce.

## 2020-06-01 - Macon, wild greens, and root vegetables

- 2 cups brown lentils, soaked 8–10 hours
- 2 cups water + more to cover stew
- 66 g applewood smoked lamb bacon (a few frozen strips), diced 5 mm
- 2-3 Tbsp olive oil
- 63 g garlic, crushed
- 1120 g carrots (12 medium, trimmed)
- 1480 g potatoes (about 10, trimmed)
- 354 g purple daikon radish (3 small, trimmed)
- 412 g purple-top turnip (2 medium/small, trimmed)
- 260 g wild greens, chopped (broad-leaf plantain, dandelion, red
  dock, violets, chickweed, narrow-leaf plantain, maybe others)
- 1 Tbsp table salt

- Simmered lentils in 2 cups water, covered, until lentils were tender
  and water was gone
- Sautéd macon until changed color
- Added garlic and olive oil, sautéd until garlic started to soften
- Chopped briskly and added to the pot as I got to them: Carrots
  (half-moons), potatoes (1 cm dice), turnips (7 mm dice), radish (7
  mm dice)
- Added lentils
- Added water to cover
- Chopped greens to 7 mm and added to pot
- Added salt
- Brought to simmer until vegetables started to soften
- Turned off fire and left covered to let vegetables finish cooking in
  their own heat

Notes:

- Almost unpleasantly bacon-y smell when freshly made, but it mellowed
  overnight into something more pleasant and balanced.

## 2020-07-12 - Potato parsnip

- several Tbsp olive oil
- 62 g trimmed garlic cloves (~12 medium), crushed
- 16 rosemary tips (med/small handful, about 5 leaf pairs on each),
  leave removed from stems and roughly chopped
- 728 g parsnips (27 small, 690 g after trimming), cut to 2-3 cm long
  and 5 to 10 mm wide
- 1078 g potatoes (6 medium, 1030 g after trimming), 10 to 15 mm dice
- 1.5 Tbsp salted butter
- 1/2 tsp + 1/2 tsp salt
- 2 cups water

Directions:

- Add garlic and rosemary to oil in pot
- Sauté for about a minute over medium heat, until garlic just
  starting to turn golden
- Add potatoes, parsnips, butter, and 1/2 tsp salt; raise heat to
  medium high and stir frequently
- After about 5-10 minutes, add water and remaining 1/2 tsp salt
- Bring to boil, then simmer with lid ajar (stirring occasionally)
  until vegetables fork tender, 20-30 minutes

Notes:

- Not entirely balanced, but pleasant
- Texture might have been improved by pureeing a small amount

## 2021-04-24 - Vegetable stew with marrow bones

Sauteed while chopping and adding vegetables:

- 2 Tbsp or so olive oil
- 58 g garlic, crushed
- three bison marrow bones, roasted at 350°F on top rack for 17 minutes
- 1180 g carrots
- 420 g whitish potatoes (small russet?)
- 400 g Yukon Gold potatoes

Added salt, and then water to stop bottom from burning:

- 1 Tbsp salt
- 1 cup water

Added remaining ingredients:

- 166 g watermelon radish (somewhat dried out)
- 838 g Hakurei turnip
- 255 g purple daikon radish
- 8 cups water
- 1 tsp salt

Turned out fine. Simple flavor.

## 2022-01-02 - Root vegetable and lentil stew with spices

- 3-4 Tbsp evoo
- 68 g garlic
- 2 tsp scallion greens
- 620 g carrots
- 1176 g potatoes (white and gold)
- 600–800 g turnips
- 734 g radishes (purple daikon, watermelon)
- 7 fresh broad-leaved sage leaves, minced
- 1 tsp dried thyme
- 17 g (1/4 cup) frozen minced parsley
- cooked lentils (from 2 cups dried)
- 1907 g water
- 5 tsp salt
