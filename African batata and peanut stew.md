# African Batata (Sweet Potato) and Peanut Stew

2020-01-20 is my usual variation.

## First

From Vegan Planet page 302, with some alterations:

- No yellow onion (had been a step before the garlic)
- With asafoetida
- Half a head of garlic instead of a clove
- Black turtle beans instead of dark red kidney beans
- No garnish of chopped peanuts

Recipe:

- 1 tbsp olive oil
- 1/8 tsp ground asafoetida resin
- 1 medium-size green bell pepper, chopped
- Half a head of garlic, minced
- 1 tsp-ish grated ginger
- 2 tsp dark brown sugar
- 1/2 tsp ground cumin
- 1/2 tsp ground cinnamon
- 1/4 tsp cayenne
- 1.5 lbs batata (sweet potato), cut into 1/2 inch dice
- 14.5 oz can diced tomatoes
- 1.5 cups vegetable stock, or water
- 1 tsp salt (may need more if using water instead of stock)
- 1.5 cups cooked (or 1 15 oz can) black turtle beans, drained and
  rinsed
- 2 tbsp (heaping) smooth peanut butter, unsalted

Steps:

- Heat the olive oil in a large saucepan over med heat.
- Add garlic, asafoetida, and green pepper, cover and cook until
  softened, about 5 minutes.
- Stir in ginger, sugar, cumin, cinnamon and cayenne and cook,
  stirring, about 30 seconds.
- Add sweet potatoes and stir to coat.
- Stir in tomatoes, water, and salt to taste.
- Bring to a boil, then reduce heat to low and simmer until the
  vegetables are becoming soft, about 20 minutes.
- Stir in the beans and simmer until heated through and vegetables
  become soft, about 10 minutes.
- Place the peanut butter in a small bowl and blend in about 1/4 cup
  of the broth, stirring until smooth, then stir it into the stew. If
  a thicker consistency is preferred, puree 1 cup of the stew in a
  blender or food processor and stir back into the stew.

## 2013-11-11: First attempt

Changes:

- Double recipe
- 1.5 sweet peppers, seeded and chopped, instead of green bell pepper
- Stock: 3 cups veggie stock (1.7 cups chickpea stock, 1.3 cups water,
  1 tbsp Seitenbacher broth and seasoning powder)
- 5.5 cups cooked black turtle beans (several cups soaked for an hour
  and then pressure-cooked for a bit too long (ended up mushy))
  instead of red kidney beans

I used 5.5 cups black beans instead of 3 or 4 because I had extra.

Turned out really well!

## 2013-12-08 for house dinner

- Quadruple recipe
- 2-3 heads garlic
- 11 cups black beans (from 4.5 cups dried) instead of kidney beans
- 2.6 kg (5.7 lb) batata
- Stock: Seitenbacher veggie stock

I actually did two double recipes, with one pot having double the
cayenne (quadruple the single-recipe amount) and the other having
none.

Turned out much thicker, and not as amazingly tasty (although still
satisfying). There might have been too much beans, and I know I burned
the garlic. The double-dose cayenne version was better.

## 2014-10-30

Alterations:

- Quadruple recipe
- No asafoetida (or onion), bell pepper, or stock

Effective recipe:

- olive oil
- 4-5 cloves garlic
- 8 tsp dark brown sugar
- 4 tsp-ish grated ginger
- 2 tsp ground cumin
- 2 tsp ground cinnamon
- 1.5 tsp cayenne
- 7 lbs batata
- 2 * 28 oz can diced tomatoes (Muir Glen "fire roasted")
- 7 cups water
- 4 tsp salt
- 10 cups cooked black turtle beans
- 10 tbsp chunky peanut butter, unsalted

Worked well. Seemed light on spices.

## 2014-11-29

Notes:

- Triple recipe, a little heavy on the spices and light on the sugar
- No asafoetida or bell pepper

Ingredients:

- 10 cloves garlic
- ~3 tsp grated ginger
- 4 tbsp brown sugar
- 2 tsp cinnamon
- 2 tsp cumin
- 1 tsp cayenne (too much?)
- 4 lbs batatas
- 48 oz canned tomatoes
- 6 cups water (too much!)
- 3 tsp salt
- ~5.25 cups of beans
    - Started with ~3.25 cups dry beans, 1 lb
    - Soaked for 24 hours
    - Cooked roughly 75 minutes
- 4 cups cooked brown rice and wild rice (to soak up water and heat)
- 6 tbsp peanut butter (in 2/3 or 3/4 of total dish)

I used a quadruple amount of cayenne (and other spices) in what was
otherwise a triple recipe (and I suspect the cayenne was stronger than
usual) so I ended up with a pretty hot result. Oops. I also added too
much water (should have stopped at 4 cups, maybe 4.5) so it was
watery. The rice was added to soak up both and may have helped.

We added chopped up banana while eating and it was very good!

## 2016-02-03

Double recipe similar to previous, but not doubling cayenne out of
caution.

- 2.8 lb / 1275 g batatas (4 medium)
- 1.5 heads garlic
- 1.5 inch ginger
- 1 tsp ground cumin
- 1 tsp ground cinnamon
- 1/4 tsp cayenne
- 28 oz ground tomatoes
- 4 tsp dark brown sugar (probably sugar + molasses...)
- 3 cups water
- 2 tsp salt
- 6 cups cooked black beans (from soaked beans)
- 3 stalks celery, chopped and sauteed
- 3 leaves chard, chopped and sauteed
- 4 tbsp salted peanut butter

Notes:

- Pretty much perfect flavor
- Could have easily used less sugar
- 4 cups dried black beans -> 12 cups cooked
- (Did this have asafoetida in it?)

## 2016-02-14

Quadruple. No sugar.

Soaked 4 cups black beans 24 hours.

- olive oil
- 4 "peas" asafoetida
- 140 g garlic (3 heads)
- 3 inches ginger
- 1/2 bunch celery
- 2 tsp cumin
- 2 tsp cinnamon
- 2.8 kg batatas
- 2 28 oz cans tomatoes
- 4 cups chickpea broth
- 2 cups black turtle bean broth
- 1 bunch chard
- 2 tsp salt
- 9.5 cups cooked black turtle beans
- 1/2 cup salted peanut butter

Was aiming for 12 cups beans.

## 2016-05-01

Quadruple. No sugar, no garlic, no hot pepper.

Soaked 5 cups beans -> 14 cups.

- olive oil
- 4 "peas" asafoetida
- 3 inches ginger
- 2 tsp cumin
- 2 tsp cinnamon
- 2.8 kg batatas
- 2 28 oz cans tomatoes
- 6 cups black turtle bean broth (from cooking the beans)
- 1 bunch chard
- 2 tsp salt
- 12 cups cooked black turtle beans

This one was split into several batches before finishing to accomodate
various dietary restrictions. To one I added almond butter and
garlic-infused olive oil; to another peanut butter and garlic-infused
olive oil; to the last just peanut butter.

## 2016-10-17

- 2760 g batata
- 3-4 inches ginger
- 3 medium cloves garlic
- 2 greeen bell peppers
- 3 tsp salt
- 2 tsp cinnamon, 2 tsp cumin
- 2 28oz cans crushed tomatoes w/ basil
- olive oil
- 3 "peas" asafoetida
- 6 cups water
- 12 cups cooked black turtles beans w/o salt (from soaked beans)
- 2 tbsp brown sugar
- 1/2 cup peanut butter

## 2016-11-05

Double-ish recipe.

- Olive oil
- 2 "peas" of asafoetida
- 5 cloves garlic
- 1 tsp cumin powder
- 1 tsp cinnamon powder
- 1/4 tsp chipotle powder
- 1.5-2 inches ginger
- 4 tsp brown sugar
- 1404 g batata
- 2 28oz cans ground tomatoes w/ basil
- 3 cups water
- 2 tsp salt
- 4 medium leaves bok choy, chopped
- 6 cups cooked black beans
- 4 tbsp salted peanut butter

Good flavor, a bit tangy. Couldn't really taste heat.

## 2016-11-13

Double, with fresh tomatoes and hot pepper.

- Olive oil
- 2-3 "peas" asafoetida
- 6 cloves garlic
- 2.5" ginger
- 1.25 tsp cumin powder
- 1 tsp cinnamon powder
- 1/2 tsp chipotle pepper powder
- 1700 g batatas
- 1600 mL diced tomatoes (from 12 off-vine ripened slicing tomatoes)
- 3 cups water
- 2 tsp salt
- 6 cups cooked black beans
- 4 tbsp peanut butter

A bit watery or mild. Possibly too much sweet potatoes, too much
water, or not enough salt.

In retrospect, probably used about double the usual tomatoes for a
double batch. That explains the wateriness.

## 2016-11-23

Double batch with cayenne.

- Olive oil
- 2 "peas" asafoetida
- 1.5 heads garlic
- 1.5" ginger
- 1 tsp cumin powder
- 1 tsp cinnamon powder
- 0.25 tsp cayenne powder
- 4 tsp brown sugar
- 3 lbs batatas
- 3 cups water
- 2 tsp salt
- ??? tomatoes (probably 1 28 oz can)
- 3 15 oz cans cooked black turtle beans (~5.5 cups)
- 4 tbsp peanut butter (unsalted)

Tasted great! Mild to moderate heat.

## 2017-11-04

Quadruple version similar to 2016-11-23, but missing the asafoetida
for reasons.

- Olive oil
- 3.5 heads garlic (108 g)
- 3" ginger (41 g)
- 2 tsp cumin powder
- 2 tsp cinnamon powder
- 0.5 tsp cayenne powder
- 8 tsp brown sugar
- 6.4 lbs batatas
- 2 28 oz cans of tomatoes
- Water to cover (maybe 8 cups, hard to say)
- [salt not listed, but would likely have been 4 tsp]
- 6 15 oz cans of cooked black turtle beans
- 1/2 cup peanut butter (creamy, salted)

Turned out very well! Moderate heat while cooking, tempering to mild
after adding the peanut butter and letting it sit overnight.

## 2017-11-22

Double recipe, this time with bell pepper. Still no asafoetida. A
little less cayenne to be on the safe side.

- Olive oil
- 2 heads garlic
- 2 medium green bell pepper, chopped
- 1.5" ginger
- 1 tsp cumin powder
- 1 tsp cinnamon powder
- 1/12 tsp cayenne powder
- 4 tsp brown sugar
- 3.6 lb batatas
- 1 28 oz can of diced tomatoes
- 3 cups water
- 2 tsp salt
- (peanut butter not recorded; presumably a proportional quantity)

## 2019-01-12

Quadruple recipe, a little scant on beans.

- Olive oil
- 2.2 kg batata, 15 mm dice
- 60 fl oz ground tomatoes
- 5.5 cups cooked black turtle beans
- 2 large heads of garlic
- 3" of ginger, roughly grated
- 8 tsp brown sugar
- 2 tsp ground cumin
- 2 tsp ground cinnamon
- 1 tsp ground cayenne
- 6 cups water
- 4 tsp salt
- 1/2 cup peanut butter

Turned out a bit spicier than I'd intended.

I also ended up simmering this for maybe 20 minutes after adding the
peanut butter, since the beans I put in turned out to have been
undercooked, and the stew ended up thicker than usual, with the sweet
potato just starting to fall apart. It was quite nice.

## 2019-01-13

2.5x recipe, except I accidentally put in way too much ginger (equal
parts with garlic) and it tastes unpleasantly gingery. (I didn't know
that was possible!)

## 2020-01-20

Quadruple recipe. Using jalapeños in place of cayenne and bell
peppers. (I basically never have bell peppers on hand to include
anyhow.)

Makes about 6.5 kg of food.

- Olive oil
- 46 g frozen jalapeños (5 smallish), trimmed of stems, ribs, and seeds
- 1/2 tsp asafoetida powder (the German pre-ground stuff)
- 113 g garlic cloves (about 2 large heads), crushed
- 83 g frozen ginger, minced
- 8 tsp dark brown sugar
- 2 tsp ground cumin
- 2 tsp ground cinnamon
- 2750 g sweet potatoes, 1/2" dice
- 1617 g canned tomatoes (jars #1 and #2 of batch 2019-08-31/1.2)
- 4 tsp iodized table salt
- 7 cups water (enough to cover)
- 6 cups cooked black turtle beans (about 1 kg; from about 2.3 cups/440 g
  dry beans)
- Heaping 1/2 cup of chunky salted peanut butter

Directions:

- Heat the olive oil in a large pot over med heat.
- Add jalapeños, asafoetida, and garlic; cover and cook until
  softened, about 5 minutes.
- Stir in ginger, sugar, cumin, and cinnamon and cook,
  stirring, about 30 seconds.
- Add sweet potatoes and stir to coat.
- Stir in tomatoes, water, and salt.
- Bring to a boil, then reduce heat to low and simmer until the
  sweet potatoes are becoming soft, about 20 minutes.
- Stir in the beans and simmer until heated through and sweet potatoes
  become soft, about 10 minutes.
- Place the peanut butter in a small bowl and blend in about 1/4 cup
  of the broth, stirring until smooth, then stir it into the stew. If
  a thicker consistency is preferred, puree 1 cup of the stew in a
  blender or food processor and stir back into the stew.

Great flavor. Just a hint of heat.

## 2020-04-29

Basically the same as 2020-01-20. More water.

- Olive oil
- 114 g garlic cloves (about 2 large heads), crushed
- 42 g frozen jalapeños (6 smallish), trimmed of stems, ribs, and seeds
- 1/2 tsp asafoetida powder (the German pre-ground stuff)
- 2 tsp ground cumin
- 2 tsp ground cinnamon
- 8 tsp dark brown sugar
- 82 g fresh and frozen ginger, minced 2 mm
- 2780 g sweet potatoes
- 1645 g canned tomatoes (Muir Glen crushed)
- 4 tsp iodized table salt
- 8 cups water (enough to cover)
- 7 cups cooked black turtle beans (1 kg; from 440 g dry beans)
- Heaping 1/2 cup of chunky salted peanut butter

Ended up adding beans at the end, after the sweet potato was fully
cooked. Turned out just fine.

## 2021-02-11

Like 2020-01-20, but less sweet potato and more black beans. Used
black bean cooking water for some of the water.

- Olive oil
- 114 g garlic cloves (about 2 large heads), crushed
- 20 g frozen jalapeños + other chiles (6 smallish) without stems, ribs, and seeds
- 1/2 tsp asafoetida powder (the German pre-ground stuff)
- 2 tsp ground cumin
- 2 tsp ground cinnamon
- 8 tsp dark brown sugar
- 78 g frozen ginger, minced 2 mm
- 2210 g sweet potatoes, 1-2 cm dice
- 1697 g canned tomatoes (San Marzano, home-canned diced, 2 jars)
- 4 tsp table salt
- 3.25 cups black bean broth + 1 cup water (enough to cover)
- 1352 g cooked black turtle beans (from 550 g dry beans)
- Heaping 1/2 cup of chunky salted peanut butter

Slightly too hot for the tastes of the 6 year old; maybe just 3
peppers next time.

## 2021-04-11

Triple recipe, more or less (3/4 of 2020-01-20).

- Olive oil
- 85 g garlic cloves (about 1.5 large heads), crushed
- 18 g frozen chiles (3 small, low heat), trimmed of stems, ribs, and seeds
- 3/8 tsp asafoetida powder (the German pre-ground stuff)
- 1.5 tsp ground cumin
- 1.5 tsp ground cinnamon
- 6 tsp dark brown sugar
- 60 g frozen ginger, minced
- 2070 g sweet potatoes (2133 g before trimming)
- 1237 g canned tomatoes (Red Fire Farm, yellow crushed)
- 3 tsp iodized table salt
- 3 cups black bean cooking water + 2 cups water (enough to cover)
- Cooked black turtle beans (from 320 g dry beans)
- 1/2 cup of chunky salted peanut butter

Flavor was a bit off -- different tomatoes? Scant on the cumin?
(Teaspoon wasn't really packed.) Seemed less flavorful/balanced.
