# Garlic and parsley potato cakes

Based on wanting a less labor-intensive version of skillet-fried
potatoes (at least the same flavors) and discovering
https://cooking.nytimes.com/recipes/1017767-garlic-parsley-potato-cakes
in the process.

It turns out this is not less labor-intesive, because you need to
hand-form all those cakes! But you can produce more in a single batch,
at least.

## 2017-10-21

- 1.9 kg purple potatoes
- 62 g curley parsley (including stems -- big handful), minced
- 34 g garlic cloves (1 head), crushed
- 29 g pastured unsalted butter (2 tbsp), melted
- 2 tsp table salt
- 1/2 cup cornmeal (approximately, as needed)
- 2 eggs
- 2 tbsp whole milk (non-homogenized)
- 1 tsp black pepper
- olive oil

Steps:

- Boil the potatoes until they begin to soften
- Mash roughly with crushed garlic, minced parsley, melted butter, salt
- Beat together eggs and milk, mash in wih potato mixture (still a
  litle chunky)
- Oil a baking sheet
- Form potato mixture into patties 1–1.5 cm thick, coat in cornmeal,
  place on sheet (makes about 40 patties)
- Bake at 450°F; at some point add some oil next to each patty so that
  the oil is pulled under; continue until browned on underside
- Flip and bake longer, but not so long that the inside dries out.

They ended up more fried than I had originally set out for, but tasted
great with ketchup.
