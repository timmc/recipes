# Potato salad

## 2024-05-25: South German potato salad with lovage

Adapted from https://cooking.nytimes.com/recipes/7726-german-potato-salad-with-lovage
with following changes:

- Yellow onion marinated in the vinegar overnight rather than red onion
- Tempeh bacon instead of pork bacon

Ingredients:

- 1/2 cup yellow onion, chopped small (2-4 mm)
- 1/4 cup apple cider vinegar
- 2.5 pounds white potatoes
- Heavy dash of salt (for cook-water)
- 1/2 pound tempeh bacon
    - 8 oz package of tempeh, diced 7-10 mm
    - 60 g tamari
    - 1/8 tsp liquid smoke
    - 1-2 g molasses (maybe just shy of 1/4 tsp)
    - 7 g (1.5 tsp) red curry paste (Thai Kitchen)
    - Several Tbsp olive oil
    - Dash of tamari for finishing
- 1 clove garlic, minced
- 1 tablespoon whole-grain mustard
- 1/4 cup chopped fresh lovage
- 2 tablespoons chopped fresh parsley
- 1 tablespoon chopped fresh chives
- 1/2 cup extra virgin olive oil
- Four-finger pinch of salt (1/4 tsp?)
- Two-finger pinch of black pepper

Steps:

- Combined onion and vinegar and stored in fridge overnight (to cut
  the sharpness of the onion).
- Mixed tempeh bacon marinade (tamari, liquid smoke, molasses, red
  curry paste), tossed tempeth in it, and let sit to marinate.
- Put potatoes into salted water and brought to boil until potatoes
  were tender. Drained and allowed to cool just enough to handle. Cut
  into 1-inch chunks and covered to keep warm.
- (Now is a good time to do other ingredient prep—chopping herbs,
  mincing garlic.)
- Tempeh bacon:
    - Brought oil to medium high heat in skillet, then added tempeh
      and cooked, stirring occasionally, until browned.
    - Put a dash of tamari into the marinade bowl and used it to
      collect some of the the residue that hadn't made it into/onto
      the tempeh, then tossed it with the tempeh with the heat still
      on medium/high. (This creates a strong burnt umami/bacon
      flavor.)
    - Turned heat off and stirred in minced garlic.
- Added mustard to onion and vinegar mixture and whisked well. Added
  potatoes, tempeh bacon, lovage, parsley and chives and tossed well,
  then added olive oil, salt, and black pepper and mixed to combine.

Notes:

- Pretty nice! It had an unpleasantly strong vinegar+lovage smell just
  after being mixed, but it mellowed after sitting for a bit.
