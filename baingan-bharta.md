# Baingan Bartha

## 2016-09-05

Based on http://indianfood.about.com/od/vegetarianrecipes/r/bainganbharta2.htm

- Olive oil
- 484 g (4) skinny eggplants
- 1 tsp cumin seeds
- 1 tbsp crushed garlic
- 3 cm ginger, grated fine
- 1 small/medium jalapeño, minced
- 71 g (large bunch) green onions (just the green parts), chopped
- 374 g (7) plum tomatoes, chopped fine
- 1/2 tsp coriander powder
- 1/2 tsp cumin powder
- 1/2 tsp garam masala
- 2 tbsp cilantro, chopped

- Pan-roast the eggplant, remove any charred skin, and mash
  coarsely. Set aside.
- In an oiled skillet over medium heat, add cumin seeds and cook until
  spluttering stops.
- Add the green onions and fry till soft and changing color.
- Add the garlic and the ginger and fry for 1 minute.
- Add the tomato, coriander powder, cumin powder, and garam
  masala. Stir well and cook for 3-5 minutes, stirring often. Add a
  little water if needed.
- Add the eggplant and mix well. Add the chopped cilantro and stir.
- Cook another minute and turn off the heat.

Turned out great. Maybe could use a bit of salt.
