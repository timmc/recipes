# Hand sanitizer

## Everclear

Making 70% w/w ethanol solution from 190 proof Everclear.

- 38 grams of tap water
- 118 grams of 190 proof Everclear (95% v/v = 92.5% w/w)

Makes 156 g of 70% ethanol by weight. That's the amount I need in my
usual container; you should scale up or down as necessary.

Recommend adding water to container first, in case of accidental overaddition.

Notes:

- Caution: "Everclear" sold in Massachusetts is generally the 190
  proof stuff—in some areas, it is sold at a far lower concentration!
  Make sure you know which you have. (And 190 proof, in the US, means
  95% alcohol by volume.)
- According to a [lookup table][ethanol-vw] I found online for ethanol
  in water, 95% by volume is 92.5% by weight.

[ethanol-vw]: https://www.bode-science-center.com/center/hand-hygiene/hand-disinfection/detail-hand-disinfection/article/ethanol-concentration-table-what-is-the-percentage-by-volume-or-weight.html

Derivation of ratio `r` of Everclear to water, by weight:

```
Mixture: 100 g Everclear + W g water = 100 + W g total
70% ethanol = 92.5% ethanol / (100 + W g total)
0.7 = 92.5 / (100 + W)
70 + 0.7 W = 92.5
0.7 W = 22.5
W = 32.1...

Answer: 100 parts Everclear to ~32 parts water
```
