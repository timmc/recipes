# Tim's recipes

Recipes I've made. Some are even original.

A few notes:

- After I got a garlic crusher, but before roughly March of 2019, I
  used "mince" and "crush" pretty much interchangeably when talking
  about garlic. There's probably a difference in some recipes, so I'll
  be more precise in the future.
- You may notice me use the word "batata" instead of "sweet potato"
  (*Ipomoea batatas*), but not consistently. This is because I
  disliked the confusion that "sweet potato" causes, so I started
  saying "batata", the Spanish word, instead. I later learned that
  "batata" refers to potatoes (*Solanum tuberosum*) in Portuguese or
  even yams in Spanish. (It's an important difference, given that
  *Ipomoea batatas* is mildly toxic raw but has edible shoots, whereas
  *Solanum tuberosum* is edible raw and has poisonous shoots. And then
  yams are a whole 'nother thing.) In any event, I've given up on
  finding an unambiguous word to use here.
