# Hummus

2022-05-12 is my current standard recipe. (It's the most I can
comfortably make in my food processor at a time.)

Tip: Chickpeas tend to expand 3x in volume when cooked.

## 2018-09-02

Prep: Soaked 3.5 cups dried chickpeas overnight. Changed water, boiled
until thoroughly cooked. Produced 10 cups cooked chickpeas.

- 4 cups cooked chickpeas
- 3/4 cup tahini
- 1 cup evoo
- 6 Tbsp lemon juice (from bottle)
- 1/4 cup water
- 1 tsp table salt
- 1 tsp ground cumin

I thought it tasted heavy on the lemon and needed more cumin; Alex
thought it tasted heavy on the olive oil and could use more
lemon. Close enough!

## 2018-09-03

- 6 cups cooked chickpeas
- 18 g garlic cloves (about 4 cloves)
- 1 cup olive oil
- 1/2 cup lemon juice
- 1 tsp ground cumin
- 1 tsp table salt

## 2018-10-10

- 922 g cooked chickpeas (6 cups)
- 15 g garlic cloves
- 316 g evoo (1.5 cup)
- 120 g tahini (1/2 cup)
- 149 g lemon juice, fresh (3 lemons)
- 1.5 tsp ground cumin
- 1.5 tsp salt

## 2019-01-30

More garlic than last time, added ancho chile powder.

- 919 g cooked chickpeas
- 29 g peeled garlic cloves
- 1.5 cups evoo
- 123 g tahini
- ~150 mL (9.5 Tbsp) lemon juice (part from 1 fresh lemon)
- 1.5 tsp ground cumin
- 1.5 sp table salt
- 1 tsp ancho chile powder

Chile powder didn't do much.

I could easily see doubling the garlic (would start to actually burn a
bit), and perhaps increasing the cumin by 50%.

## 2019-04-07

Cooked chickpeas according to Yotam Ottolenghi's preferred method.

- 2 cups dried chickpeas
- 1.5 tsp baking soda
- 10.4 cups water
- 1/4 cup olive oil
- 28 g garlic, crushed
- 8 Tbsp lemon juice
- 1 tsp ground cumin
- 1 tsp table salt

Chickpeas cooked as follows, although 1.6x as much dried chickpeas as
original:

- Soaked ~8 hours
- Drained, added to pot with baking soda
- Cooked on high heat for 4 minutes with continuous stirring. Around
  minute three, the pot started sizzling. (Original calls for three
  minutes.)
- Added water and brought to a boil
- Cooked 40 minutes, until mushy. (Original calls for "tender, almost
  mushy".)
- Drained, and rinsed with cold water. (Rinsing not part of original,
  but they tasted like baking soda, and this helped.)

Very creamy, quite tasty.

## 2019-05-21

Like 2018-10-10 for ingredients, scaled 1.15x. Used cooking method
from 2019-04-07, but with less baking soda.

- 378 g dried chickpeas (1063 g cooked)
- 1 tsp baking soda
- ~8 cups water
- 26 g garlic cloves
- 360 g evoo
- 171 g lemon juice (1 fresh lemon + 138 g lemon juice from a bottle)
- 140 g tahini
- 1.75 tsp ground cumin
- 1.75 tsp table salt

Directions:

- Soaked chickpeas 23 hours (8 would have been fine) with one change
  of water; picked out any funny looking ones
- Drained, added to pot with baking soda
- Cooked on high heat for 4 minutes with continuous stirring. Around
  minute three, the pot started sizzling.
- Added 8 cups of water to cover and brought to a boil
- Cooked 40–50 minutes, until tender
- Drained, and rinsed with cold water to remove baking soda flavor
- Process together cooked chickpeas, garlic, evoo, lemon juice,
  tahini, cumin, and salt until smooth.

Pleasantly creamy. (I wonder how much of this is due to the water
carried on and around the loose chickpea skins.) Cumin and garlic
present in flavor but not assertive, for better or worse.

Conversion ratios from this session:

- Dry: 378 g / 2 cups
- Soaked: 881 g / 5.5 cups
- Cooked: 1063 g / 5.5 cups (probably a lot of retained water held
  between the loosened and shed skins)

## 2019-05-25

Garlic-free version. More chickpea-heavy than usual.

Used the baking soda technique again (with 1 tsp of baking soda to 2
cups dried chickpeas) and again got a 40 minute cook time.

This time, instead of just leaving the copious loose chickpea skins to
float around and be incorporated into the hummus, I tried removing
them by skimming. With the pot full of water:

- Stir the pot
- Let chickpeas settle out
- Before the skins settle out, scoop them out with a sieve

After maybe 10 iterations, this had diminishing returns, so I stopped.

I don't have any particular reason to remove the skins, other than
that I know some people do it, and wanted to see how difficult it
was. They probably have good nutrients and fiber, so I likely won't do
this again.

- 2 cups dried chickpeas (800 g cooked after skins removed)
- 61 g evoo
- 61 g lemon juice (from a bottle)
- 56 g tahini
- 1.5 tsp ground cumin
- 1.5 tsp table salt

Very tasty. Tahini is suitably in the background; lemon and cumin are
more foreground.

## 2019-10-09

Baking soda method.

- 378 g dried chickpeas
- 1 tsp baking soda
- 8 cups water
- 30 g garlic cloves
- 144 g fresh lemon juice (3.5 lemons)
- 360 g evoo
- 140 g tahini
- 2 tsp ground cumin
- 1.75 tsp table salt

A little bitter, and the food processor gets pretty full. Probably
could stand to cut the evoo in half.

Garlic level is good, but might be too strong for the kiddo. Perhaps
go back down to 25 g.

## 2019-10-14

Baking soda method, but way less olive oil than last time.

- 380 g dried chickpeas
- 1 tsp baking soda (for cooking chickpeas)
- water (for cooking chickpeas, plenty to cover)
- 27 g garlic cloves (14 g for mild)
- 116 g lemon juice (3 lemons, fresh-squeezed)
- 100 g evoo
- 50 g tahini
- 2 tsp ground cumin
- 1.75 tsp table salt

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Drain, and combine in pot with baking soda
- Stir constantly over high heat until chickpea skins are ragged, 3-4
  minutes (near the end the residual water finally evaporates and
  sizzling sounds start)
- Immediately add water (to prevent burning) and bring to a boil, then
  reduce to a simmer
- Chickpeas are done when soft or even mushy; drain and rinse with
  cold water to remove residual baking soda

Combine chickpeas and remaining ingredients in food processor and
blend until smooth.

Great flavor and texture.

## 2019-10-28

Baking soda method, same as 2019-10-14

- 380 g dried chickpeas
- 1 tsp baking soda (for cooking chickpeas)
- water (for cooking chickpeas, plenty to cover)
- 24 g garlic cloves
- 131 g lemon juice (2 lemons, fresh-squeezed)
- 100 g evoo
- 50 g tahini
- 2 tsp ground cumin
- 1.75 tsp table salt

Just as good as before.

## 2020-03-08

Modified baking soda method, where baking soda is used for abrasion
step but then rinsed off before boiling.

- 380 g dried chickpeas
- 1 tsp baking soda (for cooking chickpeas)
- water (for cooking chickpeas, plenty to cover)
- 38 g garlic cloves
- 18 g lime juice (half lime, fresh squeezed)
- 77 g lemon juice (2 lemon, fresh-squeezed)
- 37 g lemon juice from bottle
- 100 g evoo
- 50 g tahini
- 2 tsp ground cumin
- 1.75 tsp table salt

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Drain, and combine in pot with baking soda
- Stir constantly over high heat until chickpea skins are ragged, 3-4
  minutes (near the end the residual water finally evaporates and
  sizzling sounds start)
- Immediately add water (to prevent burning), then rinse and drain
- Add fresh water and bring to a boil, then reduce to a simmer
- Chickpeas are done when soft or even mushy; drain and rinse with
  cold water to remove residual baking soda

Combine chickpeas and remaining ingredients in food processor and
blend until smooth.

Notes:

- Worked just fine! Took 45 minutes to cook.
- Next time, try this method *without* baking soda, to see if the
  abrasion from heated dry stirring alone is sufficient.

## 2020-03-25

Baking soda method... but without the baking soda! (Just abrasion.)

In self-isolation, so going light on lemon juice, olive oil, and
tahini, replacing some with water.

- 380 g dried chickpeas
- water (for cooking chickpeas, plenty to cover)
- 27 g garlic cloves
- 48 g lime juice (1 lime, fresh-squeezed)
- 42 g lemon juice from bottle
- 45 g evoo
- 30 g tahini
- 20 g water
- 2 tsp ground cumin
- 1.75 tsp table salt

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Drain, and put in cookpot
- Stir constantly over high heat until the residual water evaporates
  and the pot starts to sizzle, 3-4 minutes
- Immediately add water and bring to a boil, then reduce to a simmer
- Chickpeas are done when soft or even mushy, about 45 minutes after
  first placed on heat. Can let sit in hot water with fire off for a
  while longer to make them even softer without further energy use, if
  desired.

Combine chickpeas and remaining ingredients in food processor and
blend until smooth.

Notes:

- 885 g chickpeas when cooked
- Worked great! Baking soda doesn't seem to be necessary. But should
  try another batch and skip even the abrasion step, just to check
  whether these chickpeas cook abnormally fast. :-)

## 2020-04-02

Like 2020-03-25 (light on flavors), but skipping the abrasion step for
the chickpeas.

A little more water and less lemon juice, as it happened, and slightly
less garlic.

- 380 g dried chickpeas
- water (for cooking chickpeas, plenty to cover)
- 25 g garlic cloves
- 44 g lime juice (1 lime, fresh-squeezed)
- 40 g lemon juice from bottle
- 45 g evoo
- 25 g tahini
- 26 g water
- 2 tsp ground cumin
- 1.75 tsp table salt

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Drain, rinse, and put in cookpot
- Add water to cover by an inch or two and bring to a boil, then
  reduce to a simmer
- Chickpeas are done when soft or even mushy, about 45 minutes after
  first placed on heat. Can let sit in hot water with fire off for a
  while longer to make them even softer without further energy use, if
  desired.

Combine chickpeas and remaining ingredients in food processor and
blend until smooth.

Notes:

- 856 g chickpeas when cooked
- Chickpeas did cook in 45 minutes, but were slightly firmer than with
  abrasion method.
- Should continue using abrasion, but without baking soda. Baking soda
  might be more important for older, drier chickpeas?

## 2022-02-23

1.5x 2019-10-14 and without using baking soda method. About 15% extra
lemon juice because that's what I got out of the two lemons I
had. Slightly more garlic as well.

- 570 g dried chickpeas
- 24 g garlic cloves (for mild)
- 200 g lemon juice (2 large lemons, fresh-squeezed)
- 150 g evoo
- 75 g tahini
- 3 tsp ground cumin
- 2.5 tsp table salt
- water as needed for consistency (40 g?)

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Change water, covering to 2-3 cm
- Bring to a boil uncovered, remove and/or break up the foam, boil for
  another minute, then cover and reduce to a light boil
- When chickpeas are done (soft or even mushy), remove from heat and drain

Combine chickpeas and remaining ingredients in food processor and
blend until smooth.

Notes:

- This is about as much as this 14 cup food processor can handle. I
  had to add a little water to get it to mix properly. Maybe take 10%
  off this quantity.

## 2022-05-12

2022-02-23, but about 10% less. This is still a moderately garlicky
recipe; some sharpness when freshly made, and mellows a bit (but still
retains some sharpness).

- 500 g dried chickpeas (1132 g when cooked)
- 22 g garlic cloves
- 145 g lemon and lime juice
    - If scaled properly, this would have been 175 g
- 132 g evoo
- 67 g tahini
- 2.6 tsp ground cumin
- 2.2 tsp table salt
- 30 g water
    - For consistency, but also to make up for reduced citrus juice

Directions for cooking chickpeas:

- Soak chickpeas overnight
- Change water, covering to 2-3 cm
- Bring to a boil uncovered, remove and/or break up the foam, boil for
  another minute, then cover and reduce to a light boil
- When chickpeas are done (soft or even mushy), remove from heat and drain

Combine chickpeas and remaining ingredients in food processor and
blend until smooth. (May want to start with just garlic and half of
chickpeas to make sure the garlic gets extra processing.)

14-cup food processor handled it OK, with a little manual
stirring/folding. About 3.5 pints of hummus.
