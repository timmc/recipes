# Brownies

## 2015-04-19: Gluten free, teff

Adapted from http://glutenfreegirl.com/2012/04/gluten-free-brownies/
using walnuts instead of hazelnuts and large instead of extra large
eggs (itself an adaptation of a recipe from Kitchen Sense.)

- 4 ounces unsalted butter
- 2 ounces unsweetened chocolate
- 1 cup turbinado sugar
- 2 eggs
- 1 teaspoon Madecasse vanilla extract ("double-strength", it says --
  use more of other kinds?)
- 100 grams teff flour
- 2 handfuls chopped walnuts
- 2 handfuls 67% chocolate chips

Preheat to 350°F. Grease an 8-inch baking pan with butter.

- Melt together butter and unsweetened chocolate and mix
- Let cool until you can touch it
- Mix in sugar
- Mix in eggs, one at a time.
- Mix in vanilla extract
- Mix in teff flour, thoroughly
- Mix in chopped walnuts and chocolate chips

Pour into pan and spread evenly. Bake until the edges have begun to
pull away from the pan and the center is just starting to set, about
20 to 25 minutes. Cool for at least 15 minutes.

Makes about 2 dozen brownies.

## 2017-06-03: Gooey deliciousness

- 1 cup quinoa flour
- 1 cup teff flour
- 1 tsp xanthan gum
- 4 eggs
- 2 cups sugar
- 1/8 tsp salt
- 2 sticks of butter (or sub 1 cup oil)
- 1 tsp vanilla extract
- 4 oz unsweetened chocolate
- Optional: 2 habanero peppers, with seeds and ribs removed, minced

Melt the chocolate and butter (or oil) together. Add in the minced
habenero and let sit while prepping other ingredients. Mix all.

Bake in an 8"x8" pan at 350°F for 20-25 minutes. Undercook -- edges
should just be starting to set such that a toothpick comes out clean,
and the center gooey.

Notes:

- This was written down as requiring 1 cup of flour, half teff and
  half quinoa. However, this is almost certainly wrong, since the
  original recipe had *two* cups of flour but was otherwise almost
  identical; I probably added one cup of each flour. I've adjusted the
  recipe to reflect this.

## 2020-09-07

Replicating 2017-06-03 ("Gooey deliciousness") but with pecans and
without peppers. Also had to use larger pan.

- 8 oz (2 sticks) of unsalted cultured butter
- 4 oz unsweetened chocolate
- 160 g (1 cup) teff flour
- 160 g (1 cup) Bob's Red Mill GF AP flour
- 440 g (2 cups) sugar
- 1/8 tsp salt
- 1 tsp xanthan gum
- 1 cup (120 g?) chopped pecans
- 4 eggs
- 1 tsp vanilla extract

- Melt together butter and chocolate and allow to cool to merely warm
- Mix dry ingredients: Both flours, sugar, salt, xanthan gum, pecans)
    - Should have mixed in pecans separately so that I could check for
      lumps of flour
- Mix wet ingredients: Eggs, vanilla, butter/chocolate mixture
- Mix all

Bake in an 8"x12" pan at 350°F for 20-25 minutes. Undercook -- edges
should just be starting to set such that a toothpick comes out clean,
and the center gooey.

Best served cool or even cold.

## 2021-01-03 - citrus peel and pecan

2020-09-07 with chopped up citrus peels -- Persian lime, Changsha
tangerine, Meyer lemon -- from fruits that my father grew.

- 8 oz (2 sticks) of unsalted cultured butter
- 113 g (4 oz) unsweetened chocolate
- 103 g citrus peels, minced fine (about 6 small fruit)
- 160 g (1 cup) teff flour
- 160 g (1 cup) Bob's Red Mill GF AP flour
- 440 g (2 cups) sugar
- 1/8 tsp salt
- 1 tsp xanthan gum
- 120 g (1 cup) chopped pecans
- 4 eggs (67 g avg weight: extra-large)
- 1 tsp vanilla extract

- Melt together butter and chocolate and allow to cool to merely warm
- Mix in minced citrus peels and allow to sit 20-25 minutes so the
  oils leech out a bit
- Mix powders first (sifted flours, sugar, salt, xanthan gum) and then
  add pecans
- Mix wet ingredients: Eggs, vanilla, butter/chocolate mixture
- Mix all

Bake in a greased 8"x12" pan at 350°F for 20-25 minutes. Undercook -- edges
should just be starting to set such that a toothpick comes out clean,
and the center gooey.

Best served cool or even cold.

Notes:

- The citrus was strong enough in these to compete with the
  chocolate. Would work fine with half the amount.
- The little citrus pieces become noticeable and chewy at the tail
  end, once most of the bite has been swallowed. Not unpleasant to me,
  but I suggest using the freshest peels available, before they've
  started to dry out and become harder.

## 2022-09-07 - Raspberry

2020-09-07 but with raspberry preserves (and no pecans). We tried this
before with raspberry jam mixed thoroughly in and it was hard to
detect the raspberry at all, but also failed to write down the
quantities. So on this one I'm going to err on the side of more, use
preserves (more intense raspberry, maybe?), and just swirl it rather
than stirring it in.

- 8 oz (2 sticks) of unsalted cultured butter
- 4 oz unsweetened chocolate
- 160 g (1 cup) teff flour
- 160 g (1 cup) Bob's Red Mill GF AP flour
- 440 g (2 cups) sugar
- 1/8 tsp salt
- 1 tsp xanthan gum
- 4 eggs (230 g)
- 1 tsp vanilla extract
- 173 g (1/2 cup) raspberry preserves (7 g added sugar per 18 g)

- Melt together butter and chocolate and allow to cool to merely warm
- Mix dry (flours, sugar, salt, xanthan gum) and then add and mix wet
  (eggs, vanilla, butter/chocolate mixture)
- Preheat oven to 350°F
- Grease an 8"x12" pan and add batter
- Dollop the preserves randomly onto the batter, avoiding the edges,
  then swirl in with a fork

Bake for 20-25 minutes. Undercook -- edges should just be starting to
set such that a toothpick comes out clean, and the center gooey.

Best served cool or even cold.

