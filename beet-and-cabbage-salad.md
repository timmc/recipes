# Raw beet, cabbage, and orange salad

From: How to Cook Everything Vegetarian (2007, p.49)

## 2013-10-17

- Seasoning (prep first to allow dried tarragon to steep)
  - 1/2 tsp dried tarragon (was possibly a bit old?)
  - 1/2 tsp somewhat fresher dried tarragon: Grind
  - 2 tbsp Gulden's spicy brown mustard
  - 2 tbsp evoo
  - 4 tbsp red wine vinegar (original called for sherry)
  - 1/2 tsp ground pepper blend
  - 1/2 tsp salt
- 1/4 med/large cabbage: Shred
- 3 medium beets: Peel and grate
- 4 small scallions (no bulbs, already used elsewhere)
- 2 navel oranges, chopped (+ juice!)
- Mix!
- Results:
  - Not as flavorful as last time -- too little tarragon or salt?
    Still good, though!

## 2014-07-12: Fermented edition

- 8 medium beets, peeled where skin looked tough, sliced 1 cm
- 4 med scallions (whites and greens) roughly chopped
- 12 sprigs tarragon
- 2 tbsp mustard seeds, coarsely ground (some still whole)
- 5 tsp coarse salt
- 3.25 cups water
- 1 leaf Napa cabbage
