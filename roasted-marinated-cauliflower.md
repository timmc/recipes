# Marinated whole roasted cauliflower

## Inspirations:

- http://www.purewow.com/entry_detail/recipe/8821/Forget-florets--roast-the-whole-damn-cauliflower.htm
- http://www.mynewroots.org/site/2012/09/whole-roasted-tandoori-cauliflower-with-mint-chutney/

## Instructions

Ingredients vary.

- Prep marinade by mixing all non-cauliflower ingredients.
- Prep cabbage: Remove woody bits and leaves, but otherwise keep
  intact. (Save leaves for use as cabbage substitute.) Wash.
- Coat cauliflower in marinade. Perhaps put cauliflower upside-down in
  bowl and pour marinade over it and around the sides, then pour off
  extra and repeat.
- Let marinate for 0-12 hours.
- Save excess marinade for dipping.
- Roast right-side-up on greased baking sheet at 400°F for 30-50
  minutes until surface is dry and lightly browned. The marinade makes
  a crust.

## Variations

### 2015-5-24

Follows close to purewow.com recipe.

- Cauliflower
- 325 mL kefir and filmjölk (1:2) (instead of greek yogurt)
- 1 tbsp cumin
- 1 lime: juice and zest
- 1 tbsp garlic powder
- 2 tsp kosher salt
- 1 tsp ground black pepper
- 1 tsp "curry powder"
- 1 tsp chile powder (instead of 2 tbsp)

Marinated 18-ish hours, roasted at 400°F for 50 minutes and formed a
nice crust. Marinade dripped off and caramelized on the pan.

### 2016-08-06

Added tapioca to try to get sauce to stick better since filmjölk is
thinner than yoghurt.

Very approximate, did not use a measuring implement:

- Large cauliflower
- 2-3 cups filmjölk (1:2)
- 2 tbsp cumin
- 1 lime: juice
- 2 tbsp garlic powder
- 1.5 tsp kosher salt
- 1.5 tsp ground black pepper
- 2 tsp "curry powder"
- 3 tsp paprika
- 1.5 tsp modified tapioca starch

Marinated ~5 hours.

Roasted at 400°F for about 50 minutes.

Tasty! Some of the marinade on the inside of the cauliflower did not
form a crust, and stayed gooey. That was fine.

### 2017-09-09

- Cauliflower
- 325 mL whole milk goat kefir
- 2 tbsp lime juice (from bottle)
- 1 tbsp cumin
- 1 tbsp garlic powder
- 1.5 tsp table salt
- 1 tsp ground black pepper
- 1 tsp "curry powder"
- 1 tsp chile powder

Marinated at room temperature ~21 hours (with periodic bastings).

Roasted at 400°F for about 50 minutes.

Really spicy -- if using freshly ground black pepper again, use much
less.
