# Fries

## 2020-10-03 - Baked sweet potato fries with chili powder

- Large sweet potato, cut into 1 cm wide fingers
- Olive oil
- Chile powder (used about 2 tsp of my 2015-11-26 no-onion mix)

Preheat oven to 400°F. Toss sweet potato with liberal amounts of olive
oil. Bake for 45 minutes. Put back in bowl and toss with chile powder.

(Note: This sweet potato had been sitting out for months and had been
starting to sprout, and may have been on the dry side as a
result. This may have affected cooking time.)
