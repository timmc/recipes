# Lentil spaghetti sauce

A hearty sauce I've made uncounted times. I tend to use it
half-and-half on pasta, or occasionally as a topping on bread with
cheese melted on top.

Swap in fancier olives for a fancier dish.

Recipe via Alex.

## Base recipe

- 1 cup lentils
- 2 cups water
- Olive oil
- 1 head of garlic, minced
- Optional: 1 medium yellow onion, chopped (substitute: 1 "pea" of
  chunk asafoetida resin, ground into powder); I almost never include
  this, though
- 1 tbsp dried oregano (double if fresh)
- 2 28 oz cans tomatoes (usually contains some salt, adjust if not) -
  chop if whole
    - Diced tomatoes seem to work best
- 1 can black or kalamata olives, drained (pitted) (optional)
- 1 tsp salt
- 150 g frozen spinach (optional)

### Instructions

- Bring lentils to boil in water, then set to simmer
- Sauté onion (or asafoetida) and garlic in olive oil
- Add the oregano, then a few seconds later add the tomatoes
- Add salt, olives, spinach (if using)
- Turn heat to medium-high
- When water is mostly cooked off of lentils, drain and add them to
  rest of dish
- Keep pot *just* bubbling; stir occasionally to prevent burning
- Cook until desired consistency

### Instructions for a novice chef

I wrote this set of instructions when I was first learning to cook and
was having anxiety about sequencing actions while heat was being
applied to ingredients. I haven't needed this version in years, but it
might be useful to someone else!

I've made a few edits for clarity and generality.

- Open tomato cans. If necessary, chop up the tomatoes while they are
  still in the can.
- Open olive can. Drain it.
- Get a tablespoon of oregano ready.
- Put these next to stove

- Put 1 cup lentils, 2 cups water in pot#1 and bring to boil, then
  down to simmer (so that lentils are moving)
- Chop onion, mince garlic

- Put a few tbsp olive oil in pot#2, turn on heat (medium)
- When hot enough for water droplets to sizzle, add onions and garlic
- When onions are starting to turn translucent around the edges and
  get a little squishy, add the oregano, then a few seconds later add
  the tomatoes
- Chop the tomatoes into quarters with the spatula (if necessary and
  not chopped earlier)
- Add 1 tsp of salt
- Add olives
- Stir all in
- Turn heat to medium-high

- When water is no longer visible on the surface of the lentils, turn
  off their heat

- Dump lentils (pot#1) into tomatoes (pot#2).
- Adjust heat so that pot is *just* bubbling. [ETA: I used to cover
  the pot, but don't do this anymore, since I want to drive excess
  water off.]
- If you leave the pot uncovered, stir in a teaspoon of oregano a
  couple of times during the cooking. [ETA: I rarely do this anymore!]

- You can now set up pot#1 for spaghetti

- When spaghetti is done, you may want to leave the sauce cooking a
  bit to thicken it.

## Experiments

### 2013-08-04 - Low FODMAPs-ish

- Double batch (4 Muir Glen, one was plum tomato by accident)
- one can of olives
- Sauté
    - Greens of two red onions, then
    - 3 smallish carrots, then
    - 1 head of garlic
- Added at end of sauté
    - handful of (thai?) basil
    - 4-5 pinches of dried large oregano leaves
    - A couple sprigs of flowering thyme
- Added after tomato sauce
    - 1/4 tsp probably-old ground white peppercorn (actually,
      smelled kind of foul when I added it to the pot, but the scent
      disappeared when the sauce started to cool)
    - Parsley (handful)
    - Leaves of 3 smallish carrots
    - 2 tsp salt (himalayan sea salt)

#### Results

- A more bright/acidic flavor than usual
- Overall: Lacking the dusky flavor from the onions, but a very decent
  spaghetti sauce.

### 2013-10-03 - With veggies (including squash!)

- Hot oil, flash fried:
    - 4 "peas" asafoetida resin
    - 1 jalapeño & 1 serrano (?)
- Medium heat for 5-10 minutes
    - 3 med-small carrots
    - 2 med-small potatos
- Hot, 5 minutes, stirred occasionally
    - 4 cloves garlic
    - Half a kuri winter squash, shredded
- 24 sauce tomatoes (16 cups) -- quenches
- 3 cups lentils cooked with 6 cups water
- At this point, it seems too thick, but then the tomatoes start
  releasing water.
- 4 tsp salt
- Green (well, purple) bell pepper, including core and seeds
- 1 cup sprouted mung beans (long roots)
- quarter of a lemon's juice
- 1 tsp garam masala ("Whole Pantry": black pepper, cardamom,
  cinnamon, cloves, cumin, coriander) -- but I could have just used
  coriander, I think.

#### Results

- About 5 qt
- Tastes rich and filling (lentils, asafoetida, and squash: win)
- Tomatoes are pretty minimal portion of flavor
- It's trying to taste sweet, like the squash
- I should have stuck with 1/2 tsp garam masala -- it was a nice hint
  at that point, and now it is a bit more of an assertive flavor.
- There's some slow-start lingering heat from the peppers -- it's
  amazing how much that serrano added, diluted as it was.

### 2020-02-03 - With home canned tomatoes

Triple recipe, with 5 jars of tomatoes from 2019-08-31 and 1 from
2019-09-14. Each jar has about 750 mL of tomatoes and liquid, which is
reasonably close to 28 fl oz. They're definitely watery, but the
flavor is good.

Next time, consider cooking the lentils in the liquid poured off from
the tomato jars + any necessary make-up water. This might reduce the
wateriness overall. (On the other hand, the acidity might make the
lentils tough.)

### 2020-03-03

Triple recipe, soaking lentils in 3 cups of water and then adding 3
cups of the tomato juice before boiling (see note on
2020-02-03). Substituting the liquid that separated out from the
home-canned tomatoes in place of some of the water reduces the overall
water content of the dish, making it thicker.

Turned out well! This is a good approach to use in the future for
home-canned tomatoes.
