# Sausage

Not making it -- cooking already-made sausage.

This is probably pretty basic stuff, but I don't have a lot of
experience with cooking meat, so I'm keeping notes on the specifics of
what has worked well.

Notes:

- If using liquid in a skillet, make sure there's plenty of surface
  area for the liquid to evaporate from.
- Cooking in red wine may make it harder to tell if the sausage is
  browning.

## 2020-12-06 - Sautéed Lamb Sausages

Based on https://www.livestrong.com/article/460445-how-to-cook-lamb-sausage/

- Heat 2 tbsp. olive oil in a medium skillet over high heat
- Use a sharp knife to prick four to five holes in each sausage to
  prevent splitting
- Add the sausages and 1 cup white wine to the pan and cover pan
- Turn the heat down to medium-low and let cook for five minutes,
  turning once
- Uncover the pan and bring the heat back up to high
- Sauté the sausages for eight more minutes, turning frequently, until
  the sausages are browned and the wine has evaporated
- Allow the sausages to rest for 10 minutes before serving

## 2021-03-14 - Sautéed Bison Sausages

Using bison sweet Italian sausage from Hackmatack Farm in Maine.

Same as 2020-12-06, but I used a small skillet that barely fit the
sausages, and the wine took 20 minutes to evaporate instead of 8,
probably overcooking the sausage. Use a medium skillet next time -- or
less wine, I guess.

## 2023-02-19 - Baked turkey sausage

This recipe specifically avoids puncturing the sausages, I suspect
because they'll be baked rather than steamed, and the liquid needs to
be kept in. This means that when you measure with a meat thermometer,
you should already be pretty sure they're ready...

- Preheated oven to 350°F for about 20 minutes, with rack in center position
- Cut links apart with sharp knife.
- Drizzled oil on baking sheet, placed sausages in oil, and roll them
  in it. Drizzled some more on them. This was to promote browning and
  reduce sticking.
- Ensured sausages were not touching each other and placed baking
  sheet into oven.
- After 15 minutes, turned them over using spatula and tongs to avoid
  burning them.
- Baked until they reached an internal temperature of around 165°F (or
  at least, the juices ran clear) about 25 minutes total.

These weren't to my taste (I guess I really just don't like turkey so
much), but baking seemed to work OK.
