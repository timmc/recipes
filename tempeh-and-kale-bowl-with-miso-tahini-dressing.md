# Smoky Tempeh and Kale Bowl with Miso-Tahini Dressing

## Original

Makes 4 servings (with excess sauce, in my opinion.)

Ingredients:

- 1 cup dry brown rice
- 2 1/4 cups vegetable broth
- 1 package tempeh, cut into 1/2" cubes
- 2 Tbsp tamari or soy sauce
- 1 Tbsp liquid smoke
- 1 Tbsp coconut oil
- 1 medium onion, sliced
- 2 cloves garlic, minced
- 4 packed cups kale, rinsed and dried
- Sauce ingredients:
    - 1 Tbsp miso paste
    - 4 Tbsp tahini
    - juice of 1 lemon
    - 3-6 Tbsp water

Directions:

Prepare rice by bringing vegetable broth to a boil in a small pot on
the stove. Add brown rice, cover and simmer. After 40 minutes, turn
heat off and let sit covered for at least 5 minutes before serving.

Mix together tamari and liquid smoke in a small bowl. Stir in tempeh
and let marinate for 5 minutes.

Heat coconut oil at medium heat in a large pan or wok on the
stove. Add garlic, onion and tempeh and sauté until edges on tempeh
brown. This should take about 10-12 minutes, stirring occasionally.

Add kale to pot, stir in, and cook until kale wilts (about 3 minutes).

Sauce: Combine all ingredients in a small bowl or cup. Start with 1/4
cup water and add to desired consistency.

Serve rice into bowls, top with tempeh and kale mixture and drizzle
miso-tahini sauce on top.

## 2017-11-21

The version I have actually made, with substitutions:

- Quinoa instead of rice (1:1)
- Water instead of vegetable broth
- Swiss chard and asparagus beans instead of kale
- Olive oil instead of coconut oil (and more of it)
- No onion

Also, doubled.

Ingredients:

- 2 cups quinoa, washed
- Sufficient water to cook quinoa
- 2 8oz packages tempeh, cut into 8 mm cubes
- 4 Tbsp tamari
- 1 Tbsp liquid smoke
- 3 Tbsp olive oil
- 4 cloves garlic, minced
- several cups chopped chard
- 1-2 cups chopped asparagus beans
- Sauce ingredients:
    - 2 Tbsp miso paste
    - 8 Tbsp tahini
    - 4-5 Tbsp lemon juice
    - 5-8 Tbsp water

Had quite a bit of excess sauce and about 2x the quinoa that the body
of the dish required.

## 2018-03-18

No garlic in this version; no asparagus beans either.

- 2 cups quinoa, washed
- Sufficient water to cook quinoa
- 2 8oz packages tempeh, cut into 8 mm cubes
- 4 Tbsp tamari
- 1 Tbsp liquid smoke
- 1 medium bunch rainbow chard
- Sauce ingredients:
    - 2 Tbsp miso paste
    - 8 Tbsp tahini
    - 4-5 Tbsp lemon juice
    - 5-8 Tbsp water

Same experience re: sauce and quinoa. Try halving those or doubling
the tempeh/chard mix.
