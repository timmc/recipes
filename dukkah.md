# Dukkah (duqqa, du'ah, دقة)

https://cnz.to/recipes/appetizers/dukkah-egyptian-spice-mix-recipe/

- 30 grams (1 ounce, 1/4 cup) hazelnuts
- 30 grams (1 ounce, 1/4 cup) shelled pistachios (unsalted)
- 4 tablespoons sesame seeds
- 2 tablespoons coriander seeds
- 2 tablespoons cumin seeds
- 1 teaspoon fennel seeds
- 1 teaspoon black pepper berries
- 2 teaspoons dried thyme
- 1 teaspoon salt

Instructions

- Toast the hazelnuts and pistachios in a dry skillet, and set
  aside. Toast the seeds and berries (from sesame to black pepper) in
  the skillet for 2 minutes, until fragrant, shaking the skillet
  frequently. Let cool completely.
- Combine all the ingredients, and grind to a coarse powder in an
  electric grinder or with a mortar and pestle. You may have to
  proceed in two batches.
- Pour into a jar, close tightly, and keep somewhere cool and dry.

## 2019-02-23

Followed above directions except used coarse ground pepper. Used
grinder, probably should have done two batches.

Very tasty, pretty salty though. Try reducing salt by half.

## 2019-03-05

Same as before, but used only 1/2 tsp salt. Worked fine.

Used a combination of the spice grinder and the mortar and pestle. I
think the mortar and pestle has better results, especially if spices
and nuts are processed separately.
