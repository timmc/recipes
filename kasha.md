# Kasha

Kasha refers to hulled and roasted buckwheat kernels, and to dishes
made with them.

I generally follow "How to Cook Everything Vegetarian"'s recipe on page 562.

## 2019-12-15: Leek and shiitake

This went off the rails a bit, but worked out OK in the end.

- 178 g kasha (1 cup)
- 1 egg
- 2 cups warm broth (2 cups warm water + 2 tsp Seitenbacher's
  vegetable broth & seasoning powder)
- 1/8 tsp fine ground black pepper
- 1/8 tsp + 1/4 tsp iodized table salt
- 1 large leek (448 g), quartered and sliced 2 mm
- 150 g shiitake mushrooms (1 pint), 1 cm dice (finer for stems)
- olive oil
- 2 Tbsp butter

Process:

- Stir egg into kasha
- Bring a dry cast iron skillet to high heat, then pour in kasha/egg
  mixture
- Stir frequently to continuously until eggs dries onto kasha and
  pieces of kasha no longer stick to each other
- Turn to very low and add broth (⚠️ watch for steam burns, pan is
  deceptively hot)
- Stir in 1/8 tsp salt and pepper, then leave covered until liquid is
  absorbed, about 15 minutes

- Meanwhile, cook leeks in a pot over medium heat, stirring occasionally
- When bits of the leeks are starting to brown and stick to the pot,
  stir in shiitake and several Tbsp of olive oil
- Bring to medium high heat and stir occasionally, adding more olive
  oil if sticking too much
- Bring down to low, add 1/4 tsp of salt, and continue cooking until
  the mushrooms stop tasting so mushroomy

- Combine pots and add the butter, stirring it in as it melts

Notes:

- Would have been easier to add salt and pepper to egg/kasha mixture
  before adding it to pot
- Definitely should have done the shiitakes differently. Maybe bake
  them first, or at least get them cooked to the point of near
  crispiness before adding leeks.

# 2020-12-26 - Low-FODMAPs

A low-FODMAPs version -- no onion, no onion-containing vegetable broth.

The only reason to use two cups of water here is that warm water is
called for, but I only felt like using one measuring cup, and things
dissolve better in hot water. It's not important.

- 1 egg
- 1 cup kasha
- 2 cups water (one hot and one cold)
- 1/4 tsp nutritional yeast
- 1/4 tsp + 1/4 tsp salt
- 1/4 tsp black pepper, freshly ground fine
- olive oil
- 27 g garlic, crushed
- 200 g carrot, chopped
- 200 g celery stalks, chopped (including leaves)
- 1.5 Tbsp unsalted cultured butter

Directions:

- Mix hot water with nutritional yeast 1/4 tsp salt, and black pepper
- Beat egg and mix with kasha
- Put in lidded cast-iron skillet and stir over high heat until egg
  largely dried, grains separating, and the mixture smells toasty/nutty
- Add hot and cold water, cover, and bring heat down to minimum. Let
  cook undisturbed until liquid is absorbed, about 15 minutes.
- Meanwhile, sauté garlic, carrots, and celery in olive oil until a
  bit tender. Add salt partway through.
- When both parts are done cooking, combine and let the butter melt,
  mixing it in gently.
