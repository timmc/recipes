# Indian-spiced Zucchini

## Original with potato from "Cilantro and Citronella"

From
https://www.cilantroandcitronella.com/indian-spiced-potatoes-and-zucchini/
(which notes that the recipe was adapted from The Asian Vegan Kitchen)

- 450 grams (16 oz) potatoes
- 1 tablespoon vegetable oil
- 1/2 teaspoon mustard seeds
- 1/2 teaspoon cumin seeds
- A pinch of asafoetida
- 1 clove of garlic, minced
- 1/2 teaspoon turmeric
- 1/2 teaspoon cayenne pepper
- 1 teaspoon coriander powder
- 1/2 teaspoon mango powder
- 1/2 teaspoon salt
- 2 medium zucchini, sliced into rounds

- First boil the potatoes until soft. Peel and slice them into rounds.
- Heat the oil in a large pan over medium heat. Add the mustard and
  cumin seeds and fry for 30 seconds or until the mustard seeds start
  to pop. Then add the asafoetida and garlic and fry, stirring
  continuously, for about 30 seconds or until the garlic is fragrant.
- Add the remaining spices and mix well. Then add the boiled potatoes
  and zucchini and carefully stir to coat the vegetable with the
  spices. Cover the pan with a lid and allow to steam, stirring
  occasionally, until the zucchini is tender – about 5 minutes. Serve
  as a side dish or alongside rice and naan or chapati.

## 2018-07-15

Made a variant on the original, with the following changes:

- Didn't have potato, so used carrots, sautéed at high heat
- Evoo as the oil
- Extra coriander powder
- Lemon juice in place of mango powder, and probably extra
- Possibly doubled all the spices? Can't recall.

Ended up with a brighter flavor than I was hoping for, thanks to the
extra coriander and lemon, but it was very tasty, and family approved
of it.

## 2018-07-29

Roughly doubled the ratio of spices to vegetables relative to the
original, except for: turmeric (halved); cayenne (stayed same);
coriander (tripled). Again substituting lemon juice instead of mango
powder (and tripled).

Ingredients:

- Olive oil
- 360 g eggplants (2 long purple), whole
- 690 g carrots (9 medium-small), chopped
- 690 g zucchini (1 medium, green), chopped to half-moons
- 1 teaspoon mustard seeds
- 1 teaspoon cumin seeds
- 0.1 g asafoetida, ground (double pinch)
- 10 g garlic (2 cloves), crushed/minced
- 1/4 tsp turmeric
- 1/2 tsp cayenne powder
- 2 + 1 tsp coriander seed powder
- 2 + 1 tsp lemon juice
- 1 tsp salt

Method:

- Pan-roast whole eggplants in a lightly oiled skillet (cook slowly on
  low-medium heat until skin is crisped and eggplant is deflating).
- Chop eggplants.
- Sauté carrots over high heat, browing lightly.
- Heat oil in cast iron skillet over medium-high heat.
- Add mustard and cumin seeds and fry for 30 seconds or until the
  mustard seeds start to pop.
- Add the asafoetida and garlic and fry, stirring continuously, for
  about 30 seconds or until the garlic is fragrant.
- Add the carrots and sauté, stirring frequently, until the carrots
  are just starting to sear and soften.
- Add the zucchini, eggplant, and remaining spices (salt, turmeric,
  cayenne, 2 tsp coriander) and 2 tsp lemon juice, and mix well to coat the
  vegetable with the spices.
- Cover and allow to steam, stirring very once or twice, until the
  zucchini is tender – about 5 minutes.
- Stir in remaining tsp of coriander and lemon juice.

Results:

- Good flavor. More understated than last time.
- Eggplant mostly dissolved into the other vegetables.

Suggestions:

- Had to add additional lemon juice and coriander at end to adjust
  flavor; next time try adding before lid goes on.
- Cayenne did not spread evenly. Try mixing the spices together
  thoroughly next time before adding, and sprinkle more evenly.
- Consider ways to produce more of a sauce. More eggplant?
- Garlic browned more than I'd like. Either sauté carrot separately or
  add garlic and asafoetida late in the carrot sauté.

## 2018-12-12

- 487 g potatoes (4 medium red)
- 1.2 kg zucchini (6 med/small)
- Olive oil
- 1/2 teaspoon mustard seeds
- 1/2 teaspoon cumin seeds
- A pinch of asafoetida
- 5 small cloves garlic
- 1/2 teaspoon turmeric
- 1/8 teaspoon cayenne pepper
- 1 teaspoon coriander powder
- 1 Tbsp lemon juice
- 1/2 teaspoon salt

Too much veggies for the spices, I think. Tasted fine, but I'd like
stronger spices.
