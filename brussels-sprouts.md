# Brussels sprouts

## 2019-11-09

- 486 g brussels sprouts (maybe 2/3 of a stalk)
- 4 tsp olive oil
- 4 tsp unsalted butter
- 1 tsp salt
- 1/8 tsp black pepper
- 1 Tbsp beer vinegar (IPA)

Directions:

- Shred brussels sprouts using food processor with slicing attachment
- In a skillet over medium heat, melt butter and add olive oil
- Add brussels sprouts and begin to sauté
- Add salt and black pepper
- Continue sautéing until sprouts are tender but still bright green
- Turn off fire and stir in vinegar

Pretty buttery. Could probably use more oil, less butter, as evidenced
by how it "set up" in the fridge.
