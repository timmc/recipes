# Celery Garlic Soup

## 2016-10-23

- 2 clusters of soup celery (Tango variety)
- 8 medium-large cloves garlic
- Olive oil
- 2 tsp apple cider vinegar
- 1.5 tsp salt
- 1/2 tsp black pepper
- 4 small/medium potatoes
- 2 bay leaves
- 6 cups water
- Some sautéed carrots pieces (from ~2 carrots)

Make the roasted portion:

- Strip and reserve celery leaves
- Roughly chop celery ribs
- Toss celery ribs and garlic with about 1 Tbsp olive oil, vinegar,
  0.5 tsp salt, black pepper
- Roast in a baking tray at 375°F until first brown spots appear (25
  minutes)
- Chop smaller

Start the stovetop portion while roasting:

- Chop the potatoes
- Chop the celery leaves
- Sauté the potatoes and celery leaves
- Add water and bay leaves
- Add the roasted celery and garlic
- Bring to a boil, then simmer
- Add 1 tsp salt (to taste)
- When it seems about done, remove from heat
- Remove bay leaves
- Add sautéed carrots
- Purée

Notes:

- I only roughly chopped the celery ribs at first since the original
  recipe called for puréeing everything and the size didn't matter.
  Next time I might try chopping the celery more finely before
  roasting so I don't have to do it twice. On the other hand, this
  soup celery has small ribs, so the pieces became a little tough
  after roasting; that might get worse if I chop them smaller.
- 2 heads of soup celery resulted in 2.5 cups rough-chopped stems and
  6 cups chopped leaves
- I added the sautéed carrots as an afterthought to cut the bitterness
  a bit.
