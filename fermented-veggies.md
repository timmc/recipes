# Fermented vegetables

...of various sorts.

## Everything I know about lacto-fermentation

What goes in:

- 2% salt by weight
- Shredded or cut vegetables
- Sugary things can lead to goopiness

The container:

- Metal lids will corrode (unless you put plastic in between lid and
  jar) even without liquid contact.
- Glass or ceramic jars are important (plastic might be OK, never
  metal)
- Don't tighten the lid too much while it's still bubbling -- let the
  gas seep out to prevent explosions.
- Leave some headroom (an inch or two) to prevent overflow, especially
  if it's vigorous and you aren't pushing the air out regularly

Waiting:

- Not every ferment bubbles vigorously
- You can (mostly) stop a fermentation early by refrigerating it, if
  you want lower acid or more crispness

Safety:

- Keep things below liquid (or regularly submerge) to prevent mold
- If you have enough salt and it doesn't smell, look, or taste bad...
  it's safe.

## 2020-11-04: Green tomato, cabbage, and radish

Vaguely chow chow inspired.

- 250 g green tomatoes, 10 mm dice
- 250 g green cabbage leaves, chopped 7 x 15 mm
- 250 g (mostly) white daikon radish, sliced 4 x 20 x 20 mm
- 25 g garlic cloves, 6 mm dice
- 1 tsp brown mustard seeds
- 1/8 tsp celery seeds
- 15 g sea salt (about 1 Tbsp)

Tossed all, let sit, packed into jar.

Probably should have cut smaller so it could be used as a relish.

Fermentation started within a day or two.

Result: Pleasant and interesting, but after a few weeks loses some of
its distinctiveness and mostly tastes and smells like kimchi.

## 2020-11-04: Green tomato

- 1600 g green tomatoes, about 10 mm dice
- 32 g sea salt (about 2 Tbsp, rounded)

Tomatoes tossed with salt, let sit, packed into jar.

- 11/6: After 2 days, I didn't see any tiny bubbles, so just to be
  sure I dumped in a small spoonful of brine from some 12 day old
  sauerkraut.
- 11/7: Starting to bubble. Unfortunately, mostly just smelling like
  yeast -- kind of bready

Result: Odd. Seems like a good way of preserving the tomatoes, as
their texture has held up well. However, there's a strange flavor that
could be offputting -- that bready smell and flavor.
Possibly relatedly, it is prone to growing kahm yeast, even in
the fridge. The brine is goopy, but the tomatoes are not actually slimy; when
rinsed off, the tomatoes are perfectly crisp. Rinsing also removes most
or all of the yeasty flavor.
Kind of usable for salsa if making tacos, although I diced
it too coarsely.

## 2021-04-03: Curtido

Recipe based on
https://www.myfermentation.com/vegetables-and-nuts/curtido-salvadoran-fermented-cabbage-zbrz2003

Salt to 2% of vegetable weight, which ignores lime juice. (I think
this is fine because the lime juice is acidifying.)

All vegetable weights are after trimming.

- 394 g cabbage (half a small green cabbage), sliced fine and into
  pieces no longer than 4 cm
- 131 carrot (two small), grated
- 49 yellow onion (one small), sliced fine and into pieces no longer
  than 2 cm
- 9 g garlic, finely minced
- 24 g (~4.5 tsp) lime juice (half a lime)
- 1 g (~1.3 tsp) dried oregano
- 5 g jalapeño pepper minus ribs and seeds, minced (bottom half of one
  medium size pepper)
- 11.5 g (~1.5 tsp) table salt, non-iodized

600 mL when packed. Did not use a cover leaf or a weight, and just
packed back down as necessary.

Notes:

- Pretty nice. Full flavor developed after a few weeks.
- I've been melting cheese on bread or crackers and then generously
  topping it with curtido.
- There was no visible bubbling; I think the lime juice acidified it
  early. Maybe try another without the lime juice, or with it added
  late.

## 2021-05-06: With lovage

2% salt. About 5 parts cabbage/1 part onion/1 part carrot.

- 374 g green cabbage, sliced thin and 1 cm long
- 160 g red cabbage, sliced thin and 1-2 cm long
- 108 g carrot, shredded
- 105 g yellow onion, sliced thin and 1 cm long
- 16 g garlic, crushed
- 7 g lovage, chopped medium fine
- 15.4 g sea salt

No cover leaf. Packed into two 16 oz jars.

Light bubbling within 24 hours. Seemed to be finished fermenting by
5/17 -- no more bubbles, sharp onion taste was gone.

End result was pleasant, but no lovage flavor remained, even though it
was so strong at the beginning. Entirely purple as well.

## 2021-05-27: Kale, cabbage, carrot, scallions, garlic

2% salt.

- 313 g kale (208 g blades diced 1-2 cm, 105 ribs as 4 mm disks)
- 200 g red cabbage, diced small
- 95 g carrot, shredded
- 65 g scallions, chopped small
- 27 g garlic, sliced 1-2 mm thick
- 14.0 g table salt

No cover leaf. Left to sit for some hours, but a brine wasn't forming
readily (and I was impatient) so rather than letting it sit overnight
I forcefully packed into two 16 oz jars. Later a brine had formed and
I was able to pack the vegetables beneath it.

A couple days later I emptied out the jars, mixed the contents
thoroughly, and repacked them because they were uneven.

## 2021-06-10: Pickled turnips

4.5% salt by weight of water + vegetables. (Higher than intended.)

- 409 g scarlet turnips, cut into 5-10 mm thick batons
- 7 g whole garlic cloves
- 52 g diced red cabbage
- 1 fresh bay laurel leaf (bruised slightly)
- 37 g table salt, non-iodized
- 339 g water

I wasn't sure if dicing the red cabbage into small (5 mm) squares was
a good idea, but it seems to work to extract as much color as
possible: By 6/12, many of the squares had a deeply colored center and
translucent white edges.

The accidentally high salt level was definitely a flavor problem, so I
diluted it with more vegetables on 2021-06-15:

- 747 g red cabbage in 4 cm x 4 mm strips
- 296 g shredded carrot

Mixed and left to wilt overnight; repacked into half-gallon jar on
2021-06-16. This turned out well, but diluted the turnip flavor as
well. Would be good to try this again with the right salt level from
the start.

## 2021-08-22: Dilly beans

- 365 g Italian Romano flat pod beans (some quite mature)
- 5 g dill leaves (could be packed into about 1 Tbsp)
- 10 g garlic cloves, halved (3 medium-small)
- 430 g water
- 18 g iodized table salt

Packed into quart jar.

## 2021-09-20: Tomatillos

OK, not really a "vegetable", but I'm treating them more like a
vegetable here.

4% salt to try to keep the high-sugar ferment from going awry.

- 201 g tomatillos, rinsed and halved (mostly at yellow ripeness)
- 8.00 g iodized table salt

Mixed with salt and left to sit until brine formed (turning as needed
for coverage). After 1 day, tomatillos were basically submerged.

Nearing the 2 day mark, I started to see small bubbles and tasted a
bit of sharpness, so I moved it to a jar with very little headroom and
capped it to allow a CO2 atmosphere to start building up.

At 5 days: Nicely sour by now, developing more complex
flavors. Bubbling has slowed down. Started to develop kahm yeast the
next morning (probably because I had started opening it and letting
oxygen in) so I refrigerated it.

## 2021-10-30: Beet, carrot, ginger

Brine ferment, 2% salt by weight of produce + water.

- 393 g purple beets, diced about 8x18x18 mm
- 20 g young ginger, cut into matchsticks about 2x2x12 mm
- 270 g orange carrots, diced about 8x10x10 mm
- 606 g water
- 26 g iodized sea salt

Split across 3 sealed pint jars.

- 11/16: One out of three had developed mold, so I opened it and
  removed the mold (which hadn't penetrated any of the vegetables.)
  Tasted good and fizzy, which makes the mold surprising; I would have
  expected the CO2 to more or less fully displace the oxygen. But this
  jar also had the loosest seal.

Overall quite tasty and good for a snack.

## 2022-04-10: Beet, celeriac, garlic

Aimed for 750 g, went a little under.

- 11 g apple cider vinegar
- 297 g celeriac, matchsticks
- 384 chiogga beet, matchsticks
- 14 g garlic, small (3-4 mm) dice
- 15 g iodized table salt
- 1/2 tsp caraway seed

With the beets and celeriac cut this thin, the mixture was basically
self-brining (bearing in mind that the apple cider vinegar did add
some liquid.)

Set to ferment with a water-filled jar as a weight and a rubber-banded
bag over the top for air exclusion.

10 days later, seemed done. Top layer was much lighter and less pink,
with some discoloration on the pieces at the surface (oxidation, I
think).

Pretty nice with a basic remoulade: 65 g of the ferment, 2 tsp mayo,
and 1/4 tsp mustard.

## 2022-04-10: Celeriac

Tiny batch based on
https://kitchencounterculture121.wordpress.com/2015/10/13/lacto-fermented-celeriac-remoulade/

Used only 1% salt because of all the lemon juice. (Lemon juice keeps
the celeriac from browning.)

- 90 g celeriac, coarsely grated
- 15 g lemon juice, fresh squeezed
- 1.05 g iodized table salt

Packed into jar with tight lid.

At first there was no liquid layer on top, but by 4/13 a few mm of
brine cover had formed.

Tried 5/9. Kind of soft, but still has structure. Between the lemon
juice and the lactic acid it was pretty sharp!

The beet & celeriac turned out better than this did.

## 2022-04-10: Garlic beets

Bite-sized chunks, water to cover, 2% salt.

- 300 g purple beet, 6-12 mm dice
- 10 g garlic, 3-5 mm dice
- 8 g iodized table salt
- 145 g of water (to cover)

Tasted 2022-05-something. Nice and garlicky. (*Very* garlicky.)

## 2022-05-09: Pickled turnips

2% salt by weight of water + vegetables.

- 668 g purple-top turnips, cut into 5-10 mm thick batons
- 125 g diced red cabbage, plus a 14 g cap leaf
- 26 g whole garlic cloves (about 6 or 7)
- 1 fresh bay laurel leaf (bruised/crumpled slightly)
- 30 g table salt, iodized
- 650 g water

Mixed, added salt by desired volume, added cap leaf, added water to
cover. Sealed.

Opened 2022-05-25:

- Turned out pretty well!
- Turnips are a little bit of a lighter pink than I tend to see in
  restaurants, although those might have colorant added.
- Little pieces of cabbage are a bit annoying. They're a little
  rubbery in comparison to the turnip, and the texture combination
  isn't good. Consider cutting strips, next time, so that they're
  easier to separate. (Color will probably still leach appropriately.)
- Garlic quantity might be too much; Alex noted a slight kimchi
  flavor. Consider reducing the garlic a little, and *maybe* adding
  another bay leaf.
