# Lentil and squash

## 2018-11-22 - Thanksgiving at the Buttery

- butternut squash (1.3 kg cleaned weight), small dice (1 cm)
- olive oil
- salt
- 7 large cloves of garlic, crushed
- 5 small scallion leaves, minced
- small piece asafoetida, ground
- 3 cups brown lentils (dry)
- 7 cups water
- 5 Tbsp apple cider vinegar

Squash:

- Heat several Tbsp of olive oil in a large cast iron skillet to high heat
- Add a single layer of cubed squash. Stir occasionally to allow some
  browning, but not heavy browning. May need to reduce heat to medium
  or medium-high.
- Add a pinch of salt at some point in that process
- Squash is ready when firm-tender, or edges become soft
- Repeat above until all squash is cooked

Lentils:

- Heat oil in pot to high heat
- Add asafoetida and stir for a few seconds
- Sauté garlic and scallion for 30 seconds
- Add lentils and stir for 30 seconds
- Add water and bring to boil
- Simmer, adding 1 tsp of salt, until lentils are tender

Combine squash and lentils and apple cider vinegar.

## 2021-02-06

2018-11-22 again, more or less. Did all the squash in one batch.

- butternut squash (1050 g cleaned weight), small dice (1 cm)
- olive oil
- 1/8 tsp + 1tsp salt
- 35 g garlic (7 large cloves), crushed
- 5 medium scallion leaves, minced
- 1/8 tsp powdered asafoetida
- 500 g (2.5 cups) dried brown lentils, soaked overnight and drained
- 3 cups water
- 4 Tbsp apple cider vinegar

Squash:

- Heat several Tbsp of olive oil in a large cast iron skillet to high heat
- Add cubed squash and 1/8 tsp salt. Stir occasionally to allow some
  browning, but not heavy browning. May need to reduce heat to medium
  or medium-high.
- Squash is ready when firm-tender, or edges become soft

Lentils:

- Heat oil in pot to medium heat
- Add asafoetida and stir for a few seconds
- Sauté garlic and scallion for 30 seconds
- Add lentils and stir in
- Add water to cover and bring to boil
- Add 1 tsp of salt, and simmer covered until lentils are tender.
  Evaporate off remaining water if needed.

Combine squash, lentils, and apple cider vinegar.
