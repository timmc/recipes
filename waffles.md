# Waffles

## 2016-02-15 - Savory/neutral waffles, GF

Derived from http://www.healthfulpursuit.com/2013/09/sugar-free-gluten-free-waffles/

- 2 cup GF AP flour (Bob's Red Mill)
- 2 tsp ground flax seed
- 0.5 tsp baking soda
- 1/4 tsp salt

- 4 egg
- 1 cup almond milk
- 2 tsp water (probably unnecessary)
- 4 tbsp olive oil

Makes a bunch, like maybe 6 good-sized waffles.
