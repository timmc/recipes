# North Indian dal

Based on recipe by Veg Recipes of India at http://www.vegrecipesofindia.com/whole-masoor-dal-recipe-north-indian-style/ 

Original author: dassana

Alterations from original:

- substituted 14 oz canned diced tomatoes for 2 fresh tomatoes
- substituted 1 "pea" asafoetida for medium onion
- halved turmeric
- specified how to actually use the hot pepper
- quadrupled the recipe

# Ingredients

- 4 cup green lentils (the big brown-green kind), soaked for a couple
  hours (if possible)
- Water for cooking lentils in (~8 cups)
- olive oil
- 4 tsp cumin seeds
- ~4 "peas" asafoetida resin, ground to powder
- 1 jalapeño pepper, minced
- 5 cloves garlic, minced
- 1 inch of ginger, minced
- 2 28oz cans of diced tomatoes (probably with salt)
- 1/2 tsp turmeric
- 2 tsp chili powder (*not* cayenne)
- 1 Tbsp salt (use less when canned tomatoes are salted)
- 2 tsp garam masala powder
- 4 tsp crushed dry fenugreek leaves (kasuri methi)
- 0.5–0.75 cup cream (optional)
- Garnish: A few chopped cilantro leaves, some crushed fenugreek
  leaves, or cream

# Instructions

- In smaller pot, cook lentils until soft
- In larger pot, fry cumin and asafoetida in oil on high heat until
  they stop bubbling (1-2 minutes)
- Add the jalapeño towards end of frying the cumin
- Add the ginger-garlic mixture and fry for a minute or so, stirring
- Add the tomatoes and bring to a boil for a minute or so
- Add the turmeric and chili powder
- Add lentils
- Mix well and add salt
- Simmer the dal on a low or medium flame. Add water for consistency
  as needed.
- Mash some dal with your ladle or spoon at the sides of the
  pan. This is done to get a creamier consistency.
- Finally add the garam masala powder and fenugreek leaves. Stir and
  remove from heat.
- Stir in cream, if using
- Serve with basmati rice
- Add garnish

# Attempts and Variations

## 2014-03-02

Original variation as seen above, not using the optional cream or
cilantro. I added extra fenugreek leaves to a bowl of it the next day
and it worked really well, so the amount in the base recipe could
probably be increased.

## 2014-03-11

Split into two pots, with unequal distribution of chili powder and
turmeric.

Could maybe use more ginger, coriander, or cardamom.

## 2015-07-27

- 6 cups lentils instead of 4. Proportional except 3 cloves garlic, 1
  jalapeño.
- Divided into two -- garlic in one, jalapeño in other.

This turned out *way* more tomato-y than I remember!

I need to remember to stop adding cayenne, it's way stronger than I
think it is, every time.

## 2015-11-04

Did not soak lentils

- 2 poblano instead of jalapeño
- Chili powder: ancho
- 1/4 tsp turmeric powder instead of 1/2
- 1 28oz can fire-roasted whole tomatoes + 2.5 largish tomatoes ~14oz
  instead of 2 28oz cans

Turned out well.

## 2016-07-11

- 1.5x recipe
- No garlic, no jalapeño
- Paprika instead of chili powder (possibly a "hot" paprika)
- Probably 1.5x more fenugreek leaves than called-for

Very tasty, although the lack of garlic does change the character of
the dish somewhat. The amount of fenugreek was probably excessive but
worth it.

## 2018-08-19

1.5x recipe, but even more garlic and ginger, proportionately less
turmeric and salt, and a custom chili powder mix.

- 6 cups green lentils
- Water for cooking lentils in (~12 cups)
- olive oil
- 6 tsp cumin seeds
- ~6 "peas" asafoetida resin, ground to powder
- 3 small jalapeño peppers, minced
- 10 cloves garlic, minced
- 2.5" inches of ginger, minced
- 3 28oz cans of diced tomatoes (probably with salt)
- 1/2 tsp turmeric
- chili powder:
    - 1 tsp cumin
    - 1 tsp ancho chili, ground
    - 1/4 tsp chipotle chili, ground
    - 1/2 tsp salt
    - 1/2 tsp garlic powder
- 1 Tbsp salt
- 3 tsp garam masala powder
- 2 Tbsp crushed dry fenugreek leaves

## 2019-05-11

1.5x recipe, like 2018-08-19, except 25% more tomato (because I had a
104 oz can of crushed tomatoes) and brown lentils instead of green.

- 6 cups brown lentils
- 12 cups water (for cooking lentils) + 1/2 cup for rinsing tomato can
- 6 tsp cumin seeds
- 1/2 tsp asafoetida powder (from German market)
- 28 g frozen jalapeño peppers (3 medium), minced
- 64 g ginger (2.5 inches of a fat rhizome), minced
- 66 g garlic cloves (13 of varying size), crushed
- 2.94 kg crushed tomatoes (Muir Glen; includes salt)
- 1/2 tsp turmeric
- chili powder:
    - 1 tsp cumin
    - 1 tsp ancho chili, ground
    - 1/4 tsp chipotle chili, ground
    - 1/2 tsp salt
    - 1/2 tsp garlic powder
- 1 Tbsp salt
- 3 tsp garam masala powder
- 2 Tbsp crushed dry fenugreek leaves

Note to self: Yes, you can just barely cook the 6 cups lentils in your
5 qt pot, and while there's plenty of room in the 12 qt for the 1.5x
recipe, 2x might be stretching it (and would require figuring out a
different pot for the lentils anyhow.)

## 2022-04-07

No jalapeño; used pasilla for the chile powder; stirred in turmeric
and chile powder just before the tomatoes.

Used Red Fire Farm tomato puree (25.5 oz each, unsalted).

The 2 tsp of pasilla chile powder was a little too strong for the
kid. Next time consider just using 1/2 tsp of pasilla and maybe 1 tsp
of paprika?

## 2022-07-15

2019-05-11 but back to usual amount of tomatoes, and less heat even
than 2022-04-07. Turmeric and chile powder in before tomatoes. Include
missing oil (olive) in the ingredients. Specify to lower heat before
ginger/garlic goes in.

- 1300 g (6 cups) brown lentils
- 12 cups water (for cooking lentils) + 1/2 cup for consistency (and rinsing tomato jars)
- 2-3 Tbsp olive oil
- 6 tsp cumin seeds
- 1/2 tsp asafoetida powder (from German market)
- 66 g ginger (2.5 inches of a fat rhizome), minced
- 69 g garlic cloves (14 of varying size), crushed
- 2360 g crushed tomatoes (Eden brand; no salt)
- 1/2 tsp turmeric
- 1/2 tsp pasilla chile powder
- 1.5 tsp smoked paprika powder
- 1 Tbsp salt (heaping, to compensate for no-salt tomatoes)
- 3 tsp garam masala powder
- 2 Tbsp crushed dry fenugreek leaves

Directions:

- In smaller pot, cook lentils until soft
- In larger pot, fry cumin and asafoetida in oil on high heat until
  they stop bubbling (1-2 minutes)
- Lower heat to medium, add the ginger-garlic mixture, and fry for a
  minute or so, stirring
- Add the tomatoes and bring to a boil for a minute or so
- Add the turmeric and chili powder
- Add lentils (watch out for splashes)
- Mix well and add salt
- Simmer the dal on a low or medium flame. Add water for consistency
  as needed.
- Mash some dal with your ladle or spoon at the sides of the
  pan. This is done to get a creamier consistency.
- Finally add the garam masala powder and fenugreek leaves. Stir and
  remove from heat.
