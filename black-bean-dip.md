# Refried black bean dip - 2020-07-26

- 2 cups black turtle beans
- dried epazote (1/2 tsp?)
- 1 tsp table salt
- 2-3 Tbsp olive oil
- 20 g garlic, crushed
- 3 large scallion leaves, minced
- 2 tsp ground cumin
- 1 tsp salt
- 28 g frozen chopped cilantro leaves
- 237 g water

- Soaked beans for a day, then cooked with crumbled epazote. Drained
  and salted (1 tsp). Refrigerated overnight.
- Fried garlic and scallion in oil over medium-high heat until garlic
  was softened (and starting to burn, oops).
- Added cumin and stirred in for about 20 seconds, then mixed in beans.
- Mixed in remaining 1 tsp salt and cilantro.
- Poured water in with beans. Mashed beans with potato masher as best
  I could (rough mash).
- Continued on medium-high heat, turning over occasionally as the
  bottom started to brown or dry.

Notes:

- Delicious! A little high on salt (by ~20-40%) and cilantro (by
  ~50-70%) but the kid adored it, so that's good.
- A bit dry, so I mashed in another 45 g of water at the end. Could
  have added more, since it set up a bit in the refrigerator.
