# Eggplant dip

## 2024-08-08 - Spicy eggplant dip

Adapted from
https://www.eatingwell.com/recipe/8058700/spicy-eggplant-dip/ to make
vegetarian and lazier.

- ~1 pound Japanese eggplant (after roasting and trimming, 341 g)
- 18 g sizeable cloves garlic, crushed
- 1 jalapeño, minced
- 4 tsp soy sauce
- 26 g (2/3 cup) chopped cilantro leaves
- 2 tablespoons sunflower oil
- 3.5 Tbsp lime juice

- Dry pan-roasted intact, dry eggplant on both sides until skin was
  fully browned and inside was collapsed and soft. Removed tops and
  flat, hard, blackened patch.
- Crushed hot pepper and garlic with the flat of a blade against a
  cutting board, then combined in bowl with 1 tsp soy sauce.
- Mashed cilantro into garlic and pepper with a pestle.
- Tried to separate eggplant skins, then gave up on that and just put
  all ingredients through a food processor.

Notes:

- Definitely some baba ghanoush vibes here
