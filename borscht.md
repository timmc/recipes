# Borscht

## 2014-12-28

This one is made with two ingredients that bear mentioning:

- Beef drippings from a beef broth stew I made a couple weeks ago. I
  almost always eat a vegetarian diet, so this is unusual for me, but
  I was curious to see how it would change things.
- Russel (or russell?) that I made by cutting up washed but unpeeled
  beets (1 inch chunks) and letting them sit in water for 2-3 weeks. A
  colander spoon was really useful for skimming off the fermentation
  scum every 3-8 days.

Sautéed carrot, onion, potato, celery, rutabaga (?), and a small
amount of fennel leaves in beef drippings. Added crushed tomatoes and
the liquid of the russel. Cooked vegetables to desired texture, then
added the beets from the russel along with salt, ground black pepper,
and 2 tbsp apple cider vinegar.

Served with filmjölk.
