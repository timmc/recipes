# Cured egg yolk

## 2020-03-14

- Laid down 1 cm "fine grain sea salt" in small plastic container
- Set 1 egg yolk in divot in salt
- Covered yolk with maybe 5 mm salt
- Sealed container
- Kept in fridge for 7 or so days
- Removed, brushed off salt, left to dry further in open air
- Washed off remaining salt, patted dry, and let dry again

Notes:

- Tasted OK? Really nothing special. Vaguely cheeselike and salty.
- Should not have poked the yolk partway through; it broke and leaked
  a bit into the salt, resulting in a certain amount of yolk-flavored
  salt.
- Should have had container open to air in fridge; salt was very moist
  when I opened it.
