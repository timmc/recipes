# Roasting vegetables

Basics of various veggies.

## Dutch oven beets

Roasted beets without much cleanup. You'll need:

- ~6 medium beets
- 5 qt Dutch oven
- olive oil
- 1 to 2 hours of oven time, plus 15-30 minutes prep and cleanup

Based on http://kitchen-parade-veggieventure.blogspot.com/2010/10/my-favorite-way-to-roast-beets.html

Cooking beets in a Dutch oven obviates having to wrap each one in foil
(which would then have to be thrown out) since the Dutch oven holds in
moisture quite well.

This recipe attempts to leave the beets as intact as possible
throughout the roasting process so that they cook evenly, do not lose
water and shrivel, and do not leak sugary juice into the hot Dutch
oven to become sticky.

Oiling the beets helps them retain moisture. Oiling the Dutch oven
helps keep it in good shape and will make cleanup of any caked-on beet
sugar far easier.

- Preheat oven to 375°F
- Trim the beets' stems and leaves, but not closer than an inch. Leave
  the tails alone!
- Gently scrub the beets to remove grit and dirt
- Oil the inside of the Dutch oven generously (I sometimes leave up to
  a mm of oil in the bottom!)
- Oil the beets generously as well, adding them to the Dutch oven (I
  like to "grab" oil from the bottom of the Dutch oven and roll the
  beets in my hand to coat them, to make things easier)
- Close Dutch oven and put in oven for at least one hour, turning
  after 20 or 40 minutes with larger beets
- Cook for longer (try 15 minute increments) until a knife goes in
  easily
- Remove beets and allow to cool enough to handle
- As the beets cool, grab each one with a cloth and twist -- the skin
  should slip off easily in patches, and the top and tail should fall
  off as well

Additional notes:

- The cloth can be left to soak in several changes of water as you
  clean up; after that, it *should* be safe to run through the laundry
  without dyeing other fabric. Worked for me.
- Hot water and a minute or so with a scouring pad should remove any
  beet sugar from the Dutch oven.
- You can probably do multiple layers of beets in the Dutch oven. I
  haven't tried it.
- Beet leaves and stems are the same as chard, so save them for
  another recipe.

## Kohlrabi - 2020-02-08

- 1907 g 1-2.5 cm dice of kohlrabi (+ weight of bowl)
- 40 g olive oil
- 1/8 tsp fine ground black pepper
- 1/4 tsp iodized table salt

Toss all together then bake on tray in 450°F oven, stirring a couple
times, until caramelized in places, perhaps 40 minutes.

Should probably use more olive oil than I used at first; I ended up
pouring some extra into the tray partway through.

Resulted in a mix of done-ness, with some firmer than others, or more
caramelized, etc. Probably should do 1.5 cm slabs next time.

## Cauliflower - 2020-11-29

- 588 cauliflower florets (after removing 100 g core/stalk)
- 5 Tbsp olive oil
- 1/8 tsp table salt
- 1/8 tsp freshly powdered black pepper

- Tossed all ingredients together
- Roasted on baking sheet at 400°F in preheated oven for 45 minutes,
  turning every 15 minutes
