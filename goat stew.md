# Goat stew

## 2017-02-11

- Olive oil
- carrots
- celery
- potatoes
- chard
- garlic
- ground goat meat
- salt
- black pepper
- red wine/vinegar (halfway there)
- soy sauce
- chipotle pepper powder
- smoked serrano pepper powder

## 2017-03-08

Attempting a similar recipe as on 2017-02-11, although with stewing
meat instead of ground meat.

- Olive oil
- 2 inches of ginger, minced
- 3/4 head of garlic, crushed
- 1-2 lbs yukon gold potatoes, choped small (6-7 medium size)
- 1-2 lbs carrots, chopped (7 medium)
- 1.14 lb goat meat for stewing (1 inch chunks)
- Water
- 1/4 cup red wine/vinegar (halfway there)
- celery, chopped (3 stalks + core)
- chard leaves (5 medium leaves rainbow chard)
- 4 tbsp soy sauce (2 gluten-free soy sauce, 2 from the mystery jug
  labeled "tamari")
- Smoked serrano chili (powdered)

Directions:

- Fry ginger over high heat in a few tablespoons of olive oil until it
  is half browned
- Reduce heat to medium and sauté garlic (stop before it browns much)
- Return heat to high and add stewing meat; stir occasionally for a
  couple minutes to brown a couple sides
- Add carrots and potatoes, stir periodically for a couple minutes
- Add water to cover + 1 inch
- Add vinegar
- Bring to boil and then adjust heat to simmer
- Add celery, chard, and soy sauce
- Simmer until carrots are becoming soft
- Add two dashes of serrano powder

Notes:

- Very pleasant broth (would probably work just fine without the meat)
- Meat was kind of rubbery. I may not have cooked it long enough; a
  light boil for a half hour beyond the above did make it softer. Next
  time consider boiling the meat *before* adding the vegetables, to
  give it time to break down.
- Next time use a fresh or frozen hot pepper instead of powdered chilis
- Next time stop simmering shortly after adding celery, so that
  carrots stay a little crisp.

## 2017-12-21

- Olive oil
- 2 inches of ginger (18 g), minced
- 3/4 head of garlic (32 g), minced
- 600 g carrots, chopped
- 1020 g potatoes, chopped small
- 510 g ground meat
- 145 g bad red wine
- 1 Tbsp hot (jalapeño) apple cider vinegar
- 5 Tbsp soy sauce
- 260 g celery
- 280 g frozen spinach

Directions:

- Fry ginger over high heat in olive oil until it is half browned
- Reduce heat and sauté garlic
- Increase heat and add carrots and potatoes; sauté as possible (may
  need more olive oil)
- Add ground goat in approx. 2 cm chunks, gently turn with vegetables
  for a minute
- Add 85 g red wine, water to cover, 1 tsp vinegar, 3 Tbsp soy sauce;
  high heat
- Add celery and spinach
- Bring to a boil and then simmer
- Add an additional 2 tsp vinegar, 2 Tbsp soy sauce, 60 g wine
- Continue simmering until done

Tasty, although I think not as great as previous versions.
