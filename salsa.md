# Salsas

## 2021-09-21: Fermented tomatillo salsa verde

- 493 g tomatillos, mix of green and yellow-ripe, roughly chopped
- 28 g scallion greens chopped 1 cm
- 13 g jalapeño pepper without stem, seeds, or ribs (medium/small), diced 5 mm
- 10 g garlic, crushed (2 medium cloves)
- 1/2 tsp coriander seeds
- 1/4 tsp cumin seeds
- 13.60 g iodized table salt

Crammed into jar with salt poured on top, left overnight, stirred and
pushed down a couple times. Lid kept on tightly.

About 36 hours later, some gas buildup that had to be released. Just
tilting jar occasionally to make sure top stays in good condition.

On 9/23 evening or 9/24 morning I refrigerated the jar to stop/slow
the fermentation, then on 9/25 took it out to finish it.

Add:

- 1 tsp frozen cilantro

Drained and reserved liquid, added cilantro, and used immersion
blender to get to desired texture.

Notes:

- Total fermentation time was 2-3 days although it will continue to
  mature.
- Strong flavor (fairly sour, didn't feel the need to add lime!) and a
  little goes a long way. Tastes very much like salsa verde that was
  fermented instead of cooked -- no surprise there. But stronger.
- Almost no detectable heat -- the pepper turned out to be very mild.
- Coriander seeds are kind of crunchy/chewy in an unpleasant way; can
  probably omit these on account of cilantro, and as long as lime is
  added. (I can detect the citrus note they add.)
- Absolutely lovely on hummus.
- The reserved liquid is pretty nice. Hard to know quite what to do
  with it, though.
