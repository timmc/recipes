# Okonomiyaki

My take on Japanese vegetable pancakes.

## 2025-01-15 - Gluten free, low-FODMAP (ish), low egg

This makes about 8-10 pancakes, each of which is quite filling. I
don't even bother making a sauce for these; they're quite tasty on
their own. They work well as leftovers, too, although only freshly
made ones will have a crispy edge.

Roughly based on
<https://smittenkitchen.com/2013/05/japanese-cabbage-and-vegetable-pancakes/>.

Previous versions used 5-6 eggs, but I wanted to reduce that amount to
the extent possible. This required adding a bit of water to get the
right consistency, and I included chickpea in the flour blend since I
think it might help act as a binder. (It's possible that any mix of GF
flours will work just fine, though.)

A food processor works great for this. I think shredding might be
important for cohesion—it releases a lot of liquid, leaves rough
surfaces, and creates long ribbon-like strips that tangle. Chopping
might not give the same result.

Ingredients:

- 650 g winter vegetables, shredded: Cabbage, radish, turnip,
  rutabaga, parsnip, carrot. (Brightly colored ones are nice.)
    - Also nice to add scallions, leeks, onion, etc.
- ~1 head garlic, crushed
    - Can use less if other alliums are in the "vegetables" list.
- 50 g sorghum flour
- 50 g oat flour
- 50 g chickpea flour
- 1 tsp table salt
- 1/2 tsp freshly ground black pepper
- 2 large eggs
- 45 g water (or as needed)
- Olive oil, lots

Directions:

- Mix shredded vegetables and garlic.
- Mix in flours, leaving a moderate coating on the vegetables. None
  should be left in the bottom, and it should be neither sludgy nor
  powdery on the vegetables.
- Mix in salt and pepper.
- Beat eggs and mix in.
- Add water until flour coating is somewhat sludgy, allowing for
  pancakes (not too dry, but also not dripping).
- Bring small skillet with plenty of olive to medium-high heat.
- For each pancake:
    - Use a fork to plop down a portion on the skillet, then press and
      shape it to a disc about 5-8 mm thick.
    - Ensure there is enough heat and oil that bubbles form around the
      sides.
    - Allow to cook until bottom is nicely browned and pancake has
      stiffened enough that it can be flipped without
      breaking. Usually this is about a minute after browning on the
      edges becomes visible from the top.
    - Flip pancake and drizzle more oil around it so it cooks evenly
      and doesn't burn in spots.
    - Cook until nicely browned on bottom, then remove to cool... and
      start the next pancake
