# Ocopa sauce

## 2019-01-20

Various substitutions based on availability and dietary restrictions.

- 4 fl oz of frozen huacatay leaves (kind of freeze-dried after 4
  years...)
- 1 medium head of garlic (~12 cloves), peeled
- 3 ají amarillo peppers, 2 whole and one deseeded/deveined
- 4 handfuls of peanuts
- a few lugs of olive oil
- 2 cubic inches of cheddar
- 1/2 cup unsweetened almond milk
- 2 tsp quinoa flour
- 1/2 tsp salt

Toasted the garlic (whole cloves) and peanuts, and then the chopped
pepper and huacatay as an afterthought. Blended all 'til creamy.

Pretty spicy; I was supposed to have deseeded and deveined all the
peppers. Maybe it's supposed to be hot anyway.

It thickened up considerably as the flour absorbed liquid, including
after putting the leftovers away. I had to mix in perhaps another 2 oz
of almong milk to make it pourable again.
