
http://www.pickyourown.org/pumpkinpiejackolantern.php

Instead of 24 oz evaporated milk, 1.3 cups almond milk + 1.3 cups
"coconut milk" (coconut, water, guar gum), 1 tbsp xanthan gum. (Trying
to make it dairy-free.)

Result: Gluey, gloppy mess. Did not solidify properly. More of a gummy
custard with a skin. The flavor was fine, but I'm sure I added too
much xanthan gum, for one.


(Also see roasted-squash.md for roasted jack o'lantern recipe.)
