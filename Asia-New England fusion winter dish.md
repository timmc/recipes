# Asia/New England fusion winter dish

New England winter vegetables stir-fried with Sichuan and Indian spices.

## Ingredients

- Olive oil
- Serrano pepper, finely minced
- 4 small potatoes diced to ~7mm
- Cooked navy beans (1-2 cups?), drained
- 2 small onion, diced
- 2 cups diced/shredded red cabbage
- 5 collard leaves, shredded square (kind of yellowed, in this case)
- Spices, ground together to fine powder:
    - 1 tbsp sichuan pepper husks
    - 1 black kokam (Garcinia indica) dried pulp and rind
    - 1 tsp grains of paradise seed
    - 1-2 tsp coriander seeds
- Salt, 4-5 tsp (Whoops, that was probably too much. Delicious, though!)

## Process

- Heat olive oil to just about smoking
- Add serrano (WARNING: capsaicin fumes)
- Add potato, scrape and toss until evenly browned, maybe 5 minutes
- Add beans and onion, let cook 2 minutes until starting to brown,
  scrape and toss, let cook 2 more minutes
- Add cabbage, collard greens, spice mix, and salt
- Cook, tossing, until cabbage wilts (several minutes)
