# Turnip and garlic scape frittata

"Turnip Frittata with Double Garlic" retrieved from http://cravinggreens.blogspot.com/2013/07/turnip-frittata-with-double-garlic.html on 2014-06-17.

## Original recipe

- 1.5 pounds turnips, peeled and grated (approximately 2 medium-sized turnips)
- 6 eggs
- 2 tbsp milk
- 3 garlic scapes, chopped (can be replaced by one onion, or a 3-4 green onions)
- 3 garlic cloves, minced
- 1 tbsp fresh thyme leaves
- 2 tbsp olive oil
- 1/2 cup grated cheese (I used cheddar, but use up whatever you have on hand)
- Parsley, to taste
- Salt & papper, to taste

- Sprinkle the grated turnips with some salt and drain in a colander
  for 30 minutes. Press down to remove excess water.
- Heat 1 tbsp. of olive oil in a skillet over medium-low heat. Sauté
  the garlic scapes for 3 minutes. Add the grated turnips and thyme
  leaves. Cover and cook for 15 minutes, until the turnip is
  tender. Remove from the heat, season with salt & pepper and set
  aside to cool for 5 minutes.
- Meanwhile, beat the eggs and milk in a large bowl, and season with
  salt & pepper. Add the turnip mixture, the minced garlic and the
  parsley. Mix well.
- Preheat the oven broiler. 
- In a 10-inch oven-proof skillet, heat the remaining 1 tbsp. olive
  oil over medium heat. Add the egg mixture and cook, stirring a
  couple times, for 2-3 minutes. Reduce heat to low, cover and cook
  for 10 minutes.
- Remove from the heat, uncover and top with the grated cheese. Place
  under broiler for 3-5 minutes, until the cheese is melted and
  slightly golden. Remove from the oven and let cool for 10 minutes.
- Transfer to serving plate, cut into wedges and serve.

Serves 4-6.

## 2014-06-17

4/3 recipe with almond milk instead of milk and garlic cloves replaced
with scapes. Doubled scapes (and scapes-as-cloves.)

- 2.1 pounds turnips, peeled and grated (1 large turnip)
- 10 eggs
- 3 tbsp almond milk
- 8 garlic scapes, chopped
- 8 garlic scapes, minced
- 1 tsp fresh thyme
- 1 tbsp dried thyme
- 2.7 tbsp olive oil
- 1 cup grated cheddar & Monterey Jack
- leftover parsley stems, chopped fine (3-4 tbsp)
- Salt: 2 big pinches, 1 med pinch
- 1/4 tsp pepper, fresh ground

Notes:
- 12 inch cast iron skillet
- Used a couple big pinches of salt with the grated turnip
- No need to drain the turnip gratings
- If you accidentally burn the turnip gratings a little, it's OK! It
  tastes a bit like fries.
- Needed extra eggs to match the (rather spongy?) turnip. However, the
  turnip did start "releasing" the egg after a few minutes. Maybe the
  salt started breaking the egg down?
- Had to cook 20 minutes on stovetop
- 3-4 minutes under broiler (low)
- Still slightly juicy in middle, so put back in oven at 300°F for
  perhaps 10-15 minutes.
- Smelled *amazing*.

## 2014-06-22

8/3 recipe with extra parsley

- 4 medium purple-top turnip
- 16 scapes chopped
- 2.7 tbsp fresh thyme leaves
- 12 eggs
- 16 scapes minced
- 5.3 tbsp almond milk
- full fist of parsley sprigs
- 227 g (8 oz) Monterey Jack cheese

Notes:

- Turnips were much juicier this time, so they took longer to cook
- Another effect was that I didn't need as many eggs as last time
- Turnips didn't have quite the same potato-y flavor when browned
