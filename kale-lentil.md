# Kale & Lentil

## 2018-07-07

Pretty much ad-hoc, so directions are unnecessarily convoluted.

Ingredients:

- 2 cups green lentils, soaked

- Olive oil
- 1-2 tsp cumin seeds
- 2 tsp mustard seeds
- several small green jalapeños, minced

- 1 head of garlic, chopped fine
- 1.5 in² of ginger, chopped small
- 20 curry leaflets

- 28 oz diced tomatoes
- 1 tsp salt
- 1/4 tsp turmeric powder

- bundle of kale, chopped

- 1/2 tsp garam masala powder
- 2 tsp lime juice

Method:

- Set lentils to boiling with 1-2 cm water to cover
- When the water gets low, add *half* each of the garlic and ginger,
  and the kale stems
- When water is largely boiled off or absorbed, turn off heat

- In a deep, lid-enabled cast iron pot, heat copious olive oil over
  medium-high heat
- Add jalapeño, cumin, and mustard seeds (watch for spattering oil)
- Cook, stirring as needed, until spattering starts to dies down and
  cumin is just starting to brown
- Add *half* each of garlic and ginger, and all the curry leaflets
- Cook, stirring as needed, until garlic is becoming translucent
- Stir in *half* the diced tomatoes (watch for spattering), as well as
  the salt and turmeric
- Cook over medium heat for several minutes

- Add lentil mix into tomato and spice mix
- Stir in remaining diced tomatoes and kale
- Cook, covered, over medium heat for 15 minutes
- Turn off heat, stir in garam masala and lime juice

Results:

- Made a lot of food
- Tasty :-)

Notes:

- Retrospective: When did I add the kale leaves?
