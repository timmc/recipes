# Consolation spice: A dark spice for dark days

This was a novel spice mix intended for use on popcorn, largely
inspired by
http://www.awesomecuisine.com/recipes/16296/masala-popcorn.html and
http://www.krumpli.co.uk/spicy-popcorn/ but not really a variation on
either one.

I first made it on the night before the 2016 US Presidential Election,
then again on Election Day when it turned out well. (The spice mix,
that is—the election, unfortunately, did not.)

It has a strange and perhaps *feral* smell, which I think can be
largely attributed to the curry leaves. Dark, medicinal, sometimes
fruity. It smells something like food, something like not-food, and
something like a spice used for ritual purposes.

The flavor does not quite match the smell. The medicinal and fruity
components fall to the background, supporting that core of a rich,
dark taste, which is enhanced by the salt and cayenne.

(Note for searchability: This went under the temporary name "election
spice" before getting a better name.)

mk11 is the best (reproducible) recipe so far for smaller batches, but
mk19 and mk23 have worked well too at larger scale. mk23 fills a 4 oz
jar. mk26 is even larger, and indicates that I can probably just drop
the curry leaf and asafoetida!

This recipe is hard to scale up; even a 5x larger batch doesn't seem
to taste as good, even though it still only lightly covers a baking
sheet. Not sure why.

## Tips

- Use a clean, dry, and smooth baking sheet when toasting, otherwise
  you will lose powdered spices to stickiness or the rough surface.
- Toasting cayenne can produce noxious fumes; be ready to ventilate
  the room if you are sensitive.
- Asafoetida resin grinds best in a ceramic mortar and pestle; use a
  heaping measure of chunks to produce the desired measure of
  powder. For easiest grinding, use small pieces kept in the freezer.
- Toasting curry leaves for less time can produce a bitter flavor.

## 2016-11-07 - Consolation spice mk1

An experiment.

- 3 curry leaves (half-dried)
- 2 tsp brown mustard seeds
- 1 tsp coriander seeds
- 1/4 tsp salt
- 1/8 tsp cayenne powder
- 1/8 tsp asafoetida powder, fresh-ground

Process:

- Heat dry skillet to medium heat
- Shred curry leaves into skillet, let toast for 30 seconds
- Add asafoetida, mustard, coriander, and cayenne
- Stir for a minute or so
- Scrape out of skillet
- Add salt
- Grind to powder

It was difficult to get all the powdered ingredients out of the
skillet. Maybe use an aluminum or steel skillet next time instead of
rough cast iron.

Made 5 tsp; used on popcorn from 1/3 cup of kernels with 1 tbsp butter
and several tbsp olive oil.

Mild but intriguing flavor, mostly coming from the curry
leaves and possibly the asafoetida. Little to no heat.

## 2016-11-08 - Consolation spice mk2

Changes:

- Triple the recipe
- Increase the cayenne (for heat) by 1/3
- Change the pan and cooking times to try to lose less asafoetida
- Decrease the asafoetida to compensate for technique change

Ingredients:

- 9 curry leaves (half-dried)
- 2 tbsp brown mustard seed
- 1 tbsp coriander seed
- 3/4 tsp salt
- 1/2 tsp cayenne powder
- 1/4 tsp asafoetida powder, fresh-ground

Process:

- Heat dry steel skillet to medium heat
- Shred curry leaves into skillet, let toast for 30 seconds
- Add salt, mustard, coriander, and cayenne
- Stir for a couple minutes (watch out for cayenne smoke that will
  make breathing difficult -- don't let it smoke)
- Add asafoetida and stir in for a minute
- Scrape out of skillet
- Grind to powder

This had a similar, yet different smell. More from the curry leaves
this time, I think. Very hard to describe the smell. It tickled my
memory, but I don't have the vocabulary to describe it. Somewhat of a
medicinal component.

The flavor when added to popcorn was pleasant, although again a bit
different. More heat, as expected. I used 7 tsp on popcorn from 1/2
cup of kernels, plus olive oil. Made 15 tsps total.

I'm not sure the increased heat is a good idea.

## 2016-12-23 - Consolation spice mk3

30% more than mk2, more or less. Cutting down on the cayenne a
bit. Did not include salt in heating step. Heated longer than before.

- 11.5 curry leaves, mostly dried
- 8 tsp brown mustard seed
- 4 tsp coriander seed
- 0.125 tsp cayenne powder
- 1 tsp salt
- 0.375 tsp asafoetida powder, fresh-ground

- Heat dry steel skillet to low-medium heat
- Shred curry leaves into skillet, let toast for 1 minute or so
- Add mustard, coriander, and cayenne
- Stir for a few minutes (watch out for cayenne smoke that will make
  breathing difficult -- don't let it smoke)
- Add asafoetida and stir in for a minute or so
- Scrape out of skillet
- Add salt
- Grind to powder

Flavor was milder, less wild. I think I can chalk that up to the curry
leaves having dried out, and/or the longer toasting.

Made 19 tsps.

Thoughts for next time:
- Maybe don't toast the cayenne, because maybe it isn't necessary and
  I'd like to avoid the fumes.
- Possibly toast ingredients in this order: Coriander and mustard,
  curry leaves, asafoetida.
- Consider using a toaster oven to get even, moderate heating.

## 2017-01-17 - Consolation spice mk4

Like mk1, but toasted in the stove instead of a skillet, and triple
recipe. Cayenne was not toasted, to avoid fumes.

- 9 fresh curry leaves
- 6 tsp brown mustard seed
- 3 tsp coriander seed
- 0.375 tsp asafoetida powder, fresh-ground
- 0.375 tsp cayenne powder
- 0.75 tsp table salt

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, and asafoetida powder in heated
  pan, shake to get powder in contact with metal, and toast for 8
  minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Good flavor, more fruity than last time. Cayenne should be reduced, or
toasted next time, or both; again, the heat is a little distracting
(though mild). Favorable reviews from others, though.

This was so much easier than using a skillet, and I hope it will be
more predictable.

It tastes quite salty; I wonder if I accidentally put in 0.75 *tbsp*
instead of 0.75 *tsp*. Or the cayenne is confusing me.

## 2017-01-22 - Consolation spice mk5

- 27 fresh curry leaves
- 18 tsp brown mustard seeds
- 9 tsp coriander seeds
- 2.25 tsp table salt
- 1.125 tsp cayenne powder
- 1.125 tsp asafoetida powder, fresh-ground

Same directions as mk4.

Good flavor, maybe not quite as good.

Note to self: Next batch will probably use spicier mustard seeds,
since this one used up the old jar.

## 2017-03-25 - Consolation spice mk6

Aiming to be similar to mk4, but with toasted cayenne. Doubled
proportions. Leaves sat in refrigerator, and were a little blemished
-- might be less fresh.

- 18 fresh curry leaves
- 12 tsp brown mustard seed
- 6 tsp coriander seed
- 0.75 tsp asafoetida powder, fresh-ground
- 0.75 tsp cayenne powder
- 1.5 tsp table salt

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, asafoetida, and cayenne in
  heated pan, shake to get powder in contact with metal, and toast for
  8 minutes
- Remove from oven, combine with salt
- Grind to powder

Smells amazing (fruity again), tastes pretty good but not as good as
it has been. Even with toasted cayenne, has a little more heat than
necessary.

- Cayenne could be reduced by maybe 20% at most
- Reduce salt by 25-30% next time

## 2017-06-01 - mk7

Proportions taken from mk2 (times 4) and procedure taken from baking
tray iterations, but reducing cayenne and salt and adding asafeotida
partway through instead of at the beginning.

- 36 curry leaves (half-dried)
- 8 tbsp brown mustard seed
- 4 tbsp coriander seed
- 2 tsp salt
- 1 tsp cayenne powder
- 1 tsp asafoetida powder, fresh-ground

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, and cayenne in heated pan,
  shake to get powder in contact with metal, and toast for 4 minutes
- Add asafoetida and toast for 4 more minutes
- Remove from oven, combine with salt
- Grind to powder

Made 1+1/4 cups. Flavor not as great; maybe bump up the salt a
little. Not as fruity, kind of burnt flavor. Cayenne level is
good. Coriander might have been a little stale.

## 2017-06-17 - mk8

Similar to last time, but using probably-fresher coriander seeds and
the new batch (#2) of asafoetida resin (which is much gummier and does
not grind well). Because this asafoetida doesn't grind fine, I didn't
want to risk putting it in for less time.

- 32 curry leaves (a few short)
- 8 tbsp brown mustard seed
- 4 tbsp coriander seed
- 2 tsp salt
- 1 tsp cayenne powder
- 1 tsp asafoetida powder, fresh-ground (coarse)

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, asafoetida, and cayenne in
  heated pan, shake to get powder in contact with metal, and toast for
  8 minutes
- Remove from oven, combine with salt
- Grind to powder

Made about 1+1/4 cups. Lovely fruity smell. AT: "Tastes more sulfurous
than usual."

## 2017-08-22 - mk9

Trying to replicate mk4, using very fresh coriander but with the new,
gummier asafoetida. (Couldn't even grind it this time!) Not scaling
up. Accidentally cooked 40 seconds too long.

- 9 fresh curry leaves (varying sizes, very roughly 1g)
- 6 tsp brown mustard seed
- 3 tsp coriander seed (stored in freezer)
- ~0.375 tsp asafoetida resin... flattened
- 0.375 tsp cayenne powder
- 0.75 tsp table salt

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, and asafoetida in heated pan,
  toast for 8 minutes 40 seconds
- Remove from oven, combine with cayenne and salt
- Grind to powder

Dark in color. Wonderful smell. A bit of that burnt flavor again,
although other people disagreed. Cayenne is pleasant, although could
see reducing it a little. Powder remaining in bowl had an asafoetida
taste to it -- did asafoetida grind incompletely? Had positive
comments from others, in any event!

## 2017-09-03 - mk10

First time using a milligram scale to weigh ingredients. Trying to
reproduce mk9, although I feel like I might have accidentally doubled
the asafoetida that time without realizing it. Curry leaves not as
fresh; some had gone bad.

- 1.415 g (less) fresh curry leaves (9 leaves of varying size)
- 6 tsp brown mustard seed
- 3 tsp coriander seed (stored in freezer)
- 0.880 g asafoetida resin, flattened (~0.375 tsp)
- 0.375 tsp cayenne powder
- 0.75 tsp table salt

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, and asafoetida in heated pan,
  toast for 8 minutes 20 seconds
- Remove from oven, combine with cayenne and salt
- Grind to powder

Dark in color, great smell.

## 2018-02-17 - mk11

Have a new batch (#3) of asafoetida, more like batch #1 (hard, pale
brown, brittle), a new oven, a new baking pan (aluminum), new mustard
seed (small brown), and different cayenne (McCormick, orange
color). Will try to repro mk10 but will be light on the cayenne just
in case (2/3 of previous).

- 1.383 g curry leaves, very slightly dried
- 17.605 g brown mustard seed, small (6 tsp)
- 4.180 g coriander seed from freezer (3 tsp)
- 0.874 g asafoetida batch #3, powdered (~1/4 tsp, round)
- 0.650 g cayenne powder (0.25 tsp)
- 4.402 g table salt, iodized (0.75 tsp)

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Dark color. Good smell, but I had a cold and so it might have actually
smelled even better.

I notice that the asafoetida powder had lost a lot of its flavor after
toasting, and the curry leaves gave up quite a bit of scent as
well. Consider experimenting with toasting them for less time, perhaps
only the final minute, or even 30 seconds. (And I suppose use less.)

Notes from later repros of this:

- Makes very roughly 12 tsp of loose-packed powder (susceptible to
  clumping, so keep it loose). 4 tsp is about right for a big bowl of
  popcorn.
- Worked fine using upper (2nd from top) oven rack position. Should
  try with other positions as well.

## 2018-03-17 - mk12

Like mk11, but want to see what happens if I toast the curry leaves
for only 1/4 of the usual time (2 minutes instead of 8).

- 1.413 g curry leaves, several dried (about 15) - aiming for 1.383 g
- 17.603 g brown mustard seed, small - aiming for 17.605 g
- 4.180 g coriander seed from freezer
- 0.884 g asafoetida batch #3, powdered - aiming for 0.874 g
- 0.651 g cayenne powder - aiming for 0.650 g
- 4.401 g table salt, iodized - aiming for 4.402 g

- Preheat oven and metal pan to 350°F (very well preheated)
- Put mustard, coriader, and asafoetida in heated pan, toast for 6
  minutes
- Add curry leaves, toast for two more minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Medium-dark color. Amazing smell while grinding, finished product has
about the usual smell. Spouse described it as possibly slightly more
cinnamon-y than previous. Flavor has a little more bitter than
before. Try toasting curry leaves for 4 minutes instead of 2.

mk11 is still the golden recipe, but I'm curious if I can reduce some
of the "burnt" scent component without hurting the overall effect, in
case that part bothers someone.

## 2018-04-30 - mk13

Attempting to repro mk11 and establish quantities mostly rounded to 10
mg or 2 significant figures.

- 1.40 g curry leaves, very slightly dried
- 17.60 g brown mustard seed, small
- 4.20 g coriander seed from freezer
- 0.87 g asafoetida batch #3, powdered
- 0.60 g cayenne powder (still the orange stuff)
- 4.40 g table salt, iodized (or possibly less -- scale out of tare?)

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriader, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Good flavor, color, and smell. Still too hot; cut down on the cayenne
next time, and mix in more as desired.

## 2018-05-19 - mk14

3x batch made with leaves that had dried out in the fridge. (Cayenne
only 2x. Also toasted, because I accidentally added it to the bowl of
things to be toasted...)

(Edit: This was probably a 3x of mk11.)

- 4.3 g dried curry leaves
- 53 g brown mustard seed, small
- 12.6 g coriander seed from freezer
- 2.6 g asafoetida batch #3, powdered
- 1.2 g cayenne powder
- 13.2 g table salt, iodized

Nice smell, but lacked that nice fruity flavor. Tasted kind of
burned. I think this is due to the dried curry leaves. Now that I have
a curry tree, maybe I'll never have this problem again!

## 2018-09-23 - mk15

(Edit: This was probably a repro of mk11.)

- 1.6 g curry tree leaves from my own tree (2.5 leaves, no ribs)
- 19.8 g brown mustard seed, small (from DEEP)
- 4.8 g coriander seed from freezer
- 0.92 g asafoetida batch #3, powdered (0.88g after grinding... must
  have lost some)
- 0.70 g cayenne powder (McCormick, the orange stuff)
- 4.9 g table salt, iodized

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, coriader, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Pretty good. Not as fruity as I like it.

## 2018-10-13 - mk16

4.75x recipe of mk11, experimenting with leaving out asafoetida.

- 6.577 g curry leaves (slightly wet)
- 83 g mustard seeds, small (from DEEP; new bag)
- 20 g coriander seed from freezer
- 3.09 g cayenne powder (McCormick, the orage stuff)
- 20.92 g table salt, iodized

- Preheat oven and metal pan to 350°F
- Put curry leaves, mustard, and coriader in heated pan, toast for 8
  minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Well, that was pretty disappointing. The powder is a mid-brown, less
fine, and is largely missing that fruity, feral flavor. (The smell is
correct, though.) The powder clumped a little more when grinding. I
think the larger batch did not toast as thoroughly, and the slightly
wet curry leaves would have contributed to that.

So, I tried toasting some asafoetida to mix in to try to see how much
of the difference was due to batch size/water content vs. the missing
asafoetida:

- 4.152 g asafoetida batch #3, powdered

I toasted it for 4.5 minutes, probably too long. But it seems to have
slightly helped the flavor.

## 2020-01-09 - mk17

Reproducing mk11, but with batch #2 asafoetida.

- 1.38 g curry leaves
- 17.60 g brown mustard seeds (small, "DEEP" brand)
- 4.18 g coriander seed from freezer
- 0.877 g asafoetida batch #2, powdered
- 0.65 g cayenne powder (Simply Organic red)
- 4.40 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Dark color, lovely dark and fruity smell. Nice flavor.

Produced about 11 tsp of powder; probably double or triple this would
be about the right amount for a batch.

## 2020-01-26 - mk18

Reproducing mk17, but with 5x recipe and #3 asafoetida.

Curry leaves were on their way out, several weeks old.

- 7.14 g curry leaves
- 88 g brown mustard seeds (small, "DEEP" brand)
- 21 g coriander seed from freezer
- 4.40 g asafoetida batch #2, powdered
- 3.25 g cayenne powder (Simply Organic red)
- 22.00 g table salt, iodized

Lacked the really pungent, fruity, medicinal odor when grinding, and
the flavor isn't as special, either.

This is the second large batch I can recall doing, and both times it
was not as good. Ingredients, or a scaling issue?

Need to do another trial of this, but with fresher curry leaves.

## 2023-01-16 - mk19

mk11, but scaled 2.2x. Had to substitute some chile varieties.

- 3.040 g curry leaves, moderately fresh (mostly fallen leaves)
- 38.734 g brown mustard seed
- 9.190 g coriander seed from freezer
- 1.925 g asafoetida batch #1 + #3, powdered
- 1.430 g powdered chiles: ~0.6 g cayenne, ~0.6 g pasilla, ~0.1 g "super-hot cayenne"
- 9.686 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with powdered chiles and salt
- Grind to powder

Flavor was good, maybe very slightly hotter than usual (as in, I could
taste a bit of heat.) The batch size worked well, but I'd like to get
back to just using a single type of chile for simplicity and
consistency.

## 2023-01-29 - mk20

mk19 scaled 0.66x, with more adjustment of chili mix. (Re-)roasting
the pasilla this time, to make it easier to grind. Only had 75% of the
pasilla I wanted. Also put cayenne in the roasting step.

Used yellow mustard this time instead of brown.

- 2.007 g curry leaves, moderately fresh (mostly fallen leaves)
- 25.565 g brown mustard seed
- 6.066 g coriander seed from freezer
- 1.287 g asafoetida batch #3, powdered
- 0.645 g pasilla chili powder
- 0.069 g "super-hot cayenne" powder
- 6.393 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriander, asafoetida, and powdered chilis
  in heated pan, toast for 8 minutes
- Remove from oven, combine with salt
- Grind to powder

Flavor is not quite the same. Yellow mustard might not work as well,
or might need a different ratio?

## 2023-12-02 - mk21

Scaled 4.9x from mk11.

- 6.8 g curry leaves (about 6 full leaves)
- 86 g brown mustard seed
- 20.482 g coriander seed from freezer
- 4.283 g asafoetida batch #3, powdered
- 3.183 g cayenne powder
- 21.570 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Put curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, combine with cayenne and salt
- Grind to powder

Not as good. Might be the new mustard, or the curry leaves that were
taken from parts of the tree that were starting to die back.

## 2024-11-15 - mk22

Trying to reproduce mk11 as closely as possible to check how much
variation there is with the same recipe. Curry leaves are a bit
fresher this time, though.

I wonder if one of the variables is the degree to which the
ingredients have cooled before grinding. I'm generally very quick to
remove everything from the hot baking tray onto a plate or bowl, but I
might be grinding them when still hot. This time I was careful to let
the ingredients cool down to "faintly warm" before putting them in the
grinder. I don't know if I did this with mk11, but at least it's now
specified in the recipe.

- 1.380 g curry leaves
- 17.612 g brown mustard seed
- 4.181 g coriander seed from freezer
- 0.871 g asafoetida batch #3, powdered
- 0.650 g cayenne powder
- 4.409 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Spread curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, move quickly to plate to cool to just faintly warm
- Mix with cayenne and salt, grind to powder

Notes:

- Overall good flavor and smell. Less intense burst of scent than
  usual after grinding. Darker and more toasted tasting.
- Oven preheat had gone on longer than usual. A faint bit of burnt
  small in the last minute of toasting. I suspect bake
  time/temperature is a major factor overall.

## 2024-11-16 - mk23

mk22 with exact same source of ingredients, but scaled 3.23x.

- 4.458 g curry leaves
- 56.880 g brown mustard seed
- 13.500 g coriander seed from freezer
- 2.813 g asafoetida batch #3, powdered
- 2.100 g cayenne powder
- 14.240 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Spread curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, move quickly to plate to cool to just faintly warm
- Mix with cayenne and salt, grind to powder

Notes:

- Noticeably lighter color than mk22. (And didn't end up with any
  burnt smell.) Is this because the preheated tray had less heat to
  give to the seeds?
- Would be interesting to try adding the curry leaves in about halfway
  through, and letting the seeds toast for a total of 10 minutes.
- Fills a 4oz jar when sieved and handled gently (allowing it to remain fluffy).

## 2024-11-22 - mk24

Reproducing mk23. Different batch of curry leaves but they appear to
be in similar condition.

- 4.458 g curry leaves
- 56.880 g brown mustard seed
- 13.500 g coriander seed from freezer
- 2.813 g asafoetida batch #3, powdered
- 14.240 g table salt, iodized
- 2.100 g cayenne powder

- Preheat oven and metal pan to 350°F (very well preheated)
- Spread curry leaves, mustard, coriander, and asafoetida in heated pan,
  toast for 8 minutes
- Remove from oven, move quickly to plate to cool to just faintly warm
- Mix with cayenne and salt, grind to powder

Notes:

- Maybe slightly less good? Hard to tell though.

## 2024-11-25 - mk25 - Longer roast except for asafoetida and curry leaves

mk23 with 11 minute roast, except asafoetida and curry leaves are
added after 3 minutes. The idea here is to get back to a darker roast
of the bulk spices (since things got lighter after scale-up), but not
overcook the more delicate ingredients.

- 4.458 g curry leaves
- 56.880 g brown mustard seed
- 13.500 g coriander seed from freezer
- 2.813 g asafoetida batch #3, powdered
- 2.100 g cayenne powder
- 14.240 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Spread mustard and coriander in heated pan, toast for 3 minutes
- Add curry leaves and asafoetida, toast for 8 more minutes
- Remove from oven, move quickly to plate to cool to just faintly warm
- Mix with cayenne and salt, grind to powder

Notes:

- Back to a nice dark color
- More bitter than usual, with some eucalyptus notes creeping
  in. Maybe curry leaf needs more time after all?

## 2024-11-27 - mk26 - Scaling up more, separating ingredients

mk23 but scaled by 2x, using a longer toasting time to
compensate. Toasting coriander & mustard separately from curry leaves
& asafoetida.

- 114 g brown mustard seed
- 27.0 g coriander seed from freezer
- 8.92 g curry leaves
- 5.63 g asafoetida batch #3, powdered
- 4.20 g cayenne powder
- 28.5 g table salt, iodized

- Preheat oven and metal pan to 350°F (very well preheated)
- Spread mustard & coriander on heated pan and toast for 14 minutes
- Remove from oven, move quickly to plate to cool
- Move pan back to oven and allow to preheat again
- Spread curry leaves and asafoetida on heated pan and toast for 8 minutes
- Remove from oven and move quickly to plate to cool
- Once all toasted ingredients are cool, mix with cayenne and salt and
  grind to powder

Notes:

- The mustard & coriander, ground separately from the other spices,
  smelled very, very similar to regular consolation spice! Missing
  some notes, but the fruitiness and pungency is prominent. (I think
  the fruitiness must come from the coriander.)
- The curry leaf and asafoetida, ground separately, tasted a little
  green but also a bit spicy. But not a ton of flavor. I wonder if
  these have actually been contributing meaningfully to the flavor.
  May try again with less roasting time in case they lost too much
  flavor from a long roast.
