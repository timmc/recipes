# Mushroom gummies

This came out of a recipe for crispy shiitake mushrooms, of all
things. The original recipe called for 3 Tbsp corn starch, but I
didn't have corn starch, so I looked up a substitution—and following
that, I used tapioca starch and doubled the volume. This was a
mistake!  (Maybe doubling for that substitution was only meant for
when thickening a stew?) It caused everything to ball up into one
sticky mass instead of staying separate and becoming crispy.

But it turned out great, so it's a new recipe.

The result is something richly umami that can be cooled (and hardened
somewhat) in the fridge, then sliced thinly for use on sandwiches as
something bacon-like.

## 2022-09-16 - Original

- 168 g shiitake mushrooms, thinly sliced (weight after a week in a
  paper bag in the refrigerator, probably lost quite a bit of water)
- 3 Tbsp tamari
- 6 Tbsp tapioca flour
- 1 tsp garlic powder (actually fine garlic granules, ground finer in
  a mortar and pestle)
- 1/2 tsp iodized table salt

- Tossed sliced mushrooms with tamari. Moved quickly to mix them so
  that just a few big pieces didn't soak up all the tamari. Let sit
  while mixing other ingredients.
- Combined tapioca flour, garlic powder & salt, then tossed with
  mushrooms to form a gooey mass.
- Heated oil in a cast iron pan, then sautéed mushroom glop until dark
  and gummy. I pan-fried the heck out of it and it eventually looked
  and tasted cooked.

## 2022-10-08 - Fresher mushrooms

Repeat of 9/16, but with fresher mushrooms (so, more by weight --
almost double). Didn't have to be so careful with distributing tamari
since the mushrooms didn't soak it up as quickly.

- 326 g shiitake mushrooms, thinly sliced (freshly bought)
- 3 Tbsp tamari
- 45 g (6 Tbsp) tapioca flour
- 1 tsp garlic powder (actually fine garlic granules, ground finer in
  a mortar and pestle)
- 1/2 tsp iodized table salt

Took much longer to cook down, and there was a lot more residue on the
pan that I had to keep scraping up and folding back in (so it wouldn't
burn). Maybe let the mushrooms dry out more first next time!

## 2023-01-15 - Week-old mushrooms

2022-09-16 again, but with precise weights this time.

When initially purchased fresh, the mushrooms weighed 260 g. I kept
them in a paper bag in the refrigerator for a week, after which they
weighed 163 g (63% of fresh).

- 163 g week-old shiitake mushrooms, thinly sliced
- 48 g (3 Tbsp) tamari
- 53 g (6 Tbsp) tapioca flour
- 3.83 g (1 tsp) garlic powder (fine powder)
- 2.57 g (1/2 tsp) iodized table salt

- Tossed sliced mushrooms with tamari. Moved quickly to mix them so
  that just a few big pieces didn't soak up all the tamari. Let sit
  while mixing other ingredients.
- Combined tapioca flour, garlic powder & salt, then tossed with
  mushrooms to form a gooey mass.
- Heated oil in a cast iron pan, then sautéed mushroom glop until dark
  and gummy.

Notes:

- Yes, using week-old mushrooms is important!
