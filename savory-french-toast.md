# Savory French Toast

Or, since a past housemate *could not deal* with the idea of this,
"Italian Toast". The key ingredients are garlic, vinegar, and oregano;
I typically soak the oregano in the vinegar on the theory that that
will extract some flavor.

Ingredients:

- sliced bread (4-7 slices depending on porosity)
- 4-5 chicken eggs
- 1 Tbsp dried oregano
- apple cider vinegar, approx. 1 Tbsp -- for a bonus, use hot pepper
  vinegar
- 5 cloves garlic, minced fine
- pinch of salt
- pinch of black pepper
- olive oil

Directions to prepare the batter:

- Place oregano in shot glass or other narrow container
- Add apple cider vinegar, tamping down the oregano such that it is
  packed together and just covered by the vinegar
- Crack the eggs into a dish and beat together with garlic
- Add salt and black pepper
- Finally, mix in vinegar/oregano mixture

Cook as usual for french toast: Pour batter onto flat plate with
raised rim, then soak both sides of sliced bread in the batter. Heat
skillet with plenty of olive oil to medium-low and cook each side of
bread until it seems reasonably cooked-through. Cook remnants of
batter too! A silicone spatula is helpful here.

The garlic needs to be minced fine, not crushed, otherwise it burns
and prevents even contact between the bread and the skillet/oil.  The
garlic and oregano tend to stay on the plate and may need to be dumped
or spread on top of the bread for cooking.

The vinegar changes the egg to be softer and goopier and can lend an
almost buttery texture and flavor when cooked slowly on low
heat. Future experiment: Consider trying to make sous vide savory
french toast.
