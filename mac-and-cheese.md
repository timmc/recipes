# Mac and cheese

...not always macaroni, though.

## 2023-02-15 - Reginetti pasta, garlic-and-herb cheese, stovetop

Simple recipe with fancy ingredients.

- 8 oz whole wheat reginetti pasta
- water to cover
- 2 big pinches of salt

Boil to al dente, then drain in colander and set aside.

- 86 g soy milk, unsweetened
- 5 oz garlic and herb cheese (Sunny Jack from Round Table Farm)
- 2 pinches salt
- a few brinds of black pepper

In now-empty pot, gently heat soy milk and cheese and stir until fully
melted, then stir in pasta, then stir in salt and pepper.

Reasonably tasty, texture was fine.
