By Mark Scarbrough, Cooking Light
NOVEMBER 2008
Retrieved from http://www.myrecipes.com/recipe/roasted-butternut-squash-shallot-soup-10000001854009/ on October 26, 2013

# Ingredients

- 4 cups (1-inch) cubed peeled butternut squash (about 1 1/2 pounds)
- 1 tablespoon olive oil
- 1/4 teaspoon salt
- 4 large shallots, peeled and halved
- 1 (1/2-inch) piece peeled fresh ginger, thinly sliced
- 2 1/2 cups fat-free, less-sodium chicken broth
- 2 tablespoons (1-inch) slices fresh chives
- Cracked black pepper (optional)

# Preparation

1. Preheat oven to 375°.
2. Combine first 5 ingredients in a roasting pan or jelly-roll pan;
   toss well. Bake at 375° for 50 minutes or until tender, stirring
   occasionally. Cool 10 minutes.
3. Place half of squash mixture and half of broth in a blender. Remove
   center piece of blender lid (to allow steam to escape); secure
   blender lid on blender. Place a clean towel over opening in blender
   lid (to avoid splatters). Blend until smooth. Pour into a large
   saucepan. Repeat procedure with remaining squash mixture and
   broth. Cook over medium heat 5 minutes or until thoroughly
   heated. Top with chives and pepper, if desired.

# Tim's attempts

## 2013-10-26

### Ingredients

- 1 med/small butternut squash
- 4 shallots, peeled and halved (along various axes)
- Thumb of ginger, thinly sliced
- 1 tbsp evoo
- 1/4 tsp salt + salt for squash seeds
- 2 serrano peppers
- 1 jalapeño pepper
- 4 garlic cloves

### Directions

- Cut squash in half. Separate seeds fom pulp and set both aside.
- Score the cavity of the squash (recommended by a reviewer, so I did
  it.)
- Preheat oven to 375°F
- Prep seeds for roasting (spread on tray, add salt)
- Toss shallots with a ginger, evoo (eyeballed it), and salt
- Place shallot mix in squash concavities
- Put squash in oven along with tray of seeds (I used the 2nd
  lowest rack for the squash and the top rack for the seeds.) Squash
  cooked for 75 minutes, seeds for ~12 minutes.
- Put in tray of pulp (chopped), peppers, (de-stemmed and opened
  because some had rot), and garlic cloves. I flattened the pulp to 1
  cm thick and put the garlic and peppers on top of it. Let cook for
  30 minutes, then finely mince the peppers.
- Let squash cool for 10 minutes.
- Blend until smooth with pulp, peppers, and garlic and 2.5 cups
  broth.
- Cook on medium heat for a few minutes.

### Results

OK, it's too picante, and not really velvety at all. Sort of watery? I
added a 1/2 tsp of garam masala powder, a tbsp of lemon juice, and
some grated cheddar to try and improve the flavor.
