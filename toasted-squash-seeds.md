# Toasted squash seeds

Pretty basic: Salt and toast the seeds in a single layer. You can also
soak the seeds in salt water first to allow the salt to penetrate a
little (although I've never not also sprinkled salt on the tray, so
maybe this isn't true!), and the toasted seeds can be ground for use
in other dishes.

## Use of ground, toasted seeds

It's actually rather difficult to find food that this goes well with,
even though the powder tastes very good. Here's what I've found so
far:

- Add to bean stew (acts as a thickener, too)

## 2015-11-27: Blue Hubbard squash

300°F/250°F

Blue Hubbard is a variety of *Cucurbita maxima*, which apparently has
tough seeds across varieties (the conspecific Kabocha squash does as
well) that are not pleasant to eat directly. There are thick plates on
the faces of the seeds that are very chewy. Too high a temperature and
you burn these plates without cooking the inside. However, even at the
lowest temperature that will roast these, expect the seeds to pop as
the steam fails to escape the thick hulls. Some will escape your
baking tray.

The seeds on this Blue Hubbard were particularly well-developed, with
medium brown coloration on the faces. I soaked them in salt water for
a day. From one squash they entirely filled a cookie sheet. I toasted
them at 300°F until they started to pop (perhaps 20 minutes, since
there was excess water) and then 250°F for a little longer.

I ground them to a fine powder using a fairly powerful spice/coffee
grinder and then sifted out the chunks and re-ground those. (Sifting
is a little annoying because squash seed powder sticks to itself more
than flour does.)
