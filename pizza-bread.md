# Pizza bread

## 2014-06-11

Using Flora 2 gluten-free starter (mostly quinoa)

Dough:
- 1668 g Flora 2
- 200 g brown rice flour
- 200 g gluten-free sorghum flour
- 145 g gluten-free oat flour

Sauce:
- 1 med head garlic, peeled and trimmed (38 g)
- 1.5 tbsp dried oregano
- 14 oz diced tomatoes (200 mg sodium)
- 184 g roasted zucchini, summer squash, and bell pepper
- ~20 g stir-fried mushrooms

Toppings:
- 233 g block of Monterey Jack cheese, grated

Grease:
- Several tbsp olive oil

Recipe:
- Pureé sauce ingredients
- Mix dough until firm and only just not sticky (add water or flour to adjust)
- For each half of ingredients:
    - Flatten half of dough on oiled baking pan
    - Spread sauce and toppings
    - Fold over 3 edges (all but one narrow end)
    - Roll from narrow end
- Let sit loosely covered 8.5 hours (did not rise, so skip this next time?)
- Bake uncovered at 350°F for 1.25 hours.

Results:
- Very crumbly since I forgot to add xanthan gum
- Also forgot to add salt to the dough!
- Probably should have added oregano (and basil!) to dough
