# Bean salad

## 2020-01-06

- 454 g appaloosa beans
- 1 + 0.5 tsp salt
- 8 g garlic cloves, finely minced
- 2 + 1 Tbsp IPA beer vinegar
- 12 g frozen chopped parsley (maybe 1/3 cup?)
- 60 g evoo

- Brings beans to a boil in plenty of water, then down to a simmer
- Mix 2 Tbsp vinegar, garlic, and parsley, and cap with evoo
- When beans are halfway cooked, add 1 tsp salt
- Drain beans, toss with vinaigrette + remaining 1 Tbsp vinegar and
  0.5 tsp salt
    - Vinaigrette wasn't tasty enough at first

Tasty, but still not quite right. Too much olive oil, maybe.
