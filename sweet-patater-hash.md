Sweet Patater Hash
Retrieved 2013-10-23 from http://www.foodnetwork.com/recipes/jeff-mauro/sweet-patater-hash-recipe/index.html
By Jeff Mauro

----

# Ingredients

- 2 pounds sweet potato, peeled and diced into 1/4-inch cubes
- 2 tablespoons butter, melted
- 1 tablespoon vegetable oil
- 3 Cubanelle peppers, diced small
- 1/2 yellow onion, diced 1/4 inch
- 1 teaspoon chili powder
- 1 teaspoon granulated garlic
- Kosher salt and freshly ground black pepper
- 2 tablespoons chopped fresh parsley

# Directions

Preheat the oven to 400 degrees F.

Combine the sweet potatoes, butter, vegetable oil, peppers, onion,
chili powder and granulated garlic in a large mixing bowl. Season with
some salt and pepper and toss, making sure everything is coated
evenly. Spread the mixture out onto a baking tray and place in the
oven. Bake until the potatoes are knife tender with a crispy exterior,
about 20 minutes, stirring twice during the cooking process.

Remove the hash from the oven, increase the oven temperature to broil
and broil for 5 minutes to brown. To finish, toss the potatoes with
the chopped parsley and season with salt and pepper.

----

# Tim's attempts

## 2013-10-23: 

- 2.5 med/large sweet potato
- 2 tbsp butter
- 1 tablespoon olive oil
- 1/3 "pea" asafoetida resin, dissolved in the heated butter and oil
  (replaces onion)
- 1 aji dulce pepper, one jalapeño
- 1 tsp chili powder
- 1 large clove of garlic, minced
- 1 tsp salt (or less)
- 1/2 tsp ground black pepper

Results:

- Smells like butter. Maybe used too much?
- There's definitely a taste of burnt garlic which I'd like to get rid
  of.
- The heat from the peppers is nice. The aji dulce might be giving it
  a slightly odd flavor, though.
