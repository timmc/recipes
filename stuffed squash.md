


## 2014-12-29

http://www.all-creatures.org/recipes/hubbardsq-stuf.html

- Blue Hubbard squash
- Batata
- Potato
- Carrot
- Parsnip
- Frozen spinach
- Raisins
- Walnuts
- Grains of paradise
- cloves
- salt
- quinoa, a little undercooked

Astoundingly, this squash took 2.5 hours to cook instead of the 1–1.5
hours I've previously seen!

It turned out to be a bad idea to cook the extra stuffing in the tray
with the squash; it prevented the squash from cooking evenly. (The
goal was to soak up and juices that leaked out, which are delicious.)
It also dried out a bit and the quinoa was just barely cooked
enough. In the future, use a separate, covered tray.

## 2017-11-05

- 15 lb Blue Hubbard squash
- 800 g celery, 1/2 inch dice
- 815 g carrots, 1/2 inch dice
- 800 g potatoes, 1/2 inch dice
- 17 g garlic, minced/crushed
- 8 g sage leaves, minced
- 33 g parsley (flat; leaves & stems), minced
- 3.6 g rosemary leaves (fresh), minced
- 2.7 g thyme leaves (10-15 sprigs?), minced
- 4 tsp table salt
- 0.5 tsp black pepper, coarsely ground
- 4 cups quinoa (uncooked)

1. Rinse and cook quinoa
2. Briefly sauté potatoes and carrots on high heat just to get a
   little caramelization here and there
3. Microwave veggies and sage (oops) 10-15 minutes in microwave (stir
   after 5-10 minutes)
4. Mix veggies, spices, cooked quinoa. Taste the stuffing to adjust
   seasoning before putting it in the squash!
5. Preheat oven to 350°F; use lowest oven rack setting (as needed for
   large squash)
6. Carve hole high on side of squash; big enough for ladle to go into,
   and with sloped sides (like a jack'o'lantern top) to prevent it
   from falling in
7. Stuff the squash; extra stuffing can be cooked to finish in closed
   container if it needs it.
8. Bake for 80 minutes (wait for the flesh to soften to a fork or
   knife)

Made approximately 12 lbs of stuffing (9 used in squash). I thought
for sure I had made too much stuffing, but it was the right amount to
ensure the squash could be filled. In fact, it would be better to have
made even more, because the result is still squash-heavy.

I accidentally microwaved the sage with the veggies instead of adding
it afterwards -- not sure if that changed things. Next day, the sage
is definitely prominent in the stuffing. Could be reduced, but isn't
bad.
