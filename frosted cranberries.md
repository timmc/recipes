# Sugared Cranberries

From “Cooking Light”, with some clarifications.

- 1-1/2  c. granulated sugar
- 1-1/2  c. water
- 2 c. fresh cranberries
- ¾ c. fine sugar (process granulated sugar in food processor for
  a minute -- don't use powdered sugar)

Combine granulated sugar and water in a small saucepan over low heat,
stirring mixture until sugar dissolves.  Bring to a simmer; remove
from heat.  Or, combine sugar and water in a 4 cup glass measuring cup
or bowl and microwave until the sugar is dissolved.  (Do not boil or
the cranberries may pop when added.)  Stir in cranberries; pour
mixture into a bowl.  Cover and refrigerate 8 hours or overnight (or
for a day or 2!).

Drain cranberries in a colander over a bowl, reserving steeping
liquid, if desired.  Allow to drain for no more than a minute or two,
but not less than a minute; too much liquid will make the sugar clump.
Place fine sugar in a shallow dish.  Add the cranberries, rolling to
coat with sugar.  Spread sugared cranberries in a single layer on a
baking sheet; let stand at room temperature 1 hour or until dry.  (I
let them dry overnight before putting in a container and they seem to
last well that way.)

Note: Store in an airtight container in a cool place for up to a week.

You can reheat the liquid that the berries steeped in and use it for
another batch of cranberries.  I just heat it in a glass measuring cup
in the microwave until it is very hot but not boiling, and pour it
over the cranberries in a bowl or in a quart mason jar.

## 2015-11-27

- 500 mL cranberries
- 220 mL maple syrup
- 120 mL fine sugar

Steeped 2 days.

Maple syrup remaining: 160 mL

I didn't get any maple syrup flavor from this recipe, which was
disappointing, but at least I have maple syrup left over instead of
simple syrup, so that won't go to waste this time.
